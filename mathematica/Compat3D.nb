(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 6.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[     39017,        903]
NotebookOptionsPosition[     37338,        874]
NotebookOutlinePosition[     37834,        892]
CellTagsIndexPosition[     37791,        889]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"<<", "ToMatlab.m"}]], "Input",
 CellChangeTimes->{{3.581931184844022*^9, 3.581931196202772*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"0c11c500-466a-4fbf-bfab-5ad5887835ce"],

Cell[BoxData[
 RowBox[{"<<", "Optimize.m"}]], "Input",
 CellChangeTimes->{{3.582550840567561*^9, 3.5825508441223783`*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"31b3eca0-e084-4969-963b-f8fdf085e29c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"LToXi", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"L", "[", "1", "]"}], "\[Rule]", 
      RowBox[{"1", "-", "xi", "-", "eta", "-", "zeta"}]}], ",", 
     RowBox[{
      RowBox[{"L", "[", "2", "]"}], "\[Rule]", "xi"}], ",", 
     RowBox[{
      RowBox[{"L", "[", "3", "]"}], "\[Rule]", "eta"}], ",", 
     RowBox[{
      RowBox[{"L", "[", "4", "]"}], "\[Rule]", "zeta"}]}], "}"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.6011285155543537`*^9, 3.60112854498419*^9}, {
  3.663577883252184*^9, 3.6635779204429197`*^9}},
 CellLabel->"In[20]:=",ExpressionUUID->"b08e4c50-8ae4-41fd-8db1-8aab7cf0df0a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"rule", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"0", "\[Rule]", "zero"}], ",", 
     RowBox[{"1", "\[Rule]", "one"}], ",", 
     RowBox[{
      RowBox[{"-", "1"}], "\[Rule]", 
      RowBox[{"-", "one"}]}]}], "}"}]}], ";"}]], "Input",
 CellLabel->"In[21]:=",ExpressionUUID->"e70b959c-cef7-4dce-a022-ec1a91797229"],

Cell[BoxData[
 RowBox[{
  RowBox[{"EdgeFuns", "[", 
   RowBox[{"deg_", ",", "L1_", ",", "L2_"}], "]"}], ":=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"L1", "^", "i"}], " ", 
     RowBox[{"L2", "^", 
      RowBox[{"(", 
       RowBox[{"deg", "-", "i"}], ")"}]}]}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", 
      RowBox[{"deg", "-", "1"}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.665896848223641*^9, 3.665896911022053*^9}, {
  3.665896948748365*^9, 3.665896979817927*^9}, {3.665897035584772*^9, 
  3.66589703566359*^9}, {3.66591387527627*^9, 3.665913875488185*^9}},
 CellLabel->"In[22]:=",ExpressionUUID->"9cf2d738-8e7f-49c5-a686-3f9936ae9b2f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"FaceFuns", "[", 
   RowBox[{"deg_", ",", "L1_", ",", "L2_", ",", "L3_"}], "]"}], ":=", 
  RowBox[{"Flatten", "[", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"L1", "^", 
         RowBox[{"(", 
          RowBox[{"deg", "-", "i", "-", "j"}], ")"}]}], " ", 
        RowBox[{"L2", "^", "i"}], " ", 
        RowBox[{"L3", "^", "j"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", 
         RowBox[{"deg", "-", "j", "-", "1"}]}], "}"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"j", ",", 
       RowBox[{"deg", "-", "2"}]}], "}"}]}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.665897408592514*^9, 3.6658974946685543`*^9}, {
  3.665897534332258*^9, 3.665897556751333*^9}, {3.6658975945202923`*^9, 
  3.6658976412026787`*^9}},
 CellLabel->"In[23]:=",ExpressionUUID->"5264bdd8-d26d-4afd-9746-1a8e523c75f8"],

Cell[BoxData[
 RowBox[{
  RowBox[{"VolFuns", "[", "deg_", "]"}], ":=", 
  RowBox[{"Flatten", "[", 
   RowBox[{"Table", "[", 
    RowBox[{
     RowBox[{"Table", "[", 
      RowBox[{
       RowBox[{"Table", "[", 
        RowBox[{
         RowBox[{
          RowBox[{
           RowBox[{"L", "[", "1", "]"}], "^", 
           RowBox[{"(", 
            RowBox[{"deg", "-", "i", "-", "j", "-", "k"}], ")"}]}], 
          RowBox[{
           RowBox[{"L", "[", "2", "]"}], "^", "i"}], " ", 
          RowBox[{
           RowBox[{"L", "[", "3", "]"}], "^", "j"}], " ", 
          RowBox[{
           RowBox[{"L", "[", "4", "]"}], "^", "k"}]}], ",", 
         RowBox[{"{", 
          RowBox[{"i", ",", 
           RowBox[{"deg", "-", "j", "-", "k", "-", "1"}]}], "}"}]}], "]"}], 
       ",", 
       RowBox[{"{", 
        RowBox[{"j", ",", 
         RowBox[{"deg", "-", "k", "-", "1"}]}], "}"}]}], "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"k", ",", 
       RowBox[{"deg", "-", "3"}]}], "}"}]}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.665897684090014*^9, 3.665897847619782*^9}},
 CellLabel->"In[24]:=",ExpressionUUID->"c2b6ca2a-ab6f-4397-8502-88cb0810845e"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OneEdgeDegrees", "[", "deg_", "]"}], ":=", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{"deg", ">", "1"}], ",", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Exponent", "[", 
       RowBox[{
        RowBox[{"EdgeFuns", "[", 
         RowBox[{"deg", ",", 
          RowBox[{"L", "[", "1", "]"}], ",", 
          RowBox[{"L", "[", "2", "]"}]}], "]"}], ",", 
        RowBox[{"L", "[", "i", "]"}]}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "2"}], "}"}]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", "}"}], ",", 
      RowBox[{"{", "}"}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.665913898272756*^9, 3.665913943469528*^9}, {
  3.665913981044389*^9, 3.665914050511763*^9}, {3.6659141825524273`*^9, 
  3.665914189671109*^9}, {3.665914728564507*^9, 3.66591479170798*^9}},
 CellLabel->"In[25]:=",ExpressionUUID->"9fa4a722-3bd6-4d0c-85ab-6ccb31542136"],

Cell[BoxData[
 RowBox[{
  RowBox[{"OneFaceDegrees", "[", "deg_", "]"}], ":=", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{"deg", ">", "2"}], ",", 
    RowBox[{"Table", "[", 
     RowBox[{
      RowBox[{"Exponent", "[", 
       RowBox[{
        RowBox[{"FaceFuns", "[", 
         RowBox[{"deg", ",", 
          RowBox[{"L", "[", "1", "]"}], ",", 
          RowBox[{"L", "[", "2", "]"}], ",", 
          RowBox[{"L", "[", "3", "]"}]}], "]"}], ",", 
        RowBox[{"L", "[", "i", "]"}]}], "]"}], ",", 
      RowBox[{"{", 
       RowBox[{"i", ",", "3"}], "}"}]}], "]"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", "}"}], ",", 
      RowBox[{"{", "}"}], ",", 
      RowBox[{"{", "}"}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.665914213513712*^9, 3.665914236589158*^9}, {
  3.665914808559642*^9, 3.665914833645219*^9}},
 CellLabel->"In[26]:=",ExpressionUUID->"176c5481-2496-4391-a0a2-56ab3fe6a28d"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Maxdeg", "=", "5"}], ";"}]], "Input",
 CellChangeTimes->{{3.4125166982994843`*^9, 3.412516698748109*^9}, {
   3.601127193391665*^9, 3.601127193943182*^9}, {3.6011273366159277`*^9, 
   3.6011273369353952`*^9}, {3.60112961080818*^9, 3.601129611300263*^9}, {
   3.601132174136101*^9, 3.60113219348217*^9}, {3.601134052550027*^9, 
   3.6011340529434*^9}, {3.6012140808118477`*^9, 3.601214081095355*^9}, {
   3.601214226257761*^9, 3.601214226674903*^9}, {3.601307550146626*^9, 
   3.601307552382032*^9}, {3.601309266477006*^9, 3.601309267157496*^9}, {
   3.6035468984785233`*^9, 3.603546901100162*^9}, {3.66365133301721*^9, 
   3.6636513399823017`*^9}, {3.663652258648479*^9, 3.663652260160421*^9}, {
   3.663654418011038*^9, 3.6636544362251177`*^9}, {3.663918818824893*^9, 
   3.66391881917782*^9}, {3.6639198910406427`*^9, 3.663919893662088*^9}, {
   3.664092603075986*^9, 3.664092603574153*^9}, {3.6658997718015957`*^9, 
   3.665899772146858*^9}, {3.6659008058341637`*^9, 3.6659008059450283`*^9}, {
   3.665900903484172*^9, 3.665900903628582*^9}, 3.665901008706915*^9, {
   3.6659038050225487`*^9, 3.665903805497581*^9}, {3.8223936095377007`*^9, 
   3.822393609921266*^9}},
 CellLabel->"In[27]:=",ExpressionUUID->"cf0a6036-8a36-4fc8-96b3-6636a45756e9"],

Cell[BoxData[
 RowBox[{
  RowBox[{"EdgeVerts", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"2", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"3", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"3", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", "3"}], "}"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.665913262302953*^9, 3.665913318324025*^9}, {
  3.6659133794097967`*^9, 3.665913404719928*^9}, {3.6659265095802393`*^9, 
  3.6659265116607437`*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"3fc8c575-f302-46f4-b3d9-dc1e972be6e6"],

Cell[BoxData[
 RowBox[{
  RowBox[{"FaceVerts", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"2", ",", "3", ",", "4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"3", ",", "1", ",", "4"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"4", ",", "1", ",", "2"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"1", ",", "3", ",", "2"}], "}"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.665913310776609*^9, 3.665913361562022*^9}, 
   3.665913407650279*^9},
 CellLabel->"In[29]:=",ExpressionUUID->"e36dced9-abb1-4e0b-8b13-4b2f375a5453"],

Cell[BoxData[
 RowBox[{
  RowBox[{"path", "=", "\"\<../matlab/funcs/\>\""}], ";"}]], "Input",
 CellChangeTimes->{{3.603545962234582*^9, 3.603545979784953*^9}, 
   3.603546654310639*^9, {3.663652328805676*^9, 3.663652333169182*^9}, {
   3.665903821102337*^9, 3.665903823397607*^9}, {3.8223936537595167`*^9, 
   3.822393656501534*^9}},
 CellLabel->"In[30]:=",ExpressionUUID->"a93dd07f-ea5f-48e3-a93f-4d712cbb07b6"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"OutFile", "=", 
   RowBox[{"OpenWrite", "[", 
    RowBox[{"path", "<>", "\"\<DefCompatBasis3D.m\>\""}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"WriteString", "[", 
   RowBox[{
   "OutFile", ",", "\[IndentingNewLine]", 
    "\"\<function [ DB ] = DefCompatBasis3D(degree)\\n\>\"", ",", 
    "\[IndentingNewLine]", 
    "\"\<% Define, for the selected degree, the functions for the compatible \
basis\\n\\n\>\"", ",", "\[IndentingNewLine]", 
    "\"\<DB.degree = degree;\\n\>\"", ",", "\[IndentingNewLine]", 
    "\"\<\\n% The function that returns the approximation basis (of \
xi,eta,zeta)\\n\>\"", ",", 
    "\"\<DB.handleBasis = eval(sprintf('@CompatBasis_%d',degree));\\n\>\"", 
    ",", "\[IndentingNewLine]", 
    "\"\<DB.handleDBasis = eval(sprintf('@DCompatBasis_%d',degree));\\n\>\"", 
    ",", "\"\<DB.DT = eval(sprintf('DTCompatBasis_%d()',degree));\\n\>\"", 
    ",", "\[IndentingNewLine]", 
    "\"\<\\n% The dimension of the basis in each direction\\n\>\"", ",", 
    "\[IndentingNewLine]", 
    "\"\<DB.nB = (degree+1)*(degree+2)*(degree+3)/6;\\n\>\"", ",", 
    "\[IndentingNewLine]", "\"\<DB.nEdge = max(0,degree-1);\\n\>\"", ",", 
    "\[IndentingNewLine]", 
    "\"\<DB.nFace = max(0,(degree-1)*(degree-2)/2);\\n\>\"", ",", 
    "\[IndentingNewLine]", 
    "\"\<DB.nVol = max(0,(degree-1)*(degree-2)*(degree-3)/6);\\n\\n\>\"", ",",
     "\[IndentingNewLine]", "\"\<DB.EdgeVerts = \>\"", ",", 
    RowBox[{"ToMatlab", "[", "EdgeVerts", "]"}], ",", "\[IndentingNewLine]", 
    "\"\<DB.FaceVerts = \>\"", ",", 
    RowBox[{"ToMatlab", "[", "FaceVerts", "]"}], ",", "\[IndentingNewLine]", 
    "\"\<[ DB.EdgeDegrees, DB.FaceDegrees, DB.OneEdgeDegrees, \
DB.OneFaceDegrees] = \>\"", ",", 
    "\"\<eval(sprintf('EdgeFaceDegrees_%d()',degree));\\n\>\"", ",", 
    "\[IndentingNewLine]", "\"\<\\nend\\n\\n\>\""}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"For", "[", 
   RowBox[{
    RowBox[{"deg", "=", "1"}], ",", 
    RowBox[{"deg", "\[LessEqual]", "Maxdeg"}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"nE", "=", 
      RowBox[{"(", 
       RowBox[{"deg", "-", "1"}], ")"}]}], ";", 
     RowBox[{"nF", "=", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"deg", "-", "1"}], ")"}], 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"deg", "-", "2"}], ")"}], "/", "2"}]}]}], ";", 
     RowBox[{"nV", "=", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"deg", "-", "1"}], ")"}], 
       RowBox[{"(", 
        RowBox[{"deg", "-", "2"}], ")"}], 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"deg", "-", "3"}], ")"}], "/", "6"}]}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"BasisL", "=", 
      RowBox[{"Flatten", "[", 
       RowBox[{"{", "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"L", "[", "1", "]"}], ",", 
         RowBox[{"L", "[", "2", "]"}], ",", 
         RowBox[{"L", "[", "3", "]"}], ",", 
         RowBox[{"L", "[", "4", "]"}], ",", "\[IndentingNewLine]", 
         RowBox[{"Table", "[", 
          RowBox[{
           RowBox[{"EdgeFuns", "[", 
            RowBox[{"deg", ",", 
             RowBox[{"L", "[", 
              RowBox[{"EdgeVerts", "[", 
               RowBox[{"[", 
                RowBox[{"e", ",", "1"}], "]"}], "]"}], "]"}], ",", 
             RowBox[{"L", "[", 
              RowBox[{"EdgeVerts", "[", 
               RowBox[{"[", 
                RowBox[{"e", ",", "2"}], "]"}], "]"}], "]"}]}], "]"}], ",", 
           RowBox[{"{", 
            RowBox[{"e", ",", "6"}], "}"}]}], "]"}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{"Table", "[", 
          RowBox[{
           RowBox[{"FaceFuns", "[", 
            RowBox[{"deg", ",", 
             RowBox[{"L", "[", 
              RowBox[{"FaceVerts", "[", 
               RowBox[{"[", 
                RowBox[{"f", ",", "1"}], "]"}], "]"}], "]"}], ",", 
             RowBox[{"L", "[", 
              RowBox[{"FaceVerts", "[", 
               RowBox[{"[", 
                RowBox[{"f", ",", "2"}], "]"}], "]"}], "]"}], ",", 
             RowBox[{"L", "[", 
              RowBox[{"FaceVerts", "[", 
               RowBox[{"[", 
                RowBox[{"f", ",", "3"}], "]"}], "]"}], "]"}]}], "]"}], ",", 
           RowBox[{"{", 
            RowBox[{"f", ",", "4"}], "}"}]}], "]"}], ",", 
         "\[IndentingNewLine]", 
         RowBox[{"VolFuns", "[", "deg", "]"}]}], "}"}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"Basis", "=", 
      RowBox[{"Simplify", "[", 
       RowBox[{"BasisL", "/.", "LToXi"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"nB", "=", 
      RowBox[{
       RowBox[{"Dimensions", "[", "Basis", "]"}], "[", 
       RowBox[{"[", "1", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutFile", ",", 
       RowBox[{"\"\<function [ B ] = CompatBasis_\>\"", "<>", 
        RowBox[{"ToString", "[", "deg", "]"}], "<>", 
        "\"\<(xietazeta) %#ok<DEFNU>\\none = ones(size(xietazeta,1),1);\\n% \
nul = zeros(size(one));\\n\>\""}], ",", 
       "\"\<xi = xietazeta(:,1); eta = xietazeta(:,2); zeta = xietazeta(:,3);\
\\nB = \>\""}], "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutFile", ",", 
       RowBox[{"ToMatlab", "[", 
        RowBox[{"Basis", "/.", "rule"}], "]"}], ",", "\"\<end\\n\\n\>\""}], 
      "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"DBxi", "=", 
      RowBox[{"D", "[", 
       RowBox[{"Basis", ",", "xi"}], "]"}]}], ";", 
     RowBox[{"DBeta", "=", 
      RowBox[{"D", "[", 
       RowBox[{"Basis", ",", "eta"}], "]"}]}], ";", 
     RowBox[{"DZeta", "=", 
      RowBox[{"D", "[", 
       RowBox[{"Basis", ",", "zeta"}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutFile", ",", 
       RowBox[{"\"\<function [ DB ] = DCompatBasis_\>\"", "<>", 
        RowBox[{"ToString", "[", "deg", "]"}], "<>", 
        "\"\<(xietazeta) %#ok<DEFNU>\\none = \
ones(size(xietazeta,1),1);\\nzero = zeros(size(one));\\n\>\""}], ",", 
       "\"\<xi = xietazeta(:,1); eta = xietazeta(:,2); zeta = xietazeta(:,3);\
\\nDBxi = \>\""}], "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutFile", ",", 
       RowBox[{"ToMatlab", "[", 
        RowBox[{
         RowBox[{"DBxi", "/.", "rule"}], ",", "1000"}], "]"}], ",", 
       "\"\<DBeta=\>\"", ",", 
       RowBox[{"ToMatlab", "[", 
        RowBox[{
         RowBox[{"DBeta", "/.", "rule"}], ",", "1000"}], "]"}], ",", 
       "\"\<DZeta=\>\"", ",", 
       RowBox[{"ToMatlab", "[", 
        RowBox[{
         RowBox[{"DZeta", "/.", "rule"}], ",", "1000"}], "]"}], ",", 
       "\"\<DB = cat(3,DBxi, DBeta,DZeta);\\nend\\n\\n\>\""}], "]"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"EdgeDegrees", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"Exponent", "[", 
         RowBox[{
          RowBox[{"Flatten", "[", "\[IndentingNewLine]", 
           RowBox[{"Table", "[", 
            RowBox[{
             RowBox[{"EdgeFuns", "[", 
              RowBox[{"deg", ",", 
               RowBox[{"L", "[", 
                RowBox[{"EdgeVerts", "[", 
                 RowBox[{"[", 
                  RowBox[{"e", ",", "1"}], "]"}], "]"}], "]"}], ",", 
               RowBox[{"L", "[", 
                RowBox[{"EdgeVerts", "[", 
                 RowBox[{"[", 
                  RowBox[{"e", ",", "2"}], "]"}], "]"}], "]"}]}], "]"}], ",", 
             
             RowBox[{"{", 
              RowBox[{"e", ",", "6"}], "}"}]}], "]"}], "]"}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{"L", "[", "v", "]"}]}], "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"v", ",", "4"}], "}"}]}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"FaceDegrees", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"Exponent", "[", 
         RowBox[{
          RowBox[{"Flatten", "[", "\[IndentingNewLine]", 
           RowBox[{"Table", "[", 
            RowBox[{
             RowBox[{"FaceFuns", "[", 
              RowBox[{"deg", ",", 
               RowBox[{"L", "[", 
                RowBox[{"FaceVerts", "[", 
                 RowBox[{"[", 
                  RowBox[{"f", ",", "1"}], "]"}], "]"}], "]"}], ",", 
               RowBox[{"L", "[", 
                RowBox[{"FaceVerts", "[", 
                 RowBox[{"[", 
                  RowBox[{"f", ",", "2"}], "]"}], "]"}], "]"}], ",", 
               RowBox[{"L", "[", 
                RowBox[{"FaceVerts", "[", 
                 RowBox[{"[", 
                  RowBox[{"f", ",", "3"}], "]"}], "]"}], "]"}]}], "]"}], ",", 
             
             RowBox[{"{", 
              RowBox[{"f", ",", "4"}], "}"}]}], "]"}], "]"}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{"L", "[", "v", "]"}]}], "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"v", ",", "4"}], "}"}]}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutFile", ",", 
       RowBox[{
       "\"\<function [ EdgeDegrees, FaceDegrees, OneEdge, OneFace ] = \
EdgeFaceDegrees_\>\"", "<>", 
        RowBox[{"ToString", "[", "deg", "]"}], "<>", "\[IndentingNewLine]", 
        "\"\<()\\nEdgeDegrees = \>\""}], ",", 
       RowBox[{"ToMatlab", "[", "EdgeDegrees", "]"}], ",", 
       "\[IndentingNewLine]", "\"\<FaceDegrees = \>\"", ",", 
       RowBox[{"ToMatlab", "[", "FaceDegrees", "]"}], ",", 
       "\[IndentingNewLine]", "\"\<OneEdge = \>\"", ",", " ", 
       RowBox[{"ToMatlab", "[", 
        RowBox[{"OneEdgeDegrees", "[", "deg", "]"}], "]"}], ",", 
       "\[IndentingNewLine]", "\"\<OneFace = \>\"", ",", " ", 
       RowBox[{"ToMatlab", "[", 
        RowBox[{"OneFaceDegrees", "[", "deg", "]"}], "]"}], ",", 
       "\[IndentingNewLine]", "\"\<end\\n\\n\>\""}], "]"}], ";", 
     "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutFile", ",", 
       RowBox[{"\"\<function [ DT ] = DTCompatBasis_\>\"", "<>", 
        RowBox[{"ToString", "[", "deg", "]"}], "<>", 
        "\"\<() %#ok<DEFNU>\\nDT=cell(3,1);\\nDT{1}=sparse(\>\"", "<>", 
        RowBox[{"ToString", "[", "nB", "]"}], "<>", "\"\<,\>\"", "<>", 
        RowBox[{"ToString", "[", "nB", "]"}], "<>", 
        "\"\<);\\nDT{2}=DT{1};\\nDT{3}=DT{1};\\n\>\""}]}], "]"}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"xB", "=", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"xb", "[", "i", "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"i", ",", "nB"}], "}"}]}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"For", "[", 
      RowBox[{
       RowBox[{"i", "=", "1"}], ",", 
       RowBox[{"i", "\[LessEqual]", "nB"}], ",", "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"ss", "=", 
         RowBox[{
          RowBox[{"Solve", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"Flatten", "[", 
              RowBox[{"CoefficientList", "[", 
               RowBox[{
                RowBox[{
                 RowBox[{"Basis", ".", "xB"}], "-", 
                 RowBox[{"DBxi", "[", 
                  RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                RowBox[{"{", 
                 RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}], 
              "]"}], "\[Equal]", "0"}], ",", "xB"}], "]"}], "[", 
          RowBox[{"[", "1", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{"For", "[", 
         RowBox[{
          RowBox[{"k", "=", "1"}], ",", 
          RowBox[{"k", "\[LessEqual]", "nB"}], ",", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"If", "[", 
            RowBox[{
             RowBox[{
              RowBox[{"ss", "[", 
               RowBox[{"[", 
                RowBox[{"k", ",", "2"}], "]"}], "]"}], "\[NotEqual]", "0"}], 
             ",", 
             RowBox[{
              RowBox[{"kk", "=", 
               RowBox[{
                RowBox[{"ss", "[", 
                 RowBox[{"[", 
                  RowBox[{"k", ",", "1"}], "]"}], "]"}], "/.", 
                RowBox[{
                 RowBox[{"xb", "[", "x_", "]"}], "\[Rule]", "x"}]}]}], ";", 
              RowBox[{"WriteString", "[", 
               RowBox[{"OutFile", ",", 
                RowBox[{"\"\<DT{1}(\>\"", "<>", 
                 RowBox[{"ToString", "[", "kk", "]"}], "<>", "\"\<,\>\"", "<>", 
                 RowBox[{"ToString", "[", "i", "]"}], "<>", "\"\<)=\>\"", "<>", 
                 RowBox[{"ToMatlab", "[", 
                  RowBox[{"N", "[", 
                   RowBox[{"ss", "[", 
                    RowBox[{"[", 
                    RowBox[{"k", ",", "2"}], "]"}], "]"}], "]"}], "]"}]}]}], 
               "]"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
           RowBox[{"k", "++"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
        RowBox[{"ss", "=", 
         RowBox[{
          RowBox[{"Solve", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"Flatten", "[", 
              RowBox[{"CoefficientList", "[", 
               RowBox[{
                RowBox[{
                 RowBox[{"Basis", ".", "xB"}], "-", 
                 RowBox[{"DBeta", "[", 
                  RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                RowBox[{"{", 
                 RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}], 
              "]"}], "\[Equal]", "0"}], ",", "xB"}], "]"}], "[", 
          RowBox[{"[", "1", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{"For", "[", 
         RowBox[{
          RowBox[{"k", "=", "1"}], ",", 
          RowBox[{"k", "\[LessEqual]", "nB"}], ",", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"If", "[", 
            RowBox[{
             RowBox[{
              RowBox[{"ss", "[", 
               RowBox[{"[", 
                RowBox[{"k", ",", "2"}], "]"}], "]"}], "\[NotEqual]", "0"}], 
             ",", 
             RowBox[{
              RowBox[{"kk", "=", 
               RowBox[{
                RowBox[{"ss", "[", 
                 RowBox[{"[", 
                  RowBox[{"k", ",", "1"}], "]"}], "]"}], "/.", 
                RowBox[{
                 RowBox[{"xb", "[", "x_", "]"}], "\[Rule]", "x"}]}]}], ";", 
              RowBox[{"WriteString", "[", 
               RowBox[{"OutFile", ",", 
                RowBox[{"\"\<DT{2}(\>\"", "<>", 
                 RowBox[{"ToString", "[", "kk", "]"}], "<>", "\"\<,\>\"", "<>", 
                 RowBox[{"ToString", "[", "i", "]"}], "<>", "\"\<)=\>\"", "<>", 
                 RowBox[{"ToMatlab", "[", 
                  RowBox[{"N", "[", 
                   RowBox[{"ss", "[", 
                    RowBox[{"[", 
                    RowBox[{"k", ",", "2"}], "]"}], "]"}], "]"}], "]"}]}]}], 
               "]"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
           RowBox[{"k", "++"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
        RowBox[{"ss", "=", 
         RowBox[{
          RowBox[{"Solve", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"Flatten", "[", 
              RowBox[{"CoefficientList", "[", 
               RowBox[{
                RowBox[{
                 RowBox[{"Basis", ".", "xB"}], "-", 
                 RowBox[{"DZeta", "[", 
                  RowBox[{"[", "i", "]"}], "]"}]}], ",", 
                RowBox[{"{", 
                 RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}], 
              "]"}], "\[Equal]", "0"}], ",", "xB"}], "]"}], "[", 
          RowBox[{"[", "1", "]"}], "]"}]}], ";", "\[IndentingNewLine]", 
        RowBox[{"For", "[", 
         RowBox[{
          RowBox[{"k", "=", "1"}], ",", 
          RowBox[{"k", "\[LessEqual]", "nB"}], ",", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"If", "[", 
            RowBox[{
             RowBox[{
              RowBox[{"ss", "[", 
               RowBox[{"[", 
                RowBox[{"k", ",", "2"}], "]"}], "]"}], "\[NotEqual]", "0"}], 
             ",", 
             RowBox[{
              RowBox[{"kk", "=", 
               RowBox[{
                RowBox[{"ss", "[", 
                 RowBox[{"[", 
                  RowBox[{"k", ",", "1"}], "]"}], "]"}], "/.", 
                RowBox[{
                 RowBox[{"xb", "[", "x_", "]"}], "\[Rule]", "x"}]}]}], ";", 
              RowBox[{"WriteString", "[", 
               RowBox[{"OutFile", ",", 
                RowBox[{"\"\<DT{3}(\>\"", "<>", 
                 RowBox[{"ToString", "[", "kk", "]"}], "<>", "\"\<,\>\"", "<>", 
                 RowBox[{"ToString", "[", "i", "]"}], "<>", "\"\<)=\>\"", "<>", 
                 RowBox[{"ToMatlab", "[", 
                  RowBox[{"N", "[", 
                   RowBox[{"ss", "[", 
                    RowBox[{"[", 
                    RowBox[{"k", ",", "2"}], "]"}], "]"}], "]"}], "]"}]}]}], 
               "]"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
           RowBox[{"k", "++"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
        RowBox[{"i", "++"}]}]}], "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutFile", ",", "\"\<end\\n\\n\>\""}], "]"}], ";", 
     "\[IndentingNewLine]", "\[IndentingNewLine]", 
     RowBox[{"crules", "=", 
      RowBox[{"CoefficientRules", "[", 
       RowBox[{"Basis", ",", 
        RowBox[{"{", 
         RowBox[{"xi", ",", "eta", ",", "zeta"}], "}"}]}], "]"}]}], ";", 
     "\[IndentingNewLine]", 
     RowBox[{"T1", "=", 
      RowBox[{"Table", "[", 
       RowBox[{"0", ",", 
        RowBox[{"{", "nB", "}"}], ",", 
        RowBox[{"{", "nB", "}"}]}], "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"AllMonoms", "=", 
      RowBox[{"DeleteDuplicates", "[", 
       RowBox[{
        RowBox[{"Flatten", "[", "crules", "]"}], "[", 
        RowBox[{"[", 
         RowBox[{";;", ",", "1"}], "]"}], "]"}], "]"}]}], ";", 
     RowBox[{"If", "[", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"Dimensions", "[", "AllMonoms", "]"}], "[", 
         RowBox[{"[", "1", "]"}], "]"}], "\[NotEqual]", "nB"}], ",", 
       RowBox[{"Print", "[", 
        RowBox[{"\"\<Error: \>\"", "<>", 
         RowBox[{"ToString", "[", "nB", "]"}], "<>", "\"\<\[NotEqual]\>\"", "<>", 
         RowBox[{"ToString", "[", 
          RowBox[{"Dimensions", "[", "AllMonoms", "]"}], "]"}]}], "]"}]}], 
      "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"(*", " ", 
      RowBox[{
       RowBox[{
       "T1", " ", "expresses", " ", "the", " ", "basis", " ", "as", " ", "a", 
        " ", "function", " ", "of", " ", "the", " ", "monomials", " ", "in", 
        " ", "\[Xi]"}], ",", "\[Eta]", ",", "\[Zeta]"}], " ", "*)"}], 
     "\[IndentingNewLine]", 
     RowBox[{"Do", "[", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{"T1", "[", 
          RowBox[{"[", 
           RowBox[{";;", ",", "i"}], "]"}], "]"}], "=", 
         RowBox[{
          RowBox[{"AllMonoms", "[", 
           RowBox[{"[", "i", "]"}], "]"}], "/.", "crules"}]}], ";", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{"T1", "[", 
          RowBox[{"[", 
           RowBox[{";;", ",", "i"}], "]"}], "]"}], "=", 
         RowBox[{
          RowBox[{"T1", "[", 
           RowBox[{"[", 
            RowBox[{";;", ",", "i"}], "]"}], "]"}], "/.", 
          RowBox[{
           RowBox[{"AllMonoms", "[", 
            RowBox[{"[", "i", "]"}], "]"}], "\[Rule]", "0"}]}]}]}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{"i", ",", "nB"}], "}"}]}], "]"}], ";", "\[IndentingNewLine]", 
     "\[IndentingNewLine]", 
     RowBox[{"OutMesh", "=", 
      RowBox[{"OpenWrite", "[", 
       RowBox[{"path", "<>", "\"\<CompatBasis3D-\>\"", "<>", 
        RowBox[{"ToString", "[", "deg", "]"}], "<>", "\"\<.msh\>\""}], 
       "]"}]}], ";", "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutMesh", ",", 
       RowBox[{"\"\<$InterpolationScheme\\n\\\"Basis \>\"", "<>", 
        RowBox[{"ToString", "[", "deg", "]"}], "<>", 
        "\"\<\\\"\\n1\\n5\\n2\\n\>\""}]}], "]"}], ";", "\[IndentingNewLine]", 
     
     RowBox[{"WriteString", "[", 
      RowBox[{"OutMesh", ",", 
       RowBox[{"ToString", "[", "nB", "]"}], ",", "\"\< \>\"", ",", 
       RowBox[{"ToString", "[", "nB", "]"}], ",", "\"\<\\n\>\""}], "]"}], ";",
      "\[IndentingNewLine]", 
     RowBox[{"Do", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"Do", "[", 
         RowBox[{
          RowBox[{"WriteString", "[", 
           RowBox[{"OutMesh", ",", 
            RowBox[{"ToString", "[", 
             RowBox[{"CForm", "[", 
              RowBox[{"N", "[", 
               RowBox[{"T1", "[", 
                RowBox[{"[", 
                 RowBox[{"i", ",", "j"}], "]"}], "]"}], "]"}], "]"}], "]"}], 
            ",", "\"\< \>\""}], "]"}], ",", 
          RowBox[{"{", 
           RowBox[{"j", ",", "nB"}], "}"}]}], "]"}], ";", 
        RowBox[{"WriteString", "[", 
         RowBox[{"OutMesh", ",", "\"\<\\n\>\""}], "]"}]}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "nB"}], "}"}]}], "]"}], ";", "\[IndentingNewLine]", 
     
     RowBox[{"(*", " ", 
      RowBox[{
      "The", " ", "definition", " ", "of", " ", "the", " ", "monomials"}], 
      " ", "*)"}], "\[IndentingNewLine]", 
     RowBox[{"WriteString", "[", 
      RowBox[{"OutMesh", ",", 
       RowBox[{"ToString", "[", "nB", "]"}], ",", "\"\< 3\\n\>\""}], "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Do", "[", 
      RowBox[{
       RowBox[{"WriteString", "[", 
        RowBox[{"OutMesh", ",", 
         RowBox[{"ToString", "[", 
          RowBox[{"AllMonoms", "[", 
           RowBox[{"[", 
            RowBox[{"i", ",", "1"}], "]"}], "]"}], "]"}], ",", " ", 
         "\"\< \>\"", ",", 
         RowBox[{"ToString", "[", 
          RowBox[{"AllMonoms", "[", 
           RowBox[{"[", 
            RowBox[{"i", ",", "2"}], "]"}], "]"}], "]"}], ",", " ", 
         "\"\< \>\"", ",", " ", 
         RowBox[{"ToString", "[", 
          RowBox[{"AllMonoms", "[", 
           RowBox[{"[", 
            RowBox[{"i", ",", "3"}], "]"}], "]"}], "]"}], ",", " ", 
         "\"\<\\n\>\""}], "]"}], ",", 
       RowBox[{"{", 
        RowBox[{"i", ",", "nB"}], "}"}]}], "]"}], ";", "\[IndentingNewLine]", 
     
     RowBox[{"WriteString", "[", 
      RowBox[{"OutMesh", ",", "\"\<$EndInterpolationScheme\\n\>\""}], "]"}], 
     ";", "\[IndentingNewLine]", 
     RowBox[{"Close", "[", "OutMesh", "]"}], ";", "\[IndentingNewLine]", 
     RowBox[{"deg", "++"}]}]}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Close", "[", "OutFile", "]"}], ";"}]}], "Input",
 CellChangeTimes->{{3.601132245004027*^9, 3.601132430884821*^9}, {
   3.601132594592814*^9, 3.6011326189437933`*^9}, {3.601132715461028*^9, 
   3.601132782482025*^9}, {3.601132829286813*^9, 3.6011328587047243`*^9}, {
   3.601132940627326*^9, 3.601132956208002*^9}, {3.601132993409217*^9, 
   3.601132998980629*^9}, {3.601133041756887*^9, 3.6011331250165453`*^9}, {
   3.601133204852294*^9, 3.601133389161001*^9}, {3.601133441580061*^9, 
   3.6011335328308477`*^9}, {3.601133605829792*^9, 3.60113361748079*^9}, 
   3.6011339591837254`*^9, {3.6011340316866837`*^9, 3.6011340450766068`*^9}, {
   3.6011896847636957`*^9, 3.6011898055208797`*^9}, {3.601196312232204*^9, 
   3.601196315634076*^9}, {3.601196975160573*^9, 3.601196976907797*^9}, {
   3.601214040595202*^9, 3.6012140750627623`*^9}, {3.601214199363159*^9, 
   3.601214221764889*^9}, {3.601215078990087*^9, 3.601215083669231*^9}, {
   3.601283634495064*^9, 3.601283680502901*^9}, {3.601283748434536*^9, 
   3.601283792331029*^9}, {3.6012839081832523`*^9, 3.601283939593154*^9}, {
   3.601284002575677*^9, 3.601284003599106*^9}, {3.60130626841676*^9, 
   3.601306270110752*^9}, {3.601306432596546*^9, 3.601306441955625*^9}, {
   3.601306481952428*^9, 3.601306485435664*^9}, 3.6013068575092382`*^9, {
   3.601307126674473*^9, 3.601307137700227*^9}, {3.601307369617359*^9, 
   3.601307385843452*^9}, 3.601307496758322*^9, {3.601308425795426*^9, 
   3.6013084284352818`*^9}, {3.601362656555695*^9, 3.601362681892267*^9}, 
   3.6013627425252867`*^9, {3.6013629047209473`*^9, 3.6013629058284197`*^9}, {
   3.6013633618599033`*^9, 3.601363399813551*^9}, {3.603545389318647*^9, 
   3.603545449301859*^9}, {3.603545481112309*^9, 3.60354550738542*^9}, {
   3.603545562522871*^9, 3.6035455896891947`*^9}, {3.60354566045271*^9, 
   3.6035456686971827`*^9}, {3.60354571145616*^9, 3.603545737067009*^9}, {
   3.603545773020186*^9, 3.6035459545177317`*^9}, {3.603546036400073*^9, 
   3.6035460544197893`*^9}, {3.603546238373074*^9, 3.6035462478469467`*^9}, {
   3.603546872134036*^9, 3.60354688506828*^9}, {3.60355241738358*^9, 
   3.6035525292372007`*^9}, {3.603552560159205*^9, 3.603552589934114*^9}, {
   3.603552662943838*^9, 3.603552842740943*^9}, {3.603552874841304*^9, 
   3.6035529305041122`*^9}, {3.603553156277185*^9, 3.6035532083696938`*^9}, 
   3.603553445178835*^9, 3.603553479334676*^9, {3.6035536879417562`*^9, 
   3.603553690295446*^9}, {3.6035537293617563`*^9, 3.603553746778233*^9}, {
   3.603553804988511*^9, 3.603553810339548*^9}, {3.6035538557894697`*^9, 
   3.603553874848131*^9}, {3.603554816448328*^9, 3.603554822116083*^9}, 
   3.603554899247665*^9, {3.603554961761813*^9, 3.6035549880964117`*^9}, {
   3.6036299148114367`*^9, 3.603629928249022*^9}, {3.6036302249936113`*^9, 
   3.603630248849633*^9}, {3.603630517175901*^9, 3.603630521085202*^9}, {
   3.603630651676261*^9, 3.6036306565196238`*^9}, {3.603633652533558*^9, 
   3.6036337511913433`*^9}, {3.663652338516652*^9, 3.663652363725308*^9}, {
   3.663652413908702*^9, 3.663652457452739*^9}, {3.6636525002199574`*^9, 
   3.66365250210423*^9}, {3.663652771158619*^9, 3.663652855479095*^9}, {
   3.663656055607234*^9, 3.663656071259445*^9}, {3.663656655963292*^9, 
   3.6636570569361877`*^9}, {3.6636584184004917`*^9, 3.663658419964201*^9}, {
   3.6636585842866592`*^9, 3.663658586842351*^9}, {3.663658659307926*^9, 
   3.6636586951582727`*^9}, {3.663658784256925*^9, 3.66365881651089*^9}, {
   3.663687353262199*^9, 3.663687356706558*^9}, {3.663687484434618*^9, 
   3.663687505750148*^9}, {3.663687624714786*^9, 3.663687640984543*^9}, {
   3.663688080081559*^9, 3.663688125366302*^9}, {3.663688244720681*^9, 
   3.6636882767081327`*^9}, {3.663688308061282*^9, 3.663688308257399*^9}, 
   3.663688787463992*^9, {3.663689190212002*^9, 3.663689224598193*^9}, {
   3.66368926286482*^9, 3.663689267263762*^9}, {3.663689329139559*^9, 
   3.663689350141735*^9}, {3.663918894440423*^9, 3.663918921710081*^9}, {
   3.663918959842061*^9, 3.663918975629368*^9}, {3.663919413772106*^9, 
   3.663919421806232*^9}, 3.663919457403584*^9, 3.6639196407863827`*^9, 
   3.663919712637518*^9, 3.663919877044512*^9, {3.665899699571883*^9, 
   3.665899762117879*^9}, {3.6658998357178373`*^9, 3.6658998501772842`*^9}, {
   3.6658998828551607`*^9, 3.665899988314641*^9}, {3.665900021145784*^9, 
   3.665900189569262*^9}, {3.665900635331059*^9, 3.665900641050499*^9}, {
   3.6659006763647823`*^9, 3.665900679203787*^9}, {3.665900727626987*^9, 
   3.665900742520382*^9}, 3.6659009775460863`*^9, {3.6659010242725058`*^9, 
   3.665901038971973*^9}, {3.6659010902705193`*^9, 3.665901112075014*^9}, {
   3.66590167656166*^9, 3.665901777278047*^9}, {3.6659018296031227`*^9, 
   3.665901848239347*^9}, {3.6659018832139*^9, 3.665902042132409*^9}, {
   3.6659020952545853`*^9, 3.665902103420274*^9}, {3.665902140602206*^9, 
   3.6659021485637293`*^9}, {3.665914405946269*^9, 3.665914410269895*^9}, {
   3.665914464911914*^9, 3.6659145252025137`*^9}, {3.665914605580954*^9, 
   3.665914709674913*^9}, {3.665914890531886*^9, 3.665914893225788*^9}, {
   3.665914970649146*^9, 3.6659149708497458`*^9}, {3.665996355823454*^9, 
   3.665996356078389*^9}},
 CellLabel->"In[31]:=",ExpressionUUID->"80a2eada-668e-4989-b9e5-e278f678489c"]
},
WindowSize->{1440., 740.25},
WindowMargins->{{3, Automatic}, {Automatic, 27.75}},
PrivateNotebookOptions->{"VersionedStylesheet"->{"Default.nb"[8.] -> False}},
ShowSelection->True,
FrontEndVersion->"12.1 for Linux x86 (64-bit) (June 19, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"86b9e86b-225a-44e8-af7b-4924d96a7b39"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[545, 20, 199, 3, 29, "Input",ExpressionUUID->"0c11c500-466a-4fbf-bfab-5ad5887835ce"],
Cell[747, 25, 201, 3, 29, "Input",ExpressionUUID->"31b3eca0-e084-4969-963b-f8fdf085e29c"],
Cell[951, 30, 655, 17, 29, "Input",ExpressionUUID->"b08e4c50-8ae4-41fd-8db1-8aab7cf0df0a"],
Cell[1609, 49, 355, 10, 29, "Input",ExpressionUUID->"e70b959c-cef7-4dce-a022-ec1a91797229"],
Cell[1967, 61, 685, 17, 29, "Input",ExpressionUUID->"9cf2d738-8e7f-49c5-a686-3f9936ae9b2f"],
Cell[2655, 80, 924, 24, 29, "Input",ExpressionUUID->"5264bdd8-d26d-4afd-9746-1a8e523c75f8"],
Cell[3582, 106, 1161, 32, 29, "Input",ExpressionUUID->"c2b6ca2a-ab6f-4397-8502-88cb0810845e"],
Cell[4746, 140, 940, 24, 29, "Input",ExpressionUUID->"9fa4a722-3bd6-4d0c-85ab-6ccb31542136"],
Cell[5689, 166, 922, 25, 29, "Input",ExpressionUUID->"176c5481-2496-4391-a0a2-56ab3fe6a28d"],
Cell[6614, 193, 1285, 19, 29, "Input",ExpressionUUID->"cf0a6036-8a36-4fc8-96b3-6636a45756e9"],
Cell[7902, 214, 731, 20, 29, "Input",ExpressionUUID->"3fc8c575-f302-46f4-b3d9-dc1e972be6e6"],
Cell[8636, 236, 567, 15, 29, "Input",ExpressionUUID->"e36dced9-abb1-4e0b-8b13-4b2f375a5453"],
Cell[9206, 253, 412, 7, 29, "Input",ExpressionUUID->"a93dd07f-ea5f-48e3-a93f-4d712cbb07b6"],
Cell[9621, 262, 27713, 610, 1880, "Input",ExpressionUUID->"80a2eada-668e-4989-b9e5-e278f678489c"]
}
]
*)

