(* ::Package:: *)

(************************************************************************)
(* This file was generated automatically by the Mathematica front end.  *)
(* It contains Initialization cells from a Notebook file, which         *)
(* typically will have the same name as this file except ending in      *)
(* ".nb" instead of ".m".                                               *)
(*                                                                      *)
(* This file is intended to be loaded into the Mathematica kernel using *)
(* the package loading commands Get or Needs.  Doing so is equivalent   *)
(* to using the Evaluate Initialization Cells menu command in the front *)
(* end.                                                                 *)
(*                                                                      *)
(* DO NOT EDIT THIS FILE.  This entire file is regenerated              *)
(* automatically each time the parent Notebook file is saved in the     *)
(* Mathematica front end.  Any changes you make to this file will be    *)
(* overwritten.                                                         *)
(************************************************************************)



(* ::Input::Initialization:: *)
(* The coordinates of the 4 vertices in a HCT element *)
LsHCTVerts={{1,0,0,1/3},{0,1,0,1/3},{0,0,1,1/3}};


(* ::Input::Initialization:: *)
(* The vertices of each primitive in a HCT element. The central vertex (4) is always the first *)
VertsOfHCTPrims={{4,2,3},{4,3,1},{4,1,2}};


(* ::Input::Initialization:: *)
(* To draw, also to check if L[2] and L[3] are inside a given primitive element *)
HCTElem[1]=Polygon[{{1/3,1/3},{1,0},{0,1}}];
HCTElem[2]=Polygon[{{1/3,1/3},{0,1},{0,0}}];
HCTElem[3]=Polygon[{{1/3,1/3},{0,0},{1,0}}];


(* ::Input::Initialization:: *)
(* Replace the Ls of the macro with the Lis of the primitives *)
HCTLsToLis=Table[Table[L[dir]->LsHCTVerts[[dir,VertsOfHCTPrims[[prim]]]] . Lis,{dir,3}],{prim,3}];
(* Replace the Lis of the primitives with the Ls of the macro *)
HCTLisToLs=Table[Solve[Ls==(Ls/.HCTLsToLis[[prim]]),Lis][[1]],{prim,3}];


(* ::Input::Initialization:: *)
(* First dimension of b has to be 3, one for each primitive *)
ChangeHCTBaseLsToLis[b_]:=FixedLimitSimplify[Table[b[[i]]/.HCTLsToLis[[i]],{i,3}]];
ChangeHCTBaselsToLs[b_]:=FixedLimitSimplify[Table[b[[i]]/.HCTLisToLs[[i]],{i,3}]];


(* ::Input::Initialization:: *)
(* Normal derivative on side 23 *)
DnF23[f_]:=D[f,L[2]]-D[f,L[3]];
(* Rule to change to local coordinates on side 23 *)
OnSide23={L[1]->l1,L[2]->l2,L[3]->l2};


(* ::Input::Initialization:: *)
(* TDerivs is used to obtain derivatives as a function of the derivatives with respect to the area coordinates *)
(* rows 1 and 2 for the derivatives along 12 and 23, row 3 will be the normal derivative for each internal side *)
TDerivs=Table[0,3,3];
TDerivs[[1,1]]=-1;TDerivs[[2,1]]=1;TDerivs[[3,1]]=0; (* Derivative along 12 *)
TDerivs[[1,2]]=-1;TDerivs[[2,2]]=0;TDerivs[[3,2]]=1; (* Derivative along 13 *)
(* These are not the true normals (in the xy space). 
Because each one always includes a component in the direction of the normal it can be used to check 
whether a solution exists. To obtain the interpolation for a specific geometry this matrix must be overwriten *)
OutNormals={
{1,-1,0}, (* Normal derivative to side 2 (not scaled) *)
{1,0,-1},(* Normal derivative to side 3 (not scaled) *)
{-2,1,1} (* Normal derivative to side 1 (not scaled) *)}; 


(* ::Input::Initialization:: *)
(* Circulate in either direction *)
Ls123=Ls;
Ls132={L[1],L[3],L[2]};


(* ::Input::Initialization:: *)
(* The square of the length of the sides *)
LSidesSq={L12,L22,L32};
(* These lengths are usually inconsistent. They are used to know that the values are non-zero *)
SetSidesLengthNumeric=Table[LSidesSq[[i]]==1,{i,3}];


(* ::Input::Initialization:: *)
(* Generic coordinates for the frame with origin at node 1 *)
xyHCT={{0,x21,x31},{0,y21,y31}};


(* ::Input::Initialization:: *)
(* Set values for the generic coordinates in xyHCT *)
SetCoordsHCT[xy_]:={
L12->(xy[[1,3]]-xy[[1,2]])^2+(xy[[2,3]]-xy[[2,2]])^2,
L22->(xy[[1,1]]-xy[[1,3]])^2+(xy[[2,1]]-xy[[2,3]])^2,
L32->(xy[[1,2]]-xy[[1,1]])^2+(xy[[2,2]]-xy[[2,1]])^2,
x21->xy[[1,2]]-xy[[1,1]],x31->xy[[1,3]]-xy[[1,1]],
y21->xy[[2,2]]-xy[[2,1]],y31->xy[[2,3]]-xy[[2,1]]
}


(* ::Input::Initialization:: *)
MapXYToLsHCT={x->Sum[L[i]xyHCT[[1,i]],{i,3}],y->Sum[L[i]xyHCT[[2,i]],{i,3}]};


(* ::Input::Initialization:: *)
MapLsToXYHCT=Solve[{x,y,Sum[L[i],{i,3}]}==({x,y,1}/.MapXYToLsHCT),Ls][[1]];


(* ::Input::Initialization:: *)
(*The dimension by construction*)
NDofHCT[d_,r_,p_]:=If[Mod[r,2]==1,
If[d>=3(r+1)/2,
3d^2/2+9r^2/4-3d r+3d/2+3/4,
Num[d]],
If[d>=3r/2+1,
3d^2/2+9r^2/4-3d r+3d/2+1,
Num[d]]
]-3Num[p-1]


(* ::Input::Initialization:: *)
RotateFuncsHCT[iBase_]:=Module[{},
iBase[[{3,1,2}]]/.{L[1]->LL[2],L[2]->LL[3],L[3]->LL[1]}/.LL[i_]->L[i]
]


(* ::Input::Initialization:: *)
(* Generate the smallest set for continuity of order r, 
	the appropriate degree (3/2 (r + 1) or 3/2 r + 1) is determined outside *)
GenHCTSmallest[degree_,r_]:=Module[{d1,d2,d3,f2,f3,g2,g3,fs,fss},
d1=r+1;d2=degree-d1;d3=d2-1;
If[d2<0,
(* Invalid data *)
Print["No additional functions"],
(* Data is valid *)
(* This could be coded with generic i,j,k. Use 1,2,3 and permute after *)
f2=(L[1]-L[2])^d1 Pol2D[d2,Ls123,w]; 
f3=(L[1]-L[3])^d1 Pol2D[d2,Ls132,w];
Print["Degree fs is ",d2,". Degree gs is ",d3,"."];
fs=AdjustHCTFuncs[r,f2,f3];
g2=(L[1]-L[2])^d1 (L[2]-L[3])Pol2D[d3,Ls123,w];
g3=(L[1]-L[3])^d1 (L[2]-L[3])Pol2D[d3,Ls132,w];
fss=AdjustHCTFuncs[r,g2,g3];
];
{fs,fss}
]


(* ::Input::Initialization:: *)
(* Solve the constraints to impose continuity of order r *)
AdjustHCTFuncs[r_,testf1_,testf2_]:=Module[{f,a,exp,exp0},
(* The normal derivatives on side 23 *)
df[0]=testf1-testf2;
cond[0]=LimitSimplify[df[0]/.OnSide23,2,10];
For[\[Beta]=1,\[Beta]<=r,\[Beta]++,
df[\[Beta]]=DnF23[df[\[Beta]-1]];
cond[\[Beta]]=LimitSimplify[df[\[Beta]]/.OnSide23,2,10];
];
(* Odd/even conditions are automatically satisfied *)
(* s=Solve[Resolve[Table[ForAll[{l1,l2},cond[a]\[Equal]0],{a,0,r}]]][[1]]; *)
s={};
For[\[Beta]=0,\[Beta]<=r,\[Beta]++,
ss=Solve[Resolve[ForAll[{l1,l2},(cond[\[Beta]]//.s)==0]]][[1]];
s=Flatten[{s,ss}];
];
(* 3 entries, for the 3 primitive elements *)
f=LimitSimplify[{0,testf1,testf2}//.s,2,10];
exp0=Max[Exponent[{0,testf1,testf2},Ls]];
exp=Max[Exponent[f,Ls]];
If[exp==-Infinity,
Print["Degree would be ", exp0,". No solution"],
If[deb,Print["Degree is ",exp,"     ",f//MatrixForm//TraditionalForm]];
Resid=Table[cond[\[Beta]],{\[Beta],0,r}]//.s;
If[Max[Abs[Resid]]!=0,Print["Problem with constraints: ",Resid]];
];
f
]


(* ::Input::Initialization:: *)
ConstructHCTBasis[degree_,r_]:=Module[{},
(* The uniform basis *)
BaseGlobal=KroneckerProduct[{{1},{1},{1}},Base2D[degree,Ls]];
If[OddQ[r],
(* Odd continuity, skew symmetric and permute *)
Print["Odd"];
degSmallest=3(r+1)/2;
If [degree>=degSmallest,
degAdditional=degree-degSmallest;
(* Pick the second (skew-symmetric) function, which has only one parameter. Is it always w[1]? *)
fs=GenHCTSmallest[degSmallest,r][[2]];
AllWs=Cases[Variables[fs],w[_]];
If[partial,
Do[
Print[ww,"  ",FullSimplify[fs/.{ww->1,w[_]->0}]//MatrixForm],
{ww,AllWs}
];
];
fs = fs/.{AllWs[[-1]]->1,w[_]->0};
(* "Use" the 3 functions in fs for elements 123 and (changing variables) for elements 231 *)
BaseLocal=KroneckerProduct[{Base2D[degAdditional,Ls]},
Transpose[{
fs,
fs[[PrevIJK]]/.Table[L[i]->L[NextIJK[[i]]],{i,3}]
}]];
,
Print["Smallest Set is empty"];
fs={};BaseLocal={}];
,
(* Even continuity, symmetric for one or two degrees *)
Print["Even"];
degSmallest=3r/2+1;
If [degree>=degSmallest,
degAdditional=degree-degSmallest;
(* Pick the first (symmetric) function, which has only one parameter.  *)
fs=GenHCTSmallest[degSmallest,r][[1]];
AllWs=Cases[Variables[fs],w[_]];
If[partial,
Do[
Print[ww,"  ",FullSimplify[fs/.{ww->1,w[_]->0}]//MatrixForm],
{ww,AllWs}
];
];
fs=fs/.{AllWs[[-1]]->1,w[_]->0};
BaseLocal=KroneckerProduct[{Base2D[degAdditional,Ls]},Transpose[{fs}]];
If[degree>degSmallest,
(* We have one more function *)
(* Pick the first (symmetric) function, which has only one parameter. *)
fs=GenHCTSmallest[degSmallest+1,r][[1]];
AllWs=Cases[Variables[fs],w[_]];
If[partial,
Do[
Print[ww,"  ",FullSimplify[fs/.{ww->1,w[_]->0}]//MatrixForm],
{ww,AllWs}
];
];
fs = fs/.{AllWs[[-1]]->1,w[_]->0};
BaseLocal=Join[BaseLocal,KroneckerProduct[{Base2D[degAdditional-1,Ls]},Transpose[{fs}]],2]];
,
Print["Smallest Set is empty"];
fs={};BaseLocal={}];
];
BaseTotal=Join[BaseGlobal,BaseLocal,2]
]


(* ::Input::Initialization:: *)
(* Build the interpolation functions, adding the constraints for r+p continuity at the vertices *)
BuildHCTInterpolation[basis_,degree_,r_,p_]:=Module[{rNodes,aGbasis,InterpBasis,AllDerivs,Incid,NodesPerSide,NodePos,nB,conds,nconds, side,order,pos,count,list,i,j,k},
rNodes=r+p;
nB=Dimensions[basis][[2]];
conds={};nconds=0;

vertexconds=Table[0,p,3];
Incid=NodesOnSide[degree,r,p];
If[deb,Print["Incid ",Incid//MatrixForm]];
NodesPerSide=Max[Flatten[{0,Incid}]]; (* Add the zero for the case of an empty list *)

NodePos=Table[i/(NodesPerSide+1),{i,NodesPerSide}];

For[side=1,side<=3,side++,
i=side;j=NextIJK[[side]];k=PrevIJK[[side]];
Gbasis[side,0]=Transpose[{basis[[j]]}]; (* The derivatives of order 0 in direction 12, 13 and n *)
TDerivs[[;;,3]]=OutNormals[[side]];
For[order=1,order<=rNodes,order++,
(* Here the last index is the derivative *)
Monitor[
tmp=ParallelMap[FixedLimitSimplify,Grad[Gbasis[side,order-1],Ls] . TDerivs,2];
(* tmp=Transpose[ParallelTable[D[Gbasis[v,order-1],Ls[[ii]]],{ii,3}],{3,1,2}].TDerivs; *)
Gbasis[side,order]=ParallelMap[FixedLimitSimplify,
ArrayReshape[
Transpose[{tmp[[;;,1,1]],tmp[[;;,1;;order,2]],tmp[[;;,-1,-1]]}],{nB,2+order}],2];
,"Derivative "<>ToString[order]<>" on primitive "<>ToString[side]];
];

For[order=0,order<=rNodes,order++,
(* Get all the derivatives of order order along j and k for primitive element 
(at the vertex it could be k)
Just follow the lists, using the sequence xxxyyy 
with x's appearing n1 times and y n2 times, with n1+n2=order *)
list=Gbasis[side,order][[;;,1;;(1+order)]];
If[deb,Print["add derivs of order ",order," at vertex ",i," with ",Dimensions[list]]];
nconds+=order+1;
For[dir=1,dir<=1+order,dir++,
AppendTo[conds,list[[;;,dir]]/.{L[i]->1,L[_]->0}](* At vertex v *)
];
If[order>r,
(* When order > r, set the extra constraint at the vertices *)
(* There are order+1 derivatives, however only order-r are independent *)
vertexconds[[order-r,i]]+=Transpose[list[[;;,Range[order-r]]]/.{L[i]->1,L[_]->0}];
vertexconds[[order-r,k]]-=Transpose[list[[;;,Range[order-r]]]/.{L[k]->1,L[_]->0}];
If[deb,Print["Add extra derivs of order ",order," at vertices +",j," and -",k," of side ",side," with ",Dimensions[list[[;;,Range[order-r]]]]]],
(* else order <= r *)
(* The derivatives normal to side j, on primitive element j. Only one direction *)
list=Gbasis[side,order][[;;,-1]];
Do[
(* At side j, scaled by the right LSidesSq *)
nconds++;
If[deb,Print["add normal deriv of order ",order," at side ",j," with ",Dimensions[list]," to ",nconds]];
AppendTo[conds,list(*/(LSidesSq[[k]]^order)*)/.{L[k]->NodePos[[n]],L[i]->1-NodePos[[n]],L[j]->0}];
,
{n,Incid[[order+1]]}]
];
];
];
NDofSides=3((p-1) p/2- r+ degree(r+1)- r (r+p));
(* 3(d(r+1) dB-rB-rB (rB+pB)+1/2 (pB-1) pB);*)

(* Interpolation at internal nodes (bubbles) *)
NBubbles = nB-NDofSides-3Num[p-1];
BubbleAtCentre=False;
If[NBubbles>0,
If[EvenQ[r],
BubbleAtCentre=True;NBubbles--;
Generate=GenerateIISNodes,
Generate=GenerateINodes
];
If[Mod[NBubbles,3]!=0,Print["Something wrong with the number of bubbles, not multiple of 3"]];
NBubblesPerElem = NBubbles/3;
bubbleconds={};
If[NBubblesPerElem>0,
OrderBubble=InvNum[NBubblesPerElem];
If[!IntegerQ[OrderBubble],Print["Something unexpected with the number of bubbles, ", OrderBubble," is not integer"]];
(* The Li coordinates of the internal nodes and the corresponding replacement rule *)
BubbleNodes=Generate[OrderBubble];
AtNodes=Table[Flatten[Table[Li[v]->BubbleNodes[[n,v]],{v,3}]],{n,Dimensions[BubbleNodes][[1]]}];
(* The functions at the nodes *)
bubbleconds=Flatten[Table[basis[[prim]]/.HCTLsToLis[[prim]]/.AtNodes,{prim,3}],1];
];
If[BubbleAtCentre,
(* Geometric centre of the element, or vertex 1 of all primitive elements, use element 1 *)
bubbleconds=Join[bubbleconds,{basis[[1]]}/.{L[_]->1/3}];
];
];

tmpconds=conds;
NDeps=0;
If[NBubbles+If[BubbleAtCentre,1,0]>0,
nconds+=NBubbles+If[BubbleAtCentre,1,0];
conds=Join[conds,bubbleconds];
];

nvertexconds=0;
If[p>0,
(* Insert the conditions for order > r at the vertices *)
vertexconds=Flatten[Table[Flatten[vertexconds[[order]],1],{order,p}],1];
nvertexconds=Dimensions[vertexconds][[1]];
(* Place vertexconds first *)
conds=Join[vertexconds,conds];
];

conds=Join[conds,Join[Table[0,nvertexconds,nconds],IdentityMatrix[nconds]],2];

If[NDofSides+NBubbles+If[BubbleAtCentre,1,0]!=nconds,
Print["Opps, dimensions: ",NDofSides," \[NotEqual] ",nconds];,
Print[nB," parameters, with ",nvertexconds," - ",NDeps," vertex conditions. ",NDofSides," dofs at the boundary, plus ",nB-NDofSides-nvertexconds+NDeps, " bubbles"]
];
aaa=conds;

PivotCols=Table[0,nconds+nvertexconds];
PivotRows=Table[0,nB];
Pivots=PivotCols;
PendingRows={};
Monitor[
For[row=1,row<=nconds+nvertexconds,row++,
sRow=LimitSimplify[conds[[row,1;;nB]],2,10];
AbsRow=Abs[Select[sRow,NumericQ]];
val=Max[AbsRow];
col=0;
If[val>0,
col=Position[Abs[sRow],val,{1}][[1,1]],
If[deb,Print["Not numeric in row ",row,{sRow}//MatrixForm]];
PendingRows=AppendTo[PendingRows,row];
];
If[col>0,
PivotCols[[row]]=col;
PivotRows[[col]]=row;
Pivots[[row]]=conds[[row,col]];
conds=LimitSimplify[Pivot[conds,row,col],1,5]
];
];
,"Row "<>ToString[row]];
Print[Length[PendingRows]," conditions that are not numeric"];
Do[
sRow=LimitSimplify[conds[[row,1;;nB]],2,10];
(* The position of the terms with only one side length squared *)
single =Flatten[ Position[Map[Dimensions,Map[Variables,sRow]],{1},1]];
If[Length[single]>0,
If[deb,Print[{single,sRow[[single]]}//MatrixForm]];
AbsRow=Simplify[Abs[sRow[[single]]/.{L12->1,L22->1,L32->1}]];
val=AbsRow[[1]]; (* Max[AbsRow]; (* Has to be non-zero *) *)
col=single[[Position[AbsRow,val,{1}][[1,1]]]];
,
(* Otherwise, choose the shortest expression *)
Print["This should not happen, still trying..."];
counts=Map[LeafCount,sRow]-1;
nonzeros=Flatten[Position[counts,_Integer?Positive]];
If[Length[nonzeros]>0,
val=Min[counts[[nonzeros]]];
col=Position[counts,val,{1}][[1,1]];
,
If[deb,Print["Really dependent? ", {Map[Dimensions,Map[Variables,sRow]]}//MatrixForm]];
badRow=sRow;
NDeps++
];
];
If[col>0,
PivotCols[[row]]=col;
PivotRows[[col]]=row;
Pivots[[row]]=conds[[row,col]];
conds=LimitSimplify[Pivot[conds,row,col],1,5]
];
,{row,PendingRows}];
Print[NDeps," dependent conditions"];
(* Explicitly construct the interpolating basis, only for dofs on the boundary *)
Coefs=conds[[;;,nB+Range[nconds]]];
InterpBasis=basis[[;;,PivotCols]] . Coefs
]


(* ::Input::Initialization:: *)
(* The x, y coordinates of node n on side i *)
InterpSideHCT[i_,n_]:=Module[{},
aux=xyHCT[[;;,PrevIJK[[i]]]]NodePos[[n]]+xyHCT[[;;,NextIJK[[i]]]](1-NodePos[[n]]);
{x->aux[[1]],y->aux[[2]]}]


(* ::Input::Initialization:: *)
(* The matrix that rotates the derivatives at a vertex *)
VertexRotationMatrixHCT[Jac_,maxorder_]:=Module[{},
J=Table[Table[0,order+1,order+1],{order,0,maxorder}];
(* The value of the function is not scaled *)
J[[1,1,1]]=1;
For[order=1,order<=maxorder,order++,
JPrev=J[[order]];
J[[order+1,1;;order,1;;order]]+=JPrev Jac[[1,1]];
J[[order+1,1;;order,2;;(order+1)]]+=JPrev Jac[[1,2]];
J[[order+1,order+1,1;;order]]+=JPrev[[order]] Jac[[2,1]];
J[[order+1,order+1,2;;(order+1)]]+=JPrev[[order]] Jac[[2,2]];
];
J
]


(* ::Input::Initialization:: *)
(* The matrix that rotates all the derivatives for an element and (de)scales the normal rotation of order n by factor^n *)
XYRotationMatrixHCT[Jac_,degree_,r_,p_,factor_]:=Module[{vertex,d,n,offset},
VRot=VertexRotationMatrixHCT[Jac,r+p];
Incid=NodesOnSide[degree,r,p];
NodesPerSide=Max[Flatten[{0,Incid}]]; (* Add the zero for the case of an empty list *)
NodePos=Table[i/(NodesPerSide+1),{i,NodesPerSide}];
DofsPerSidePerDegree=Table[Dimensions[Incid[[d]]][[1]],{d,r+1}];
NDofs=NDofHCT[degree,r,p];
RMat=IdentityMatrix[NDofs];
offset=0;
For[vertex=1,vertex<=3,vertex++,
For[d=0,d<=r+p,d++,
RMat[[(offset+1);;(offset+d+1),(offset+1);;(offset+d+1)]]=VRot[[d+1]];
If[d<=r,
For[n=1,n<=DofsPerSidePerDegree[[d+1]],n++,
RMat[[offset+d+1+n,offset+d+1+n]]=factor^d;
];
];
offset+=d+1+If[d<=r,DofsPerSidePerDegree[[d+1]],0];
];
];
RMat
]


(* ::Input::Initialization:: *)
(* Compute the values of the DBaseXY at the nodes (vertices and midside) *)
CheckAtNodesHCT[DBaseXY_,xyHCTi_,Naux_]:= Module[{},
NormalsHCT=Table[{},r+1];
NormalsHCT[[1]]={{1,1,1}};
NormalsHCT[[2]]=Join[{ Naux[[;;,1]]},{Naux[[;;,2]]},1];
For[d=3,d<=r+1,d++,
NormalsHCT[[d]]=Join[NormalsHCT[[d-1]] . DiagonalMatrix[Naux[[;;,1]]],Table[0,1,3]]+Join[Table[0,1,3],NormalsHCT[[d-1]] . DiagonalMatrix[Naux[[;;,2]]],1];
];
Do[Print["Node ",node,", Order ",d,", Component ",c," ",Chop[N[Simplify[DBaseXY[[d+1,c,{NextIJK[[node]],PrevIJK[[node]]}]]/.{x->xyHCT[[1,node]],y->xyHCT[[2,node]]}/.SetCoordsHCT[xyHCTi] ]]]//MatrixForm],{node,3},{d,0,r+p},{c,1,d+1}];
Do[Do[Do[Print["Side ",side,", Order ",d,", Node ",n,"   ",
(* We need to adjust numbering of the sides... *)
Chop[N[Simplify[{NormalsHCT[[d+1,;;,PrevIJK[[side]]]] . DBaseXY[[d+1,;;,side,;;]]}/.InterpSideHCT[side,n]/.SetCoordsHCT[xyHCTi]]]//MatrixForm]],{n,Incid[[d+1]]}],{d,0,r}],{side,3}]
]
