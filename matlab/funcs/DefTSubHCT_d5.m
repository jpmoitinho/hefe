% The transformation of the basis on the mater element to each primitive
TSub = cat(3, ...
[(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),( ...
  1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),( ...
  1/243),(1/243),(1/243),(1/243),(1/243);0,(1/81),(2/81),(1/27),( ...
  4/81),(5/81),0,(1/81),(2/81),(1/27),(4/81),0,(1/81),(2/81),(1/27), ...
  0,(1/81),(2/81),0,(1/81),0;0,0,(1/27),(1/9),(2/9),(10/27),0,0,( ...
  1/27),(1/9),(2/9),0,0,(1/27),(1/9),0,0,(1/27),0,0,0;0,0,0,(1/9),( ...
  4/9),(10/9),0,0,0,(1/9),(4/9),0,0,0,(1/9),0,0,0,0,0,0;0,0,0,0,( ...
  1/3),(5/3),0,0,0,0,(1/3),0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0, ...
  0,0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,(1/81),(1/81),(1/81),(1/81),( ...
  1/81),(2/81),(2/81),(2/81),(2/81),(1/27),(1/27),(1/27),(4/81),( ...
  4/81),(5/81);0,0,0,0,0,0,0,(1/27),(2/27),(1/9),(4/27),0,(2/27),( ...
  4/27),(2/9),0,(1/9),(2/9),0,(4/27),0;0,0,0,0,0,0,0,0,(1/9),(1/3),( ...
  2/3),0,0,(2/9),(2/3),0,0,(1/3),0,0,0;0,0,0,0,0,0,0,0,0,(1/3),(4/3) ...
  ,0,0,0,(2/3),0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0, ...
  0;0,0,0,0,0,0,0,0,0,0,0,(1/27),(1/27),(1/27),(1/27),(1/9),(1/9),( ...
  1/9),(2/9),(2/9),(10/27);0,0,0,0,0,0,0,0,0,0,0,0,(1/9),(2/9),(1/3) ...
  ,0,(1/3),(2/3),0,(2/3),0;0,0,0,0,0,0,0,0,0,0,0,0,0,(1/3),1,0,0,1, ...
  0,0,0;0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0, ...
  0,0,0,0,0,0,(1/9),(1/9),(1/9),(4/9),(4/9),(10/9);0,0,0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,(1/3),(2/3),0,(4/3),0;0,0,0,0,0,0,0,0,0,0,0,0,0,0, ...
  0,0,0,1,0,0,0;0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,(1/3),(1/3),( ...
  5/3);0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0;0,0,0,0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,0,0,0,1], ...
[(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),( ...
  1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),( ...
  1/243),(1/243),(1/243),(1/243),(1/243);0,0,0,0,0,0,(1/81),(1/81),( ...
  1/81),(1/81),(1/81),(2/81),(2/81),(2/81),(2/81),(1/27),(1/27),( ...
  1/27),(4/81),(4/81),(5/81);0,0,0,0,0,0,0,0,0,0,0,(1/27),(1/27),( ...
  1/27),(1/27),(1/9),(1/9),(1/9),(2/9),(2/9),(10/27);0,0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,(1/9),(1/9),(1/9),(4/9),(4/9),(10/9);0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,0,0,0,0,(1/3),(1/3),(5/3);0,0,0,0,0,0,0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,1;(5/81),(4/81),(1/27),(2/81),(1/81),0,(4/81),( ...
  1/27),(2/81),(1/81),0,(1/27),(2/81),(1/81),0,(2/81),(1/81),0,( ...
  1/81),0,0;0,0,0,0,0,0,(4/27),(1/9),(2/27),(1/27),0,(2/9),(4/27),( ...
  2/27),0,(2/9),(1/9),0,(4/27),0,0;0,0,0,0,0,0,0,0,0,0,0,(1/3),(2/9) ...
  ,(1/9),0,(2/3),(1/3),0,(2/3),0,0;0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,( ...
  2/3),(1/3),0,(4/3),0,0;0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0;( ...
  10/27),(2/9),(1/9),(1/27),0,0,(2/9),(1/9),(1/27),0,0,(1/9),(1/27), ...
  0,0,(1/27),0,0,0,0,0;0,0,0,0,0,0,(2/3),(1/3),(1/9),0,0,(2/3),(2/9) ...
  ,0,0,(1/3),0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,0,1,(1/3),0,0,1,0,0,0,0, ...
  0;0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0;(10/9),(4/9),(1/9),0, ...
  0,0,(4/9),(1/9),0,0,0,(1/9),0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,(4/3),( ...
  1/3),0,0,0,(2/3),0,0,0,0,0,0,0,0,0;0,0,0,0,0,0,0,0,0,0,0,1,0,0,0, ...
  0,0,0,0,0,0;(5/3),(1/3),0,0,0,0,(1/3),0,0,0,0,0,0,0,0,0,0,0,0,0,0; ...
  0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0;1,0,0,0,0,0,0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,0], ...
[(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),( ...
  1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),(1/243),( ...
  1/243),(1/243),(1/243),(1/243),(1/243);(5/81),(4/81),(1/27),(2/81) ...
  ,(1/81),0,(4/81),(1/27),(2/81),(1/81),0,(1/27),(2/81),(1/81),0,( ...
  2/81),(1/81),0,(1/81),0,0;(10/27),(2/9),(1/9),(1/27),0,0,(2/9),( ...
  1/9),(1/27),0,0,(1/9),(1/27),0,0,(1/27),0,0,0,0,0;(10/9),(4/9),( ...
  1/9),0,0,0,(4/9),(1/9),0,0,0,(1/9),0,0,0,0,0,0,0,0,0;(5/3),(1/3), ...
  0,0,0,0,(1/3),0,0,0,0,0,0,0,0,0,0,0,0,0,0;1,0,0,0,0,0,0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,0;0,(1/81),(2/81),(1/27),(4/81),(5/81),0,(1/81),( ...
  2/81),(1/27),(4/81),0,(1/81),(2/81),(1/27),0,(1/81),(2/81),0,( ...
  1/81),0;0,(4/27),(2/9),(2/9),(4/27),0,0,(1/9),(4/27),(1/9),0,0,( ...
  2/27),(2/27),0,0,(1/27),0,0,0,0;0,(2/3),(2/3),(1/3),0,0,0,(1/3),( ...
  2/9),0,0,0,(1/9),0,0,0,0,0,0,0,0;0,(4/3),(2/3),0,0,0,0,(1/3),0,0, ...
  0,0,0,0,0,0,0,0,0,0,0;0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;0, ...
  0,(1/27),(1/9),(2/9),(10/27),0,0,(1/27),(1/9),(2/9),0,0,(1/27),( ...
  1/9),0,0,(1/27),0,0,0;0,0,(1/3),(2/3),(2/3),0,0,0,(2/9),(1/3),0,0, ...
  0,(1/9),0,0,0,0,0,0,0;0,0,1,1,0,0,0,0,(1/3),0,0,0,0,0,0,0,0,0,0,0, ...
  0;0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;0,0,0,(1/9),(4/9),( ...
  10/9),0,0,0,(1/9),(4/9),0,0,0,(1/9),0,0,0,0,0,0;0,0,0,(2/3),(4/3), ...
  0,0,0,0,(1/3),0,0,0,0,0,0,0,0,0,0,0;0,0,0,1,0,0,0,0,0,0,0,0,0,0,0, ...
  0,0,0,0,0,0;0,0,0,0,(1/3),(5/3),0,0,0,0,(1/3),0,0,0,0,0,0,0,0,0,0; ...
  0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;0,0,0,0,0,1,0,0,0,0,0,0, ...
  0,0,0,0,0,0,0,0,0]);