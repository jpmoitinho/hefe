function [ s ] = CalcStress0KT(xy, DS, s0)
Basis = DS.handleBasis(xy);
% s = cellfun(@(x) mtimes(Basis, x*s0), DS.T0, 'UniformOutput', false);
s = cellfun(@(x) mtimes(Basis, mtimes(x, s0)), DS.T0, 'UniformOutput', false);
s = permute(cat(3, s{1}, s{2}, s{3}), [3 2 1]);
end

