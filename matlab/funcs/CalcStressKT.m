function [ s ] = CalcDStressKT(xy, DS)
Basis = DS.handleBasis(xy);
s = cellfun(@(x) mtimes(Basis, x), DS.T, 'UniformOutput', false);
s= permute(cat(3, s{1}, s{2}, s{3}), [3 2 1]);
end

