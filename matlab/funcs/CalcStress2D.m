% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ s ] = CalcStress2D(xy, DS)
Basis = DS.handleBasis(xy);
s = cellfun(@(x) mtimes(Basis, x), DS.T, 'UniformOutput', false);
s= permute(cat(3, s{1}, s{2}, s{3}), [3 2 1]);
end

