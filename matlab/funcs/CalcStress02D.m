% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ s ] = CalcStress02D(xy, DS, s0)
Basis = DS.handleBasis(xy);
% s = cellfun(@(x) mtimes(Basis, x*s0), DS.T0, 'UniformOutput', false);
s = cellfun(@(x) mtimes(Basis, mtimes(x, s0)), DS.T0, 'UniformOutput', false);
s = permute(cat(3, s{1}, s{2}, s{3}), [3 2 1]);
end

