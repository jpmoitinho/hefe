function [ s ] = CalcStress03D(xy, DS, s0)
Basis = DS.handleBasis(xy);
s = cellfun(@(x) mtimes(Basis, mtimes(x, s0)), DS.T0, 'UniformOutput', false);
s = permute(cat(3, s{1}, s{2}, s{3}, s{4}, s{5}, s{6}), [3 2 1]);
end

