function [ dsx, dsy ] = CalcDStress0KT(xy, DS, s0)
Basis = DS.handleDBasis(xy);
dsx = cellfun(@(x) mtimes(Basis(:,:,1), x*s0), DS.T0, 'UniformOutput', false);
dsx = permute(cat(3, dsx{1}, dsx{2}, dsx{3}), [3 2 1]);
dsy = cellfun(@(x) mtimes(Basis(:,:,2), x*s0), DS.T0, 'UniformOutput', false);
dsy = permute(cat(3, dsy{1}, dsy{2}, dsy{3}), [3 2 1]);
end

