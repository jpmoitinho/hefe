function [ s ] = CalcStress3D(xy, DS)
Basis = DS.handleBasis(xy);
s = cellfun(@(x) mtimes(Basis, x), DS.T, 'UniformOutput', false);
s = permute(cat(3, s{1}, s{2}, s{3}, s{4}, s{5}, s{6}), [3 2 1]);
end

