function [B] = BasisHCT_d3(L2, L3)
L1 = 1 - L2 - L3;
B=[L1.^3,L1.^2.*L2,L1.*L2.^2,L2.^3,L1.^2.*L3,L1.*L2.*L3,L2.^2.*L3,L1.*L3.^2,L2.*L3.^2,L3.^3];
end
