o1=F21.^(-1);
o2=(-1).*F12.*o1;
o3=(-3/20).*F12.*o1;
o4=(1/60).*F12.*o1;
o5=(-1/30).*F12.*o1;
o6=(-1/20).*F12.*o1;
o7=(-1/10).*F12.*o1;
o8=(-1/15).*F12.*o1;
o9=(1/6).*F12.*o1;
o10=(-1/6).*F12.*o1;
o11=(-7).*F22.*o1;
o12=(1/2).*F22.*o1;
o13=F21.^2;
o14=o13.^(-1);
o15=4.*F21;
o16=(-11).*F22;
o17=o15+o16;
o18=(-9).*F21;
o19=4.*F22;
o20=(-5).*F22;
o21=11.*F22;
o22=3.*F21;
o23=(-4).*F22;
o24=o22+o23;
o25=(-2).*F21;
o26=F22+o25;
o27=(-1/12).*F12.*o1;
o28=7.*F22.*o1;
o29=(-7/2).*F22.*o1;
o30=F22.*o1;
o31=7.*F21;
o32=o16+o31;
o33=(-3).*F21;
o34=5.*F22;
o35=(-1).*F21;
o36=10.*F21;
o37=(-7).*F22;
o38=(-1).*F22;
o39=F21+o38;
o40=7.*F22;
o41=F22.^2;
o42=7.*o14.*o41;
o43=(-1).*F22.*o1;
o44=(1/6).*F22.*o1;
o45=F21.^3;
o46=o45.^(-1);
o47=9.*o13;
o48=3.*o13;
o49=14.*o41;
o50=5.*o13;
o51=(-8).*F21.*F22;
o52=2.*o41;
o53=(-2).*F22;
o54=F21+o53;
o55=(-5).*F21.*F22;
o56=6.*o13;
o57=6.*o41;
o58=o20+o22;
o59=(-3).*F21.*F22;
o60=o41+o48+o59;
o61=F12.*o1;
o62=F21+o37;
o63=(1/2).*o1.*o62;
o64=(7/2).*F22.*o1;
o65=(3/2).*F22.*o1;
o66=(-10).*F22;
o67=(1/6).*F12.*o14.*o26;
o68=(1/3).*F12.*o14.*o26;
o69=(-7).*o14.*o41;
o70=(1/3).*F22.*o1;
o71=11.*o41;
o72=(-5).*o13;
o73=(-10).*o41;
o74=2.*o13;
o75=(-2).*F21.*F22;
o76=5.*o41;
o77=F21+F22;
o78=(-7).*F21.*F22;
o79=3.*o41;
o80=o50+o78+o79;
o81=2.*F21;
o82=(-3).*F22;
o83=o39.^2;
o84=17.*o45;
o85=F22.^3;
o86=(-35).*o85;
o87=15.*o41;
o88=5.*o14.*o41;
o89=11.*F21;
o90=F21+o20;
o91=F21.^4;
o92=o91.^(-1);
o93=93.*F21.*o41;
o94=3.*o45;
o95=(-22).*F22.*o13;
o96=(-6).*o45;
o97=11.*F22.*o13;
o98=11.*o13;
o99=34.*o41;
o100=(-5).*F22.*o13;
o101=(-2).*o85;
o102=(-4).*F21.*F22;
o103=(-4).*o45;
o104=6.*F22.*o13;
o105=(-4).*F21.*o41;
o106=(-1).*F11.*o1;
o107=F11.*o1;
o108=F21+o40;
o109=(1/2).*o1.*o108;
o110=2.*F22.*o1;
o111=3.*F11.*F21;
o112=(-3).*F12.*F21;
o113=4.*F11.*F22;
o114=3.*F12.*F22;
o115=(1/2).*F11.*o1;
o116=(-1/2).*F11.*o1;
o117=2.*o14.*o41;
o118=23.*F21.*F22;
o119=o19+o22;
o120=13.*o41;
o121=4.*o13;
o122=2.*F22;
o123=F21+o122;
o124=3.*F11.*F21.*F22;
o125=(-1/3).*F12.*o46.*o60;
o126=F21+o19;
o127=F11.*F22.*o126;
o128=35.*o85;
o129=8.*o45;
o130=32.*o13;
o131=(5/12).*F22.*o1;
o132=(-13).*F22;
o133=(-9).*F22;
o134=(-5/12).*F22.*o1;
o135=(-15).*o45;
o136=6.*F21.*o41;
o137=3.*F12.*o45;
o138=15.*o45;
o139=(-42).*F22.*o13;
o140=36.*F21.*o41;
o141=(-11).*o85;
o142=o138+o139+o140+o141;
o143=(-6).*F21.*F22;
o144=(-4).*o85;
o145=11.*o91;
o146=F22.^4;
o147=28.*o146;
o148=7.*o45;
o149=(-16).*o85;
o150=7.*F21.*o41;
o151=13.*o13;
o152=24.*o41;
o153=4.*o41;
o154=(-12).*F22;
o155=(-8).*F22;
o156=F21.^5;
o157=o156.^(-1);
o158=(-90).*F22.*o45;
o159=115.*o146;
o160=(-15).*o91;
o161=5.*o85;
o162=15.*o91;
o163=30.*F21.*o41;
o164=(-10).*o85;
o165=9.*o45;
o166=o22+o40;
o167=o40+o81;
o168=F21+o82;
o169=F12.*o83;
o170=F21.*F22;
o171=3.*F22;
o172=(1/3).*F11.*o1;
o173=(-6).*F22;
o174=F21+o173;
o175=(-1/3).*F11.*o1;
o176=5.*o45;
o177=21.*F22.*o13;
o178=19.*o13;
o179=35.*o41;
o180=(-56).*F22.*o13;
o181=8.*o13;
o182=(-24).*F21.*F22;
o183=31.*o41;
o184=(5/4).*o14.*o41;
o185=5.*F21;
o186=o16+o185;
o187=F21+o23;
o188=F21+o34;
o189=3.*F21.*F22;
o190=(-43).*F21.*o41;
o191=4.*o45;
o192=(-7).*F22.*o13;
o193=4.*F21.*o41;
o194=(-1).*o13;
o195=9.*F22.*o13;
o196=(1/4).*F11.*F22.*o14;
o197=(-1/6).*F12.*o39.*o60.*o92;
o198=o196+o197;
o199=6.*F21.*F22;
o200=(-1/4).*F11.*F22.*o14;
o201=3.*o91;
o202=21.*o13.*o41;
o203=14.*o146;
o204=2.*o91;
o205=2.*o85;
o206=(-17).*F21.*F22;
o207=(-1).*F21.*F22;
o208=10.*o41;
o209=o132+o31;
o210=o20+o81;
o211=(-3).*o13;
o212=26.*o41;
o213=20.*F21.*o85;
o214=(-15).*F22.*o45;
o215=(-5).*F21.*o85;
o216=2.*o146;
o217=3.*F11.*o13.*o41;
o218=30.*o91;
o219=9.*F21.*F22;
o220=(-24).*o91;
o221=(-1/5).*F12.*o1;
o222=9.*o91;
o223=60.*o13.*o41;
o224=(-40).*F21.*o85;
o225=10.*o146;
o226=(1/5).*F12.*o1;
o227=3.*o156;
o228=F22.^5;
o229=(-14).*o228;
o230=20.*o41;
o231=(-12).*F21.*F22;
o232=12.*o41;
o233=F21.^6;
o234=o233.^(-1);
o235=o170+o194+o76;
o236=(-5).*o41;
o237=(-2).*o228;
o238=30.*o41;
o239=(-16).*o13.*o85;
o240=10.*F21.*o146;
o241=(-1).*o228;
o242=F11.*o13.*o85;
o243=4.*F21.*F22;
o244=(-20).*F21.*o41;
o245=70.*F21.*o146;
o246=(1/10).*F12.*o1;
o247=o13+o207+o49;
o248=F21+o155;
o249=7.*F21.*F22;
o250=o122+o22;
o251=6.*F22;
o252=F21+o251;
o253=2.*F21.*o41;
o254=32.*F22.*o13;
o255=17.*o41;
o256=o255+o55+o74;
o257=o133+o81;
o258=o39.^3;
o259=(-5).*F21.*o41;
o260=(-1).*F12.*o258;
o261=12.*F22.*o13;
o262=5.*F21.*o41;
o263=(-15).*o41;
o264=(1/12).*F11.*o14.*o90;
o265=(-1/12).*F11.*o14.*o90;
o266=4.*o91;
o267=F22.*o45;
o268=18.*o13.*o41;
o269=4.*o85;
o270=(-1/5).*F22.*o1;
o271=(-31).*F21.*F22;
o272=62.*o41;
o273=o271+o272+o98;
o274=19.*o41;
o275=o22+o66;
o276=25.*F22.*o13;
o277=124.*o85;
o278=(-19).*o41;
o279=6.*F11.*F22.*o13.*o168;
o280=18.*F22.*o45;
o281=(-20).*o13.*o41;
o282=10.*F21.*o85;
o283=(-2).*o146;
o284=20.*F22.*o13;
o285=3.*F12.*o13;
o286=(-2).*F21.*o85;
o287=13.*F22.*o13;
o288=25.*o41;
o289=o206+o288+o56;
o290=8.*o41;
o291=o34+o81;
o292=8.*o156;
o293=2.*o156;
o294=9.*o156;
o295=o22+o37;
o296=(-57).*o41.*o45;
o297=55.*o13.*o85;
o298=(-26).*F21.*o146;
o299=5.*o228;
o300=F12.*o91;
o301=o33+o40;
o302=F12.*o45;
o303=F22.^6;
o304=4.*o303;
o305=12.*o13.*o85;
o306=(-25).*F21.*F22;
o307=o238+o306+o47;
o308=o170+o48+o79;
o309=o154+o185;
o310=F21.^7;
o311=o310.^(-1);
o312=30.*o85;
o313=4.*F11.*o13.*o54.*o85;
aux = [1,1,1; ... 
1,29,o2; ... 
1,30,(3/20).*F12.*o1; ... 
1,31,o3; ... 
1,33,o4; ... 
1,34,o5; ... 
1,37,o4; ... 
1,38,o6; ... 
2,1,(-1); ... 
2,2,1; ... 
2,29,(7/5).*F12.*o1; ... 
2,30,(-13/10).*F12.*o1; ... 
2,31,o7; ... 
2,32,(11/30).*F12.*o1; ... 
2,33,o8; ... 
2,34,o9; ... 
2,36,(1/30).*F12.*o1; ... 
2,37,o10; ... 
2,41,(1/15).*F12.*o1; ... 
3,1,(1/2).*(5+o11); ... 
3,2,o12; ... 
3,3,(1/2); ... 
3,29,(1/10).*F12.*((-25).*F21+37.*F22).*o14; ... 
3,30,(1/10).*F12.*o14.*o17; ... 
3,31,(1/10).*F12.*o14.*(o18+o19); ... 
3,32,(11/60).*F12.*F22.*o14; ... 
3,33,(1/60).*F12.*o14.*(6.*F21+o20); ... 
3,34,(1/60).*F12.*o14.*((-12).*F21+o21); ... 
3,36,(1/60).*F12.*F22.*o14; ... 
3,37,(1/30).*F12.*o14.*o24; ... 
3,38,(3/20).*F12.*o14.*o26; ... 
3,41,(1/30).*F12.*F22.*o14; ... 
4,1,1; ... 
4,2,(-1); ... 
4,4,1; ... 
4,29,(-2).*F12.*o1; ... 
4,30,(7/4).*F12.*o1; ... 
4,31,(1/4).*F12.*o1; ... 
4,32,(-5/3).*F12.*o1; ... 
4,33,o27; ... 
4,34,o10; ... 
4,35,(3/4).*F12.*o1; ... 
4,36,o27; ... 
4,37,o9; ... 
4,41,o10; ... 
5,1,(1/2).*((-3)+o28); ... 
5,2,2+o29; ... 
5,3,(-1/2); ... 
5,4,o30; ... 
5,5,(1/2); ... 
5,29,(1/2).*F12.*o14.*o32; ... 
5,30,F12.*o14.*(o33+o34); ... 
5,31,(1/2).*F12.*o14.*(F22+o35); ... 
5,32,(1/12).*F12.*o14.*((-31).*F22+o36); ... 
5,33,(1/12).*F12.*((-4).*F21+F22).*o14; ... 
5,34,(1/12).*F12.*o14.*(o36+o37); ... 
5,35,(3/4).*F12.*F22.*o14; ... 
5,36,(1/6).*F12.*o14.*o39; ... 
5,37,(1/12).*F12.*o14.*((-10).*F21+o40); ... 
5,41,(1/3).*F12.*o14.*o39; ... 
6,1,(9/2)+(-21/2).*F22.*o1+o42; ... 
6,2,(1/2).*F22.*o14.*o24; ... 
6,3,(1/2)+o43; ... 
6,4,(1/3).*o14.*o41; ... 
6,5,o44; ... 
6,6,(1/3); ... 
6,29,(-1/2).*F12.*o46.*((-23).*F21.*F22+16.*o41+o47); ... 
6,30,(1/4).*F12.*o46.*((-14).*F21.*F22+o48+o49); ... 
6,31,(-1/4).*F12.*o46.*(o50+o51+o52); ... 
6,32,(7/12).*F12.*F22.*o46.*o54; ... 
6,33,(1/12).*F12.*o46.*(o13+o52+o55); ... 
6,34,(-1/12).*F12.*o46.*((-11).*F21.*F22+o56+o57); ... 
6,35,(1/4).*F12.*o41.*o46; ... 
6,36,(1/12).*F12.*F22.*o39.*o46; ... 
6,37,(1/12).*F12.*o39.*o46.*o58; ... 
6,38,(-1/4).*F12.*o46.*o60; ... 
6,41,(1/6).*F12.*F22.*o39.*o46; ... 
7,1,(-1); ... 
7,2,1; ... 
7,4,(-1); ... 
7,7,1; ... 
7,29,o61; ... 
7,30,o2; ... 
7,32,o61; ... 
7,35,o2; ... 
8,1,o63; ... 
8,2,(-1)+o64; ... 
8,3,(1/2); ... 
8,4,(1/2).*(3+o11); ... 
8,5,(-1/2); ... 
8,7,o65; ... 
8,8,(1/2); ... 
8,29,(1/2).*F12.*o14.*(o18+o21); ... 
8,30,(1/2).*F12.*o14.*(o31+o66); ... 
8,31,(-1/2).*F12.*o14.*o26; ... 
8,32,(1/6).*F12.*((-19).*F21+29.*F22).*o14; ... 
8,33,o67; ... 
8,34,o68; ... 
8,35,(3/2).*F12.*o14.*o54; ... 
8,36,o67; ... 
8,37,(-1/3).*F12.*o14.*o26; ... 
8,41,o68; ... 
9,1,(-3)+o28+o69; ... 
9,2,3+(-8).*F22.*o1+o42; ... 
9,3,o30; ... 
9,4,(1/3).*F22.*o14.*o32; ... 
9,5,(1/3)+o43; ... 
9,6,(-1/3); ... 
9,7,o14.*o41; ... 
9,8,o70; ... 
9,9,(1/3); ... 
9,29,F12.*o46.*((-15).*F21.*F22+7.*o13+o71); ... 
9,30,F12.*o46.*(13.*F21.*F22+o72+o73); ... 
9,31,(-1).*F12.*o46.*(o41+o74+o75); ... 
9,32,(4/3).*F12.*o46.*(o13+o55+o76); ... 
9,33,(1/3).*F12.*o14.*o77; ... 
9,34,(1/3).*F12.*o46.*o80; ... 
9,35,F12.*F22.*o46.*(o81+o82); ... 
9,36,(1/3).*F12.*F22.*o26.*o46; ... 
9,37,(-1/3).*F12.*o46.*o80; ... 
9,41,(2/3).*F12.*o46.*o83; ... 
10,1,(1/4).*o46.*((-63).*F22.*o13+77.*F21.*o41+o84+o86); ... 
10,2,(1/4).*F22.*o46.*((-22).*F21.*F22+o47+o87); ... 
10,3,(1/4).*(3+(-6).*F22.*o1+o88); ... 
10,4,(1/12).*o41.*o46.*((-15).*F22+o89); ... 
10,5,(1/12).*F22.*o14.*o58; ... 
10,6,(1/12).*o1.*o90; ... 
10,7,(1/4).*o46.*o85; ... 
10,8,(1/12).*o14.*o41; ... 
10,9,(1/12).*F22.*o1; ... 
10,10,(1/4); ... 
10,29,(-1/4).*F12.*o92.*((-71).*F22.*o13+o84+(-43).*o85+o93); ... 
10,30,(1/4).*F12.*o92.*(41.*F21.*o41+(-24).*o85+o94+o95); ... 
10,31,(1/4).*F12.*o92.*((-8).*F21.*o41+o85+o96+o97); ... 
10,32,(1/12).*F12.*F22.*o92.*((-41).*F21.*F22+o98+o99); ... 
10,33,(1/12).*F12.*(o100+o101+8.*F21.*o41+o45).*o92; ... 
10,34,(-1/12).*F12.*o92.*(24.*F21.*o41+(-9).*o85+o94+o95); ... 
10,35,(1/4).*F12.*o24.*o41.*o92; ... 
10,36,(1/12).*F12.*F22.*(o102+o13+o52).*o92; ... 
10,37,(1/12).*F12.*o92.*((-16).*F22.*o13+20.*F21.*o41+(-8).*o85+o94); ... 
10,38,(1/4).*F12.*(o103+o104+o105+o85).*o92; ... 
10,41,(1/3).*F12.*F22.*o83.*o92; ... 
11,1,1; ... 
11,2,(-1); ... 
11,4,1; ... 
11,7,(-1); ... 
11,11,1; ... 
11,29,o106; ... 
11,30,o107; ... 
11,32,o106; ... 
11,35,o107; ... 
11,39,o106; ... 
12,1,o109; ... 
12,2,o29; ... 
12,3,(-1/2); ... 
12,4,(1/2).*((-1)+o28); ... 
12,5,(1/2); ... 
12,7,1+o29; ... 
12,8,(-1/2); ... 
12,11,o110; ... 
12,12,(1/2); ... 
12,29,(1/2).*o14.*(3.*F12.*o39+(-4).*F11.*o77); ... 
12,30,(1/2).*(o111+o112+o113+o114).*o14; ... 
12,31,o115; ... 
12,32,(-1/2).*(2.*F11.*F21+o112+o113+o114).*o14; ... 
12,33,o116; ... 
12,35,(1/2).*(F11.*F21+o112+o113+o114).*o14; ... 
12,36,o115; ... 
12,39,(-2).*F11.*F22.*o14; ... 
12,40,o116; ... 
13,1,(5/2)+o29+o42; ... 
13,2,(-2)+(9/2).*F22.*o1+o69; ... 
13,3,(-1/2)+o43; ... 
13,4,(11/6)+(-11/2).*F22.*o1+o42; ... 
13,5,(1/6)+o30; ... 
13,6,(1/3); ... 
13,7,(5/2).*F22.*o14.*o54; ... 
13,8,(1/6)+o43; ... 
13,9,(-1/3); ... 
13,11,o117; ... 
13,12,o12; ... 
13,13,(1/3); ... 
13,29,(1/2).*o46.*(F12.*(o118+(-13).*o13+(-14).*o41)+(-4).*F11.*F22.*o77); ... 
13,30,(1/2).*o46.*(F11.*F22.*o119+F12.*((-20).*F21.*F22+o120+o47)); ... 
13,31,(1/2).*o46.*(F11.*F21.*F22+F12.*(o121+o41+o59)); ... 
13,32,(1/6).*((-6).*F11.*F22.*o123+F12.*(57.*F21.*F22+(-21).*o13+(-38).*o41)).*o46; ... 
13,33,(-1/6).*o46.*(o124+F12.*(o41+o56+o59)); ... 
13,34,o125; ... 
13,35,(1/2).*o46.*(o127+3.*F12.*(o102+o13+o79)); ... 
13,36,(1/6).*F22.*(3.*F12.*F21+(-1).*F12.*F22+o111).*o46; ... 
13,37,(1/3).*F12.*o46.*o60; ... 
13,39,(-2).*F11.*o41.*o46; ... 
13,40,(-1/2).*F11.*F22.*o14; ... 
13,41,o125; ... 
14,1,(1/4).*(o128+35.*F22.*o13+(-49).*F21.*o41+(-5).*o45).*o46; ... 
14,2,(1/4).*o46.*(o129+(-37).*F22.*o13+54.*F21.*o41+o86); ... 
14,3,(1/4).*((-3)+o110+(-5).*o14.*o41); ... 
14,4,(1/12).*F22.*((-87).*F21.*F22+o130+75.*o41).*o46; ... 
14,5,(1/4).*(2+(-11/3).*F22.*o1+o88); ... 
14,6,(1/4)+o131; ... 
14,7,(1/4).*(8.*F21+o132).*o41.*o46; ... 
14,8,(1/12).*F22.*o14.*(o133+o15); ... 
14,9,o134; ... 
14,10,(-1/4); ... 
14,11,o46.*o85; ... 
14,12,(1/4).*o14.*o41; ... 
14,13,o44; ... 
14,14,(1/4); ... 
14,29,(1/4).*((-4).*F11.*o41.*o77+F12.*((-79).*F22.*o13+101.*F21.*o41+21.*o45+(-47).*o85)).*o92; ... 
14,30,(1/4).*(F11.*o119.*o41+F12.*(63.*F22.*o13+o135+(-89).*F21.*o41+43.*o85)).*o92; ... 
14,31,(1/4).*o92.*(F11.*F21.*o41+(-2).*F12.*(o101+(-8).*F22.*o13+o136+o94)); ... 
14,32,(1/12).*((-6).*F11.*o123.*o41+F12.*((-93).*F22.*o13+177.*F21.*o41+12.*o45+(-98).*o85)).*o92; ... 
14,33,(1/12).*((-6).*F12.*F22.*o13+o137+(-3).*F11.*F21.*o41+(-1).*F12.*o85).*o92; ... 
14,34,(1/12).*F12.*o142.*o92; ... 
14,35,(1/4).*F22.*(o127+9.*F12.*(o13+o52+o59)).*o92; ... 
14,36,(1/12).*F22.*(o124+(-2).*F12.*(o143+o48+o52)).*o92; ... 
14,37,(-1/12).*F12.*o142.*o92; ... 
14,39,(-1).*F11.*o85.*o92; ... 
14,40,(-1/4).*F11.*o41.*o46; ... 
14,41,(1/6).*F12.*o92.*((-12).*F22.*o13+o144+12.*F21.*o41+o94); ... 
15,1,(1/4).*(o145+o147+91.*o13.*o41+(-49).*F22.*o45+(-77).*F21.*o85).*o92; ... 
15,2,(1/4).*F22.*((-26).*F22.*o13+o148+o149+33.*F21.*o41).*o92; ... 
15,3,(1/4).*((-6).*F22.*o13+o144+o150+o45).*o46; ... 
15,4,(1/12).*((-33).*F21.*F22+o151+o152).*o41.*o92; ... 
15,5,(1/12).*F22.*o46.*(o48+o57+o78); ... 
15,6,(1/12).*o14.*(o153+o48+o59); ... 
15,7,(1/20).*o85.*((-16).*F22+o89).*o92; ... 
15,8,(1/60).*(o154+o31).*o41.*o46; ... 
15,9,(1/60).*F22.*o14.*(o155+o22); ... 
15,10,(-1/20).*o1.*o126; ... 
15,11,(1/5).*o146.*o92; ... 
15,12,(1/20).*o46.*o85; ... 
15,13,(1/30).*o14.*o41; ... 
15,14,(1/20).*F22.*o1; ... 
15,15,(1/5); ... 
15,29,(1/20).*o157.*((-4).*F11.*o77.*o85+F12.*((-176).*o146+(-571).*o13.*o41+285.*F22.*o45+509.*F21.*o85+(-55).*o91)); ... 
15,30,(1/20).*o157.*(F11.*o119.*o85+F12.*(o158+o159+252.*o13.*o41+(-284).*F21.*o85+10.*o91)); ... 
15,31,(1/20).*o157.*(F11.*F21.*o85+F12.*(o146+o160+(-41).*o13.*o41+45.*F22.*o45+15.*F21.*o85)); ... 
15,32,(-1/60).*F22.*o157.*(6.*F11.*o123.*o41+F12.*(249.*F22.*o13+(-402).*F21.*o41+(-45).*o45+200.*o85)); ... 
15,33,(1/60).*o157.*(F12.*F22.*(33.*F22.*o13+o135+o161+(-30).*F21.*o41)+(-3).*F11.*F21.*o85); ... 
15,34,(-1/60).*F12.*o157.*(38.*o146+o158+o162+180.*o13.*o41+(-135).*F21.*o85); ... 
15,35,(1/20).*o157.*o41.*(o127+6.*F12.*(o48+o51+o76)); ... 
15,36,(1/60).*o157.*(3.*F11.*F21.*o85+F12.*F22.*((-21).*F22.*o13+o163+o164+o94)); ... 
15,37,(1/60).*F12.*o157.*(35.*o146+150.*o13.*o41+(-72).*F22.*o45+(-120).*F21.*o85+12.*o91); ... 
15,38,(-1/20).*F12.*o157.*(o145+3.*o146+30.*o13.*o41+(-30).*F22.*o45+(-15).*F21.*o85); ... 
15,39,(-1/5).*F11.*o146.*o157; ... 
15,40,(-1/20).*F11.*o85.*o92; ... 
15,41,(1/30).*F12.*F22.*o157.*((-30).*F22.*o13+o163+o164+o165); ... 
15,42,o6; ... 
16,1,(-1); ... 
16,2,1; ... 
16,4,(-1); ... 
16,7,1; ... 
16,11,(-1); ... 
16,16,1; ... 
16,29,o107; ... 
16,30,o106; ... 
16,32,o107; ... 
16,35,o106; ... 
16,39,o107; ... 
16,44,o106; ... 
17,1,(1/2).*((-3)+o11); ... 
17,2,1+o64; ... 
17,3,(1/2); ... 
17,4,(-1/2).*o1.*o108; ... 
17,5,(-1/2); ... 
17,7,o64; ... 
17,8,(1/2); ... 
17,11,o63; ... 
17,12,(-1/2); ... 
17,16,(5/2).*F22.*o1; ... 
17,17,(1/2); ... 
17,29,(1/2).*F11.*o14.*o166; ... 
17,30,(-1/2).*F11.*o14.*o167; ... 
17,31,o116; ... 
17,32,(1/2).*F11.*o108.*o14; ... 
17,33,o115; ... 
17,35,(-7/2).*F11.*F22.*o14; ... 
17,36,o116; ... 
17,39,(-1/2).*F11.*o14.*o62; ... 
17,40,o115; ... 
17,44,(-5/2).*F11.*F22.*o14; ... 
17,45,o116; ... 
18,1,(-3)+o69; ... 
18,2,2+o42+o43; ... 
18,3,o1.*o77; ... 
18,4,(-4/3)+o110+o69; ... 
18,5,(-2/3)+o43; ... 
18,6,(-1/3); ... 
18,7,o14.*(o13+7.*o41+o59); ... 
18,8,(1/3)+o30; ... 
18,9,(1/3); ... 
18,11,2.*F22.*o14.*o168; ... 
18,12,o43; ... 
18,13,(-1/3); ... 
18,16,(10/3).*o14.*o41; ... 
18,17,(2/3).*F22.*o1; ... 
18,18,(1/3); ... 
18,29,o46.*(o169+2.*F11.*(o13+o170+o79)); ... 
18,30,(-1).*o46.*(o169+F11.*(o13+o170+o57)); ... 
18,31,(-1).*F11.*o14.*o77; ... 
18,32,(1/3).*o46.*(F11.*(o13+18.*o41)+3.*F12.*o83); ... 
18,33,(1/3).*F11.*o14.*(o171+o81); ... 
18,34,o172; ... 
18,35,o46.*(F11.*F22.*o174+(-1).*F12.*o83); ... 
18,36,(-1/3).*F11.*o14.*(F21+o171); ... 
18,37,o175; ... 
18,39,(-2).*F11.*F22.*o168.*o46; ... 
18,40,F11.*F22.*o14; ... 
18,41,o172; ... 
18,44,(-10/3).*F11.*o41.*o46; ... 
18,45,(-2/3).*F11.*F22.*o14; ... 
18,46,o175; ... 
19,1,(-1/4).*(o128+o176+o177+(-21).*F21.*o41).*o46; ... 
19,2,(1/4).*F22.*((-26).*F21.*F22+o178+o179).*o46; ... 
19,3,(1/4).*(5+o110+o88); ... 
19,4,(1/12).*o46.*(o129+o180+(-105).*o85+o93); ... 
19,5,(-1/12).*o14.*(o170+o181+o87); ... 
19,6,(1/12).*((-7)+(-5).*F22.*o1); ... 
19,7,(1/4).*F22.*(o181+o182+o183).*o46; ... 
19,8,(1/3)+(-1/3).*F22.*o1+o184; ... 
19,9,(1/3)+o131; ... 
19,10,(1/4); ... 
19,11,(1/2).*o186.*o41.*o46; ... 
19,12,(1/4).*F22.*o14.*o187; ... 
19,13,(-1/12).*o1.*o188; ... 
19,14,(-1/4); ... 
19,16,(5/2).*o46.*o85; ... 
19,17,(1/2).*o14.*o41; ... 
19,18,(1/4).*F22.*o1; ... 
19,19,(1/4); ... 
19,29,(1/4).*(2.*F11.*F22.*(o13+o189+o71)+F12.*(39.*F22.*o13+o190+(-13).*o45+17.*o85)).*o92; ... 
19,30,(1/4).*((-2).*F11.*(F21+o21).*o41+F12.*((-32).*F22.*o13+o149+o165+39.*F21.*o41)).*o92; ... 
19,31,(1/4).*((-2).*F11.*F21.*F22.*o123+F12.*(o191+o192+o193+(-1).*o85)).*o92; ... 
19,32,(1/12).*(3.*F11.*F22.*(o194+22.*o41+o75)+F12.*(87.*F22.*o13+(-113).*F21.*o41+(-21).*o45+47.*o85)).*o92; ... 
19,33,(1/12).*o92.*(3.*F11.*F21.*F22.*o126+F12.*(o105+o195+o85+o96)); ... 
19,34,o198; ... 
19,35,(1/4).*(F11.*F22.*(o13+o199+(-22).*o41)+3.*F12.*o187.*o83).*o92; ... 
19,36,(1/12).*F22.*((-12).*F11.*F21.*F22+F12.*(o102+o41+o48)).*o92; ... 
19,37,o200+(1/6).*F12.*o39.*o60.*o92; ... 
19,39,(-1/2).*F11.*o186.*o41.*o92; ... 
19,40,(-1/4).*F11.*F22.*o187.*o46; ... 
19,41,o198; ... 
19,44,(-5/2).*F11.*o85.*o92; ... 
19,45,(-1/2).*F11.*o41.*o46; ... 
19,46,o200; ... 
20,1,(-1/2).*(o201+o202+o203+(-7).*F22.*o45+(-21).*F21.*o85).*o92; ... 
20,2,(1/2).*(o203+o204+22.*o13.*o41+(-9).*F22.*o45+(-23).*F21.*o85).*o92; ... 
20,3,(1/2).*(2.*F22.*o13+o205+(-1).*F21.*o41+o45).*o46; ... 
20,4,(1/6).*F22.*(o129+(-35).*F22.*o13+51.*F21.*o41+(-36).*o85).*o92; ... 
20,5,(-1/6).*F22.*o46.*(o50+o55+o57); ... 
20,6,(-1/6).*o14.*(o170+o48+o52); ... 
20,7,(1/10).*o41.*((-45).*F21.*F22+16.*o13+44.*o41).*o92; ... 
20,8,(1/30).*F22.*(o152+o181+o206).*o46; ... 
20,9,(1/30).*o14.*(o207+o208+o56); ... 
20,10,(1/5).*((3/2)+o30); ... 
20,11,(1/5).*o209.*o85.*o92; ... 
20,12,(1/10).*o210.*o41.*o46; ... 
20,13,(1/30).*F22.*o14.*o62; ... 
20,14,(-1/10).*o1.*o123; ... 
20,15,(-1/5); ... 
20,16,o146.*o92; ... 
20,17,(1/5).*o46.*o85; ... 
20,18,(1/10).*o14.*o41; ... 
20,19,(1/10).*F22.*o1; ... 
20,20,(1/5); ... 
20,29,(1/10).*o157.*(2.*F11.*(o120+o189+o194).*o41+F12.*(64.*o146+219.*o13.*o41+(-113).*F22.*o45+(-191).*F21.*o85+35.*o91)); ... 
20,30,(1/10).*o157.*((-1).*F11.*(o170+o211+o212).*o41+F12.*((-59).*o146+(-184).*o13.*o41+87.*F22.*o45+171.*F21.*o85+(-20).*o91)); ... 
20,31,(1/10).*o157.*((-1).*F11.*F21.*o188.*o41+F12.*((-5).*o146+o160+o213+(-35).*o13.*o41+26.*F22.*o45)); ... 
20,32,(1/30).*o157.*(3.*F11.*(o102+o211+o212).*o41+F12.*(145.*o146+o162+357.*o13.*o41+(-126).*F22.*o45+(-388).*F21.*o85)); ... 
20,33,(1/30).*o157.*(F12.*(o162+o214+o215+o216+15.*o13.*o41)+15.*F11.*F21.*o85); ... 
20,34,(1/30).*o157.*(o217+F12.*(13.*o146+o218+90.*o13.*o41+(-63).*F22.*o45+(-55).*F21.*o85)); ... 
20,35,(1/10).*F22.*o157.*(F11.*F22.*(o219+(-26).*o41+o74)+6.*F12.*o210.*o83); ... 
20,36,(1/30).*o157.*(3.*F11.*F21.*o41.*o90+F12.*(5.*o146+o202+(-6).*F22.*o45+(-20).*F21.*o85+(-3).*o91)); ... 
20,37,(1/30).*o157.*((-3).*F11.*o13.*o41+F12.*((-13).*o146+o220+(-90).*o13.*o41+63.*F22.*o45+55.*F21.*o85)); ... 
20,38,o221; ... 
20,39,(-1/5).*F11.*o157.*o209.*o85; ... 
20,40,(-1/10).*F11.*o210.*o41.*o92; ... 
20,41,(1/30).*o157.*(o217+F12.*(o222+o223+o224+o225+(-36).*F22.*o45)); ... 
20,42,o226; ... 
20,44,(-1).*F11.*o146.*o157; ... 
20,45,(-1/5).*F11.*o85.*o92; ... 
20,46,(-1/10).*F11.*o41.*o46; ... 
20,47,o7; ... 
21,1,(1/4).*o157.*(42.*F21.*o146+o227+o229+49.*o41.*o45+(-63).*o13.*o85+(-21).*F22.*o91); ... 
21,2,(1/4).*F22.*o157.*(o201+o225+27.*o13.*o41+(-14).*F22.*o45+(-24).*F21.*o85); ... 
21,3,(1/4).*(o216+5.*o13.*o41+(-2).*F22.*o45+(-4).*F21.*o85+o91).*o92; ... 
21,4,(1/12).*o157.*o41.*((-27).*F22.*o13+o140+o148+(-20).*o85); ... 
21,5,(1/12).*F22.*(o100+o136+o144+o45).*o92; ... 
21,6,(-1/12).*(3.*F22.*o13+o205+(-2).*F21.*o41+o45).*o46; ... 
21,7,(1/20).*o157.*(o182+o230+o47).*o85; ... 
21,8,(1/60).*o41.*(o231+o232+o50).*o92; ... 
21,9,(1/60).*F22.*o46.*(o102+o48+o57); ... 
21,10,(1/20).*(3+o117); ... 
21,11,(1/10).*o146.*o157.*o58; ... 
21,12,(1/20).*o54.*o85.*o92; ... 
21,13,(1/60).*o168.*o41.*o46; ... 
21,14,(-1/20).*o14.*o41; ... 
21,15,(-1/10).*o1.*o77; ... 
21,16,(1/6).*o157.*o228; ... 
21,17,(1/30).*o146.*o92; ... 
21,18,(1/60).*o46.*o85; ... 
21,19,(1/60).*o14.*o41; ... 
21,20,(1/30).*F22.*o1; ... 
21,21,(1/6); ... 
21,29,(1/20).*o234.*(2.*F11.*o235.*o85+F12.*((-292).*F21.*o146+(-15).*o156+80.*o228+(-323).*o41.*o45+433.*o13.*o85+125.*F22.*o91)); ... 
21,30,(1/20).*o234.*(2.*F11.*(o13+o236).*o85+F12.*(190.*F21.*o146+o227+(-58).*o228+143.*o41.*o45+(-240).*o13.*o85+(-40).*F22.*o91)); ... 
21,31,(1/20).*o234.*((-2).*F11.*F21.*o146+F12.*(2.*F21.*o146+(-8).*o156+o237+(-20).*o41.*o45+7.*o13.*o85+15.*F22.*o91)); ... 
21,32,(1/60).*o234.*(F11.*(o143+o238+o72).*o85+F12.*F22.*(o159+336.*o13.*o41+(-140).*F22.*o45+(-330).*F21.*o85+20.*o91)); ... 
21,33,(1/60).*o234.*(F12.*(o156+o239+o240+o241+11.*o41.*o45)+(-1).*F11.*F21.*o174.*o85); ... 
21,34,(1/60).*o234.*(o242+F12.*((-76).*F21.*o146+o227+17.*o228+(-111).*o41.*o45+135.*o13.*o85+45.*F22.*o91)); ... 
21,35,(1/20).*o234.*o41.*(F11.*F22.*(o13+o243+o73)+10.*F12.*o54.*o83); ... 
21,36,(1/60).*o234.*o41.*(2.*F11.*F21.*F22.*o168+F12.*(24.*F22.*o13+o161+o244+(-10).*o45)); ... 
21,37,(1/60).*o234.*((-1).*F11.*o13.*o85+F12.*(o227+(-16).*o228+o245+95.*o41.*o45+(-120).*o13.*o85+(-36).*F22.*o91)); ... 
21,38,(-1/20).*F12.*o234.*(6.*F21.*o146+6.*o156+o241+20.*o41.*o45+(-15).*o13.*o85+(-13).*F22.*o91); ... 
21,39,(-1/10).*F11.*o146.*o234.*o58; ... 
21,40,(-1/20).*F11.*o157.*o54.*o85; ... 
21,41,(1/60).*o234.*(o242+F12.*F22.*(o222+o223+o224+o225+(-38).*F22.*o45)); ... 
21,42,(1/20).*F12.*F22.*o14; ... 
21,43,o246; ... 
21,44,(-1/6).*F11.*o228.*o234; ... 
21,45,(-1/30).*F11.*o146.*o157; ... 
21,46,(-1/60).*F11.*o85.*o92; ... 
21,47,(-1/60).*F12.*F22.*o14; ... 
21,48,o5; ... 
22,1,1; ... 
22,2,(-1); ... 
22,4,1; ... 
22,7,(-1); ... 
22,11,1; ... 
22,16,(-1); ... 
22,22,1; ... 
22,29,o106; ... 
22,30,o107; ... 
22,32,o106; ... 
22,35,o107; ... 
22,39,o106; ... 
22,44,o107; ... 
22,50,o106; ... 
23,1,(1/2).*(5+o28); ... 
23,2,(-2)+o29; ... 
23,3,(-1/2); ... 
23,4,(1/2).*(3+o28); ... 
23,5,(1/2); ... 
23,7,(-1)+o29; ... 
23,8,(-1/2); ... 
23,11,o109; ... 
23,12,(1/2); ... 
23,16,o29; ... 
23,17,(-1/2); ... 
23,22,3.*F22.*o1; ... 
23,23,(1/2); ... 
23,29,(-1/2).*F11.*o14.*(o185+o40); ... 
23,30,(1/2).*F11.*o14.*(o15+o40); ... 
23,31,o115; ... 
23,32,(-1/2).*F11.*o14.*o166; ... 
23,33,o116; ... 
23,35,(1/2).*F11.*o14.*o167; ... 
23,36,o115; ... 
23,39,(-1/2).*F11.*o108.*o14; ... 
23,40,o116; ... 
23,44,(7/2).*F11.*F22.*o14; ... 
23,45,o115; ... 
23,50,(-3).*F11.*F22.*o14; ... 
23,51,o116; ... 
24,1,(9/2)+o42+o64; ... 
24,2,(-3)+(-5/2).*F22.*o1+o69; ... 
24,3,(-3/2)+o43; ... 
24,4,(11/6)+o42+o65; ... 
24,5,(7/6)+o30; ... 
24,6,(1/3); ... 
24,7,(-1)+(-1/2).*F22.*o1+o69; ... 
24,8,(-5/6)+o43; ... 
24,9,(-1/3); ... 
24,11,(1/2).*o14.*o247; ... 
24,12,(1/2)+o30; ... 
24,13,(1/3); ... 
24,16,(5/6).*F22.*o14.*o248; ... 
24,17,(-1/6)+o43; ... 
24,18,(-1/3); ... 
24,22,o88; ... 
24,23,(5/6).*F22.*o1; ... 
24,24,(1/3); ... 
24,29,(-1/2).*F11.*o46.*(o249+o47+o49); ... 
24,30,(1/2).*F11.*o46.*(5.*F21.*F22+o49+o56); ... 
24,31,(1/2).*F11.*o14.*o250; ... 
24,32,(-1/6).*F11.*o46.*(o219+42.*o41+o98); ... 
24,33,(-1/6).*F11.*o14.*(o251+o31); ... 
24,34,o175; ... 
24,35,(1/2).*F11.*o46.*(o170+o49+o74); ... 
24,36,(1/6).*F11.*o14.*(o185+o251); ... 
24,37,o172; ... 
24,39,(-1/2).*F11.*o247.*o46; ... 
24,40,(-1/2).*F11.*o123.*o14; ... 
24,41,o175; ... 
24,44,(-5/6).*F11.*F22.*o248.*o46; ... 
24,45,(1/6).*F11.*o14.*o252; ... 
24,46,o172; ... 
24,50,(-5).*F11.*o41.*o46; ... 
24,51,(-5/6).*F11.*F22.*o14; ... 
24,52,o175; ... 
25,1,(1/4).*o46.*(o128+o150+o177+o84); ... 
25,2,(-1/4).*(o128+o129+15.*F22.*o13+o253).*o46; ... 
25,3,(-1/4).*o14.*(o199+o47+o76); ... 
25,4,(1/12).*o46.*(o129+o254+(-9).*F21.*o41+105.*o85); ... 
25,5,(4/3)+(13/12).*F22.*o1+o184; ... 
25,6,(1/12).*(11+5.*F22.*o1); ... 
25,7,(-1/4).*F22.*o46.*(o179+o181+o51); ... 
25,8,(-1/12).*o14.*(8.*F21.*F22+o181+o87); ... 
25,9,(-2/3)+o134; ... 
25,10,(-1/4); ... 
25,11,(1/2).*F22.*o256.*o46; ... 
25,12,(1/4).*o14.*(o13+o170+o76); ... 
25,13,(5/12).*o1.*o77; ... 
25,14,(1/4); ... 
25,16,(5/6).*o257.*o41.*o46; ... 
25,17,(-7/6).*o14.*o41; ... 
25,18,(-1/6)+o134; ... 
25,19,(-1/4); ... 
25,22,5.*o46.*o85; ... 
25,23,(5/6).*o14.*o41; ... 
25,24,o70; ... 
25,25,(1/4); ... 
25,29,(1/4).*(F12.*o258+2.*F11.*((-9).*F22.*o13+o259+o45+(-17).*o85)).*o92; ... 
25,30,(1/4).*(o260+F11.*(o261+o262+(-1).*o45+34.*o85)).*o92; ... 
25,31,(-1/4).*F11.*(o13+o143+o236).*o46; ... 
25,32,(1/12).*(3.*F12.*o258+F11.*((-23).*F22.*o13+o45+(-102).*o85)).*o92; ... 
25,33,(1/12).*F11.*o46.*((-13).*F21.*F22+o263+o74); ... 
25,34,o264; ... 
25,35,(1/4).*o92.*(o260+F11.*F22.*(o50+o55+o99)); ... 
25,36,(-1/12).*F11.*o46.*(o13+o263+o51); ... 
25,37,o265; ... 
25,39,(-1/2).*F11.*F22.*o256.*o92; ... 
25,40,(-1/4).*F11.*F22.*o188.*o46; ... 
25,41,o264; ... 
25,44,(-5/6).*F11.*o257.*o41.*o92; ... 
25,45,(7/6).*F11.*o41.*o46; ... 
25,46,o265; ... 
25,50,(-5).*F11.*o85.*o92; ... 
25,51,(-5/6).*F11.*o41.*o46; ... 
25,52,(-1/3).*F11.*F22.*o14; ... 
26,1,(1/4).*(o145+o147+o202+7.*F22.*o45+(-7).*F21.*o85).*o92; ... 
26,2,(-1/4).*(o147+o266+o267+o268+(-11).*F21.*o85).*o92; ... 
26,3,(-1/4).*(o104+o148+o269+3.*F21.*o41).*o46; ... 
26,4,(1/12).*(84.*o146+o266+49.*o13.*o41+(-8).*F22.*o45+(-45).*F21.*o85).*o92; ... 
26,5,(1/12).*o46.*(o129+o262+12.*o85+o97); ... 
26,6,(1/12).*o14.*(o151+o153+o249); ... 
26,7,(1/20).*F22.*(o129+o180+79.*F21.*o41+(-136).*o85).*o92; ... 
26,8,(-1/60).*o46.*(o129+o254+o262+60.*o85); ... 
26,9,(-1/60).*o14.*(o118+o130+o230); ... 
26,10,(-11/20)+o270; ... 
26,11,(1/10).*o273.*o41.*o92; ... 
26,12,(1/20).*F22.*o46.*(o121+o274+o75); ... 
26,13,(1/60).*o14.*(11.*F21.*F22+o230+o98); ... 
26,14,(1/5).*((7/4)+o30); ... 
26,15,(1/5); ... 
26,16,(1/2).*o275.*o85.*o92; ... 
26,17,(1/10).*o248.*o41.*o46; ... 
26,18,(-1/20).*F22.*o14.*o252; ... 
26,19,(-3/20)+o270; ... 
26,20,(-1/5); ... 
26,22,3.*o146.*o92; ... 
26,23,(1/2).*o46.*o85; ... 
26,24,(1/5).*o14.*o41; ... 
26,25,(3/20).*F22.*o1; ... 
26,26,(1/5); ... 
26,29,(1/20).*o157.*((-2).*F11.*F22.*(o103+o150+o276+62.*o85)+F12.*((-20).*o146+(-91).*o13.*o41+55.*F22.*o45+69.*F21.*o85+(-83).*o91)); ... 
26,30,(1/20).*o157.*(F11.*F22.*(34.*F22.*o13+o259+o277+(-3).*o45)+F12.*(19.*o146+80.*o13.*o41+(-44).*F22.*o45+(-64).*F21.*o85+43.*o91)); ... 
26,31,(1/20).*o157.*(F11.*F21.*F22.*(16.*F21.*F22+o274+o72)+F12.*(o146+o215+11.*o13.*o41+(-11).*F22.*o45+40.*o91)); ... 
26,32,(1/60).*o157.*((-12).*F11.*o41.*(o143+o183+o56)+F12.*((-56).*o146+(-227).*o13.*o41+117.*F22.*o45+187.*F21.*o85+(-60).*o91)); ... 
26,33,(1/60).*o157.*(3.*F11.*F21.*F22.*((-10).*F21.*F22+o278+o48)+(-1).*F12.*(o146+o214+o215+13.*o13.*o41+69.*o91)); ... 
26,34,(1/60).*o157.*(o279+F12.*(o280+o281+o282+o283+(-51).*o91)); ... 
26,35,(1/20).*o157.*(F11.*F22.*(o190+o277+o284+o45)+3.*F12.*o258.*o90+3.*F12.*o91); ... 
26,36,(1/60).*o157.*((-3).*F11.*F21.*F22.*(o102+o13+o278)+F12.*((-1).*o146+o218+(-7).*o13.*o41+3.*F22.*o45+5.*F21.*o85)); ... 
26,37,(1/60).*o157.*((-6).*F11.*F22.*o13.*o168+F12.*(o216+20.*o13.*o41+(-18).*F22.*o45+(-10).*F21.*o85+39.*o91)); ... 
26,38,o226; ... 
26,39,(-1/10).*F11.*o157.*o273.*o41; ... 
26,40,(-1/20).*(o137+F11.*F22.*(o13+o274+o75)).*o92; ... 
26,41,(1/60).*o157.*(o279+F12.*(o280+o281+o282+o283+(-27).*o91)); ... 
26,42,o221; ... 
26,44,(-1/2).*F11.*o157.*o275.*o85; ... 
26,45,(-1/10).*F11.*o248.*o41.*o92; ... 
26,46,(1/20).*((-2).*F11.*F21.*F22+o285+6.*F11.*o41).*o46; ... 
26,47,o226; ... 
26,50,(-3).*F11.*o146.*o157; ... 
26,51,(-1/2).*F11.*o85.*o92; ... 
26,52,(-1/5).*F11.*o41.*o46; ... 
26,53,o3; ... 
27,1,(1/4).*o157.*((-14).*F21.*o146+o227+14.*o228+(-7).*o41.*o45+21.*o13.*o85+7.*F22.*o91); ... 
27,2,(-1/4).*F22.*o157.*(o202+o203+(-10).*F22.*o45+(-16).*F21.*o85+5.*o91); ... 
27,3,(-1/4).*(o201+o216+3.*o13.*o41+2.*F22.*o45).*o92; ... 
27,4,(1/12).*F22.*o157.*(40.*o146+o266+45.*o13.*o41+(-17).*F22.*o45+(-44).*F21.*o85); ... 
27,5,(1/12).*(6.*o146+o204+o267+o286+7.*o13.*o41).*o92; ... 
27,6,(1/12).*(5.*F22.*o13+o148+o205+o253).*o46; ... 
27,7,(1/20).*o157.*o41.*(o129+(-39).*F22.*o13+56.*F21.*o41+(-60).*o85); ... 
27,8,(-1/60).*o41.*(o178+o231+28.*o41).*o92; ... 
27,9,(-1/60).*o46.*(o129+o193+o287+10.*o85); ... 
27,10,(-1/20).*o14.*(o243+o47+o52); ... 
27,11,(1/10).*o157.*o289.*o85; ... 
27,12,(1/20).*o41.*(o290+o59+o74).*o92; ... 
27,13,(1/60).*F22.*(o121+o170+9.*o41).*o46; ... 
27,14,(1/20).*o14.*(2.*F21.*F22+o48+o52); ... 
27,15,(1/10).*o1.*(F22+o22); ... 
27,16,(1/6).*o146.*o157.*o17; ... 
27,17,(1/30).*o257.*o85.*o92; ... 
27,18,(-7/60).*o46.*o85; ... 
27,19,(-1/60).*F22.*o14.*o291; ... 
27,20,(1/30).*((-4)+(-3).*F22.*o1); ... 
27,21,(-1/6); ... 
27,22,o157.*o228; ... 
27,23,(1/6).*o146.*o92; ... 
27,24,(1/15).*o46.*o85; ... 
27,25,(1/20).*o14.*o41; ... 
27,26,(1/15).*F22.*o1; ... 
27,27,(1/6); ... 
27,29,(1/20).*o234.*(2.*F11.*o41.*(o192+F21.*o41+o45+(-25).*o85)+(-1).*F12.*((-108).*F21.*o146+7.*o156+28.*o228+(-129).*o41.*o45+167.*o13.*o85+77.*F22.*o91)); ... 
27,30,(1/20).*o234.*(10.*F11.*(o13+o207+o76).*o85+(-1).*F12.*(98.*F21.*o146+o156+(-26).*o228+105.*o41.*o45+(-145).*o13.*o85+(-50).*F22.*o91)); ... 
27,31,(1/20).*o234.*((-2).*F11.*F21.*o41.*(o13+(-4).*o41+o75)+F12.*((-10).*F21.*o146+2.*o228+o292+(-24).*o41.*o45+22.*o13.*o85+27.*F22.*o91)); ... 
27,32,(1/60).*o234.*(F12.*(240.*F21.*o146+(-67).*o228+o293+200.*o41.*o45+(-324).*o13.*o85+(-67).*F22.*o91)+(-1).*F11.*o41.*(o276+(-54).*F21.*o41+150.*o85+o94)); ... 
27,33,(1/60).*o234.*(F11.*F21.*o41.*((-24).*o41+o48+o55)+(-1).*F12.*((-4).*F21.*o146+o228+o294+(-15).*o41.*o45+11.*o13.*o85+33.*F22.*o91)); ... 
27,34,(1/60).*o234.*(F11.*o13.*o295.*o41+(-1).*F12.*(15.*o156+o296+o297+o298+o299+48.*F22.*o91)); ... 
27,35,(1/20).*F22.*o234.*(5.*F12.*o168.*o258+o300+F11.*F22.*(o195+(-26).*F21.*o41+o45+50.*o85)); ... 
27,36,(1/60).*o234.*((-2).*F11.*F21.*(F21+o154).*o85+F12.*(o237+o239+o240+o293+10.*o41.*o45+9.*F22.*o91)); ... 
27,37,(1/60).*o234.*(F11.*o13.*o301.*o41+F12.*(o227+o296+o297+o298+o299+42.*F22.*o91)); ... 
27,38,(1/10).*F12.*o14.*(F22+o81); ... 
27,39,(-1/10).*F11.*o234.*o289.*o85; ... 
27,40,(-1/20).*F22.*o157.*(o302+F11.*F22.*(o13+o290+o59)); ... 
27,41,(1/60).*F22.*o234.*(F11.*F22.*o13.*o295+F12.*((-4).*o146+o213+o220+(-40).*o13.*o41+38.*F22.*o45)); ... 
27,42,(-1/10).*F12.*o14.*o77; ... 
27,43,o7; ... 
27,44,(-1/6).*F11.*o146.*o17.*o234; ... 
27,45,(-1/30).*F11.*o157.*o257.*o85; ... 
27,46,(1/60).*F22.*(o285+F11.*F22.*o301).*o92; ... 
27,47,(1/60).*F12.*o14.*o291; ... 
27,48,o246; ... 
27,50,(-1).*F11.*o228.*o234; ... 
27,51,(-1/6).*F11.*o146.*o157; ... 
27,52,(-1/15).*F11.*o85.*o92; ... 
27,53,(-1/20).*F12.*F22.*o14; ... 
27,54,o8; ... 
28,1,(1/4).*o234.*(18.*o13.*o146+(-3).*F22.*o156+(-10).*F21.*o228+o233+o304+(-17).*o45.*o85+11.*o41.*o91); ... 
28,2,(1/28).*F22.*o234.*(50.*F21.*o146+o227+(-24).*o228+51.*o41.*o45+(-72).*o13.*o85+(-22).*F22.*o91); ... 
28,3,(-1/28).*o157.*((-6).*F21.*o146+o227+4.*o228+o305+(-5).*o41.*o45+6.*F22.*o91); ... 
28,4,(1/84).*o234.*o41.*(o145+60.*o146+108.*o13.*o41+(-51).*F22.*o45+(-100).*F21.*o85); ... 
28,5,(1/84).*F22.*o157.*(o201+o225+o268+(-5).*F22.*o45+(-12).*F21.*o85); ... 
28,6,(1/84).*(o145+4.*o146+o286+10.*o13.*o41+5.*F22.*o45).*o92; ... 
28,7,(1/140).*o234.*((-72).*F22.*o13+100.*F21.*o41+o84+(-80).*o85).*o85; ... 
28,8,(1/420).*o157.*o41.*((-36).*F22.*o13+o140+o176+(-40).*o85); ... 
28,9,(-1/420).*F22.*(o176+o284+(-6).*F21.*o41+16.*o85).*o92; ... 
28,10,(-1/140).*o46.*(o253+o261+o269+o84); ... 
28,11,(1/70).*o146.*o234.*o307; ... 
28,12,(1/140).*o157.*(o143+o208+o48).*o85; ... 
28,13,(1/420).*o41.*(o232+o50+o59).*o92; ... 
28,14,(1/140).*F22.*o308.*o46; ... 
28,15,(1/70).*o14.*(o189+o47+o52); ... 
28,16,(1/42).*o228.*o234.*o309; ... 
28,17,(1/210).*o146.*o157.*o275; ... 
28,18,(1/420).*o248.*o85.*o92; ... 
28,19,(-1/420).*o252.*o41.*o46; ... 
28,20,(-1/210).*F22.*o119.*o14; ... 
28,21,(1/42).*((-5)+(-2).*F22.*o1); ... 
28,22,(1/7).*o234.*o303; ... 
28,23,(1/42).*o157.*o228; ... 
28,24,(1/105).*o146.*o92; ... 
28,25,(1/140).*o46.*o85; ... 
28,26,(1/105).*o14.*o41; ... 
28,27,(1/42).*F22.*o1; ... 
28,28,(1/7); ... 
28,29,(1/140).*o311.*((-10).*F11.*o146.*(o13+o207+o57)+F12.*((-892).*o13.*o146+133.*F22.*o156+480.*F21.*o228+(-35).*o233+(-108).*o303+885.*o45.*o85+(-525).*o41.*o91)); ... 
28,30,(1/140).*o311.*(2.*F11.*(4.*F22.*o13+o312+(-10).*F21.*o41+o45).*o85+F12.*(575.*o13.*o146+(-44).*F22.*o156+(-346).*F21.*o228+6.*o233+84.*o303+(-488).*o45.*o85+233.*o41.*o91)); ... 
28,31,(1/140).*o311.*(2.*F11.*F21.*o235.*o85+F12.*(17.*o13.*o146+31.*F22.*o156+(-14).*F21.*o228+9.*o233+o304+3.*o45.*o85+(-8).*o41.*o91)); ... 
28,32,(-1/420).*F22.*o311.*(2.*F11.*o41.*(o191+o287+(-45).*F21.*o41+90.*o85)+F12.*((-685).*F21.*o146+(-22).*o156+182.*o228+(-676).*o41.*o45+990.*o13.*o85+227.*F22.*o91)); ... 
28,33,(1/420).*o234.*(2.*F11.*(o13+o170+o263).*o85+(-1).*F12.*((-15).*F21.*o146+7.*o228+o292+o305+22.*o41.*o45+10.*F22.*o91)); ... 
28,34,(1/420).*o311.*(o313+F12.*((-266).*o13.*o146+27.*F22.*o156+119.*F21.*o228+(-39).*o233+(-22).*o303+303.*o45.*o85+(-204).*o41.*o91)); ... 
28,35,(1/140).*o311.*o41.*(5.*F12.*o258.*o295+o300+2.*F11.*F22.*(o104+o244+o312+o45)); ... 
28,36,(1/420).*o311.*(2.*F11.*F21.*o85.*(o13+o55+o87)+F12.*F22.*(35.*F21.*o146+(-7).*o228+o293+44.*o41.*o45+(-60).*o13.*o85+(-1).*F22.*o91)); ... 
28,37,(1/420).*o311.*((-4).*F11.*o13.*o54.*o85+F12.*(245.*o13.*o146+(-36).*F22.*o156+(-112).*F21.*o228+12.*o233+21.*o303+(-272).*o45.*o85+173.*o41.*o91)); ... 
28,38,(1/140).*F12.*o311.*((-21).*o13.*o146+23.*F22.*o156+7.*F21.*o228+5.*o233+(-1).*o303+35.*o45.*o85+(-31).*o41.*o91); ... 
28,39,(-1/70).*F11.*o146.*o307.*o311; ... 
28,40,(-1/140).*o234.*o41.*(o302+2.*F11.*F22.*(o13+o59+o76)); ... 
28,41,(1/420).*o311.*(o313+F12.*F22.*(o229+o245+o294+136.*o41.*o45+(-140).*o13.*o85+(-71).*F22.*o91)); ... 
28,42,(-1/140).*F12.*o308.*o46; ... 
28,43,(-1/70).*F12.*o14.*o250; ... 
28,44,(-1/42).*F11.*o228.*o309.*o311; ... 
28,45,(-1/210).*F11.*o146.*o234.*o275; ... 
28,46,(1/420).*o157.*(o285+4.*F11.*F22.*(o122+o35)).*o41; ... 
28,47,(1/420).*F12.*F22.*o252.*o46; ... 
28,48,(1/210).*F12.*o119.*o14; ... 
28,49,(1/21).*F12.*o1; ... 
28,50,(-1/7).*F11.*o303.*o311; ... 
28,51,(-1/42).*F11.*o228.*o234; ... 
28,52,(-1/105).*F11.*o146.*o157; ... 
28,53,(-1/140).*F12.*o41.*o46; ... 
28,54,(-1/105).*F12.*F22.*o14; ... 
28,55,(-1/42).*F12.*o1; ... 
29,29,(-17/140); ... 
29,31,(-3/140); ... 
29,34,(-1/420); ... 
29,38,(-1/140); ... 
30,29,(3/20); ... 
30,30,(-3/20); ... 
30,33,(-1/60); ... 
30,34,(1/60); ... 
30,37,(-1/60); ... 
31,29,(3/20); ... 
31,31,(-3/20); ... 
31,34,(-1/60); ... 
31,38,(-1/20); ... 
32,29,(-1/4); ... 
32,30,(1/5); ... 
32,31,(1/20); ... 
32,32,(-11/60); ... 
32,33,(-1/60); ... 
32,34,(-1/30); ... 
32,36,(-1/60); ... 
32,37,(1/30); ... 
32,41,(-1/30); ... 
33,29,(-1/10); ... 
33,30,(1/10); ... 
33,33,(-1/10); ... 
33,34,(1/10); ... 
33,37,(-1/10); ... 
34,29,(-1/20); ... 
34,31,(1/20); ... 
34,34,(-1/20); ... 
34,38,(-3/20); ... 
35,29,(1/4); ... 
35,30,(-1/4); ... 
35,32,(1/4); ... 
35,35,(-1/4); ... 
36,29,(-1/4); ... 
36,31,(1/4); ... 
36,32,(1/12); ... 
36,33,(-1/12); ... 
36,34,(-1/6); ... 
36,36,(-1/12); ... 
36,37,(1/6); ... 
36,41,(-1/6); ... 
37,29,(3/4); ... 
37,30,(-1/4); ... 
37,31,(-1/2); ... 
37,33,(1/4); ... 
37,34,(1/4); ... 
37,37,(-1/4); ... 
38,29,(1/4); ... 
38,31,(-1/4); ... 
38,34,(1/4); ... 
38,38,(-1/4); ... 
39,29,(1/5); ... 
39,30,(-1/5); ... 
39,32,(1/5); ... 
39,35,(-1/5); ... 
39,39,(1/5); ... 
40,29,1; ... 
40,30,(-3/4); ... 
40,31,(-1/4); ... 
40,32,(1/2); ... 
40,33,(1/4); ... 
40,35,(-1/4); ... 
40,36,(-1/4); ... 
40,40,(1/4); ... 
41,29,(-2); ... 
41,30,1; ... 
41,31,1; ... 
41,32,(-1/3); ... 
41,33,(-2/3); ... 
41,34,(-1/3); ... 
41,36,(1/3); ... 
41,37,(1/3); ... 
41,41,(-1/3); ... 
42,29,1; ... 
42,30,(-1/4); ... 
42,31,(-3/4); ... 
42,33,(1/4); ... 
42,34,(1/2); ... 
42,37,(-1/4); ... 
42,38,(-1/4); ... 
42,42,(1/4); ... 
43,29,(1/5); ... 
43,31,(-1/5); ... 
43,34,(1/5); ... 
43,38,(-1/5); ... 
43,43,(1/5); ... 
44,29,(-1/10); ... 
44,30,(1/10); ... 
44,32,(-1/10); ... 
44,35,(1/10); ... 
44,39,(-1/10); ... 
44,44,(1/6); ... 
45,29,(-3/5); ... 
45,30,(1/2); ... 
45,31,(1/10); ... 
45,32,(-2/5); ... 
45,33,(-1/10); ... 
45,35,(3/10); ... 
45,36,(1/10); ... 
45,39,(-1/5); ... 
45,40,(-1/10); ... 
45,45,(1/5); ... 
46,29,(-5/2); ... 
46,30,(3/2); ... 
46,31,1; ... 
46,32,(-3/4); ... 
46,33,(-3/4); ... 
46,34,(-1/4); ... 
46,35,(1/4); ... 
46,36,(1/2); ... 
46,37,(1/4); ... 
46,40,(-1/4); ... 
46,41,(-1/4); ... 
46,46,(1/4); ... 
47,29,(-5/2); ... 
47,30,1; ... 
47,31,(3/2); ... 
47,32,(-1/4); ... 
47,33,(-3/4); ... 
47,34,(-3/4); ... 
47,36,(1/4); ... 
47,37,(1/2); ... 
47,38,(1/4); ... 
47,41,(-1/4); ... 
47,42,(-1/4); ... 
47,47,(1/4); ... 
48,29,(-3/5); ... 
48,30,(1/10); ... 
48,31,(1/2); ... 
48,33,(-1/10); ... 
48,34,(-2/5); ... 
48,37,(1/10); ... 
48,38,(3/10); ... 
48,42,(-1/10); ... 
48,43,(-1/5); ... 
48,48,(1/5); ... 
49,29,(-1/10); ... 
49,31,(1/10); ... 
49,34,(-1/10); ... 
49,38,(1/10); ... 
49,43,(-1/10); ... 
49,49,(1/6); ... 
50,29,(9/70); ... 
50,30,(-9/70); ... 
50,32,(9/70); ... 
50,35,(-9/70); ... 
50,39,(9/70); ... 
50,44,(-5/42); ... 
50,50,(1/7); ... 
51,29,(9/10); ... 
51,30,(-3/4); ... 
51,31,(-3/20); ... 
51,32,(3/5); ... 
51,33,(3/20); ... 
51,35,(-9/20); ... 
51,36,(-3/20); ... 
51,39,(3/10); ... 
51,40,(3/20); ... 
51,44,(-1/6); ... 
51,45,(-2/15); ... 
51,51,(1/6); ... 
52,29,(5/2); ... 
52,30,(-17/10); ... 
52,31,(-4/5); ... 
52,32,(21/20); ... 
52,33,(13/20); ... 
52,34,(3/20); ... 
52,35,(-11/20); ... 
52,36,(-1/2); ... 
52,37,(-3/20); ... 
52,39,(1/5); ... 
52,40,(7/20); ... 
52,41,(3/20); ... 
52,45,(-1/5); ... 
52,46,(-3/20); ... 
52,52,(1/5); ... 
53,29,5; ... 
53,30,(-5/2); ... 
53,31,(-5/2); ... 
53,32,1; ... 
53,33,(3/2); ... 
53,34,1; ... 
53,35,(-1/4); ... 
53,36,(-3/4); ... 
53,37,(-3/4); ... 
53,38,(-1/4); ... 
53,40,(1/4); ... 
53,41,(1/2); ... 
53,42,(1/4); ... 
53,46,(-1/4); ... 
53,47,(-1/4); ... 
53,53,(1/4); ... 
54,29,(5/2); ... 
54,30,(-4/5); ... 
54,31,(-17/10); ... 
54,32,(3/20); ... 
54,33,(13/20); ... 
54,34,(21/20); ... 
54,36,(-3/20); ... 
54,37,(-1/2); ... 
54,38,(-11/20); ... 
54,41,(3/20); ... 
54,42,(7/20); ... 
54,43,(1/5); ... 
54,47,(-3/20); ... 
54,48,(-1/5); ... 
54,54,(1/5); ... 
55,29,(9/10); ... 
55,30,(-3/20); ... 
55,31,(-3/4); ... 
55,33,(3/20); ... 
55,34,(3/5); ... 
55,37,(-3/20); ... 
55,38,(-9/20); ... 
55,42,(3/20); ... 
55,43,(3/10); ... 
55,48,(-2/15); ... 
55,49,(-1/6); ... 
55,55,(1/6); ... 
56,29,(9/70); ... 
56,31,(-9/70); ... 
56,34,(9/70); ... 
56,38,(-9/70); ... 
56,43,(9/70); ... 
56,49,(-5/42); ... 
56,56,(1/7); ... 
];
T0=sparse(aux(:,1),aux(:,2),aux(:,3),56,56);