o1=F22.^(-1);
aux = [1,1,1; ... 
2,1,1+(-1).*F21.*o1; ... 
2,2,F21.*o1; ... 
2,5,(F11+(-1).*F12).*o1; ... 
2,6,F12.*o1; ... 
2,7,(-1).*F11.*o1; ... 
3,2,1; ... 
4,4,1; ... 
4,5,(-1); ... 
4,6,1; ... 
5,3,1; ... 
6,4,1; ... 
7,5,1; ... 
8,6,1; ... 
9,7,1; ... 
];
TD=sparse(aux(:,1),aux(:,2),aux(:,3),9,7);