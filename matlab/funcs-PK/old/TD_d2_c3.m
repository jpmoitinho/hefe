o1=F12.^(-1);
o2=(-1).*F22;
o3=F21+o2;
o4=o1.*o3;
o5=F12.^2;
o6=o5.^(-1);
o7=(-1).*F12;
o8=F11+o7;
aux = [1,1,(1/2); ... 
1,5,(-1); ... 
1,7,(1/2); ... 
2,1,1; ... 
3,2,1; ... 
4,1,(1/2); ... 
4,6,(1/2); ... 
4,9,(-1/2); ... 
5,3,1; ... 
6,4,1; ... 
7,5,1; ... 
8,6,1; ... 
9,7,1; ... 
10,8,1; ... 
11,9,1; ... 
12,2,(1/2); ... 
12,3,(-1/2); ... 
12,7,(1/2); ... 
13,10,1; ... 
14,1,F21.*o1; ... 
14,2,(-1).*F21.*o1; ... 
14,5,(-2).*o1.*o3; ... 
14,7,o4; ... 
14,10,2+(-2).*F11.*o1; ... 
14,11,F11.*o1; ... 
15,11,1; ... 
16,1,(-1/2).*(F11+(-2).*F12).*F21.*o6; ... 
16,2,(-1/2).*(F12.*F21+F11.*((-2).*F21+F22)).*o6; ... 
16,3,(1/2).*((-1).*F12.*F21+F11.*F22).*o6; ... 
16,4,(-1).*F11.*F21.*o6; ... 
16,5,o3.*o6.*o8; ... 
16,6,(1/2).*F22.*o1; ... 
16,7,(-1/2).*o3.*o6.*o8; ... 
16,9,(-1/2).*F22.*o1; ... 
16,10,o6.*o8.^2; ... 
16,11,F11.*((-1).*F11+F12).*o6; ... 
16,12,F11.^2.*o6; ... 
17,2,o4; ... 
17,3,F22.*o1; ... 
17,4,(-2).*F21.*o1; ... 
17,11,1+(-1).*F11.*o1; ... 
17,12,2.*F11.*o1; ... 
18,12,1; ... 
];
TD=sparse(aux(:,1),aux(:,2),aux(:,3),18,12);