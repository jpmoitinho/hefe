o1=F12.^(-1);
o2=F12.^2;
o3=o2.^(-1);
o4=3.*F11;
o5=5.*F11;
o6=(-8).*F12;
o7=(-2).*F11;
o8=F12+o7;
o9=(-1).*F11;
o10=F12+o9;
o11=(-3).*F11.*o1;
o12=(1/2).*F11.*o1;
o13=(1/6).*F21.*o1;
o14=(1/3).*F21.*o1;
o15=(-1/6).*F21.*o1;
o16=F12.^3;
o17=o16.^(-1);
o18=F11.^2;
o19=(-5).*o18;
o20=(-2).*F12;
o21=F11+o20;
o22=4.*o2;
o23=(-5).*F11.*F12;
o24=(-1).*F12;
o25=5.*o18.*o3;
o26=(-5/6).*F11.*o1;
o27=7.*F12;
o28=(1/6).*F11.*o1;
o29=(-4).*F11;
o30=F12+o29;
o31=3.*F11.*o1;
o32=(-1/3).*F21.*o1;
o33=F12.^4;
o34=o33.^(-1);
o35=F11.^3;
o36=17.*F21;
o37=7.*o18;
o38=8.*o2;
o39=6.*F21.*o16;
o40=28.*F21;
o41=3.*F22;
o42=o40+o41;
o43=4.*o18;
o44=4.*F21.*o2;
o45=4.*F21;
o46=12.*F12.*o18;
o47=6.*o18;
o48=(-5).*o17.*o35;
o49=5.*o18;
o50=3.*o2;
o51=15.*o18;
o52=(-1/3).*F11.*o1;
o53=(-3).*F11;
o54=2.*F12;
o55=(-1).*F11.*F12;
o56=o18+o2+o55;
o57=2.*o2;
o58=6.*F22;
o59=(-3).*F11.*F12;
o60=o18+o50+o59;
o61=F21.*o2;
o62=2.*F21;
o63=F22+o62;
o64=3.*o18;
o65=(-5).*o18.*o3;
o66=F12+o5;
o67=(-5).*F11;
o68=(-9).*F11;
o69=(1/3).*F11.*o1;
o70=F12.*F21;
o71=F11.*F22;
o72=o70+o71;
o73=(1/2).*F21.*o21.*o3;
o74=3.*F11.*F22;
o75=F22.*o1;
o76=(-1).*F22.*o1;
o77=F12.^5;
o78=o77.^(-1);
o79=2.*F22;
o80=F11.^4;
o81=19.*F21;
o82=15.*F21.*o33;
o83=15.*F22;
o84=36.*F22;
o85=60.*o18.*o2;
o86=3.*F21.*o33;
o87=4.*F21.*o16;
o88=10.*F21;
o89=9.*F22;
o90=(3/20).*F21.*o1;
o91=20.*F21;
o92=F22+o91;
o93=3.*o80;
o94=(-4).*F11.*o16;
o95=4.*o16;
o96=(-8).*F11.*F12;
o97=(-2)+o11;
o98=3.*F21;
o99=5.*F22;
o100=o62+o99;
o101=(-4).*F21.*o16;
o102=38.*F21;
o103=(-4).*F12.*o18;
o104=6.*F11.*o2;
o105=(-2).*o16;
o106=o103+o104+o105+o35;
o107=5.*F21;
o108=8.*F21;
o109=o108+o83;
o110=2.*F11;
o111=F11+F12;
o112=12.*o18;
o113=4.*F11;
o114=(-7).*F11;
o115=(-2).*F21.*o18;
o116=(-4).*F21.*o2;
o117=3.*F11.*F12.*o63;
o118=o115+o116+o117;
o119=(1/6).*o118.*o17;
o120=12.*F22;
o121=F21+o120;
o122=F21+F22;
o123=(5/6).*F11.*o1;
o124=3.*F12;
o125=F12+o4;
o126=(1/2).*F22.*o1;
o127=6.*F11;
o128=F12+o127;
o129=(-1/2).*F22.*o1;
o130=F12.^6;
o131=o130.^(-1);
o132=7.*F21;
o133=4.*F22;
o134=F11.^5;
o135=F21.*o77;
o136=2.*F21.*o33;
o137=o79+o98;
o138=F21+o41;
o139=10.*o18;
o140=o139+o50+o96;
o141=F12+o110;
o142=o113+o24;
o143=2.*F11.*o2;
o144=2.*o16;
o145=F11.*F12;
o146=3.*o35;
o147=F11+o54;
o148=o41+o88;
o149=48.*o18;
o150=(-29).*F11.*F12;
o151=11.*o2;
o152=o149+o150+o151;
o153=7.*F11;
o154=o153+o24;
o155=7.*F11.*F12;
o156=F12+o114;
o157=F21.*o35;
o158=o45+o89;
o159=(-1).*F12.*o158.*o18;
o160=57.*F22;
o161=o70+o74;
o162=(1/12).*o161.*o3;
o163=19.*o18;
o164=(-7).*F11.*F12;
o165=o163+o164+o50;
o166=(1/3).*F22.*o1;
o167=(-1/3).*F22.*o1;
o168=o139+o2+o55;
aux = [1,1,(-3/20); ... 
1,2,(-1/60); ... 
1,4,(-1/60); ... 
2,1,(1/10); ... 
2,2,(-1/10); ... 
2,4,(-1/10); ... 
3,1,(1/5); ... 
3,2,(-1/60); ... 
3,3,(-11/60); ... 
3,4,(1/30); ... 
3,5,(-1/60); ... 
3,8,(-1/30); ... 
4,1,(-1/4); ... 
4,2,(1/4); ... 
4,4,(-1/4); ... 
5,2,(-1/12); ... 
5,3,(1/12); ... 
5,4,(1/6); ... 
5,5,(-1/12); ... 
5,8,(-1/6); ... 
6,2,(-1/12); ... 
6,3,(1/12); ... 
6,5,(1/12); ... 
6,6,(-1/6); ... 
6,9,(-1/12); ... 
7,1,(-1/4); ... 
7,2,(1/4); ... 
7,4,(-1/4); ... 
7,7,(1/4); ... 
8,1,1; ... 
8,2,(-2/3); ... 
8,3,(-1/3); ... 
8,4,(1/3); ... 
8,5,(1/3); ... 
8,8,(-1/3); ... 
9,1,1; ... 
9,2,(-1/3); ... 
9,3,(-2/3); ... 
9,5,(1/3); ... 
9,6,(1/3); ... 
9,9,(-1/3); ... 
10,1,(-1/4); ... 
10,3,(1/4); ... 
10,6,(-1/4); ... 
10,10,(1/4); ... 
11,1,(1/10); ... 
11,2,(-1/10); ... 
11,4,(1/10); ... 
11,7,(-1/10); ... 
11,11,(1/5); ... 
12,1,1; ... 
12,2,(-3/4); ... 
12,3,(-1/4); ... 
12,4,(1/2); ... 
12,5,(1/4); ... 
12,7,(-1/4); ... 
12,8,(-1/4); ... 
12,12,(1/4); ... 
13,1,2; ... 
13,2,(-1); ... 
13,3,(-1); ... 
13,4,(1/3); ... 
13,5,(2/3); ... 
13,6,(1/3); ... 
13,8,(-1/3); ... 
13,9,(-1/3); ... 
13,13,(1/3); ... 
14,1,1; ... 
14,2,(-1/4); ... 
14,3,(-3/4); ... 
14,5,(1/4); ... 
14,6,(1/2); ... 
14,9,(-1/4); ... 
14,10,(-1/4); ... 
14,14,(1/4); ... 
15,1,(1/10); ... 
15,3,(-1/10); ... 
15,6,(1/10); ... 
15,10,(-1/10); ... 
15,15,(1/5); ... 
16,1,(-3/20); ... 
16,2,(3/20); ... 
16,4,(-3/20); ... 
16,7,(3/20); ... 
16,11,(-2/15); ... 
16,16,(1/6); ... 
17,1,(-4/5); ... 
17,2,(13/20); ... 
17,3,(3/20); ... 
17,4,(-1/2); ... 
17,5,(-3/20); ... 
17,7,(7/20); ... 
17,8,(3/20); ... 
17,11,(-1/5); ... 
17,12,(-3/20); ... 
17,17,(1/5); ... 
18,1,(-2); ... 
18,2,(5/4); ... 
18,3,(3/4); ... 
18,4,(-2/3); ... 
18,5,(-7/12); ... 
18,6,(-1/6); ... 
18,7,(1/4); ... 
18,8,(5/12); ... 
18,9,(1/6); ... 
18,12,(-1/4); ... 
18,13,(-1/6); ... 
18,18,(1/4); ... 
19,1,(-5/2); ... 
19,2,1; ... 
19,3,(3/2); ... 
19,4,(-1/4); ... 
19,5,(-3/4); ... 
19,6,(-3/4); ... 
19,8,(1/4); ... 
19,9,(1/2); ... 
19,10,(1/4); ... 
19,13,(-1/4); ... 
19,14,(-1/4); ... 
19,19,(1/4); ... 
20,1,(-4/5); ... 
20,2,(3/20); ... 
20,3,(13/20); ... 
20,5,(-3/20); ... 
20,6,(-1/2); ... 
20,9,(3/20); ... 
20,10,(7/20); ... 
20,14,(-3/20); ... 
20,15,(-1/5); ... 
20,20,(1/5); ... 
21,1,(-3/20); ... 
21,3,(3/20); ... 
21,6,(-3/20); ... 
21,10,(3/20); ... 
21,15,(-2/15); ... 
21,21,(1/6); ... 
22,1,(-11/10).*F21.*o1; ... 
22,2,(-1/12).*F21.*o1; ... 
22,3,(11/60).*F21.*o1; ... 
22,4,(-2/15).*F21.*o1; ... 
22,5,(1/60).*F21.*o1; ... 
22,8,(1/30).*F21.*o1; ... 
22,22,1; ... 
23,1,(1/4).*(13.*F11+(-10).*F12).*F21.*o3; ... 
23,2,(1/12).*F21.*o3.*((-5).*F12+o4); ... 
23,3,(1/12).*((-12).*F11.*F21+5.*F12.*F21).*o3; ... 
23,4,(1/12).*F21.*o3.*(o5+o6); ... 
23,5,(1/12).*F21.*o3.*o8; ... 
23,6,(1/6).*F11.*F21.*o3; ... 
23,8,(1/6).*F21.*o10.*o3; ... 
23,9,(1/12).*F11.*F21.*o3; ... 
23,22,2+o11; ... 
23,23,(1/2); ... 
23,24,o12; ... 
24,1,F21.*o1; ... 
24,2,(1/12).*F21.*o1; ... 
24,3,(-13/12).*F21.*o1; ... 
24,4,o13; ... 
24,5,(-1/4).*F21.*o1; ... 
24,6,o14; ... 
24,8,o15; ... 
24,9,o13; ... 
24,22,(-1); ... 
24,24,1; ... 
25,1,F21.*o17.*(8.*F11.*F12+o19+(-4).*o2); ... 
25,2,(-1/2).*F11.*F21.*o17.*o21; ... 
25,3,(1/6).*F21.*o17.*((-14).*F11.*F12+13.*o18+o22); ... 
25,4,(-1/3).*F21.*o17.*(2.*o18+o22+o23); ... 
25,5,(1/6).*F11.*F21.*o17.*((-4).*F12+o4); ... 
25,6,(1/3).*F11.*F21.*o17.*o8; ... 
25,8,(1/3).*F21.*o17.*(F11+o24).^2; ... 
25,9,(1/3).*F11.*F21.*o10.*o17; ... 
25,22,3+(-7).*F11.*o1+o25; ... 
25,23,(1/3)+o26; ... 
25,24,(1/6).*F11.*((-10).*F11+o27).*o3; ... 
25,25,(1/3); ... 
25,26,o28; ... 
25,27,(1/3).*o18.*o3; ... 
26,1,F21.*o3.*o8; ... 
26,2,(-1/6).*F21.*o3.*(o20+o4); ... 
26,3,(1/6).*F21.*o3.*(15.*F11+o6); ... 
26,4,(-1/3).*F21.*o21.*o3; ... 
26,5,(1/6).*F21.*o3.*((-6).*F12+o5); ... 
26,6,(1/3).*F21.*o3.*o30; ... 
26,8,(1/3).*F21.*o21.*o3; ... 
26,9,(2/3).*F21.*o10.*o3; ... 
26,22,(-1)+o31; ... 
26,23,(-1/2); ... 
26,24,(3/2)+o11; ... 
26,26,(1/2); ... 
26,27,F11.*o1; ... 
27,2,o32; ... 
27,3,o14; ... 
27,5,o14; ... 
27,6,(-2/3).*F21.*o1; ... 
27,9,o32; ... 
27,22,1; ... 
27,24,(-1); ... 
27,27,1; ... 
28,1,(1/4).*o34.*((-12).*F21.*o16+(-41).*F12.*F21.*o18+38.*F11.*F21.*o2+o35.*(F22+o36)); ... 
28,2,(1/12).*F11.*F21.*o34.*((-18).*F11.*F12+o37+o38); ... 
28,3,(1/12).*o34.*(51.*F12.*F21.*o18+(-32).*F11.*F21.*o2+o39+(-1).*o35.*o42); ... 
28,4,(1/12).*F21.*o34.*((-12).*o16+(-24).*F12.*o18+30.*F11.*o2+7.*o35); ... 
28,5,(-1/6).*F11.*F21.*o34.*((-9).*F11.*F12+o22+o43); ... 
28,6,(1/12).*F11.*o34.*((-15).*F11.*F12.*F21+o44+3.*o18.*(F22+o45)); ... 
28,8,(1/12).*F21.*o34.*(3.*o16+(-12).*F11.*o2+(-4).*o35+o46); ... 
28,9,(1/12).*F11.*F21.*o34.*((-12).*F11.*F12+5.*o2+o47); ... 
28,10,(-1/4).*F22.*o34.*o35; ... 
28,22,2+(-8).*F11.*o1+10.*o18.*o3+o48; ... 
28,23,(1/6).*o3.*(o23+o49+o50); ... 
28,24,(1/6).*F11.*o17.*((-20).*F11.*F12+o38+o51); ... 
28,25,o52; ... 
28,26,(1/6).*F11.*o3.*o8; ... 
28,27,(1/3).*o17.*o18.*(o53+o54); ... 
28,28,(1/4); ... 
28,29,(1/12).*F11.*o1; ... 
28,30,(1/12).*o18.*o3; ... 
28,31,(1/4).*o17.*o35; ... 
29,1,o17.*(F22.*o18+2.*F21.*o56); ... 
29,2,(-1/6).*F21.*o17.*(9.*F11.*F12+o19+o57); ... 
29,3,(-1/6).*o17.*((-21).*F11.*F12.*F21+10.*F21.*o2+o18.*(o36+o58)); ... 
29,4,(1/3).*F21.*o17.*o60; ... 
29,5,(-1/6).*F21.*o17.*((-15).*F11.*F12+o22+o37); ... 
29,6,(1/3).*o17.*((-6).*F11.*F12.*F21+o61+3.*o18.*o63); ... 
29,8,(-1/3).*F21.*o17.*o60; ... 
29,9,(1/3).*F21.*o17.*((-6).*F11.*F12+o57+o64); ... 
29,10,(-1).*F22.*o17.*o18; ... 
29,22,(-2)+4.*F11.*o1+o65; ... 
29,23,(1/6).*o1.*o66; ... 
29,24,(11/6)+(-29/6).*F11.*o1+o25; ... 
29,25,(-1/3); ... 
29,26,(1/6).*o1.*(F12+o67); ... 
29,27,(1/3).*F11.*o3.*(5.*F12+o68); ... 
29,29,(1/3); ... 
29,30,o69; ... 
29,31,o18.*o3; ... 
30,1,(3/2).*o3.*o72; ... 
30,2,o73; ... 
30,3,(-1/2).*o3.*(F11.*F21+o70+o74); ... 
30,5,(-1/2).*F21.*o21.*o3; ... 
30,6,(1/2).*o3.*(2.*F11.*F21+(-1).*F12.*F21+o74); ... 
30,9,o73; ... 
30,10,(-3/2).*F11.*F22.*o3; ... 
30,22,o11; ... 
30,23,(1/2); ... 
30,24,(-1/2)+o31; ... 
30,26,(-1/2); ... 
30,27,1+o11; ... 
30,30,(1/2); ... 
30,31,(3/2).*F11.*o1; ... 
31,1,o75; ... 
31,3,o76; ... 
31,6,o75; ... 
31,10,o76; ... 
31,22,(-1); ... 
31,24,1; ... 
31,27,(-1); ... 
31,31,1; ... 
32,1,(1/10).*o78.*(50.*F11.*F21.*o16+(-82).*F21.*o18.*o2+(-15).*F21.*o33+F12.*o35.*(61.*F21+o79)+(-1).*o80.*(o58+o81)); ... 
32,2,(1/60).*o78.*(20.*F11.*F21.*o16+(-63).*F21.*o18.*o2+F12.*o35.*(70.*F21+o41)+(-21).*F21.*o80+o82); ... 
32,3,(1/60).*o78.*((-80).*F11.*F21.*o16+195.*F21.*o18.*o2+o82+(-1).*F12.*o35.*(196.*F21+o83)+o80.*(75.*F21+o84)); ... 
32,4,(-1/30).*F21.*o78.*((-44).*F11.*o16+18.*o33+(-35).*F12.*o35+8.*o80+o85); ... 
32,5,(-1/60).*o78.*(24.*F11.*F21.*o16+(-75).*F21.*o18.*o2+F12.*o35.*(80.*F21+o41)+(-25).*F21.*o80+o86); ... 
32,6,(1/30).*F11.*o78.*((-27).*F11.*F21.*o2+o87+F12.*o18.*(42.*F21+o89)+(-2).*o35.*(o88+o89)); ... 
32,7,o90; ... 
32,8,(1/60).*F21.*o78.*((-34).*F11.*o16+9.*o33+(-40).*F12.*o35+10.*o80+o85); ... 
32,9,(1/60).*F11.*o78.*(16.*F21.*o16+(-54).*F11.*F21.*o2+(-20).*F21.*o35+3.*F12.*o18.*o92); ... 
32,10,(1/20).*(12.*F11+(-7).*F12).*F22.*o35.*o78; ... 
32,12,(-1/20).*F21.*o1; ... 
32,13,(-1/30).*F11.*F21.*o3; ... 
32,14,(-1/20).*F22.*o34.*o35; ... 
32,15,(-1/5).*F22.*o78.*o80; ... 
32,22,o34.*(8.*o18.*o2+o33+(-7).*F12.*o35+o93+o94); ... 
32,23,(-1/6).*F11.*o17.*((-4).*F11.*F12+o22+o64); ... 
32,24,(1/6).*F11.*o34.*(21.*F12.*o18+(-16).*F11.*o2+(-12).*o35+o95); ... 
32,25,(1/15).*o3.*(o50+o55+o64); ... 
32,26,(1/30).*F11.*o17.*(9.*o18+o22+o96); ... 
32,27,(1/15).*o18.*o34.*((-21).*F11.*F12+18.*o18+o38); ... 
32,28,(1/20).*o97; ... 
32,29,(1/60).*F11.*((-6).*F11+F12).*o3; ... 
32,30,(1/60).*o17.*o18.*(4.*F12+o68); ... 
32,31,(1/20).*((-12).*F11+o27).*o34.*o35; ... 
32,32,(1/5); ... 
32,33,(1/20).*F11.*o1; ... 
32,34,(1/30).*o18.*o3; ... 
32,35,(1/20).*o17.*o35; ... 
32,36,(1/5).*o34.*o80; ... 
33,1,(1/2).*F11.*o34.*((-1).*o100.*o18+(-3).*F21.*o2+F11.*F12.*(F22+o98)); ... 
33,2,(1/12).*o34.*((-11).*F11.*F21.*o2+(-7).*F21.*o35+o87+F12.*o18.*(o41+o91)); ... 
33,3,(1/12).*o34.*(o101+29.*F11.*F21.*o2+o35.*(30.*F22+o81)+(-1).*F12.*o18.*(o102+o89)); ... 
33,4,(-1/6).*F21.*o106.*o34; ... 
33,5,(1/12).*o34.*((-8).*F21.*o16+23.*F11.*F21.*o2+9.*F21.*o35+(-1).*F12.*o18.*o42); ... 
33,6,(-1/6).*F11.*o34.*(o109.*o18+8.*F21.*o2+(-3).*F11.*F12.*(o107+o79)); ... 
33,8,(1/6).*F21.*o106.*o34; ... 
33,9,(1/12).*o34.*(3.*F12.*(F22+o108).*o18+(-20).*F11.*F21.*o2+(-8).*F21.*o35+o39); ... 
33,10,(5/4).*F22.*o18.*(o110+o24).*o34; ... 
33,13,o15; ... 
33,14,(-1/4).*F22.*o17.*o18; ... 
33,15,(-1).*F22.*o34.*o35; ... 
33,22,F11.*o17.*(o22+o23+o49); ... 
33,23,(-2/3)+(-5/6).*o18.*o3; ... 
33,24,(2/3)+(-4).*F11.*o1+(35/6).*o18.*o3+o48; ... 
33,25,(1/3).*o1.*o111; ... 
33,26,(1/6).*o3.*((-2).*F11.*F12+o49+o57); ... 
33,27,(1/3).*F11.*o17.*((-11).*F11.*F12+o112+o22); ... 
33,28,(-1/4); ... 
33,29,(-1/12).*o1.*(F12+o113); ... 
33,30,(1/12).*F11.*o3.*(o114+o54); ... 
33,31,(5/4).*o17.*o18.*o8; ... 
33,33,(1/4); ... 
33,34,o28; ... 
33,35,(1/4).*o18.*o3; ... 
33,36,o17.*o35; ... 
34,1,o17.*((-1).*F11.*F12.*F21+(-4).*F22.*o18+o61); ... 
34,2,o119; ... 
34,3,(1/6).*o17.*((-3).*F11.*F12.*F22+2.*o121.*o18+(-2).*F21.*o2); ... 
34,5,(1/6).*o17.*(2.*F21.*o18+o44+(-3).*F11.*F12.*o63); ... 
34,6,(-1/3).*o17.*((-3).*F11.*F12.*o122+2.*o18.*(F21+o58)+o61); ... 
34,9,o119; ... 
34,10,(1/2).*F11.*(8.*F11+(-3).*F12).*F22.*o17; ... 
34,14,(-1/2).*F11.*F22.*o3; ... 
34,15,(-2).*F22.*o17.*o18; ... 
34,22,2+(-1).*F11.*o1+o25; ... 
34,23,(-2/3)+o26; ... 
34,24,(-4/3)+(11/6).*F11.*o1+o65; ... 
34,25,(1/3); ... 
34,26,(1/3)+o123; ... 
34,27,1+(-8/3).*F11.*o1+o25; ... 
34,29,(-1/3); ... 
34,30,o26; ... 
34,31,(1/2).*F11.*((-8).*F11+o124).*o3; ... 
34,34,(1/3); ... 
34,35,o12; ... 
34,36,2.*o18.*o3; ... 
35,1,(-1).*F22.*o125.*o3; ... 
35,2,o126; ... 
35,3,(1/2).*F22.*o128.*o3; ... 
35,5,o129; ... 
35,6,(-3).*F11.*F22.*o3; ... 
35,9,o126; ... 
35,10,(1/2).*F22.*(o127+o24).*o3; ... 
35,14,o129; ... 
35,15,(-2).*F11.*F22.*o3; ... 
35,22,1+o31; ... 
35,23,(-1/2); ... 
35,24,(-1/2)+o11; ... 
35,26,(1/2); ... 
35,27,o31; ... 
35,30,(-1/2); ... 
35,31,(1/2)+o11; ... 
35,35,(1/2); ... 
35,36,2.*F11.*o1; ... 
36,1,o76; ... 
36,3,o75; ... 
36,6,o76; ... 
36,10,o75; ... 
36,15,o76; ... 
36,22,1; ... 
36,24,(-1); ... 
36,27,1; ... 
36,31,(-1); ... 
36,36,1; ... 
37,1,(1/20).*o131.*((10.*F22+o132).*o134+(-44).*F21.*o16.*o18+25.*F11.*F21.*o33+(49.*F21+o133).*o2.*o35+(-2).*F21.*o77+(-4).*F12.*(F22+o132).*o80); ... 
37,2,(-1/60).*o131.*((-5).*F21.*o134+18.*F21.*o16.*o18+5.*F11.*F21.*o33+5.*F21.*o77+o2.*o35.*((-29).*F21+o79)+F12.*(21.*F21+o133).*o80); ... 
37,3,(1/60).*o131.*((-2).*o109.*o134+o135+50.*F21.*o16.*o18+(-20).*F11.*F21.*o33+F12.*(55.*F21+16.*F22).*o80+(-2).*o2.*o35.*(o102+o99)); ... 
37,4,(1/60).*F11.*o131.*((-37).*F11.*F21.*o16+(35.*F21+F22).*o18.*o2+25.*F21.*o33+(-16).*F12.*F21.*o35+3.*F21.*o80); ... 
37,5,(1/60).*o131.*((-6).*F21.*o134+o135+23.*F21.*o16.*o18+(-2).*F11.*F21.*o33+((-36).*F21+F22).*o2.*o35+F12.*(25.*F21+o133).*o80); ... 
37,6,(1/60).*F11.*o131.*(o136+(-13).*F11.*F21.*o16+(-10).*F12.*o137.*o35+10.*o138.*o80+o18.*o2.*(32.*F21+o89)); ... 
37,7,(-1/20).*F21.*o111.*o3; ... 
37,8,(-1/60).*F11.*o131.*((-18).*F11.*F21.*o16+10.*F21.*o33+(-10).*F12.*F21.*o35+2.*F21.*o80+o18.*o2.*o92); ... 
37,9,(1/60).*F11.*o131.*((-17).*F11.*F21.*o16+28.*F21.*o18.*o2+(-4).*F12.*(F22+o107).*o35+5.*F21.*o80+o86); ... 
37,10,(-1/20).*F22.*o131.*o140.*o35; ... 
37,11,(1/15).*F21.*o1; ... 
37,12,(1/60).*F21.*o141.*o3; ... 
37,13,(1/60).*o18.*o34.*o72; ... 
37,14,(1/60).*F22.*o142.*o35.*o78; ... 
37,15,(1/15).*F22.*o131.*(o20+o5).*o80; ... 
37,17,(-1/30).*F21.*o1; ... 
37,18,(-1/60).*F11.*F21.*o3; ... 
37,19,(-1/60).*F22.*o34.*o35; ... 
37,20,(-1/30).*F22.*o78.*o80; ... 
37,21,(-1/6).*F22.*o131.*o134; ... 
37,22,(-1).*F11.*o56.^2.*o78; ... 
37,23,(1/6).*o34.*(2.*o18.*o2+o33+(-1).*F12.*o35+o80); ... 
37,24,(1/6).*F11.*o78.*(9.*o18.*o2+o33+(-8).*F12.*o35+5.*o80+o94); ... 
37,25,(-1/15).*o17.*(o143+o144+o35); ... 
37,26,(-1/30).*o18.*o34.*(o22+o43+o59); ... 
37,27,(1/15).*o18.*(o144+(-9).*F11.*o2+(-10).*o35+o46).*o78; ... 
37,28,(1/20).*o3.*(o145+o18+o50); ... 
37,29,(1/60).*(o143+o146).*o17; ... 
37,30,(1/60).*o18.*o34.*(o47+o57+o59); ... 
37,31,(1/20).*o140.*o35.*o78; ... 
37,32,(-1/15).*o1.*o147; ... 
37,33,(-1/60).*F11.*o141.*o3; ... 
37,34,(-1/30).*o17.*o35; ... 
37,35,(1/60).*o30.*o34.*o35; ... 
37,36,(1/15).*(o54+o67).*o78.*o80; ... 
37,37,(1/6); ... 
37,38,(1/30).*F11.*o1; ... 
37,39,(1/60).*o18.*o3; ... 
37,40,(1/60).*o17.*o35; ... 
37,41,(1/30).*o34.*o80; ... 
37,42,(1/6).*o134.*o78; ... 
38,1,(1/5).*o78.*(o100.*o18.*o2+5.*F21.*o33+(-2).*F12.*o122.*o35+o121.*o80); ... 
38,2,(1/60).*o78.*((-25).*F11.*F21.*o16+(41.*F21+(-15).*F22).*o18.*o2+(-35).*F21.*o33+(-7).*F12.*o35.*(o107+o41)+9.*F21.*o80); ... 
38,3,(1/60).*o78.*(25.*F11.*F21.*o16+(-25).*F21.*o33+F12.*(59.*F21+45.*F22).*o35+(-3).*(48.*F22+o132).*o80+(-5).*o18.*o2.*(13.*F21+o89)); ... 
38,4,(1/30).*o78.*((-7).*F11.*F21.*o16+o148.*o18.*o2+13.*F21.*o33+(-5).*F12.*F21.*o35+F21.*o80); ... 
38,5,(1/60).*o78.*(39.*F11.*F21.*o16+9.*F21.*o33+3.*F12.*(15.*F21+7.*F22).*o35+(-11).*F21.*o80+o18.*o2.*((-61).*F21+o89)); ... 
38,6,(1/30).*o78.*(o136+(-8).*F11.*F21.*o16+9.*o137.*o18.*o2+(-1).*F12.*o35.*(33.*F22+o40)+2.*o80.*(o107+o84)); ... 
38,7,(-3/20).*F21.*o1; ... 
38,8,(1/60).*o78.*(14.*F11.*F21.*o16+(-2).*o148.*o18.*o2+(-17).*F21.*o33+10.*F12.*F21.*o35+(-2).*F21.*o80); ... 
38,9,(1/60).*o78.*(o136+(-32).*F11.*F21.*o16+3.*(18.*F21+(-1).*F22).*o18.*o2+(-1).*F12.*(40.*F21+21.*F22).*o35+10.*F21.*o80); ... 
38,10,(-1/20).*F22.*o152.*o18.*o78; ... 
38,12,o90; ... 
38,13,(1/30).*o17.*(2.*F11.*F12.*F21+3.*F22.*o18+o61); ... 
38,14,(1/20).*F22.*o154.*o18.*o34; ... 
38,15,(3/5).*F22.*o35.*(o24+o4).*o78; ... 
38,18,(-1/10).*F21.*o1; ... 
38,19,(-1/10).*F22.*o17.*o18; ... 
38,20,(-1/5).*F22.*o34.*o35; ... 
38,21,(-1).*F22.*o78.*o80; ... 
38,22,(-1).*o34.*(3.*o18.*o2+o33+(-2).*F12.*o35+o93); ... 
38,23,(1/6).*o17.*(o146+F12.*o18+4.*F11.*o2+o95); ... 
38,24,(1/6).*o34.*(17.*o18.*o2+2.*o33+(-15).*F12.*o35+18.*o80+o94); ... 
38,25,(-1/15).*o3.*(4.*F11.*F12+o38+o64); ... 
38,26,(-1/30).*o17.*((-1).*F12.*o18+12.*F11.*o2+15.*o35+o95); ... 
38,27,(1/15).*F11.*o34.*(33.*F12.*o18+(-25).*F11.*o2+(-42).*o35+o95); ... 
38,28,(1/20).*(7+o31); ... 
38,29,(1/60).*(o112+o151+o155).*o3; ... 
38,30,(1/60).*F11.*o17.*(27.*o18+o23+o38); ... 
38,31,(1/20).*o152.*o18.*o34; ... 
38,32,(-1/5); ... 
38,33,(-3/20).*o1.*o111; ... 
38,34,(-1/30).*F11.*o3.*o66; ... 
38,35,(1/20).*o156.*o17.*o18; ... 
38,36,(3/5).*o34.*o35.*(F12+o53); ... 
38,38,(1/5); ... 
38,39,(1/10).*F11.*o1; ... 
38,40,(1/10).*o18.*o3; ... 
38,41,(1/5).*o17.*o35; ... 
38,42,o34.*o80; ... 
39,1,(1/4).*o34.*(3.*F21.*o16+19.*F22.*o35+F12.*o18.*(F21+o79)+F11.*o2.*((-2).*F21+o89)); ... 
39,2,(1/12).*(o157+o159+(-5).*F21.*o16+F11.*((-9).*F22+o107).*o2).*o34; ... 
39,3,(1/12).*o34.*(o101+F12.*o138.*o18+F11.*(F21+(-18).*F22).*o2+(-1).*(F21+o160).*o35); ... 
39,4,o162; ... 
39,5,(1/12).*o34.*(F12.*o158.*o18+(-1).*F21.*o35+F11.*o2.*((-5).*F21+o58)+o87); ... 
39,6,(1/12).*F11.*o34.*((-1).*F11.*F12.*(o107+o120)+4.*o138.*o2+o18.*(o160+o62)); ... 
39,8,(-1/12).*o161.*o3; ... 
39,9,(1/12).*(o157+o159+(-3).*F21.*o16+F11.*((-3).*F22+o107).*o2).*o34; ... 
39,10,(-1/4).*F11.*F22.*o165.*o34; ... 
39,13,o162; ... 
39,14,(3/4).*F22.*o17.*o18; ... 
39,15,F22.*o142.*o18.*o34; ... 
39,19,(-1/4).*F11.*F22.*o3; ... 
39,20,(-1/2).*F22.*o17.*o18; ... 
39,21,(-5/2).*F22.*o34.*o35; ... 
39,22,(-2)+o11+o48; ... 
39,23,(1/6).*o3.*(5.*F11.*F12+o38+o49); ... 
39,24,(1/6).*o17.*((-5).*F12.*o18+13.*F11.*o2+30.*o35+o95); ... 
39,25,(-1/3).*o1.*o147; ... 
39,26,(-1/6).*o3.*(3.*F11.*F12+o22+o49); ... 
39,27,(-5/3).*F11.*o17.*(o2+o55+o64); ... 
39,28,(1/4); ... 
39,29,(5/12)+o69; ... 
39,30,(1/4)+o28+(5/6).*o18.*o3; ... 
39,31,(1/4).*F11.*o165.*o17; ... 
39,33,(-1/4); ... 
39,34,(-1/6).*o1.*o141; ... 
39,35,(-3/4).*o18.*o3; ... 
39,36,o17.*o18.*o30; ... 
39,39,(1/4); ... 
39,40,(1/4).*F11.*o1; ... 
39,41,(1/2).*o18.*o3; ... 
39,42,(5/2).*o17.*o35; ... 
40,1,F22.*o17.*(2.*F11.*F12+o49+o50); ... 
40,2,(-1/6).*F22.*o3.*(o27+o5); ... 
40,3,(-1/6).*F22.*o17.*(o151+o155+30.*o18); ... 
40,4,o166; ... 
40,5,(5/6).*F22.*o111.*o3; ... 
40,6,(1/3).*F22.*o17.*(o145+o50+o51); ... 
40,8,o167; ... 
40,9,(-1/6).*F22.*o3.*(o124+o5); ... 
40,10,(-1/2).*F22.*o168.*o17; ... 
40,13,o166; ... 
40,14,(1/6).*F22.*o3.*o66; ... 
40,15,(2/3).*F11.*F22.*o154.*o17; ... 
40,19,o167; ... 
40,20,(-2/3).*F11.*F22.*o3; ... 
40,21,(-10/3).*F22.*o17.*o18; ... 
40,22,(-3)+(-2).*F11.*o1+o65; ... 
40,23,(7/6)+o123; ... 
40,24,(11/6)+(7/6).*F11.*o1+o25; ... 
40,25,(-1/3); ... 
40,26,(-5/6).*o1.*o111; ... 
40,27,(-1)+o52+o65; ... 
40,29,(1/3); ... 
40,30,(1/2)+o123; ... 
40,31,(1/2).*o168.*o3; ... 
40,34,(-1/3); ... 
40,35,(-1/6).*o1.*o66; ... 
40,36,(2/3).*F11.*o156.*o3; ... 
40,40,(1/3); ... 
40,41,(2/3).*F11.*o1; ... 
40,42,(10/3).*o18.*o3; ... 
41,1,F22.*o3.*(o4+o54); ... 
41,2,o129; ... 
41,3,(-3/2).*F22.*o141.*o3; ... 
41,5,o126; ... 
41,6,F22.*o125.*o3; ... 
41,9,o129; ... 
41,10,(-1/2).*F22.*o128.*o3; ... 
41,14,o126; ... 
41,15,3.*F11.*F22.*o3; ... 
41,20,o129; ... 
41,21,(-5/2).*F11.*F22.*o3; ... 
41,22,o97; ... 
41,23,(1/2); ... 
41,24,(3/2)+o31; ... 
41,26,(-1/2); ... 
41,27,(-1)+o11; ... 
41,30,(1/2); ... 
41,31,(1/2)+o31; ... 
41,35,(-1/2); ... 
41,36,o11; ... 
41,41,(1/2); ... 
41,42,(5/2).*F11.*o1; ... 
42,1,o75; ... 
42,3,o76; ... 
42,6,o75; ... 
42,10,o76; ... 
42,15,o75; ... 
42,21,o76; ... 
42,22,(-1); ... 
42,24,1; ... 
42,27,(-1); ... 
42,31,1; ... 
42,36,(-1); ... 
42,42,1; ... 
];
T0=sparse(aux(:,1),aux(:,2),aux(:,3),42,42);