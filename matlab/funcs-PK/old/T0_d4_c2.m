o1=F21.^(-1);
o2=(1/6).*F12.*o1;
o3=(-1/2).*F12.*o1;
o4=(-2).*F22.*o1;
o5=F21.^2;
o6=o5.^(-1);
o7=2.*F22;
o8=(-1).*F22;
o9=F21+o8;
o10=(-1).*F11.*o1;
o11=F11.*o1;
o12=2.*F22.*o1;
o13=(-2).*F21;
o14=F22+o13;
o15=(-2).*F21.*F22;
o16=F22.^2;
o17=(-2).*F22;
o18=F21+o17;
o19=F21.^3;
o20=o19.^(-1);
o21=2.*o5;
o22=3.*o16;
o23=(-1/2).*F11.*o1;
o24=(-1).*F21.*F22;
o25=F21+F22;
o26=(-5).*F22;
o27=F21+o26;
o28=o16.*o6;
o29=(-3).*F21.*F22;
o30=o16+o21+o29;
o31=(-3).*F22;
o32=F21+o31;
o33=F22.^3;
o34=F21.^4;
o35=o34.^(-1);
o36=2.*o19;
o37=F11.*F21.*o16;
aux = [1,1,1; ... 
1,11,(-2/3).*F12.*o1; ... 
1,12,o2; ... 
1,13,o3; ... 
1,15,o2; ... 
2,1,(-1); ... 
2,2,1; ... 
2,12,o3; ... 
2,13,(1/2).*F12.*o1; ... 
2,15,o3; ... 
3,1,1+o4; ... 
3,2,(1/2).*F22.*o1; ... 
3,3,(1/2); ... 
3,11,F12.*F22.*o6; ... 
3,12,(-1/2).*F12.*F22.*o6; ... 
3,13,(1/2).*F12.*o6.*((-3).*F21+o7); ... 
3,15,(1/2).*F12.*o6.*o9; ... 
4,1,1; ... 
4,2,(-1); ... 
4,4,1; ... 
4,11,o10; ... 
4,12,o11; ... 
4,14,o10; ... 
5,1,o12; ... 
5,2,(1/2)+o4; ... 
5,3,(-1/2); ... 
5,4,F22.*o1; ... 
5,5,(1/2); ... 
5,11,(-1).*(F12.*F21+F11.*F22).*o6; ... 
5,12,(1/2).*(2.*F11+F12).*F22.*o6; ... 
5,13,(-1/2).*F12.*o14.*o6; ... 
5,14,(-1).*F11.*F22.*o6; ... 
5,15,(1/2).*F12.*o14.*o6; ... 
6,1,(o15+2.*o16+o5).*o6; ... 
6,2,(1/2).*F22.*o18.*o6; ... 
6,3,(-1/2).*F22.*o1; ... 
6,4,(1/3).*o16.*o6; ... 
6,5,(1/6).*F22.*o1; ... 
6,6,(1/3); ... 
6,11,(1/3).*F22.*((-1).*F11.*F22+F12.*o18).*o20; ... 
6,12,(1/6).*F22.*((-2).*F12.*F21+2.*F11.*F22+3.*F12.*F22).*o20; ... 
6,13,(-1/6).*F12.*o20.*((-12).*F21.*F22+5.*o16+6.*o5); ... 
6,14,(-1/3).*F11.*o16.*o20; ... 
6,15,(1/6).*F12.*o20.*((-6).*F21.*F22+o21+o22); ... 
7,1,(-1); ... 
7,2,1; ... 
7,4,(-1); ... 
7,7,1; ... 
7,11,o11; ... 
7,12,o10; ... 
7,14,o11; ... 
7,17,o10; ... 
8,1,(-1)+o4; ... 
8,2,(1/2)+o12; ... 
8,3,(1/2); ... 
8,4,o4; ... 
8,5,(-1/2); ... 
8,7,(3/2).*F22.*o1; ... 
8,8,(1/2); ... 
8,11,F11.*o6.*(F21+o7); ... 
8,12,(-1/2).*F11.*(F21+4.*F22).*o6; ... 
8,13,o23; ... 
8,14,2.*F11.*F22.*o6; ... 
8,15,(1/2).*F11.*o1; ... 
8,17,(-3/2).*F11.*F22.*o6; ... 
8,18,o23; ... 
9,1,(-1)+(-2).*o16.*o6; ... 
9,2,(1/2).*(4.*o16+o24+o5).*o6; ... 
9,3,(1/2).*o1.*o25; ... 
9,4,(1/3).*F22.*o27.*o6; ... 
9,5,(-1/6).*(F21+3.*F22).*o1; ... 
9,6,(-1/3); ... 
9,7,o28; ... 
9,8,(1/3).*F22.*o1; ... 
9,9,(1/3); ... 
9,11,(1/3).*(F12.*F21.*((-1).*F21+F22)+F11.*F22.*(F21+5.*F22)).*o20; ... 
9,12,(1/6).*F22.*o20.*((-10).*F11.*F22+F12.*o9); ... 
9,13,(1/6).*o20.*((-2).*F11.*F21.*F22+F12.*o30); ... 
9,14,(-1/3).*F11.*F22.*o20.*o27; ... 
9,15,(1/6).*o20.*(2.*F11.*F21.*F22+(-1).*F12.*o30); ... 
9,17,(-1).*F11.*o16.*o20; ... 
9,18,(-1/3).*F11.*F22.*o6; ... 
10,1,(-1).*F22.*o20.*(o16+o24+o5); ... 
10,2,(1/4).*F22.*o20.*(o15+o22+o5); ... 
10,3,(1/4).*(1+o28); ... 
10,4,(1/6).*o16.*o20.*o32; ... 
10,5,(-1/6).*o16.*o6; ... 
10,6,(-1/6).*o1.*o25; ... 
10,7,(1/4).*o20.*o33; ... 
10,8,(1/12).*o16.*o6; ... 
10,9,(1/12).*F22.*o1; ... 
10,10,(1/4); ... 
10,11,(1/6).*o35.*(3.*F11.*o33+F12.*((-1).*F21.*o16+o33+o36)); ... 
10,12,(1/12).*o35.*(F11.*(F21+(-6).*F22).*o16+(-1).*F12.*((-3).*F21.*o16+o19+2.*o33+F22.*o5)); ... 
10,13,(-1/12).*o35.*(o37+F12.*(10.*F21.*o16+6.*o19+(-3).*o33+(-10).*F22.*o5)); ... 
10,14,(-1/6).*F11.*o16.*o32.*o35; ... 
10,15,(1/12).*o35.*(o37+F12.*(6.*F21.*o16+(-2).*o33+o36+(-5).*F22.*o5)); ... 
10,16,o2; ... 
10,17,(-1/4).*F11.*o33.*o35; ... 
10,18,(-1/12).*F11.*o16.*o20; ... 
10,19,(-1/12).*F12.*o1; ... 
11,11,(-1/6); ... 
11,13,(-1/12); ... 
12,12,(-1/6); ... 
12,13,(1/6); ... 
12,15,(-1/6); ... 
13,11,(1/3); ... 
13,13,(-1/3); ... 
14,11,(1/3); ... 
14,12,(-1/3); ... 
14,14,(1/3); ... 
15,11,(-1); ... 
15,12,(1/2); ... 
15,13,(1/2); ... 
15,15,(-1/2); ... 
16,11,(1/3); ... 
16,13,(-1/3); ... 
16,16,(1/3); ... 
17,11,(-1/6); ... 
17,12,(1/6); ... 
17,14,(-1/6); ... 
17,17,(1/4); ... 
18,11,(-1); ... 
18,12,(2/3); ... 
18,13,(1/3); ... 
18,14,(-1/3); ... 
18,15,(-1/3); ... 
18,18,(1/3); ... 
19,11,(-1); ... 
19,12,(1/3); ... 
19,13,(2/3); ... 
19,15,(-1/3); ... 
19,16,(-1/3); ... 
19,19,(1/3); ... 
20,11,(-1/6); ... 
20,13,(1/6); ... 
20,16,(-1/6); ... 
20,20,(1/4); ... 
];
T0=sparse(aux(:,1),aux(:,2),aux(:,3),20,20);