o1=F22.^(-1);
o2=(-1).*F12;
o3=F11+o2;
o4=F12.*o1;
o5=(-1).*F22;
o6=F21+o5;
o7=o6.^2;
o8=F22.^2;
o9=o8.^(-1);
o10=(-1).*F21;
o11=F22+o10;
o12=6.*F21.*o11.*o9;
o13=F21.^2;
o14=(-1).*F12.*F21;
o15=F11.*F22;
o16=o14+o15;
o17=F12.*F21;
o18=(-2).*F21;
o19=F22+o18;
o20=F11.*o19;
o21=o17+o20;
o22=(-3).*o21.*o9;
o23=(-1).*F11.*F22;
o24=o17+o23;
o25=(1/2).*o24.*o9;
o26=o6.^3;
o27=F22.^3;
o28=o27.^(-1);
o29=F21.^3;
o30=3.*F11.*F21;
o31=(-2).*F12.*F21;
o32=o23+o30+o31;
o33=2.*F21;
o34=o33+o5;
o35=(-5/6).*o24.*o28.*o34;
o36=(-3).*F11.*F21;
o37=2.*F11.*F22;
o38=o17+o36+o37;
o39=10.*F21.*o11.*o9;
o40=(-5).*o21.*o9;
o41=o24.*o9;
o42=o6.^4;
o43=F22.^4;
o44=o43.^(-1);
o45=(-20).*F21.*o26.*o44;
o46=10.*o13.*o44.*o7;
o47=F21.^4;
o48=4.*F11.*F21;
o49=(-3).*F12.*F21;
o50=o23+o48+o49;
o51=5.*o44.*o50.*o7;
o52=3.*F21;
o53=o5+o52;
o54=2.*F11.*F21;
o55=o14+o23+o54;
o56=(-5).*F21.*o44.*o55.*o6;
o57=o16.*o9;
o58=(-1/3).*o24.*o28.*o34;
o59=(-2).*F22;
o60=o52+o59;
o61=(-3).*F11.*F22;
o62=(-4/3).*o24.*o28.*o34;
o63=F21.*o24.*o28;
o64=12.*F21.*o11.*o9;
o65=(-6).*o21.*o9;
o66=o6.^5;
o67=F22.^5;
o68=o67.^(-1);
o69=F21.^5;
o70=5.*F11.*F21;
o71=(-4).*F12.*F21;
o72=o23+o70+o71;
o73=(-1).*o24.*o44.*o7;
o74=4.*F21;
o75=o5+o74;
o76=(-2).*F11.*F22;
o77=o49+o70+o76;
o78=(1/5).*o24.*o44.*o53.*o6;
o79=o31+o61+o70;
o80=F11+F12;
o81=F22.*o80;
o82=o14+o81;
o83=(-3).*F22;
o84=o74+o83;
o85=(-5).*F11.*F21;
o86=4.*F11.*F22;
o87=o17+o85+o86;
o88=18.*o13.*o44.*o7;
o89=o24.*o28.*o6;
o90=(-9).*F21.*o44.*o55.*o6;
o91=(-1/2).*o24.*o28.*o34;
o92=(-4).*F11.*F21;
o93=3.*F11.*F22;
o94=o17+o92+o93;
o95=o13.*o24.*o44;
o96=o6.^6;
o97=F22.^6;
o98=o97.^(-1);
o99=(-4).*o26.*o29.*o98;
o100=F21.^6;
o101=F12.*o19;
o102=o101+o37;
o103=(-1/15).*o102.*o9;
o104=5.*F12.*F21;
o105=(-6).*F21;
o106=F22+o105;
o107=F11.*o106;
o108=o104+o107;
o109=5.*F21;
o110=o109+o5;
o111=o109+o59;
o112=2.*o13.*o55.*o7.*o98;
o113=F12.*o7;
o114=2.*F22;
o115=o10+o114;
o116=F11.*F22.*o115;
o117=o113+o116;
o118=o109+o83;
o119=o14+o30+o76;
o120=(-4).*F22;
o121=o109+o120;
o122=2.*o13;
o123=(-3).*F21.*F22;
o124=o122+o123+o8;
o125=(-1/5).*F21.*o24.*o44.*o60;
o126=20.*o11.*o29.*o44;
o127=3.*o13;
o128=(-4).*F21.*F22;
o129=o127+o128+o8;
o130=(-5).*o13.*o44.*o94;
o131=F22.^7;
o132=o131.^(-1);
o133=(-3).*F21;
o134=4.*F22;
o135=o133+o134;
o136=F11.*F22.*o135;
o137=F12.*o129;
o138=o136+o137;
o139=6.*F21;
o140=7.*F11.*F21;
o141=(-1).*F12.*o26;
o142=3.*o8;
o143=o123+o13+o142;
o144=F11.*F22.*o143;
o145=o141+o144;
o146=(-5).*F11.*F22;
o147=3.*F12.*F21;
aux = [1,1,1; ... 
2,1,7+(-7).*F21.*o1; ... 
2,2,F21.*o1; ... 
2,25,7.*o1.*o3; ... 
2,26,o4; ... 
2,27,(-1).*F11.*o1; ... 
3,2,1; ... 
4,1,21.*o7.*o9; ... 
4,2,o12; ... 
4,3,o13.*o9; ... 
4,25,(-21).*o3.*o6.*o9; ... 
4,26,3.*o16.*o9; ... 
4,27,o22; ... 
4,28,o4; ... 
4,29,o25; ... 
4,30,(-1).*F11.*F21.*o9; ... 
5,2,6+(-6).*F21.*o1; ... 
5,3,2.*F21.*o1; ... 
5,27,6.*o1.*o3; ... 
5,29,o4; ... 
5,30,(-2).*F11.*o1; ... 
6,3,1; ... 
7,1,(-35).*o26.*o28; ... 
7,2,15.*F21.*o28.*o7; ... 
7,3,5.*o11.*o13.*o28; ... 
7,4,o28.*o29; ... 
7,25,35.*o28.*o3.*o7; ... 
7,26,5.*o24.*o28.*o6; ... 
7,27,(-5).*o28.*o32.*o6; ... 
7,28,(5/3).*o16.*o9; ... 
7,29,o35; ... 
7,30,(-5/3).*F21.*o28.*o38; ... 
7,31,o4; ... 
7,32,(1/3).*o24.*o9; ... 
7,33,(1/3).*F21.*o24.*o28; ... 
7,34,(-1).*F11.*o13.*o28; ... 
8,2,15.*o7.*o9; ... 
8,3,o39; ... 
8,4,3.*o13.*o9; ... 
8,27,(-15).*o3.*o6.*o9; ... 
8,29,(5/2).*o16.*o9; ... 
8,30,o40; ... 
8,32,o4; ... 
8,33,o41; ... 
8,34,(-3).*F11.*F21.*o9; ... 
9,3,5+(-5).*F21.*o1; ... 
9,4,3.*F21.*o1; ... 
9,30,5.*o1.*o3; ... 
9,33,o4; ... 
9,34,(-3).*F11.*o1; ... 
10,4,1; ... 
11,1,35.*o42.*o44; ... 
11,2,o45; ... 
11,3,o46; ... 
11,4,4.*o11.*o29.*o44; ... 
11,5,o44.*o47; ... 
11,25,(-35).*o26.*o3.*o44; ... 
11,26,(-5).*o24.*o44.*o7; ... 
11,27,o51; ... 
11,28,(5/3).*o24.*o28.*o6; ... 
11,29,(5/6).*o24.*o44.*o53.*o6; ... 
11,30,o56; ... 
11,31,o57; ... 
11,32,o58; ... 
11,33,(-1/3).*F21.*o24.*o44.*o60; ... 
11,34,o13.*o44.*(o14+o48+o61); ... 
11,35,o4; ... 
11,36,(1/4).*o24.*o9; ... 
11,37,(1/6).*F21.*o24.*o28; ... 
11,38,(1/4).*o13.*o24.*o44; ... 
11,39,(-1).*F11.*o29.*o44; ... 
12,2,(-20).*o26.*o28; ... 
12,3,20.*F21.*o28.*o7; ... 
12,4,12.*o11.*o13.*o28; ... 
12,5,4.*o28.*o29; ... 
12,27,20.*o28.*o3.*o7; ... 
12,29,(10/3).*o24.*o28.*o6; ... 
12,30,(-20/3).*o28.*o32.*o6; ... 
12,32,(4/3).*o16.*o9; ... 
12,33,o62; ... 
12,34,(-4).*F21.*o28.*o38; ... 
12,36,o4; ... 
12,37,(2/3).*o24.*o9; ... 
12,38,o63; ... 
12,39,(-4).*F11.*o13.*o28; ... 
13,3,10.*o7.*o9; ... 
13,4,o64; ... 
13,5,6.*o13.*o9; ... 
13,30,(-10).*o3.*o6.*o9; ... 
13,33,2.*o16.*o9; ... 
13,34,o65; ... 
13,37,o4; ... 
13,38,(3/2).*o24.*o9; ... 
13,39,(-6).*F11.*F21.*o9; ... 
14,4,4+(-4).*F21.*o1; ... 
14,5,4.*F21.*o1; ... 
14,34,4.*o1.*o3; ... 
14,38,o4; ... 
14,39,(-4).*F11.*o1; ... 
15,5,1; ... 
16,1,(-21).*o66.*o68; ... 
16,2,15.*F21.*o42.*o68; ... 
16,3,(-10).*o13.*o26.*o68; ... 
16,4,6.*o29.*o68.*o7; ... 
16,5,3.*o11.*o47.*o68; ... 
16,6,o68.*o69; ... 
16,9,(3/5).*F12.*o1; ... 
16,10,(3/20).*o24.*o9; ... 
16,14,(-1/5).*F12.*o1; ... 
16,15,(1/10).*o16.*o9; ... 
16,25,21.*o3.*o42.*o68; ... 
16,26,3.*o24.*o26.*o68; ... 
16,27,(-3).*o26.*o68.*o72; ... 
16,28,o73; ... 
16,29,(-1/2).*o24.*o68.*o7.*o75; ... 
16,30,2.*F21.*o68.*o7.*o77; ... 
16,31,(3/5).*o24.*o28.*o6; ... 
16,32,o78; ... 
16,33,(3/5).*F21.*o24.*o34.*o6.*o68; ... 
16,34,(-6/5).*o13.*o6.*o68.*o79; ... 
16,35,(3/5).*o82.*o9; ... 
16,36,(-3/10).*o24.*o28.*o6; ... 
16,37,(-1/10).*F21.*o24.*o44.*o60; ... 
16,38,(-3/20).*o13.*o24.*o68.*o84; ... 
16,39,(-3/5).*o29.*o68.*o87; ... 
16,40,(1/10).*F21.*o24.*o28; ... 
16,41,(1/10).*o13.*o24.*o44; ... 
16,42,(1/5).*o24.*o29.*o68; ... 
16,43,(-1).*F11.*o47.*o68; ... 
17,2,15.*o42.*o44; ... 
17,3,o45; ... 
17,4,o88; ... 
17,5,12.*o11.*o29.*o44; ... 
17,6,5.*o44.*o47; ... 
17,10,(3/4).*F12.*o1; ... 
17,15,(-1/2).*F12.*o1; ... 
17,27,(-15).*o26.*o3.*o44; ... 
17,29,(-5/2).*o24.*o44.*o7; ... 
17,30,o51; ... 
17,32,o89; ... 
17,33,o24.*o44.*o53.*o6; ... 
17,34,o90; ... 
17,36,(3/4).*o82.*o9; ... 
17,37,o91; ... 
17,38,(-3/4).*F21.*o24.*o44.*o60; ... 
17,39,(-3).*o13.*o44.*o94; ... 
17,40,o25; ... 
17,41,(1/2).*F21.*o24.*o28; ... 
17,42,o95; ... 
17,43,(-5).*F11.*o29.*o44; ... 
18,3,(-10).*o26.*o28; ... 
18,4,18.*F21.*o28.*o7; ... 
18,5,18.*o11.*o13.*o28; ... 
18,6,10.*o28.*o29; ... 
18,30,10.*o28.*o3.*o7; ... 
18,33,2.*o24.*o28.*o6; ... 
18,34,(-6).*o28.*o32.*o6; ... 
18,37,o57; ... 
18,38,(-3/2).*o24.*o28.*o34; ... 
18,39,(-6).*F21.*o28.*o38; ... 
18,40,o4; ... 
18,41,o41; ... 
18,42,2.*F21.*o24.*o28; ... 
18,43,(-10).*F11.*o13.*o28; ... 
19,4,6.*o7.*o9; ... 
19,5,o64; ... 
19,6,10.*o13.*o9; ... 
19,34,(-6).*o3.*o6.*o9; ... 
19,38,(3/2).*o16.*o9; ... 
19,39,o65; ... 
19,41,o4; ... 
19,42,(2.*F12.*F21+o76).*o9; ... 
19,43,(-10).*F11.*F21.*o9; ... 
20,5,3+(-3).*F21.*o1; ... 
20,6,5.*F21.*o1; ... 
20,39,3.*o1.*o3; ... 
20,42,o4; ... 
20,43,(-5).*F11.*o1; ... 
21,6,1; ... 
22,1,7.*o96.*o98; ... 
22,2,(-6).*F21.*o66.*o98; ... 
22,3,5.*o13.*o42.*o98; ... 
22,4,o99; ... 
22,5,3.*o47.*o7.*o98; ... 
22,6,2.*o11.*o69.*o98; ... 
22,7,o100.*o98; ... 
22,9,(1/5).*o82.*o9; ... 
22,10,(-1/10).*o24.*o28.*o6; ... 
22,13,(1/3).*F12.*o1; ... 
22,14,o103; ... 
22,15,(1/30).*o24.*o28.*o60; ... 
22,18,(-1/6).*F12.*o1; ... 
22,19,(1/15).*o16.*o9; ... 
22,20,(1/20).*F21.*o16.*o28; ... 
22,25,(-7).*o3.*o66.*o98; ... 
22,26,(-1).*o24.*o42.*o98; ... 
22,27,(-1).*o108.*o42.*o98; ... 
22,28,(1/3).*o24.*o26.*o68; ... 
22,29,(1/6).*o110.*o24.*o26.*o98; ... 
22,30,(-5/3).*F21.*o26.*o32.*o98; ... 
22,31,(-1/5).*o24.*o44.*o7; ... 
22,32,(-1/15).*o24.*o68.*o7.*o75; ... 
22,33,(-2/15).*F21.*o111.*o24.*o7.*o98; ... 
22,34,o112; ... 
22,35,(1/5).*o117.*o28; ... 
22,36,(3/20).*o24.*o44.*o7; ... 
22,37,(1/10).*F21.*o24.*o34.*o6.*o68; ... 
22,38,(1/10).*o118.*o13.*o24.*o6.*o98; ... 
22,39,(-1).*o119.*o29.*o6.*o98; ... 
22,40,(-1/10).*F21.*o24.*o44.*o6; ... 
22,41,(-1/30).*o13.*o24.*o68.*o84; ... 
22,42,(-1/15).*o121.*o24.*o29.*o98; ... 
22,43,(-1/3).*((-6).*F11.*F21+5.*F11.*F22+o17).*o47.*o98; ... 
22,44,(1/20).*o13.*o24.*o44; ... 
22,45,(1/15).*o24.*o29.*o68; ... 
22,46,(1/6).*o24.*o47.*o98; ... 
22,47,(-1).*F11.*o69.*o98; ... 
23,2,(-6).*o66.*o68; ... 
23,3,10.*F21.*o42.*o68; ... 
23,4,(-12).*o13.*o26.*o68; ... 
23,5,12.*o29.*o68.*o7; ... 
23,6,10.*o11.*o47.*o68; ... 
23,7,6.*o68.*o69; ... 
23,10,(3/10).*o82.*o9; ... 
23,14,(2/5).*F12.*o1; ... 
23,15,(-1/5).*o102.*o9; ... 
23,19,(-2/5).*F12.*o1; ... 
23,20,(3/10).*o16.*o9; ... 
23,27,6.*o3.*o42.*o68; ... 
23,29,o24.*o26.*o68; ... 
23,30,(-2).*o26.*o68.*o72; ... 
23,32,(-2/5).*o24.*o44.*o7; ... 
23,33,(-2/5).*o24.*o68.*o7.*o75; ... 
23,34,(12/5).*F21.*o68.*o7.*o77; ... 
23,36,(3/10).*o117.*o28; ... 
23,37,o78; ... 
23,38,(9/10).*F21.*o124.*o24.*o68; ... 
23,39,(-12/5).*o13.*o6.*o68.*o79; ... 
23,40,(-2/5).*o24.*o28.*o6; ... 
23,41,o125; ... 
23,42,(-2/5).*o13.*o24.*o68.*o84; ... 
23,43,(-2).*o29.*o68.*o87; ... 
23,44,(3/10).*F21.*o24.*o28; ... 
23,45,(2/5).*o13.*o24.*o44; ... 
23,46,o24.*o29.*o68; ... 
23,47,(-6).*F11.*o47.*o68; ... 
24,3,5.*o42.*o44; ... 
24,4,(-12).*F21.*o26.*o44; ... 
24,5,o88; ... 
24,6,o126; ... 
24,7,15.*o44.*o47; ... 
24,15,(1/2).*F12.*o1; ... 
24,20,(-3/4).*F12.*o1; ... 
24,30,(-5).*o26.*o3.*o44; ... 
24,33,o73; ... 
24,34,3.*o44.*o50.*o7; ... 
24,37,(1/2).*o24.*o28.*o6; ... 
24,38,(3/4).*o129.*o24.*o44; ... 
24,39,o90; ... 
24,40,(1/2).*o82.*o9; ... 
24,41,o91; ... 
24,42,(-1).*F21.*o24.*o44.*o60; ... 
24,43,o130; ... 
24,44,(3/4).*o24.*o9; ... 
24,45,o63; ... 
24,46,(5/2).*o13.*o24.*o44; ... 
24,47,(-15).*F11.*o29.*o44; ... 
25,4,(-4).*o26.*o28; ... 
25,5,12.*F21.*o28.*o7; ... 
25,6,20.*o11.*o13.*o28; ... 
25,7,20.*o28.*o29; ... 
25,34,4.*o28.*o3.*o7; ... 
25,38,o89; ... 
25,39,(-4).*o28.*o32.*o6; ... 
25,41,(2/3).*o16.*o9; ... 
25,42,o62; ... 
25,43,(-20/3).*F21.*o28.*o38; ... 
25,44,o4; ... 
25,45,(4/3).*o24.*o9; ... 
25,46,(10/3).*F21.*o24.*o28; ... 
25,47,(-20).*F11.*o13.*o28; ... 
26,5,3.*o7.*o9; ... 
26,6,o39; ... 
26,7,15.*o13.*o9; ... 
26,39,(-3).*o3.*o6.*o9; ... 
26,42,o57; ... 
26,43,o40; ... 
26,45,o4; ... 
26,46,(5/2).*o24.*o9; ... 
26,47,(-15).*F11.*F21.*o9; ... 
27,6,2+(-2).*F21.*o1; ... 
27,7,6.*F21.*o1; ... 
27,43,2.*o1.*o3; ... 
27,46,o4; ... 
27,47,(-6).*F11.*o1; ... 
28,7,1; ... 
29,1,(-1).*o132.*o6.^7; ... 
29,2,F21.*o132.*o96; ... 
29,3,(-1).*o13.*o132.*o66; ... 
29,4,o132.*o29.*o42; ... 
29,5,(-1).*o132.*o26.*o47; ... 
29,6,o132.*o69.*o7; ... 
29,7,o100.*o11.*o132; ... 
29,8,F21.^7.*o132; ... 
29,9,(1/35).*o117.*o28; ... 
29,10,(3/140).*o24.*o44.*o7; ... 
29,13,(1/21).*o82.*o9; ... 
29,14,(-1/105).*o138.*o28; ... 
29,15,(1/70).*o124.*o16.*o44; ... 
29,17,(1/7).*F12.*o1; ... 
29,18,(-1/42).*o102.*o9; ... 
29,19,(1/105).*o24.*o28.*o60; ... 
29,20,(3/140).*F21.*o24.*o44.*o6; ... 
29,22,(-1/7).*F12.*o1; ... 
29,23,(1/21).*o16.*o9; ... 
29,24,(1/35).*F21.*o16.*o28; ... 
29,25,o132.*o3.*o96; ... 
29,26,(1/7).*o132.*o24.*o66; ... 
29,27,(1/7).*(6.*F12.*F21+F11.*((-7).*F21+F22)).*o132.*o66; ... 
29,28,(-1/21).*o24.*o42.*o98; ... 
29,29,(-1/42).*o132.*o24.*o42.*(o139+o5); ... 
29,30,(1/7).*F21.*o132.*o42.*((-5).*F12.*F21+o140+o76); ... 
29,31,(1/35).*o24.*o26.*o68; ... 
29,32,(1/105).*o110.*o24.*o26.*o98; ... 
29,33,(1/21).*F21.*o132.*o24.*o26.*o53; ... 
29,34,(-1/7).*o13.*o132.*o26.*(o140+o61+o71); ... 
29,35,(1/35).*o145.*o44; ... 
29,36,(-1/35).*o24.*o26.*o68; ... 
29,37,(-1/105).*F21.*o111.*o24.*o7.*o98; ... 
29,38,(-1/14).*o13.*o132.*o24.*o34.*o7; ... 
29,39,(1/7).*o132.*o29.*((-4).*F11.*F22+o140+o49).*o7; ... 
29,40,(1/35).*F21.*o24.*o68.*o7; ... 
29,41,(1/105).*o118.*o13.*o24.*o6.*o98; ... 
29,42,(1/21).*o132.*o24.*o29.*o6.*o60; ... 
29,43,(-1/7).*o132.*(o140+o146+o31).*o47.*o6; ... 
29,44,(-1/140).*o13.*o24.*o68.*o84; ... 
29,45,(-1/105).*o121.*o24.*o29.*o98; ... 
29,46,(-1/42).*o132.*((-5).*F22+o139).*o24.*o47; ... 
29,47,(-1/7).*o132.*((-7).*F11.*F21+6.*F11.*F22+o17).*o69; ... 
29,48,(1/35).*o13.*o24.*o44; ... 
29,49,(1/35).*o24.*o29.*o68; ... 
29,50,(1/21).*o24.*o47.*o98; ... 
29,51,(1/7).*o132.*o24.*o69; ... 
29,52,(-1).*F11.*o100.*o132; ... 
30,2,o96.*o98; ... 
30,3,(-2).*F21.*o66.*o98; ... 
30,4,3.*o13.*o42.*o98; ... 
30,5,o99; ... 
30,6,5.*o47.*o7.*o98; ... 
30,7,6.*o11.*o69.*o98; ... 
30,8,7.*o100.*o98; ... 
30,10,(1/20).*o117.*o28; ... 
30,14,(1/15).*o82.*o9; ... 
30,15,(-1/30).*o138.*o28; ... 
30,18,(1/6).*F12.*o1; ... 
30,19,o103; ... 
30,20,(1/10).*o24.*o28.*o6; ... 
30,23,(-1/3).*F12.*o1; ... 
30,24,(1/5).*o16.*o9; ... 
30,27,(-1).*o3.*o66.*o98; ... 
30,29,(-1/6).*o24.*o42.*o98; ... 
30,30,(-1/3).*o108.*o42.*o98; ... 
30,32,(1/15).*o24.*o26.*o68; ... 
30,33,(1/15).*o110.*o24.*o26.*o98; ... 
30,34,(-1).*F21.*o26.*o32.*o98; ... 
30,36,(1/20).*o145.*o44; ... 
30,37,(-1/30).*o24.*o68.*o7.*o75; ... 
30,38,(-1/10).*F21.*o111.*o24.*o7.*o98; ... 
30,39,o112; ... 
30,40,(1/10).*o24.*o44.*o7; ... 
30,41,(1/10).*F21.*o124.*o24.*o68; ... 
30,42,(2/15).*o118.*o13.*o24.*o6.*o98; ... 
30,43,(-5/3).*o119.*o29.*o6.*o98; ... 
30,44,(-1/20).*F21.*o24.*o44.*o60; ... 
30,45,(-1/15).*o13.*o24.*o68.*o84; ... 
30,46,(-1/6).*o121.*o24.*o29.*o98; ... 
30,47,(6.*F11.*F21+o14+o146).*o47.*o98; ... 
30,48,(1/5).*F21.*o24.*o28; ... 
30,49,(1/5).*o13.*o24.*o44; ... 
30,50,(1/3).*o24.*o29.*o68; ... 
30,51,o24.*o47.*o98; ... 
30,52,(-7).*F11.*o69.*o98; ... 
31,3,(-1).*o66.*o68; ... 
31,4,3.*F21.*o42.*o68; ... 
31,5,(-6).*o13.*o26.*o68; ... 
31,6,10.*o29.*o68.*o7; ... 
31,7,15.*o11.*o47.*o68; ... 
31,8,21.*o68.*o69; ... 
31,15,(1/10).*o82.*o9; ... 
31,19,(1/5).*F12.*o1; ... 
31,20,(1/20).*(o147+(-3).*F22.*o80).*o9; ... 
31,24,(-3/5).*F12.*o1; ... 
31,30,o3.*o42.*o68; ... 
31,33,(1/5).*o24.*o26.*o68; ... 
31,34,(-3/5).*o26.*o68.*o72; ... 
31,37,(-1/10).*o24.*o44.*o7; ... 
31,38,(-3/20).*o24.*o68.*o7.*o75; ... 
31,39,(6/5).*F21.*o68.*o7.*o77; ... 
31,40,(1/10).*o117.*o28; ... 
31,41,(1/10).*o129.*o24.*o44; ... 
31,42,(3/5).*F21.*o124.*o24.*o68; ... 
31,43,(-2).*o13.*o6.*o68.*o79; ... 
31,44,(-3/20).*o24.*o28.*o34; ... 
31,45,o125; ... 
31,46,(-1/2).*o13.*o24.*o68.*o84; ... 
31,47,(-3).*o29.*o68.*o87; ... 
31,48,(3/5).*o24.*o9; ... 
31,49,(3/5).*F21.*o24.*o28; ... 
31,50,o95; ... 
31,51,3.*o24.*o29.*o68; ... 
31,52,(-21).*F11.*o47.*o68; ... 
32,4,o42.*o44; ... 
32,5,(-4).*F21.*o26.*o44; ... 
32,6,o46; ... 
32,7,o126; ... 
32,8,35.*o44.*o47; ... 
32,34,(-1).*o26.*o3.*o44; ... 
32,38,(-1/4).*o24.*o44.*o7; ... 
32,39,o44.*o50.*o7; ... 
32,41,(1/6).*o24.*o28.*o6; ... 
32,42,(1/3).*o129.*o24.*o44; ... 
32,43,o56; ... 
32,44,(1/4).*o16.*o9; ... 
32,45,o58; ... 
32,46,(-5/6).*F21.*o24.*o44.*o60; ... 
32,47,o130; ... 
32,48,o4; ... 
32,49,o41; ... 
32,50,(5/3).*F21.*o24.*o28; ... 
32,51,5.*o13.*o24.*o44; ... 
32,52,(-35).*F11.*o29.*o44; ... 
33,5,(-1).*o26.*o28; ... 
33,6,5.*F21.*o28.*o7; ... 
33,7,15.*o11.*o13.*o28; ... 
33,8,35.*o28.*o29; ... 
33,39,o28.*o3.*o7; ... 
33,42,(1/3).*o24.*o28.*o6; ... 
33,43,(-5/3).*o28.*o32.*o6; ... 
33,45,(1/3).*o16.*o9; ... 
33,46,o35; ... 
33,47,(-5).*F21.*o28.*o38; ... 
33,49,o4; ... 
33,50,(5/3).*o24.*o9; ... 
33,51,5.*F21.*o24.*o28; ... 
33,52,(-35).*F11.*o13.*o28; ... 
34,6,o7.*o9; ... 
34,7,o12; ... 
34,8,21.*o13.*o9; ... 
34,43,(-1).*o3.*o6.*o9; ... 
34,46,(1/2).*o16.*o9; ... 
34,47,o22; ... 
34,50,o4; ... 
34,51,(o147+o61).*o9; ... 
34,52,(-21).*F11.*F21.*o9; ... 
35,7,1+(-1).*F21.*o1; ... 
35,8,7.*F21.*o1; ... 
35,47,o1.*o3; ... 
35,51,o4; ... 
35,52,(-7).*F11.*o1; ... 
36,8,1; ... 
37,12,(1/35); ... 
37,25,(-1); ... 
37,26,(1/7); ... 
37,27,(-1/7); ... 
37,29,(1/42); ... 
37,30,(-1/21); ... 
37,33,(1/105); ... 
37,34,(-1/35); ... 
37,38,(1/140); ... 
38,11,(1/20); ... 
38,26,(-1); ... 
38,28,(1/3); ... 
38,29,(-1/6); ... 
38,32,(1/15); ... 
38,33,(-1/15); ... 
38,37,(1/30); ... 
39,12,(1/5); ... 
39,27,(-1); ... 
39,29,(1/6); ... 
39,30,(-1/3); ... 
39,33,(1/15); ... 
39,34,(-1/5); ... 
39,38,(1/20); ... 
40,16,(1/10); ... 
40,28,(-1); ... 
40,31,(3/5); ... 
40,32,(-1/5); ... 
40,36,(3/20); ... 
40,37,(-1/10); ... 
40,40,(1/10); ... 
41,11,(3/10); ... 
41,29,(-1); ... 
41,32,(2/5); ... 
41,33,(-2/5); ... 
41,37,(1/5); ... 
42,12,(3/5); ... 
42,30,(-1); ... 
42,33,(1/5); ... 
42,34,(-3/5); ... 
42,38,(3/20); ... 
43,10,(1/4); ... 
43,31,(-1); ... 
43,35,1; ... 
44,16,(1/2); ... 
44,32,(-1); ... 
44,36,(3/4); ... 
44,37,(-1/2); ... 
44,40,(1/2); ... 
45,11,(3/4); ... 
45,33,(-1); ... 
45,37,(1/2); ... 
46,12,1; ... 
46,34,(-1); ... 
46,38,(1/4); ... 
47,9,1; ... 
48,10,1; ... 
49,16,1; ... 
49,37,(-1); ... 
49,40,1; ... 
50,11,1; ... 
51,12,1; ... 
52,13,1; ... 
53,14,1; ... 
54,15,1; ... 
55,16,1; ... 
56,11,(3/4); ... 
56,38,(3/4); ... 
56,41,(-1/2); ... 
57,12,(3/5); ... 
57,39,(3/5); ... 
57,42,(-1/5); ... 
58,17,1; ... 
59,18,1; ... 
60,19,1; ... 
61,20,1; ... 
62,16,(1/2); ... 
62,41,(1/2); ... 
62,44,(-3/4); ... 
63,11,(3/10); ... 
63,38,(3/10); ... 
63,41,(-1/5); ... 
63,42,(2/5); ... 
63,45,(-2/5); ... 
64,12,(1/5); ... 
64,39,(1/5); ... 
64,42,(-1/15); ... 
64,43,(1/3); ... 
64,46,(-1/6); ... 
65,21,1; ... 
66,22,1; ... 
67,23,1; ... 
68,24,1; ... 
69,20,(1/4); ... 
69,44,(1/4); ... 
69,48,(-1); ... 
70,16,(1/10); ... 
70,41,(1/10); ... 
70,44,(-3/20); ... 
70,45,(1/5); ... 
70,49,(-3/5); ... 
71,11,(1/20); ... 
71,38,(1/20); ... 
71,41,(-1/30); ... 
71,42,(1/15); ... 
71,45,(-1/15); ... 
71,46,(1/6); ... 
71,50,(-1/3); ... 
72,12,(1/35); ... 
72,39,(1/35); ... 
72,42,(-1/105); ... 
72,43,(1/21); ... 
72,46,(-1/42); ... 
72,47,(1/7); ... 
72,51,(-1/7); ... 
73,25,1; ... 
74,26,1; ... 
75,27,1; ... 
76,28,1; ... 
77,29,1; ... 
78,30,1; ... 
79,31,1; ... 
80,32,1; ... 
81,33,1; ... 
82,34,1; ... 
83,35,1; ... 
84,36,1; ... 
85,37,1; ... 
86,38,1; ... 
87,39,1; ... 
88,9,(3/5); ... 
88,14,(-1/5); ... 
88,35,(3/5); ... 
89,10,(3/4); ... 
89,15,(-1/2); ... 
89,36,(3/4); ... 
90,40,1; ... 
91,41,1; ... 
92,42,1; ... 
93,43,1; ... 
94,9,(1/5); ... 
94,13,(1/3); ... 
94,14,(-1/15); ... 
94,18,(-1/6); ... 
94,35,(1/5); ... 
95,10,(3/10); ... 
95,14,(2/5); ... 
95,15,(-1/5); ... 
95,19,(-2/5); ... 
95,36,(3/10); ... 
96,15,(1/2); ... 
96,20,(-3/4); ... 
96,40,(1/2); ... 
97,44,1; ... 
98,45,1; ... 
99,46,1; ... 
100,47,1; ... 
101,9,(1/35); ... 
101,13,(1/21); ... 
101,14,(-1/105); ... 
101,17,(1/7); ... 
101,18,(-1/42); ... 
101,22,(-1/7); ... 
101,35,(1/35); ... 
102,10,(1/20); ... 
102,14,(1/15); ... 
102,15,(-1/30); ... 
102,18,(1/6); ... 
102,19,(-1/15); ... 
102,23,(-1/3); ... 
102,36,(1/20); ... 
103,15,(1/10); ... 
103,19,(1/5); ... 
103,20,(-3/20); ... 
103,24,(-3/5); ... 
103,40,(1/10); ... 
104,48,1; ... 
105,49,1; ... 
106,50,1; ... 
107,51,1; ... 
108,52,1; ... 
];
TD=sparse(aux(:,1),aux(:,2),aux(:,3),108,52);