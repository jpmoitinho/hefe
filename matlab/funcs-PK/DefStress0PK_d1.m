function [fp] = DefStress0PK_d1()
fp{1} = @DefStress0PK_d1_c1;
fp{2} = @DefStress0PK_d1_c2;
fp{3} = @DefStress0PK_d1_c3;
fp{4} = @DefStress0PK_d1_c4;
end

function [T0,L0] = DefStress0PK_d1_c1(F11,F21,F12,F22)
L0=[1,6];
aux = [1,1,(-0.100000000000000E1); ... 
2,1,(-0.100000000000000E1).*F11.^(-1).*F21; ... 
2,2,0.100000000000000E1; ... 
];
T0=sparse(aux(:,1),aux(:,2),aux(:,3),2,2);
end

function [T0,L0] = DefStress0PK_d1_c2(F11,F21,F12,F22)
L0=[6,1];
aux = [1,1,0.100000000000000E1; ... 
1,2,(-0.100000000000000E1).*F12.*F21.^(-1); ... 
2,2,(-0.100000000000000E1); ... 
];
T0=sparse(aux(:,1),aux(:,2),aux(:,3),2,2);
end

function [T0,L0] = DefStress0PK_d1_c3(F11,F21,F12,F22)
L0=[1,8];
aux = [1,1,(-0.100000000000000E1); ... 
2,1,(-0.100000000000000E1).*F12.^(-1).*F21; ... 
2,2,0.100000000000000E1; ... 
];
T0=sparse(aux(:,1),aux(:,2),aux(:,3),2,2);
end

function [T0,L0] = DefStress0PK_d1_c4(F11,F21,F12,F22)
L0=[2,4];
aux = [1,1,0.100000000000000E1; ... 
1,2,(-0.100000000000000E1).*F12.*F22.^(-1); ... 
2,2,(-0.100000000000000E1); ... 
];
T0=sparse(aux(:,1),aux(:,2),aux(:,3),2,2);
end

