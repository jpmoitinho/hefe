function [f] = Basis_d1
f = @Basis;
end

function [phi] = Basis(L2,L3)
phi=[0.100000000000000E1+(-0.100000000000000E1).*L2+(-0.100000000000000E1).*L3,L2,L3];
end
