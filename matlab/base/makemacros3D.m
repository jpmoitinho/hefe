function nameout = makemacros3D(namein)

aux = extractBetween(namein,1,".msh");
% base = [aux{1} '%s.msh'];

% base = '../3D/data/cube2%s.msh';
% base = '../3D/data/Test%s.msh';
% base = '../3D/data/8Tet%s.msh';
% base = '../3D/data/Cube%s.msh';

% namein = sprintf(base, '');
nameout = sprintf('%s-m.msh',aux{1});

m = chf_gmsh();
m.load(namein);

out = fopen(nameout, 'w');

for i=1:m.nc
    [~, ~, verts] = m.get_fev_c(i);
    if (length(verts) ~= 4)
        fprintf ('Element %d (%d) has %d verts instead of 4\n', ...
            i, m.elems_faces(i), length(verts));
    end
end

fprintf(out, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n$Nodes\n%d\n', ...
    m.nv+m.nc);

for i=1:m.nv
    node = m.nodes_verts(i);
    fprintf(out, '%d %f %f %f\n', i , m.coords(i,:));
end

for i=1:m.nc
    [~, ~, verts] = m.get_fev_c(i);
    nodes = m.nodes_verts(verts);
    pos = sum(m.coords(nodes,:))/4;
    fprintf(out, '%d %f %f %f\n', m.nv+i , pos);
end

fprintf(out, '$EndNodes\n$Elements\n%d\n', ...
    m.n_elem_by_dimension(3)+m.nc*4);

count = 0;
for i=1:m.n_elem
    if (m.ele_infos(i,2) == 2) % triangle
        count = count + 1;
        fprintf(out, '%d 2 %d', count, ...
            length(m.ele_tags{i}));
        fprintf(out, ' %d', m.ele_tags{i});
        fprintf(out, ' %d %d %d\n', m.ele_nodes{i});
    end
end

subcell = [ 1 3 2; 2 3 4; 3 1 4; 4 1 2];
for i=1:m.nc
    [~, ~, verts] = m.get_fev_c(i);
    elem = m.elems_cells(i);
    for k = 1:4
        count = count + 1;
        fprintf(out, '%d 4 %d', count, length(m.ele_tags{elem}));
        fprintf(out, ' %d', m.ele_tags{elem});
        fprintf(out, ' %d %d %d %d\n', m.nv+i, verts(subcell(k,:)));
    end
end

fprintf(out, '$EndElements\n');

fclose(out);
