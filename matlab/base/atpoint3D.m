function [ val ] = atpoint3D( str, coords )
%atpoint Evaluates the function handle at point coords

f = inline(str, 'x', 'y', 'z', 'zero', 'one' );
one = ones(size(coords,1),1);
zero = zeros(size(one));

val = f(coords(:,1), coords(:,2), coords(:,3), zero, one);
end
