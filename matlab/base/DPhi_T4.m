function [ DPhi_T4 ] = DPhi_T4(~)
% FFORMA Calcula as derivadas das funcoes de forma no elemento mestre
%  Linha 1 -> d Phi/d chi
%  Linha 2 -> d Phi/d eta

%  Coluna 1 -> Phi 1
%  Coluna 2 -> Phi 2
%  Coluna 3 -> Phi 3

DPhi_T4 = zeros(3,4);

DPhi_T4(1,1) = -1;
DPhi_T4(1,2) = 1;

DPhi_T4(2,1) = -1;
DPhi_T4(2,3) = 1;

DPhi_T4(3,1) = -1;
DPhi_T4(3,4) = 1;
