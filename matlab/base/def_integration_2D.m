% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ Rule2D, Rule1D ] = def_integration_2D( degree2D, degree1D )
%Define the numerical integration rules


% RULES FOR TRIS
aux = set_xiao_gimbutas_standard(degree2D);

Rule2D.xieta = aux(:,1:2)';
Rule2D.GW =  aux(:,3)';
Rule2D.npts = size(aux,1);

% the weights are adjusted for the triangle
% Rule2D.GW = Rule2D.GW / 2;

% Integration rule for the sides
rule = ((degree1D - mod(degree1D,2))/2) + 1;

[Rule1D.xi, Rule1D.GW] = Gauss(rule);
% Adjust for the line [-1, 1]
Rule1D.GW = Rule1D.GW / 2;
Rule1D.npts = rule;
