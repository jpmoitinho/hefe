% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [P] = load_problem (filename, P)
% Load the data for a problem
%
% This files creates a generic structure with properties that are
% assigned to the topological entities according to their tags.
% If the structure already exists new fields will be added, old ones
% will be overwritten.
% Data checking is very primitive
% We can check for existing fields with isfield(Prob,'Edges')
% 
fid = fopen(filename, 'r');

if (~exist('P','var'))
    P = struct();
end

tline = fgetl(fid);

while (~feof(fid))
    if (size(tline,2) > 0)
        parsed{1} = sscanf(tline, '*%s {%*[^}]}');
        parsed{2} = sscanf(tline, '*%*s {%[^}]}');
        if (size(parsed{2},1) == 1) % Second item was found
            Block = strcat('P.', parsed{1});
            form = strcat(['%d ', parsed{2}]); % horiz cat to preserve blank
            
            % Reading the block is similar, whatever the entity
            aux = textscan(fid, form); %#ok<NASGU> (aux is used in eval)
            eval(strcat(Block,'.n = size(aux{1},1);'));
            eval(strcat(Block,'.tags = double(aux{1});'));
            eval(strcat(Block,'.vals = aux(2:end);'));
        end
    end
    tline = fgetl(fid);
end

fclose(fid);

return