% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ val ] = atpoint2D( str, coords )
%atpoint Evaluates the function handle at point coords

f = str2func(['@(x, y, zero, one)', str]);
one = ones(size(coords,1),1);
zero = zeros(size(one));

val = f(coords(:,1), coords(:,2), zero, one);
end

