%% CHE Compact Half-Edge (CHE) data structure abstract class.
%
%% Copyright notice
%%%
% * (c) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * (c) Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% Creates a structure for two-manifold meshes with cells that may be
% any polygon (arbitrary number of vertices of a cell).
% A valid two-manifold mesh can only have two adjacent cells for an
% interior edge and one adjacent cell for a boundary edge.
% Furthermore, the vertices of each cell are ordered in a consistent
% order (either clockwise or counter-clockwise).
%
% An object of this class stores:
%%%
% * the mate half-edge of each half-edge on the adjacent cell (vector *other*)
% * the offset index for cell half-edges (vector *offset_hec*)
% * the index of one of the half-edges of each vertex (vector *he_v*)
% * the index of the edge of each half-edge (vector *e_he*)
% * the index of one of the two half-edges of each edge (vector *he_e*)
% * the indices of the two vertices of each edge (vector *vs_e*)
%
% These vectors are created based on a conventional mesh representation
% in which the vertex adjacency information of cells is known.  This
% mesh representation is handled in an abstract fashion in this class.
% For this, two abstract methods (functions) are defined:
%%% 
% * *get_nv_cell*, which returns the number of vertices of a cell
% * *get_v_cell*, which returns a vertex of a cell
%
% To use this class, clients should create a subclass of this class
% in which these two abstract methods are implemented.
%
% Note that this (super)class does have a matrix property for storing 
% the vertex incidence lists of the mesh cells.  This conventional
% vertex-cell adjacency information is used generically through the
% abstract methods *get_nv_cell* and *get_v_cell*.
%
% Vector *offset_hec* holds the offset index of the first half-edge
% of each cell.
%
% An element of vector *he_v* holds, for a boundary vertex, the only 
% half-edge of that vertex that is on boundary and, for an interior
% vertex, any of the half-edges of that vertex.
%
%% References
% * Estruturas de Dados Topologicas Escalonaveis para Variedades de
% Dimensao 2 e 3.
% Pontificia Universidade Catolica do Rio de Janeiro - PUC-Rio.
% Marcos de Oliveira Lage Ferreira.
% Advisor: Helio Cortes Vieira Lopes.
% http://www.maxwell.lambda.ele.puc-rio.br/Busca_etds.php?strSecao=resultado&nrSeq=8176@1.
% Technical report: preprint number Mat.13/2005.
% Department of Mathematics - PUC-Rio.
% http://zeus.mat.puc-rio.br/tomlew/pdfs/che_puc.pdf
%
% * Lage, M.; Martha, L.F.; Moitinho de Almeida, J.P.; Lopes, H.,
% �IBHM: index-based data structures for 2D and 3D hybrid meshes�,
% Engineering with Computers, Vol. 33, Issue 4, pp. 727-744.
% Springer-Verlag London,
% ISSN: 0177-0667, DOI: 10.1007/s00366-015-0395-0.
% http://link.springer.com/article/10.1007%2Fs00366-015-0395-0
%
%% Notes
%%%
% 1.
% Different than the original CHE data structure, the current CHE does
% not have a vector for holding the number of the initial vertex of
% each half-edge.  Instead, it uses the *offset_hec* vector, that holds
% the offset index of the first half-edge of each cell.  Using this
% offset vector allows the use of cells with any number of vertices
% and stored by clients in any order.
%
% 2.
% This (super)class does not handle any geometry information.
% If a client wants to extend it to also store vertex coordinates,
% it should create a subclass for doing this.
%
% 3.
% This version of CHE corresponds to level 3 of the original CHE.
% In this level, cells, edges, vertices, and boundary curves (connected
% portions of mesh boundary) are represented explicitly, and it is 
% possible to access adjacency information among these entities locally
% (without global searches).
%
% 4.
% This version of CHE uses an explicit storage of the cell of each
% half-edge (vector *cell_he*). The article "IBHM: index-based data
% structures for 2D and 3D hybrid meshes" describes three alternatives
% for accessing a cell for a given half-edge: explicit access; sum of
% terms of *offset_hec* vector - "cell_id = sum(obj.offset_hec < he);";
% and a binary search equivalent to "sum(obj.offset_hec < he);".
% In this version, only the first alternative is implemented.
%
% 5.
% This implementation of CHE uses an auxiliary temporary container map:
% *adj_he_map*. This container map is used to build the adjacency
% information of mate half-edges stored in vector *other*.
% A key value (that identifies an edge) for acessing the container
% *hf_he_map* is defined using an auxiliary class (static) function:
% *key = che_key(vs, nv)*, in which *vs* is a vector with indices of the
% two vertices of an edge and *nv* is the total number of vertices in the
% mesh.
%
%% History
%
% @VERSION 1.00 on 09-May-2012. Initial version.
%
% @VERSION 1.01 on 10-Dec-2012.
% Modifications:
% Replaced "face" by "cell".
%
% @VERSION 1.02 on 23-Jun-2013.
% Modifications:
% Set protected access to properties: *nv*, *nc*, *ne*.
% Created get methods.
% Renamed *offset_hes* by *offset_he_c*.
% Created private methods *build_mate_adjacency* and *build_edge_arrays*
% that are called by public method build.
% Implemented vector of boundary curves that holds the index of a half-edge
% on a connected portion of current mesh.
%
% @VERSION 1.03 on 24-Jun-2013.
% Modifications:
% Created three alternatives for function *get_cell_he* that returns the
% index of a cell for a given half-edge:
% 1 - explicit, 2 - sum; 3 - binary search.
%
% @VERSION 1.04 on 07-Jul-2013.
% Modifications:
% Renamed name of vector *offset_he_c* to *offset_hec*.
% Created a set method for property *get_cell_he_opt*.
% Made some minor comment modifications.
% In build method, renamed variable *aux* to *nv_c*.
%
% @VERSION 1.05 on 16-Jul-2013.
% Modifications:
% Modified function *get_cell_he_bsearch* to handle the case of just one cell.
%
% @VERSION 2.00 on 30-Jul-2019.
% Modifications:
% Formating comments for Publish command.
% Made this class a subclass of *handle* instead of *hgsetget*, i.e.,
% currently this is a handle class without set and get methods. 
% Got rid of function *get_cell_he* that returned the index of a cell for a
% given half-edge. Previously, this function used three alternatives:
% 1 - explicit, 2 - sum; 3 - binary search. Currently, this version of CHE
% uses an explicit storage of the cell of each half-edge (vector *cell_he*).
%
%% Class definition
classdef che < handle
    %%
    % <https://www.mathworks.com/help/matlab/ref/handle-class.html
    % See documentation on *handle* super-class>.

    %% Public properties
    properties (Access = public)
        nv = 0;          % number of vertices
        nc = 0;          % number of cells
        ne = 0;          % number of edges
        nbdry = 0;       % number of boundary curves (connected portions of boundary)
    end

    %% Protected properties
    properties (Access = protected)
        nhe = 0;         % number of half-edges
        offset_hec = []; % compact vector of offset index of cell half-edges (size = "nc")
        cell_he = [];    % vector of the cell of each half-edge (size = "nhe")
        other = [];      % vector of mate half-edge on adjacent cell (size = "nhe")
        he_v = [];       % vector of one half-edge of vertex (size = "nv")
        he_e = [];       % vector of one of the two half-edges of edge (size = "ne")
        e_he = [];       % vector of owner edge of half-edge (size = "nhe")
        vs_e = [];       % array with the two vertices of edge (size = "ne")
        nhe_bdry = [];   % vector of number of half-edges of boundary curve (size = "nbdry")
        he_bdry = [];    % vector of first half-edge of boundary curve (size = "nbdry")
    end

    %% Private properties
    properties (Access = private)
        adj_he_map = [];   % container map of one half-edge of each edge
        nonmanifold_v = 0; % flag for found vertex adjacent to two boundary half-edges
    end

    %% Constructor method
    methods
        %-----------------------------------------------------------------------
        % Initializes number of vertices and number of cells based on given
        % values.
        function obj = che(num_v, num_c)
            if (nargin > 0)
                obj.nv = num_v; % Stores given number of vertices in object
                obj.nc = num_c; % Stores given number of cells in object
            end
        end
    end

    %% Class (static) auxiliary function
    methods (Static)
        %-----------------------------------------------------------------------
        % Creates an unique key with a pair of vertices.
        function key = che_key(v1, v2, nv)
            if (v1 > v2)
                key = v2*nv + v1;
            else
                key = v1*nv + v2;
            end
        end
    end

    %% Abstract methods: should be implemented in subclasses
    methods (Abstract)
        %-----------------------------------------------------------------------
        % Returns the number of vertices of a given cell.
        nv_cell = get_nv_cell(obj, c_id)

        %-----------------------------------------------------------------------
        % Returns a vertex index given its position k on a given cell.
        v_cell = get_v_cell(obj, c_id, k)
    end

    %% Protected methods: access from methods in subclasses
    methods (Access = protected)
        %-----------------------------------------------------------------------
        % Returns the index of an edge for a given cell and position on cell.
        function e_id = get_edge_cell(obj, c_id, k)
            e_id = obj.e_he(obj.offset_hec(c_id) + k);
        end

        %-----------------------------------------------------------------------
        % Returns the next half-edge and vertex on boundary.
        % It is assumed that the given half-edge (he) is really on boundary.
        function [he_next, v_next] = get_next_bdry_he(obj, he)
            e_id = obj.e_he(he);
            v_next = obj.vs_e(2,e_id);
            he_next = obj.he_v(v_next);
        end
    end

    %% Methods private to this superclass
    methods (Access = private)
        %-----------------------------------------------------------------------
        % Builds vector of mate half-edge on adjacent cell ("other").
        % Uses the (abstract) methods "get_nv_cell" and "get_v_cell".
        % The "other" vector is indexed with a half-edge id ("he_id").
        % Each element of this vector holds the index of the mate
        % half-edge on the adjacent cell.  A boundary half-edge, 
        % that has no mate, will have a nil value in this vector.
        % The "other" vector and the vector of boundary curves are built
        % using an auxiliary container map ("adj_he_map").
        % Variable "key" is an unique identifier of a pair of vertices
        % that is used as an entry to the container map, which holds
        % half-edge ids.
        function obj = build_mate_adjacency(obj)
            for c_id = 1:obj.nc
                nvc = get_nv_cell(obj, c_id);
                for k = 1:nvc % "k" is the index of current vertex on cell
                    k1 = mod(k, nvc) + 1; % "k1" is the index of next vertex
                    v1 = get_v_cell(obj, c_id, k); % "v1" is the current vertex on cell
                    if (v1 > obj.nv)
                        fprintf('Error: vertex index %d exceeds number of vertices!\n', v1);
                        return;
                    end
                    v2  = get_v_cell(obj, c_id, k1); % "v2" is the next vertex on cell
                    if (v2 > obj.nv)
                        fprintf('Error: vertex index %d exceeds number of vertices!\n', v2);
                        return;
                    end
                    key = che.che_key(v1, v2, obj.nv);
                    he_id = obj.offset_hec(c_id) + k;
                    % If the half-edge associated to the current pair of
                    % consecutive vertices on a cell has its mate half-edge
                    % already in the container map, then load the half-edge
                    % indices in the appropriate locations of "other" vector
                    % and remove the mate half-edge from the container map.
                    % Otherwise, put this half-edge in the container map.
                    % At the end, there should be only half-edges with no
                    % mate (i.e., boundary half-edges) in the container map. 
                    if (isKey(obj.adj_he_map, key))
                        obj.other(he_id) = obj.adj_he_map(key);
                        obj.other(obj.other(he_id)) = he_id;
                        remove(obj.adj_he_map, key);
                    else
                        obj.adj_he_map(key) = he_id;
                    end
                end
            end
        end

        %-----------------------------------------------------------------------
        % Builds half-edge and edge vectors of data structure.
        function obj = build_edge_arrays(obj)
            e_id = 0; % index of current edge
            for c_id = 1:obj.nc
                offset = obj.offset_hec(c_id);
                nv_cell = get_nv_cell(obj, c_id);
                for k = 1:nv_cell
                    he_id = offset + k; % index of current half-edge
                    v1 = get_v_cell(obj, c_id, k); % v1 is the current vertex on cell
                    k1 = mod(k, nv_cell) + 1;
                    v2  = get_v_cell(obj, c_id, k1); % v2 is the next vertex on cell
                    if (obj.other(he_id))
                        % Interior half-edge: store first found half-edge
                        % associated to "v1" in vector "he_v".
                        if (obj.he_v(v1) == 0)
                            obj.he_v(v1) = he_id;
                        end
                        % When the first half-edge of an edge appears,
                        % store current half-edge in "he_e" vector, store
                        % current edge in "e_he" vector, and store two
                        % current vertices on cell in array "vs_e".
                        if (obj.e_he(obj.other(he_id))==0)
                            % Edge appears for the first time
                            e_id = e_id + 1;
                            obj.he_e(e_id) = he_id;
                            obj.e_he(he_id) = e_id;
                            obj.vs_e(:,e_id) = [v1 ; v2];
                        else
                            % Edge has already appeared: just store the edge
                            % owned by the mate half-edge in current half-edge.
                            obj.e_he(he_id) = obj.e_he(obj.other(he_id));
                        end
                    else
                        % Boundary half-edge: first check to see whether current
                        % vertex is already associated to a boundary half-edge.
                        if (obj.he_v(v1))
                            if (obj.other(obj.he_v(v1))==0)
                                fprintf('Warning: vertex %d already belongs to a boundary half-edge!\n', v1);
                                obj.nonmanifold_v = 1;
                            end
                        end
                        % Store current half-edge associated to "v1" in vector "he_v".
                        obj.he_v(v1) = he_id;
                        % Store current half-edge in "he_e" vector, store
                        % current edge in "e_he" vector, and store two
                        % current vertices on cell in array "vs_e".
                        e_id = e_id + 1;
                        obj.he_e(e_id) = he_id;
                        obj.e_he(he_id) = e_id;
                        obj.vs_e(:,e_id) = [v1 ; v2];
                    end
                end
            end
        end

        %-----------------------------------------------------------------------
        % Builds vector of boundary curves.  Each element of this vector holds
        % the index of a half-edge on a connected portion of current mesh.
        % If it was detected that there is a non-manifold vertex (that is
        % associated to more than one half-edge), does not do anything.
        function obj = build_boundary(obj)
            if (obj.nonmanifold_v)
                return;
            end
            bdry_id = 0;
            map_length = length(obj.adj_he_map);
            while (map_length)
                bdry_id = bdry_id + 1;
                nbhe = 0; % number of half-edge on boundary curve

                % Get first half-edge of container map.
                bdry = values(obj.adj_he_map);
                he_id = cell2mat(bdry(1,1));

                % Insert first half-edge as the half-edge of current
                % boundary connected portion.
                obj.he_bdry(bdry_id) = he_id;

                % Get vertices of first edge on boundary curve.
                e_id = obj.e_he(he_id);
                v_first = obj.vs_e(1,e_id);
                v_curr = v_first;
                v_next = obj.vs_e(2,e_id);

                % Remove first half-edge from container map.
                key = che.che_key(v_curr, v_next, obj.nv);
                remove(obj.adj_he_map, key);
                nbhe = nbhe + 1;

                % Traverse current boundary curve (boundary connected portion)
                % and remove all visited half-edges from container map.
                [he_id, v_next] = get_next_bdry_he(obj, he_id);
                while (v_next ~= v_first)
                    % Update current vertex on boundary, and get next
                    % half-edge and vertex on boundary.
                    v_curr = v_next;
                    [he_id, v_next] = get_next_bdry_he(obj, he_id);
                    % Remove current half-edge from container map.
                    key = che.che_key(v_curr, v_next, obj.nv);
                    remove(obj.adj_he_map, key);
                    nbhe = nbhe + 1;
                end

                % Update number of half-edges in current boundary curve.
                obj.nhe_bdry(bdry_id) = nbhe;

                % Get current length of half-edge container map and
                % repeat until it is not empty.
                map_length = length(obj.adj_he_map);
            end

            % Update number of boundary curves (number of connected portions).
            obj.nbdry = bdry_id;
        end
    end

    %% Public methods for building and cleaning mesh data structure
    methods
        %-----------------------------------------------------------------------
        % Builds a CHE mesh data structure based on the property values for
        % number of vertices and number of cells.  These property values must
        % be set previously.
        % Creates the CHE data structure using the (abstract) methods
        % "get_nv_cell" and "get_v_cell".
        function obj = build(obj)

            if (obj.nv==0)
                return;
            end
            if (obj.nc==0)
                return;
            end

            % Build "offset_hec" vector and compute total number of half-edges.
            obj.offset_hec = zeros(1, obj.nc);
            obj.nhe = 0;
            for c_id = 1:obj.nc
                obj.offset_hec(c_id) = obj.nhe;
                obj.nhe = obj.nhe + get_nv_cell(obj, c_id);
            end

            % Build vector "cell_he" that explicitly stores the cell index
            % of each half-edge.
            obj.cell_he = zeros(1, obj.nhe);
            offset = 0;
            for c_id = 1:obj.nc
                nv_c = get_nv_cell(obj, c_id);
                obj.cell_he(offset + (1:nv_c)) = c_id;
                offset = offset + nv_c;
            end

            % Dimension mate adjacency ("other") vector and initialize
            % container map of adjacent half-edges.
            obj.other = zeros(1, obj.nhe);
            obj.adj_he_map = ...
                containers.Map('KeyType', 'int64', 'ValueType', 'int64');

            % Build mate adjacency (other vector).
            build_mate_adjacency(obj);

            % Compute number of edges: sum the number of interior half-edges 
            % divided by two with number of boundary half-edges.
            obj.ne = sum(obj.other>0)/2 + sum(obj.other==0);

            % Dimension half-edge and edge arrays.
            obj.he_v = zeros(1, obj.nv);
            obj.e_he = zeros(1, obj.nhe);
            obj.he_e = zeros(1, obj.ne);
            obj.vs_e = zeros(2, obj.ne);

            % Reset flag for found non-manifold vertex: a vertex tha is
            % adjacent to more than one boundary half-edge.
            obj.nonmanifold_v = 0;

            % Build half-edge and edge arrays.
            build_edge_arrays(obj);

            % If did not find a non-manifold vertex, build vector of boundary
            % curves connected portions of boundary using remaining container
            % map (which should contain only boundary half-edges).
            if (obj.nonmanifold_v == 0)
                build_boundary(obj);
            end

            % Destroy auxiliary container map, since it is not needed anymore.
            % (which should be empty in case vector of boundary curves was
            % created).
            delete(obj.adj_he_map);
        end

        %-----------------------------------------------------------------------
        % Cleans mesh data structure of an CHE object.
        function obj = clean(obj)
            obj.nv = 0;
            obj.nc = 0;
            obj.ne = 0;
            obj.nhe = 0;
            obj.nbdry = 0;
            obj.offset_hec = [];
            obj.cell_he = [];
            obj.other = [];
            obj.he_v = [];
            obj.e_he = [];
            obj.he_e = [];
            obj.vs_e = [];
            obj.adj_he_map = [];
            obj.he_bdry = [];
        end
    end

    %% Public methods for returning local adjacency information
    methods
        %-----------------------------------------------------------------------
        % Returns two vectors: one with the edges and the other with the
        % vertices of cell.
        function [edges,vertices] = get_ev_c(obj, c_id)
            num_v = get_nv_cell(obj, c_id);
            edges = zeros(1, num_v);
            vertices = zeros(1, num_v);
            for k = 1:num_v
                edges(k) = get_edge_cell(obj, c_id, k);
                vertices(k) = get_v_cell(obj, c_id, k);
            end
        end

        %-----------------------------------------------------------------------
        % Returns two vectors: one with the cells adjacent to an edge and
        % the other with the two vertices of the same edge.
        % If the edge is on the boundary, there is only one adjacent cell.
        % Otherwise, there are two adjacent cells.
        function [cells,vertices] = get_cv_e(obj, e_id)
            if (obj.other(obj.he_e(e_id)))
                % interior edge
                cells = zeros(1, 2);
                cells(1) = obj.cell_he(obj.he_e(e_id));
                cells(2) = obj.cell_he(obj.other(obj.he_e(e_id)));
            else
                % boundary edge
                cells = zeros(1, 1);
                cells(1) = obj.cell_he(obj.he_e(e_id));
            end
            vertices = zeros(1, 2);
            vertices(1) = obj.vs_e(1,e_id);
            vertices(2) = obj.vs_e(2,e_id);
        end

        %-----------------------------------------------------------------------
        % Returns two vectors that hold the ordered stars of cells and edges
        % around a given vertex (given by its index "v_id").
        % It is assumed a maximum valency of 20.
        % "he_id" is the index of current half-edge around vertex. If "v_id" is
        % a boundary vertex, the first "he_id" is on the boundary.
        % "c_first" is the first cell around vertex.  If "v_id" is a boundary
        % vertex, the first cell owns the first half-edge of vertex.
        % The algorithm traverses the half-edges around the given vertex and
        % allocates visited cells and edges in the output star vectors.
        % It stops traversing in two situations:
        % - for a boundary vertex: when there is no next half-edge (or next
        %   cell) around vertex.
        % - for an interior vertex: when the next cell is the first visited.
        % "c_curr" is the index of current cell around vertex.
        % "c_next" is the index of next cell around vertex.
        function [cells, edges] = get_ce_v(obj, v_id)
            max_valency = 20;
            cells = zeros(1, max_valency);
            edges = zeros(1, max_valency);
            ncells = 0;

            he_id = obj.he_v(v_id);
            c_first = obj.cell_he(he_id);
            c_next = c_first;

            notdone = true;
            while notdone
                % Update index of current cell.
                c_curr = c_next;
                % "k" is the index of "v_id" in current cell.
                k = he_id - obj.offset_hec(c_curr);
                ncells = ncells + 1;
                % Add current cell and edge to output star vectors.
                cells(ncells) = c_curr;
                edges(ncells) = obj.e_he(he_id);
                % "k1" is the index of previous vertex of current cell.
                nvc = get_nv_cell(obj, c_curr);
                k1 = mod(k + nvc - 2, nvc) + 1;
                % Get the next half-edge on next cell and get the next cell.
                % If there is no next half-edge (null value), the next cell
                % will also be null.
                he_id = obj.other(obj.offset_hec(c_curr) + k1);
                if (he_id)
                    c_next = obj.cell_he(he_id);
                else
                    c_next = 0;
                end
                notdone = (c_next) && (c_next ~= c_first);
            end

            % Update total number of edges and considers the case of last
            % edge on star of a boundary vertex. In this case, "c_next" is
            % null, and there is an additional edge on the star. Variable "k1"
            % from the above loop is still used to get the last edge.
            nedges = ncells;
            if (~c_next)
                nedges = nedges + 1;
                edges(nedges) = get_edge_cell(obj, c_curr, k1);
            end

            % Reallocate the output star vectors with their actual sizes.
            cells = cells(1:ncells);
            edges = edges(1:nedges);
        end

        %-----------------------------------------------------------------------
        % Returns an array of edges of a boundary curve (connected portion
        % of a boundary) that is given by a boundary curve index.
        function [edges] = get_e_bdry(obj, bdry_id)
            nbhe = obj.nhe_bdry(bdry_id);
            edges = zeros(1, nbhe);
            % Get first half-edge boundary connected portion.
            he_id = obj.he_bdry(bdry_id);
            for k = 1:nbhe
                % Get owner edge of current boundary half-edge.
                edges(k) = obj.e_he(he_id);
                % Get next boundary half-edge on boundary connected portion.
                [he_id, ~] = get_next_bdry_he(obj, he_id);
            end
        end
    end
end