%% CHF Modified Compact Half-Face data structure abstract class.
%
%% Copyright notice
%%%
% * $\copyright$ Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
% * $\copyright$ Luiz Fernando Martha (lfm@tecgraf.puc-rio.br)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
%% Description
% This class creates a structure for three-manifold meshes with solid
% cells that may be any polyhedron (a two-manifold object with arbitrary
% numbers of faces and vertices).
% A valid three-manifold mesh can only have two adjacent solid cells for
% an interior face and one adjacent solid cell for a boundary face.
%
% A half-face represents the use of a face by a solid cell of a mesh.
% A face in the interior of a mesh has two half-faces and a boundary
% face has only one half-face.
%
% An object of this class stores:
%%%
% * *other* - a vector with the mate half-face on the adjacent cell of each half-face
% * *offset_hfc* - a vector with the offset index for cell half-faces
% * *hf_v* - a vector with the index of one of the half-faces of each vertex
% * *f_hf* - a vector with the index of the face of each half-face
% * *hf_e_map* - a container map with one half-face of each edge
% * *hf_e_key* - a vector with the key to *hf_e_map* container of each edge
%
% This data structure is created based on a conventional mesh representation
% in which the vertex adjacency information of solid cells is known.  This
% mesh representation is handled in an abstract fashion in this class.
% For this, an abstract method (function) is defined: *get_cell*.
% This method returns, for a given cell index, its type (a type of cell
% defined by client at registration) and a vertex incidence list of a solid
% cell. The topology of a cell in relation to its local vertex incidence
% list is registered by the client using method *register_cell_templ* of
% this class.
%
% To use this class, clients should create a subclass of this class
% in which this abstract method is implemented.
% Note that this (super)class does have a matrix property for storing 
% the vertex incidence lists of the mesh cells.  This conventional
% vertex-cell adjacency information is used generically through the
% abstract method *get_cell*.
%
% The vector *offset_hfc* holds the offset index of the first half-face
% of each solid cell.
%
% An element of vector *hf_v* holds one of the half-faces of a vertex.
% In case a vertex is at the boundary of the mesh, the referred adjacent
% half-face is a boundary half-face.
%
% Each element of container *hf_e_map* is a vector with two elements:
%%%
% * index of an edge
% * index of one of the adjacent half-faces of an edge
%
% In case an edge is at the boundary of the mesh, the referred
% adjacent half-face is on the boundary.
%
% Vector *hf_e_key* contains keys to *hf_e_map* for each edge.
% A key value (that identifies an edge) for acessing the container
% *hf_e_map* is defined using an auxiliary class (static) function:
% *key = chf_key(vs, nv)*, in which *vs* is a vector with two indices of
% the vertices of an edge and *nv* is the total number of vertices in the
% mesh.
%
%% References
%%%
% * Marcos Lage, Thomas Lewiner, Helio Lopes, and Luiz Velho,
% "CHF: A Scalable Topological Data Structure for Tetrahedral Meshes",
% Proceedings of the XVIII Brazilian Symposium on Computer Graphics and
% Image Processing (SIBGRAPI'05), pp. 349-356, 2005.
%
% * Lage, M.; Martha, L.F.; Moitinho de Almeida, J.P.; Lopes, H.,
% �IBHM: index-based data structures for 2D and 3D hybrid meshes�,
% Engineering with Computers, Vol. 33, Issue 4, pp. 727-744.
% Springer-Verlag London,
% ISSN: 0177-0667, DOI: 10.1007/s00366-015-0395-0.
% http://link.springer.com/article/10.1007%2Fs00366-015-0395-0
%
%% Notes
%%%
% 1.
% Different than the original CHF data structure, the current CHF does
% not have a vector for holding the number of the initial vertex of
% each half-face.  Instead, it uses the *offset_hfc* vector, that holds
% the offset index of the first half-face of each cell.  Using this
% offset vector allows the use arbitrary topology cells, i.e., a cell
% may be any polyhedron (a two-manifold object with arbitrary numbers
% of faces and vertices) and stored by clients in any order.
%
% 2.
% In this (super)class there is no data structure for holding the solid
% cells of a mesh. The cells are obtained through the *get_cell* arbitrary
% method, which should be implemented by a (client) subclass derived from
% this class. However, the total number of cells is a required property.
% Each solid cell is identified in this class by its index, which spans
% from 1 to the total number of cells (*nc*). Therefore, the subclass 
% should implement a data structure for storing the solid cells of the
% mesh and the vertex incidences of all the cells. The index of a cell is
% an input argument to *get_cell* method.  With the cell index, the client
% subclass identifies a solid cell of a mesh.
%
% 3.
% Similarly, the total number of vertices is a required property.  It
% is assumed that the vertex indices given by method *get_cell* spam
% from 1 to the total number of vertices (*nv*).
%
% 4.
% A companion auxiliary class <chf_cell.html *chf_cell*> is used for
% creating a generic polyhedral cell template to be used by this class.
% Please, refer to the <chf_cell.html description> of that class.
% A generic cell template is defined by the following properties:
%%%
% * *nv*         - number of vertices of cell
% * *nf*         - number of faces of cell
% * *face_incid* - matrix (*nf* x *max_num_face_vertices*) with local
%                  numbering of face vertex incidence:
%                  For each face indicates a list of its vertices, given in
%                  counter-clockwise order as looking from outside the
%                  cell. *max_num_face_vertices* is implicitly indicated by
%                  the number of columns of *face_incid*. Faces with less
%                  than *max_num_face_vertices* have null indices at the end of
%                  the corresponding lines in *face_incid*.
% * *fv*         - matrix (*nv* x *nv*) with used face indices:
%                  *fv* indicates edge adjacency relationships between vertices.
%                  *fv* contains face indices and null values. Each face
%                  index corresponds to a pair of adjacent-by-edge
%                  vertices. The indicated face is the one that uses the edge
%                  formed by the two vertices in counter-clockwise order.
%                  *fv* has non-zero values placed symmetrically.
%                  The pair of faces *fv(i,j)* e *fv(j,i)* are adjcent by edge.
%                  *fv* is automatically built from *nv*, *nf*, and *face_incid*.
% * *ff*         - matrix (*nf* x *max_num_face_vertices*) with face-face
%                  adjacency relationships:
%                  Each row of *ff* indicates, for a face, a counter-clockwise
%                  ordered list of adjacent faces, starting from the first
%                  edge.  Faces with less than *max_num_face_vertices* have null
%                  indices at the end of the corresponding lines in *ff*.
%
% 5.
% This (super)class does not handle any geometry information.
% If a client wants to extend it to also store vertex coordinates,
% it should create a subclass for doing this.
%
% 6.
% This version of CHF corresponds to level 2 of the original CHF.
% In this level, solid cells, faces, edges, and vertices are represented
% explicitly, and it is possible to access adjacency information among
% these entities locally (without global searches). In the next level 3,
% which is not implemented by this class, the boundaries of the mesh would
% be explicitly represented by a compact half-edge data structure.
%
% 7.
% This version of CHF uses an explicit storage of the cell of each
% half-face (vector *cell_hf*). The article "IBHM: index-based data
% structures for 2D and 3D hybrid meshes" describes three alternatives
% for accessing a cell for a given half-face: explicit access; sum of
% terms of *offset_hfc* vector - "cell_id = sum(obj.offset_hfc < hf);";
% and a binary search equivalent to "sum(obj.offset_hfc < hf);".
% In this version, only the first alternative is implemented.
%
% 8.
% This implementation of CHF uses two container maps. The container map
% *hf_e_map*, described above, is part of the data structure of a solid mesh
% and is persistent. The other container map is temporary. This is the
% auxiliary *adj_hf_map* container map that is used to build the adjacency
% information of mate half-faces stored in vector *other*.
% A key value (that identifies a face) for acessing the container
% *adj_hf_map* is defined using the same auxiliary class (static) function
% that is used to define keys for the *hf_e_map* container:
% *key = chf_key(vs, nv)*, in which *vs* is a vector with indices of the
% vertices of a face and *nv* is the total number of vertices in the mesh.
% Method *chf_key* defines a key value to identify a face using the three
% lowest indices of the vertices of a face.
%
%% History
%
% @VERSION 1.00 on 01-Jul-2012. Initial version.
%
% @VERSION 1.01 on 23-Jan-2013.
% Modifications:
% Replaced "solid" by "cell.
% Set protected access to properties: *nv*, *nf*, *face_indic*, *fv*, *ff*.
% Made this class a subclass of *hgsetget* instead of *handle*.
% Modified get methods to the formal get methods of a subclass of
% *hgsetget*: obj.get('XXX').
%
% @VERSION 1.02 on 08-Jul-2013.
% Modifications:
% Set protected access to properties: *nv*, *nc*, *ne*, *nf*.
% Created get methods.
% Created private methods *build_mate_adjacency* and *build_edge_map*
% that are called by public method *build*.
% Created private property *adj_hf_map*, which replaces local variable
% *adj* in previous version of method *build*.
% Created protected property *neu*, which replaces local variable
% *num_eu* in previous version of method *build*.
% Created three alternatives for function *get_cell_hf* that returns
% the index of a cell for a given half-face:
% 1 - explicit, 2 - sum; 3 - binary search.
% Created a set method for property *get_cell_hf_opt*.
% Created functions *get_faces_c* and *get_vertices_f_c*.
% Replaced calls to *templ.get_XXX()* by calls to *templ.get('XXX').
% Made minor comment modifications.
%
% @VERSION 1.03 on 16-Jul-2013.
% Modifications:
% In function *get_cell_hf_bsearch* name of variable *offset_hfc* was
% incorrect in line "high = size(obj.offset_hfc, 2);".
% Modified this function to handle the case of just one cell.
%
% @VERSION 1.04 on 02-Aug-2013.
% Modifications:
% In function *get_cfe_v* correct the way how the *other* half-faces
% are obtained. Modify the search procedures, removing a priori
% faces and edges that have already been inserted. Use sorted vectors
% as required by the ismembc function. Because sorting is done by insertion
% some preallocating is no longer necessary.
%
% @VERSION 2.00 on 26-Jul-2019.
% Modifications:
% Formating comments for Publish command.
% Made this class a subclass of *handle* instead of *hgsetget*, i.e.,
% currently this is a handle class without set and get methods. 
% Got rid of function *get_cell_hf* that returned the index of a cell for a
% given half-face. Previously, this function used three alternatives:
% 1 - explicit, 2 - sum; 3 - binary search. Currently, this version of CHF
% uses an explicit storage of the cell of each half-face (vector *cell_hf*).
% 
%
%% Class definition
classdef chf < handle
    %%
    % <https://www.mathworks.com/help/matlab/ref/handle-class.html
    % See documentation on *handle* super-class>.

    %% Public properties
    properties (Access = public)
        nv = 0;          % number of vertices
        ne = 0;          % number of edges
        nf = 0;          % number of faces
        nc = 0;          % number of solid cells
    end
 
    %% Protected properties
    properties  (Access = protected)
        num_templ = 0;   % number of cell template types
        cell_templ = {}; % vector of solid cell templates (objects of class "chf_cell")
        max_nv_c = 0;    % maximum number of vertices of all solid cells
        neu = 0;         % total number of edge-uses (uses of edges by half-faces)
        nhf = 0;         % total number of half-faces
        templ_index = sparse(0);  % sparse array, indexed by a cell type, in which each 
                                  % element holds an index to the vector cell_templ
        offset_hfc = []; % compact vector of offset index for cell half-faces (size = "nc")
        cell_hf = [];    % vector of the cell of each half-face (size = "nhf")
        other = [];      % vector of mate half-face on adjacent cell (size = "nhf")
        hf_v = [];       % vector of one half-face of vertex (size = "nv")
        hf_f = [];       % vector of one of the two half-faces of face (size = "nf")
        f_hf = [];       % vector of owner face of half-face (size = "nhf")
        hf_e_map = [];   % container map of one half-face of each edge
        hf_e_key = [];   % vector of edge key to "hf_e_map" container (size = "ne")
    end

    %% Private properties
    properties (Access = private)
        adj_hf_map = [];   % container map of one half-face of each face
    end

    %% Constructor method
    methods
        %-----------------------------------------------------------------------
        % Initializes number of vertices and number of solid cells
        % based on given values.
        function obj = chf(num_v, num_c)
            if (nargin > 0)
                obj.nv = num_v; % Stores given number of vertices in object
                obj.nc = num_c; % Stores given number of solid cells in object
            end
        end
    end

    %% Class (static) auxiliary functions
    methods (Static)
        %-----------------------------------------------------------------------
        % Creates an unique key for a set of vertices.
        % In case there are more than two vertices, defines a key using
        % the three lowest indices of given set of the vertices.
        function key = chf_key(vs, nv)
            v = sort(vs, 2);
            if (size(vs,2) == 2)
                key = v(:,1)*nv + v(:,2);
            else
                key = (v(:,1)*nv + v(:,2))*nv + v(:,3);
            end
        end

        %-----------------------------------------------------------------------
        % From an unique key returns the set of vertices that generated it.
        function verts = inv_chf_key(key, nv)
            v3 = mod(key-1, nv) + 1;
            v12 = (key-v3)/nv;
            if (v12 <= nv)
                verts = [ v12 v3 ];
            else
                v2 = mod(v12-1, nv) + 1;
                v1 = (v12-v2)/nv;
                verts = [ v1 v2 v3 ];
            end
        end
    end

    %% Abstract method: should be implemented in subclasses
    methods (Abstract)
        %-----------------------------------------------------------------------
        % Returns the type and the vertex list of a given solid cell.
        [ type, verts ] = get_cell(obj, c_id)
    end

    %% Protected methods: access from methods in subclasses
    methods (Access = protected)
        %-----------------------------------------------------------------------
        % Returns the indices of the faces of a given solid cell.
        function faces = get_faces_c(obj, c_id)
            [ type, ~ ] = obj.get_cell(c_id);
            templ = obj.cell_templ{obj.templ_index(type)};
            nf_c = templ.nf;
            faces = obj.f_hf(obj.offset_hfc(c_id) + (1:nf_c));
        end

        %-----------------------------------------------------------------------
        % Returns the index of a face for a given solid cell and position on cell.
        function f_id = get_face_c(obj, c_id, f)
            [ type, ~ ] = obj.get_cell(c_id);
            templ = obj.cell_templ{obj.templ_index(type)};
            nf_c = templ.nf;
            if (f > nf_c || f < 1)
                f_id = 0;
            else
                f_id = obj.f_hf(obj.offset_hfc(c_id) + f);
            end
        end

        %-----------------------------------------------------------------------
        % Returns the indices of the vertices of a face on solid cell.
        % The face position on cell is given.
        function verts = get_vertices_f_c(obj, c_id, f)
            [ type, c_verts ] = obj.get_cell(c_id);
            templ = obj.cell_templ{obj.templ_index(type)};
            nf_c = templ.nf;
            f_incid_c = templ.face_incid;
            verts = zeros(1, nf_c);
            if (f > nf_c || f < 1)
                return;
            else
                verts = c_verts(f_incid_c(f,f_incid_c(f,:)>0));
            end
        end

        %-----------------------------------------------------------------------
        % Returns the index of a vertex of a face on solid cell. The face
        % position on cell and the vertex position on face are given.
        function v_id = get_vertex_f_c(obj, c_id, f, v)
            [ type, c_verts ] = obj.get_cell(c_id);
            templ = obj.cell_templ{obj.templ_index(type)};
            nf_c = templ.nf;
            f_incid_c = templ.face_incid;
            if (f > nf_c || f < 1)
                v_id = 0;
            else
                if ( v > count(f_incid_c(f,:)>0) || v < 1)
                    v_id = 0;
                else
                    v_id = c_verts(f_incid_c(f,v));
                end
            end
        end

        %-----------------------------------------------------------------------
        % Returns the indices of the two vertex of an edge (given by its id).
        function verts = get_verts_e(obj, e_id)
            key = obj.hf_e_key(e_id);
            verts = chf.inv_chf_key(key, obj.nv);
        end

        %-----------------------------------------------------------------------
        % Returns the index of an edge from a given pair of vertices.
        function e_id = get_edge_vs(obj, v1_id, v2_id)
            key = chf.chf_key([v1_id v2_id], obj.nv);
            aux = obj.hf_e_map(key);
            e_id = aux(1);
        end

        %-----------------------------------------------------------------------
        % Returns the index of first half-face of given edge.
        % If this edge is on the boundary, the returned half-face is also
        % on the boundary.
        function hf_id = get_hf_e(obj, e_id)
            aux = obj.hf_e_map(obj.hf_e_key(e_id));
            hf_id = aux(2);
        end

        %-----------------------------------------------------------------------
        % Given an edge (given by its id) and a half-face "hf_id" that uses
        % this edge, returns the half-face on the same solid cell of the given
        % half-face that is adjacent to it by the given edge.
        function hf_adj = get_adj_hf_e(obj, e_id, hf_id)

            % Get solid cell of half-face and its local cell adjacency
            c_id = obj.cell_hf(hf_id);    % solid cell of hf_id
            [ type, c_verts ] = obj.get_cell(c_id);
            templ = obj.cell_templ{obj.templ_index(type)};
            f_incid_c = templ.face_incid;
            ff_c = templ.ff;

            % Get local half-face on cell
            f = hf_id - obj.offset_hfc(c_id);

            % Get vertices of half-face
            nodei = c_verts(f_incid_c(f,f_incid_c(f,:)>0));
            nodej = circshift(nodei, [0 -1]);

            % Get the two vertices of edge
            e_verts = obj.get_verts_e(e_id);

            % Look for edge-use of given edge on face
           edge_use = 0;
           fverts = sort([ nodei; nodej],1);
           for e = 1:size(fverts, 2)
               if (e_verts(1)==fverts(1,e) && e_verts(2)==fverts(2,e))
                   edge_use = e;
                   break;
               end
           end
           if (edge_use==0)  % Could not find edge on face: something wrong!
                hf_adj = 0;
                fprintf('Problem in get_adj_hf_e: edge is not in half-face!!!\n');
            else
                % To find the half-face in the same cell that is adjacent by edge
                % to the given half-face, use matrix ff of local cell.
                hf_adj = obj.offset_hfc(c_id) + ff_c(f,edge_use);
            end
        end

        %-----------------------------------------------------------------------
        % Given an edge (given by its id) and a half-face "hf_id" that uses
        % this edge, returns the radial half-face around the edge.
        % First, it is found the half-face on the same solid cell of the given
        % half-face that is adjacent to it by the given edge.
        % The returned radial half-face is the mate (other) half-face of the
        % adjacent face.
        function hf_radial = get_radial_hf_e(obj, e_id, hf_id)

            % Get the adjacent half-face
            hf_adj = obj.get_adj_hf_e(e_id, hf_id);

            % The returned next half-face is the mate (other) of hf_adj
            hf_radial = obj.other(hf_adj);
        end
    end

    %% Methods private to this superclass
    methods (Access = private)
        %-----------------------------------------------------------------------
        % Builds vector of mate half-face on adjacent cell ("other").
        % Uses the (abstract) method "get_cell".
        % The "other" vector is indexed with a half-face id ("hf_id").
        % Each element of this vector holds the index of the mate
        % half-face on the adjacent solid cell.  A boundary half-face, 
        % which has no mate, will have a null value in this vector.
        % The "other" vector is built using an auxiliary container map
        % ("adj_hf_map").
        % Variable "key" is an unique identifier of a tuple of vertices
        % of a cell face that is used as an entry to the container map,
        % which holds half-face ids.
        % Also builds "f_hf" vector and counts total number of edge-uses.
        function obj = build_mate_adjacency(obj)
            num_f = 0;   % counts the number of unique faces of mesh
            obj.neu = 0; % number of edge-uses of all faces in all solid cells
                         % just needed to dimension "hf_e_key" vector
            for c_id = 1:obj.nc
                [ type, c_verts ] = obj.get_cell(c_id);
                templ = obj.cell_templ{obj.templ_index(type)};
                nf_c = templ.nf;
                f_incid_c = templ.face_incid;
                for f = 1:nf_c  % "f" is the index of current half-face of cell
                    % Get vertices of half-face
                    f_verts = c_verts(f_incid_c(f,f_incid_c(f,:)>0));
                    % Build a key to "adj_hf_map" container with half-face vertices
                    key = chf.chf_key(f_verts, obj.nv);
                    % Get index of current global half-face
                    hf_id = obj.offset_hfc(c_id) + f;
                    % If current half-face has its mate half-face already
                    % in the "adj_hf_map" container map, then load the half-face
                    % indices in the appropriate locations of "other" vector
                    % and remove the mate half-face from the "adj_hf_map" container map.
                    % Otherwise, put this half-face in the "adj_hf_map" container map.
                    % At the end, there should be only half-faces with no
                    % mate (i.e., boundary half-faces) in the "adj_hf_map" container map.
                    % Also update "f_hf" vector (face of half-face).
                    if (isKey(obj.adj_hf_map, key))
                        obj.other(hf_id) = obj.adj_hf_map(key);
                        obj.other(obj.other(hf_id)) = hf_id;
                        obj.f_hf(hf_id) = obj.f_hf(obj.other(hf_id));
                        remove(obj.adj_hf_map, key);
                    else
                        obj.adj_hf_map(key) = hf_id;
                        num_f = num_f + 1;
                        obj.f_hf(hf_id) = num_f;
                    end
                    % Update number of total edge-uses.
                    obj.neu = obj.neu + size(f_verts,2);
                end
            end
        end

        %-----------------------------------------------------------------------
        % Build "hf_e_map" container map and "hf_e_key" vector, which
        % contains keys to "hf_e_map".
        % Each element of container "hf_e_map" is a vector with two elements: 
        % the index of an edge; and the index of one of the adjacent
        % half-faces of an edge.
        % In case an edge is at the boundary of the mesh, the referred
        % adjacent half-face is on the boundary.
        function obj = build_edge_map(obj)
            % Over dimension vector "hf_e_key" based on half the total number
            % of edge-uses
            obj.hf_e_key = zeros(1, obj.neu/2);
            obj.hf_e_map = containers.Map('KeyType', 'int64', 'ValueType', 'any');
            
            for c_id = 1:obj.nc
                [ type, c_verts ] = obj.get_cell(c_id);
                templ = obj.cell_templ{obj.templ_index(type)};
                nf_c = templ.nf;
                f_incid_c = templ.face_incid;
                for f = 1:nf_c
                    nodei = c_verts(f_incid_c(f,f_incid_c(f,:)>0));
                    nodej = circshift(nodei, [0 -1]);
                    keys = chf.chf_key([nodei' nodej'], obj.nv);
                    hf_id = obj.offset_hfc(c_id) + f;
                    is_boundary = (obj.other(hf_id)==0);
                    for key = keys' % the keys of the edges
                        if (~isKey(obj.hf_e_map, key)) % The edge is new
                            obj.ne = obj.ne + 1;
                            obj.hf_e_map(key) = [ obj.ne, hf_id ];
                            obj.hf_v(nodei) = hf_id;
                            obj.hf_e_key(obj.ne) = key;
                        else
                            if (is_boundary)
                                old = obj.hf_e_map(key);
                                obj.hf_e_map(key) = [ old(1), hf_id ];
                                obj.hf_v(nodei) = hf_id;
                            end
                        end
                    end
                end
            end
            % Redimension hf_e_key vector based on actual number of edges            
            obj.hf_e_key = obj.hf_e_key(1:obj.ne);
        end
    end

   %% Public methods for building and cleaning mesh data structure
   methods
        %-----------------------------------------------------------------------
        % Register a solid cell template associated to a given client type
        function obj = register_cell_templ(obj, type, nv_c, nf_c, f_incid_c)
            if (nv_c > obj.max_nv_c)
                obj.max_nv_c = nv_c;
            end
            nct = obj.num_templ + 1;
            templ = chf_cell(nv_c, nf_c, f_incid_c);
            obj.cell_templ{nct} = templ;
            obj.templ_index(type) = nct;
            obj.num_templ = nct;
        end
        
        %-----------------------------------------------------------------------
        % Build a mesh data structure based on solid cells defined by
        % client through abstract method "get_cell".
        function obj = build(obj)
            if (obj.nv==0)
                return;
            end
            if (obj.nc==0)
                return;
            end

            % Build vectors "offset_hfc" and "hf_v", and compute total
            % total number of half-faces.
            obj.offset_hfc = zeros(1, obj.nc);
            obj.nhf = 0;
            obj.hf_v = zeros(1, obj.nv);           
            for c_id = 1:obj.nc
                obj.offset_hfc(c_id) = obj.nhf;
                [ type, ~ ] = obj.get_cell(c_id);
                templ = obj.cell_templ{obj.templ_index(type)};
                nf_c = templ.nf;
                obj.nhf = obj.nhf + nf_c;
            end

            % Build vector "cell_hf" that explicitly stores the cell index
            % of each half-face.
            obj.cell_hf = zeros(1, obj.nhf);
            offset = 0;
            for c_id = 1:obj.nc
                [ type, ~ ] = obj.get_cell(c_id);
                templ = obj.cell_templ{obj.templ_index(type)};
                nf_c = templ.nf;
                obj.cell_hf(offset + (1:nf_c)) = c_id;
                offset = offset + nf_c;
            end

            % Dimension mate adjacency ("other") vector, dimension face of
            % half-faces ("f_hf") vector and initialize container map of
            % adjacent half-faces.
            obj.other = zeros(1, obj.nhf);
            obj.f_hf = zeros(1, obj.nhf);
            obj.adj_hf_map = ...
                containers.Map('KeyType', 'int64', 'ValueType', 'int64');
            
            % Build mate adjacency ("other" vector).
            % Also build "f_hf" vector and count the total number of edge-uses.
            build_mate_adjacency(obj);

            % Compute number of faces in CHF mesh:
            % sum the number of interior half-faces divided by two with
            % number of boundary half-faces.
            obj.nf = sum(obj.other>0)/2 + sum(obj.other==0);

            % Allocate and fill in "hf_f" vector based on "f_hf" vector
            obj.hf_f = zeros(1, obj.nf);
            for i = 1:obj.nhf
                obj.hf_f(obj.f_hf(i)) = i;
            end

            % Build "hf_e_map" container map and "hf_e_key" vector, which
            % contains keys to "hf_e_map".
            build_edge_map(obj);

            % Destroy auxiliary "adj_hf_map" container map, since it is not
            % needed anymore. The remaining half-faces in this container
            % are all boundary half-faces (which have no mate).
            % In the future, this container could be used to build the
            % boundary of a mesh, which would be explicitly represented
            % as lists of connected groups of boundary half-faces.
            delete(obj.adj_hf_map);
        end
        
        %-----------------------------------------------------------------------
        % Cleans mesh data structure of an CHF object.
        function obj = clean(obj)
            obj.num_templ = 0;
            obj.cell_templ = {};
            obj.nv = 0;
            obj.nc = 0;
            obj.ne = 0;
            obj.nf = 0;
            obj.max_nv_c = 0;
            obj.neu = 0;
            obj.nhf = 0;
            obj.templ_index = sparse(0);
            obj.offset_hfc = [];
            obj.cell_hf = [];
            obj.other = [];
            obj.hf_v = [];
            obj.hf_f = [];
            obj.f_hf = [];
            obj.hf_e_map = [];
            obj.hf_e_key = [];
        end
    end

    %% Public methods for returning local adjacency information
    methods
        %-----------------------------------------------------------------------
        % Returns three adjacency vectors in relation to a solid cell.
        function [faces, edges, verts] = get_fev_c(obj, c_id)
          % get the cell type and its vertices
          [ type, verts ] = obj.get_cell(c_id);

          % get the cell template
          templ = obj.cell_templ{obj.templ_index(type)};

          % Redimension vertices vector based on actual number of vertices
          nv_c = templ.nv;
          verts = verts(1:nv_c);

          % get the edge adjacency (using only the lower triangular matrix)
          cell_fv = tril( templ.fv );
          % find the non-zero entries
          [row, col] = find(cell_fv);
          % get the global ids of the pair of edge vertices
          vrow = verts(row);
          vcol = verts(col);
          % get the edges
          nedges = size(vrow,2);
          edges = zeros(1,nedges);
          for k = 1:nedges
              edges(k) = obj.get_edge_vs(vrow(k), vcol(k));
          end

          % get the cell face ids
          faces = obj.get_faces_c(c_id);
        end

        %-----------------------------------------------------------------------
        % Returns three adjacency vectors in relation to a face.
        function [cells, edges, verts] = get_cev_f(obj, f_id)
            % convert from face index to half-face index.
            hf_id = obj.hf_f(f_id);
            % get one cell incident to the face.
            c_id = obj.cell_hf(hf_id);
            % get the other incident cell (if it exists).
            if (obj.other(hf_id) > 0)
                % interior face
                cells = zeros(1, 2);
                cells(1) = c_id;
                cells(2) = obj.cell_hf(obj.other(hf_id));
            else
                % boundary face
                cells = zeros(1, 1);
                cells(1) = c_id;
            end

            % local id of target face.
            f_lid = hf_id - obj.offset_hfc(c_id);
            % gets the vertices of the f_lid face of cell c_id.
            verts = obj.get_vertices_f_c(c_id, f_lid);
            nverts = size(verts,2);

            % shifts the vertices ids
            vertshift = circshift(verts, [0 -1]);
            % builds the edges
            edges = zeros(1,nverts);
            for k = 1:nverts
                edges(k) = obj.get_edge_vs(verts(k), vertshift(k));
            end
        end

        %-----------------------------------------------------------------------
        % Returns three adjacency vectors in relation to an edge.
        function [cells, faces, verts] = get_cfv_e(obj, e_id)

            max_valency = 20;
            faces = zeros(1, max_valency);
            cells = zeros(1, max_valency);
            ncells = 0;

            hf_first = obj.get_hf_e(e_id);
            hf_curr = hf_first;

            % Traverse radially around given edge and add visited
            % cells and faces to output star vectors.
            notdone = true;
            while notdone

                ncells = ncells + 1;
                cells(ncells) = obj.cell_hf(hf_curr);
                faces(ncells) = obj.f_hf(hf_curr);

                % Get the adjacent-by-edge half-face in current cell
                hf_adj = obj.get_adj_hf_e(e_id, hf_curr);

                % Next radial half-face is the mate (other) of adjacent hf.
                hf_curr = obj.other(hf_adj);

                notdone = (hf_curr) && (hf_curr ~= hf_first);
            end

            % Update total number of faces and consider the case of last
            % face on star of a boundary edge. In this case, hf_curr is
            % null, and there is an additional face on the star.
            % The last face is the owner face of the last half-face h_adj 
            % of the above loop.
            nfaces = ncells;
            if (~hf_curr)
                nfaces = nfaces + 1;
                faces(nfaces) = obj.f_hf(hf_adj);
            end
            
            % Reallocate the output star vectors with their actual sizes.
            cells = cells(1:ncells);
            faces = faces(1:nfaces);

            % Get the two vertices of given edge.
            verts = obj.get_verts_e(e_id);
        end

        %-----------------------------------------------------------------------
        % Returns three adjacency vectors in relation to a vertex.
        function [cells, faces, edges] = get_cfe_v(obj, v_id)
            
            hf_curr = obj.hf_v(v_id);
            
            % initialization
            max_valency = 20;
            st = zeros(1,max_valency);
            st(1) =  hf_curr;
            stsize = 1;

            % cells loop
            cells = [];
            faces = [];
            edges = [];
            newfaces = st;
            while (stsize ~= 0)
                % pop
                hf_curr = st(stsize);
                stsize = stsize - 1;
                
                % gets the id of the current cell
                c_curr = obj.cell_hf(hf_curr);
                % find if it was used before
                if( find(cells == c_curr) ) 
                    continue;
                end
                
                % stores the cell
                before = cells < c_curr;
                cells = [ cells(before) c_curr cells(~before) ];
            
                % get the solid faces and edges of this cell
                [allfaces,alledges,~] = obj.get_fev_c(c_curr);

                % Sort (mandatory for ismembc)
                allfaces = sort(allfaces);
                alledges = sort(alledges);
                
                % Remove repeated entities
                allfaces = allfaces(~ismembc(allfaces, faces));
                alledges = alledges(~ismembc(alledges, edges));
                               
                % Get the vertices of the faces and check for intersection
                % (when we have a vectorial form for get_cev_f and get_cfv_e
                % we can also vectorise these operations)
                nnewfaces = 0;
                for f = allfaces
                    [~, ~, f_verts] = obj.get_cev_f(f);
                    % find the faces incident to v_id
                    if (~isempty(find(f_verts == v_id, 1)))
                        nnewfaces = nnewfaces + 1;
                        newfaces(nnewfaces) = f;
                        before = faces < f;
                        faces = [ faces(before) f faces(~before)];
                    end
                end
                
                % Get the vertices of the edges
                for e = alledges
                    [~,~, e_verts] = obj.get_cfv_e(e);
                    % find the edges incident to v_id
                    if (~isempty(find( e_verts == v_id, 1)))
                        before = edges < e;
                        edges = [ edges(before) e edges(~before) ];
                    end
                end
                               
                % for the faces we have just inserted
                % get the half_faces outside the current cell
                o_hfs = obj.hf_f(newfaces(1:nnewfaces));
                for i=1:size(o_hfs,2)
                    if (obj.cell_hf(o_hfs(i)) == c_curr)
                        o_hfs(i) = obj.other(o_hfs(i));
                    end
                end
                o_hfs = o_hfs( o_hfs ~= 0 );
                
                new_hfs = size(o_hfs,2);
                if (new_hfs > 0)
                    % adds the other half faces to the stack
                    st(stsize+(1:new_hfs)) = o_hfs;
                    stsize = stsize + new_hfs;
                end
            end
        end

% OLD ----
%        function [cells, faces, edges] = get_cfe_v(obj, v_id)
%            
%            hf_curr = obj.hf_v(v_id);
%            
%            % initialization
%            max_valency = 20;
%            st = zeros(1,max_valency);
%            st(1) =  hf_curr;
%            stsize = 1;
%
%            cells = [];
%            faces = [];
%            verts = [];
%            
%            % loop on the cells adjacent to the half-faces
%            while (stsize ~= 0)
%                % pop
%                hf_curr = st(stsize);
%                stsize = stsize - 1;
%                
%                % gets the id of the current cell
%                c_curr = obj.cell_hf(hf_curr);
%                % find if it was used before
%                if( find(cells == c_curr) ) 
%                    continue;
%                end
%                
%                % store the cell as visited
%                before = cells < c_curr;
%                cells = [ cells(before) c_curr cells(~before) ];
%                
%                % cell info
%                [ type, cell_verts ] = obj.get_cell(c_curr);
%                templ = obj.cell_templ{obj.templ_index(type)};
%                nv_c = templ.nv;
%                cell_verts = cell_verts(1:nv_c); % we only want the vertex nodes
%                cell_fv = templ.fv;
%                cell_face_incid = templ.face_incid;
%                
%                % the position of the current vertex on the template
%                curr_vert = find(cell_verts == v_id);
%                
%                % the vertices that are connected to v_id 
%                % (instead of explicitly storing the corresponding edges)
%                % and sort (mandatory for ismembc)
%                opposed_verts = sort(cell_verts(cell_fv(curr_vert,:)>0));
%                
%                % the half faces that are adjacent to v_id and sort
%                % (the half-faces are sorted, but in general the faces
%                % will not be)
%                adjacent_faces = sort(obj.f_hf(obj.offset_hfc(c_curr) + ...
%                    find(any(cell_face_incid==curr_vert,2))));
%                
%                % Remove repeated entities
%                adjacent_faces = adjacent_faces(~ismembc(adjacent_faces, faces));
%                opposed_verts = opposed_verts(~ismembc(opposed_verts, verts));
%
%                % insert on lists
%                % can we avoid another sort?
%                faces = sort([ faces, adjacent_faces]);
%                verts = sort([ verts, opposed_verts]);
%                                              
%                % for the faces we have just inserted
%                % get the half_faces that are outside the current cell
%                o_hfs = obj.hf_f(adjacent_faces);
%                for i=1:size(o_hfs,2)
%                    if (obj.cell_hf(o_hfs(i)) == c_curr)
%                        o_hfs(i) = obj.other(o_hfs(i));
%                    end
%                end
%                o_hfs = o_hfs( o_hfs ~= 0 );
%                
%                new_hfs = size(o_hfs,2);
%                if (new_hfs > 0)
%                    % adds the other half faces to the stack
%                    st(stsize+(1:new_hfs)) = o_hfs;
%                    stsize = stsize + new_hfs;
%                end
%            end
%            % transform pairs "vertex, opposed vertices" into edges
%            edges = zeros(size(verts));
%            for i = 1:size(verts,2)
%                edges(i) = obj.get_edge_vs(v_id, verts(i));
%            end
%        end
    end
end

