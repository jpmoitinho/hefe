function [ prob ] = DefineSolidProblem( meshname, probname, funcs)
% DefineProblem - Load the mesh and initialize variables
%   
% Define 
%
%


% The mesh

[a,b,c] = fileparts(meshname);
prob.meshname.path = a;
prob.meshname.name = b;
prob.meshname.ext = c;

prob.mesh = chf_gmsh();
prob.mesh.load(meshname);

prob.geom.center = zeros(3, prob.mesh.nc);
prob.geom.type = ones(prob.mesh.nc, 1);

% Could use a formula for this
% verts_of_edges = [ 2 3 ; 3 1 ; 1 2 ; 1 4 ; 2 4 ; 3 4 ];

for elem = 1:prob.mesh.nc
    [ ~, ~, cell_verts ] = prob.mesh.get_fev_c(elem);
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), :);
    prob.geom.center(:,elem) = sum(elem_coords)/size(cell_verts,2);
    if (cell_verts(2) > cell_verts(3))
        prob.geom.type = 2;
    end
end

% The problem (boundary conditions, properties)

[a,b,c] = fileparts(probname);
prob.probname.path = a;
prob.probname.name = b;
prob.probname.ext = c;

% The path to the directory where the functions are

prob.funcs = funcs;

prob.prob = load_problem(probname);

% Organise the boundary conditions

% The faces explicitly defined in the gmsh file
prob.prob.g_faces = find(prob.mesh.types(prob.mesh.ele_infos(:,2),1)==2)';
prob.prob.ng_faces = size(prob.prob.g_faces,2);

% In both models we assume that all faces are free
% (the supports are set a posteriori)
prob.prob.incid_support = nan(3, prob.prob.ng_faces);
prob.prob.dir_support = zeros(1, prob.prob.ng_faces);

% Check for faces with kinematic constraints
% num_fix indicates the number of the supports
prob.prob.nR = 0;
for i = 1:prob.prob.ng_faces
    g_face = prob.prob.g_faces(i);
    tag = prob.mesh.ele_tags{g_face}(1); % Use the first tag
    which_tag = (prob.prob.Faces.tags == tag);
    if (any(which_tag))
        aux = prob.prob.Faces.vals{3}(which_tag);
        if (isnumeric(aux))
            prob.prob.dir_support(i) = aux;
        end
        for dir = 1:3
            if (prob.prob.Faces.vals{dir}(which_tag) == 0)
                prob.prob.incid_support(dir,i) = prob.prob.nR;
                prob.prob.nR = prob.prob.nR + 1;
            end
        end
    end
end

% Just initialize the other parts
prob.equil.degree_s = nan;
prob.equil.degree_v = nan;
prob.equil.calc_mats = false;
prob.equil.global = false;
prob.equil.recovered = false;

prob.compat.degree_u = nan;
prob.compat.calc_mats = false;
prob.compat.global = false;
prob.compat.recovered = false;

end

