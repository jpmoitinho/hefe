function [ Phi_T4 ] = Phi_T4( x )
% Shape functions for the 4 node tet

Phi_T4 = zeros(size(x,2),4);
Phi_T4(:,1) = 1 - x(1,:) - x(2,:) - x(3,:);
Phi_T4(:,2) = x(1,:);
Phi_T4(:,3) = x(2,:);
Phi_T4(:,4) = x(3,:);
