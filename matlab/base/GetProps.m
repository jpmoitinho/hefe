% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ props, string ] = GetProps( elem, map_entity, entity_tags, Block )
% Props2D Fields for a planar element
%   Detailed explanation goes here

g_elem = map_entity(elem);
tag = entity_tags{g_elem}(1); % Use the first tag
which_tag = find(Block.tags == tag);
if (isempty(which_tag))
    fprintf('Tag %d of elem %d is not defined\n', tag, elem);
else
    aux = [ Block.vals{1:(end-1)} ];
    props =  double(aux(which_tag,:));
    string = Block.vals{end}{which_tag};
end
    
end

