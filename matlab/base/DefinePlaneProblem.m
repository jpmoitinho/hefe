% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = DefinePlaneProblem( meshname, probname, funcs, dim_support_edge, dim_support_vert)
% DefineProblem - Load the mesh and initialize variables
%   
% The mesh

[a,b,c] = fileparts(meshname);
prob.meshname.path = a;
prob.meshname.name = b;
prob.meshname.ext = c;

prob.mesh = che_gmsh();
prob.mesh.load(meshname);

prob.geom.center = zeros(2, prob.mesh.nc);
prob.geom.sign = ones(prob.mesh.nc, 3);

for elem = 1:prob.mesh.nc
    [ cell_edges, cell_verts ] = prob.mesh.get_ev_c(elem);
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);
    prob.geom.center(:,elem) = sum(elem_coords)/size(cell_verts,2);
    for side = 1:3
        edge = cell_edges(side);
        [ ~, verts_edge ] = prob.mesh.get_cv_e(edge);
        if (verts_edge(1) ~= cell_verts(side)) % Same orientation
            prob.geom.sign(elem,side) = -1;
        end
    end
end

% The problem (boundary conditions, properties)

[a,b,c] = fileparts(probname);
prob.probname.path = a;
prob.probname.name = b;
prob.probname.ext = c;

% The path to the directory where the functions are

prob.funcs = funcs;

prob.prob = load_problem(probname);

% Organise the boundary conditions

% The edges explicitly defined in the gmsh file
prob.prob.g_edges = find(prob.mesh.types(prob.mesh.ele_infos(:,2),1)==1)';
prob.prob.ng_edges = size(prob.prob.g_edges,2);

% In both models we assume that all edges are free
% (the supports are set a posteriori)
prob.prob.incid_support = nan(dim_support_edge, prob.prob.ng_edges);
prob.prob.dir_support = zeros(1, prob.prob.ng_edges);

% Check for edges with kinematic constraints
% num_fix indicates the number of the supports
prob.prob.nR = 0;
for i = 1:prob.prob.ng_edges
    g_edge = prob.prob.g_edges(i);
    tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
    which_tag = (prob.prob.Edges.tags == tag);
    if (any(which_tag))
        aux = prob.prob.Edges.vals{dim_support_edge+1}(which_tag);
        if (isnumeric(aux))
            prob.prob.dir_support(i) = aux;
        end
        for dir = 1:dim_support_edge
            if (prob.prob.Edges.vals{dir}(which_tag) == 0)
                prob.prob.incid_support(dir,i) = prob.prob.nR;
                prob.prob.nR = prob.prob.nR + 1;
            end
        end
    end
end

if (exist('dim_support_vert','var'))
    % The verts explicitly defined in the gmsh file
    prob.prob.g_verts = find(prob.mesh.types(prob.mesh.ele_infos(:,2),1)==0)';
    prob.prob.ng_verts = size(prob.prob.g_verts,2);
    
    % In both models we assume that all verts are free
    % (the supports are set a posteriori)
    prob.prob.incid_support_verts = nan(dim_support_vert, prob.prob.ng_verts);
    prob.prob.dir_support_verts = zeros(1, prob.prob.ng_verts);
    
    % Check for verts with kinematic constraints
    % num_fix indicates the number of the supports
    prob.prob.nRv = 0;
    if (isfield(prob.prob, 'Verts'))
        for i = 1:prob.prob.ng_verts
            g_vert = prob.prob.g_verts(i);
            tag = prob.mesh.ele_tags{g_vert}(1); % Use the first tag
            which_tag = (prob.prob.Verts.tags == tag);
            if (any(which_tag))
                aux = prob.prob.Verts.vals{dim_support_vert+1}(which_tag);
                if (isnumeric(aux))
                    prob.prob.dir_support_verts(i) = aux;
                end
                for dir = 1:dim_support_vert
                    if (prob.prob.Verts.vals{dir}(which_tag) == 0)
                        prob.prob.incid_support_verts(dir,i) = prob.prob.nRv;
                        prob.prob.nRv = prob.prob.nRv + 1;
                    end
                end
            end
        end
    end
end

% Just initialize the other parts
prob.equil.degree_s = nan;
prob.equil.degree_v = nan;
prob.equil.calc_mats = false;
prob.equil.global = false;
prob.equil.recovered = false;

prob.compat.degree_u = nan;
prob.compat.calc_mats = false;
prob.compat.global = false;
prob.compat.recovered = false;

end

