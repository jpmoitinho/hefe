classdef chf_gmsh < chf
    % CHF_GMSH Subclass of chf class for a mesh defined in a gmsh file.
    % Please, refer to documentation of chf class.
    %
    % Using this instantiation the user can look at the data in the gmsh
    % file from two viewpoints:
    %   - as defined in gmsh (the elements)
    %   - as a set of topological entities of decreasing dimension
    %         (cells, faces, edges, vertices)
    %
    % TODO:
    % Devia haver iteradores nas faces, nos vertices?
    
    properties
        %
        % From the mesh, related to the elements
        %
        n_elem       % Number of elements (as defined in gmsh, i.e. n_elem may 
                     %                     include nodes, lines and volumes)
        n_elem_by_dimension % 
        ele_infos    % Information about the elements: 1 - element number, 2 - type, 3 - ntags
        ele_nodes    % The nodes of the elements
        ele_tags     % The tags assigned to the element
        elem_of_number %
        %
        % From the mesh, related to the nodes
        %
        n_nodes        % Number of nodes
        coords         % The coordinates of the nodes
        %
        % Some of these tables can probably be implemented as functions of the
        % class, which return the input value when the mapping is an identity
        % and the value in the table otherwise
        %
        number_of_node % The number assigned to each node
        node_of_number
        %
        % Maps needed for interacting with the topological entities
        % Some of these numbers may be zero
        % ZP Cuidado com number_of_node e com esses meninos
        %
        elems_cells % The number of the 3D element of a given cell
        elems_faces % The number of the 2D element of a given face
        elems_edges % The number of the 1D element of a given edge
        faces_elems % The face of a 2D element with a given number
        %
        % The number of the topological entity (cell, face, edge)
        % correponding to a given element
        topol_elems 
        % The mapping from vertex number to node number
        nodes_verts
        verts_nodes
        %
        % Specific to the class
        % ZP Deviam ser static, ou algo assim?
        %
        % Characteristics (0D/1D/2D/3D, nnodes, what_cell)
        % of each type of element defined in gmsh
        types   
        % Should be possible to automatically update from the source!
        % cells1 % Always a line
        % cells2 % Not really necessary either a triangle or a
        % quadrilateral (we keep the number of sides)
        cells3
    end

    properties % ZP Should be private?
    end
    
    methods
        % Constructor method.
        function obj = chf_gmsh
            obj = obj@chf;        % using constructor without arguments
            obj.types = [ ...
                1 2 2 ; ... % Lines L2     % 1 
                2 3 3 ; ... % Tris T3
                2 4 4 ; ... % Quads Q4
                3 4 1 ; ... % Tets Tet4
                3 8 2 ; ... % Hexas H8     % 5
                3 6 3 ; ... % Prism Pr6
                3 5 4 ; ... % Pyramid Py5
                1 3 2 ; ... % Lines L3
                2 6 3 ; ... % Tris T6
                2 8 4 ; ... % Quads Q9     % 10
                3 10 1 ; ... % Tets T10
                3 27 2 ; ... % Hexas H27
                3 18 3 ; ... % Prism Pr18
                3 14 4 ; ... % Pyramid 14
                0 1 1 ; ... % Points       % 15
                2 8 4 ; ... % Quads Q8 
                3 20 1 ; ... % Hexas H20
                3 15 2 ; ... % Prism Pr15
                3 13 3 ]; % Pyramid 13
            obj.cells3 = struct(...
                'nv', {4, 8, 6, 5}, ...
                'nf', {4, 6, 5, 5}, ...
                'fv', {...
                [2 3 4; 3 1 4; 4 1 2; 1 3 2], ... % Tets
                [1 4 3 2; 5 6 7 8; 1 5 8 4; 2 3 7 6; 1 2 6 5; 4 8 7 3], ... % Hexas
                [1 3 2 0; 4 5 6 0; 1 2 5 4; 2 3 6 5; 3 1 4 6], ... % Prisms
                [1 4 3 2; 1 2 5 0; 2 3 5 0; 3 4 5 0; 4 1 5 0]}); % Pyramids
        end

        % Load data from a file
        function load(obj, name)
            % Inspired in load_gmsh2.m by JP Moitinho de Almeida (moitinho@civil.ist.utl.pt)
            % and  R Lorphevre(r(point)lorphevre(at)ulg(point)ac(point)be)
            % Clean up the existing object
            obj.clean();
                     
            fid = fopen(name, 'r');
            
            %% Identify file format
            tline = fgetl(fid);
            if (feof(fid))
                fprintf('Empty file: %s',  filename);
                % In case of error the clean object is returned
                return;
            end
            if (strcmp(tline, '$NOD'))
                fileformat = 1;
            else
                if (strcmp(tline, '$MeshFormat'))
                    fileformat = 2;
                    tline = fgetl(fid);
                    if (feof(fid))
                        fprintf('Syntax error (no version) in: %s',  filename);
                        fileformat = 0;
                    end
                    [ form ] = sscanf( tline, '%f %d %d');
                    if (form(1) ~= 2 && (form(1) ~= 2.1 && form(1) ~= 2.2))
                        fprintf('Unknown mesh format: %s', tline);
                        fileformat = 0;
                    end
                    if (form(2) ~= 0)
                        fprintf('Sorry, this program can only read ASCII format');
                        fileformat = 0;
                    end
                    fgetl(fid);    % this should be $EndMeshFormat
                    if (feof(fid))
                        fprintf('Syntax error (no $EndMeshFormat) in: %s',  filename);
                        fileformat = 0;
                    end
                    tline = fgetl(fid);    % this should be $Nodes or $PhysicalNames
                    if (feof(fid))
                        fprintf('Syntax error (no $Nodes or $PhysicalNames) in: %s',  filename);
                        fileformat = 0;
                    end
                else
                    fprintf('Syntax error (no version?) in: %s',  filename);
                    fileformat = 0;
                end
            end
            
            if (~fileformat)
                return
            end
            
            if strcmp(tline, '$PhysicalNames')
                tline = fgetl(fid);
                while ~strcmp(tline, '$EndPhysicalNames')
                    tline = fgetl(fid);
                    if (feof(fid))
                        fprintf('Syntax error (no $EndPhysicalNames) in: %s',  filename);
                        fileformat = 0;
                    end
                end
                if (~fileformat)
                    return
                end
                tline = fgetl(fid); % this should be $Nodes
            end
      
            %% Read nodes
            
            if strcmp(tline, '$NOD') || strcmp(tline, '$Nodes')
                obj.n_nodes = fscanf(fid, '%d', 1);
                aux = fscanf(fid, '%g', [4 obj.n_nodes]);
                obj.coords = aux(2:4,:)';
                obj.number_of_node = aux(1,:); % This may not be an identity
                fgetl(fid); % End previous line
                fgetl(fid); % Has to be $ENDNOD $EndNodes
            else
                fprintf('Syntax error (no $Nodes/$NOD) in: %s',  filename);
                fileformat = 0;
            end
            
            if (~fileformat)
                obj.clean();
                return
            end
            
            %% Read elements
            tline = fgetl(fid);
            if strcmp(tline,'$ELM') || strcmp(tline, '$Elements')
                obj.n_elem = fscanf(fid, '%d', 1);
                obj.topol_elems = zeros(1, obj.n_elem);
                % read all info about elements into aux (it is faster!)
                aux = fscanf(fid, '%g', inf);
                start = 1;
                obj.ele_infos = zeros(obj.n_elem, 3); % 1 - id, 2 - type, 3 - ntags
                obj.ele_nodes = cell(obj.n_elem,1);
                obj.nodes_verts = [];
                obj.ele_tags = cell(obj.n_elem,1);
                % Defaults for ele_tags
                % format 1: 1 - physical number, 2 - geometrical number
                % format 2: 1 - physical number, 2 - geometrical number, 3 - mesh partition number
                for I = 1:obj.n_elem
                    if (fileformat == 2)
                        finish = start + 2;
                        obj.ele_infos(I, 1:3) = aux(start:finish);
                        ntags = aux(finish);
                        start = finish + 1;
                        finish = start + ntags -1;
                        obj.ele_tags{I} = aux(start:finish);
                        start = finish + 1;
                    else
                        finish = start + 1;
                        obj.ele_infos(I, 1:2) = aux(start:finish);
                        start = finish + 1; 
                        % the third element is ntags, which was fixed
                        obj.ele_infos(I, 3) = 2;
                        finish = start + 1;
                        obj.ele_tags{I} = aux(start:finish);
                        start = finish + 2; % ZP Porque +2 e nao +1?
                    end
                    type = obj.ele_infos(I, 2);
                    nnodes = obj.types(type,2);
                    if (obj.types(type,1) == 3)
                        % Filter, so that only the "topological nodes" 
                        % of 3D cells are inserted as vertices
                        nverts = obj.cells3(obj.types(type,3)).nv;
                        finish = start + nverts - 1;
                        % ZP Check use of IDS !!!!!
                        obj.nodes_verts = unique( [ obj.nodes_verts aux(start:finish)']);
                    end
                    finish = start + nnodes - 1;
                    % ZP Check use of IDS !!!!!
                    obj.ele_nodes{I} = aux(start:finish);
                    start=finish + 1;
                end
                fgetl(fid); % Has to be $ENDELM or $EndElements
            else
                fprintf('Syntax error (no $Elements/$ELM) in: %s',  filename);
                fileformat = 0;
            end
            
            if (~fileformat)
                obj.clean();
                return
            end
            fclose(fid);

            sparseovhd = 2; % The overhead of using a sparse vector
            % 2 is a guess for memory, may be tuned

            % Do the mapping between numbers
            max_node_number = max(obj.number_of_node);
            if (obj.n_nodes*sparseovhd < max_node_number) 
                obj.node_of_number = sparse(1, obj.number_of_node, 1:obj.n_nodes, ...
                    1, max_node_number);
            else
                obj.node_of_number = zeros(1, max_node_number);
                obj.node_of_number(obj.number_of_node) = 1:obj.n_nodes;
            end

            max_elem_number = max(obj.ele_infos(:,1));
            if (obj.n_elem*sparseovhd < max_elem_number) 
                obj.elem_of_number = sparse(1, obj.ele_infos(:,1), 1:obj.n_elem, ...
                    1, max_elem_number);
            else
                obj.elem_of_number = zeros(1, max_elem_number);
                obj.elem_of_number(obj.ele_infos(:,1)) = 1:obj.n_elem;
            end

            
            % Is this really necessary?
            for dim = 0:3
                obj.n_elem_by_dimension(dim+1) = sum(obj.types(obj.ele_infos(:,2),1)==dim);
            end
            
            obj.nv = size(obj.nodes_verts,2);
            obj.nc = obj.n_elem_by_dimension(4); % 1 is for 0D
            
            % Build the inverse map verts_nodes
            if (obj.nv*sparseovhd < obj.n_nodes) 
                obj.verts_nodes = sparse(1, obj.nodes_verts, 1:obj.nv, 1, obj.n_nodes);
            else
                obj.verts_nodes = zeros(1, obj.n_nodes);
                obj.verts_nodes(obj.nodes_verts) = 1:obj.nv;
            end
            
            % Only register the 3d cell types that are present in the mesh
            % (are there reasons for registering everything?)
            all3d = unique(obj.ele_infos(obj.types(obj.ele_infos(:,2),1) == 3,2));
            allcell = unique(obj.types(all3d, 3));
            for cell_type = allcell'
                obj.register_cell_templ(cell_type, obj.cells3(cell_type).nv, ...
                    obj.cells3(cell_type).nf, obj.cells3(cell_type).fv);
            end
            
            % Build the inverse map elems_cells (Always dense?)
            obj.elems_cells = find(obj.types(obj.ele_infos(:,2),1)==3);
            obj.build();

            % Build the map (and set the inverse) elems_edges 
            % (Always sparse?)
            if (0)
                obj.elems_edges = sparse(1, obj.ne);
                for elem = find(obj.types(obj.ele_infos(:,2),1)==1)'
                    % Could be simpler? (Pick one element adjacent to the
                    % vertex and use its info to track the edge?)
                    verts = obj.verts_nodes(obj.ele_nodes{elem});
                    [~, ~, edges_start] = obj.get_cfe_v(verts(1));
                    [~, ~, edges_end] = obj.get_cfe_v(verts(2));
                    edge = intersect(edges_start, edges_end);
                    if (numel(edge)==0 || numel(elem)==0)
                        fprintf('%d %d\n', elem, edge);
                    end
                    obj.elems_edges(edge) = elem;
                    obj.topol_elems(elem) = edge;
                end
            end
            
            % Build the map (and inverse) elems_faces (Always sparse?)
            if (1)
                obj.elems_faces = sparse(1, obj.nf);
                obj.faces_elems = sparse(1, obj.n_elem);
                % Loop for all 2D elements
                for elem = find(obj.types(obj.ele_infos(:,2),1)==2)' 
                    % the verts of this 2D element
                    verts = obj.verts_nodes(obj.ele_nodes{elem}); 
                    % Two of the edges of this 2D element
                    e1 = obj.get_edge_vs(verts(1), verts(2));
                    e2 = obj.get_edge_vs(verts(2), verts(3));
                    % The faces of these edges
                    [ ~, f1, ~ ] = obj.get_cfv_e(e1);
                    [ ~, f2, ~ ] = obj.get_cfv_e(e2);
                    % The face we were looking for is the only member of the
                    % intersection
                    face = intersect(f1, f2);
                    obj.elems_faces(face) = elem;
                    obj.faces_elems(elem) = face;
                end
            end
        end
        
        %% 
        % Implementation of the abstract method that returns  
        % the type and the vertex list of a given solid cell.
        function [ type, verts ] = get_cell(obj, c_id)
            elem = obj.elems_cells(c_id);
            type = obj.types(obj.ele_infos(elem,2),3);
            % Should only return the topology nodes
            verts = obj.verts_nodes(obj.ele_nodes{elem});
        end

    end
    
end

