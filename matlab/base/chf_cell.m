%% CHF_CELL Compact Half-Face solid cell template class.
%
%% Description
% This is an auxiliary class that creates a generic polyhedral cell
% template to be used by class <chf.html *chf*>.
%
% Generic cell topological description:
%%%
% * *nv*         - number of vertices of cell
% * *nf*         - number of faces of cell
% * *face_incid* - matrix (*nf* x *max_num_face_vertices*) with local
%                  numbering of face vertex incidence:
%                  For each face indicates a list of its vertices, given in
%                  counter-clockwise order as looking from outside the
%                  cell. *max_num_face_vertices* is implicitly indicated by
%                  the number of columns of *face_incid*. Faces with less
%                  than *max_num_face_vertices* have null indices at the end of
%                  the corresponding lines in *face_incid*.
% * *fv*         - matrix (*nv* x *nv*) with used face indices:
%                  *fv* indicates edge adjacency relationships between vertices.
%                  *fv* contains face indices and null values. Each face
%                  index corresponds to a pair of adjacent-by-edge
%                  vertices. The indicated face is the one that uses the edge
%                  formed by the two vertices in counter-clockwise order.
%                  *fv* has non-zero values placed symmetrically.
%                  The pair of faces *fv(i,j)* e *fv(j,i)* are adjcent by edge.
%                  *fv* is automatically built from *nv*, *nf*, and *face_incid*.
% * *ff*         - matrix (*nf* x *max_num_face_vertices*) with face-face
%                  adjacency relationships:
%                  Each row of *ff* indicates, for a face, a counter-clockwise
%                  ordered list of adjacent faces, starting from the first
%                  edge.  Faces with less than *max_num_face_vertices* have null
%                  indices at the end of the corresponding lines in *ff*.
%
%% Reference
%%%
% * Lage, M.; Martha, L.F.; Moitinho de Almeida, J.P.; Lopes, H.,
% �IBHM: index-based data structures for 2D and 3D hybrid meshes�,
% Engineering with Computers, Vol. 33, Issue 4, pp. 727-744.
% Springer-Verlag London,
% ISSN: 0177-0667, DOI: 10.1007/s00366-015-0395-0.
% http://link.springer.com/article/10.1007%2Fs00366-015-0395-0
%
%% History
%
% @VERSION 1.00 on 17-Jan-2013. Initial version.
%
% @VERSION 1.01 on 08-Jul-2013.
% Modifications:
% Set protected access to properties: *nv*, *nf*, *face_indic*, *fv*, *ff*.
% Made this class a subclass of *hgsetget* instead of *handle*.
% Modified get methods to the formal get methods of a subclass of
% *hgsetget*: obj.get('XXX').
%
% @VERSION 2.00 on 26-Jul-2019.
% Modifications:
% Formating comments for Publish command.
% Made this class a subclass of *handle* instead of *hgsetget*, i.e.,
% currently this is a handle class without set and get methods. 
%
%% Class definition
classdef chf_cell < handle
    %%
    % <https://www.mathworks.com/help/matlab/ref/handle-class.html
    % See documentation on *handle* super-class>.

    %% Public properties
    properties (Access = public)
        nv = 0;          % number of vertices of cell
        nf = 0;          % number of faces of cell
        face_incid = []; % matrix with local numbering of face vertex incidence
        fv = [];         % matrix with used face indices
        ff = [];         % matrix with face-face adjacency relationships
    end

   %% Constructor method
   methods
        %-----------------------------------------------------------------------
        % Builds a cell template based of given local face vertex incidence
        function obj = chf_cell(num_v, num_f, f_incid)
            obj.nv = num_v; % Stores given number of vertices in object
            obj.nf = num_f; % Stores given number of faces in object
            obj.face_incid = f_incid; % Stored face vertex incidences in object

            % Build fv matrix, which stores the number of the oriented edges of
            % the faces.
            % Adjacent faces are in symmetric positions (more detail in the
            % class description).
            obj.fv = zeros(obj.nv, obj.nv);
            for f = 1:obj.nf
                % The starting/ending node of each edge of face f
                nodei = obj.face_incid(f,obj.face_incid(f,:)>0);
                nodej = circshift(nodei, [0 -1]);
                % sub2ind seems necessary to store f in the location of our edges
                % (is there another way to do it in matlab without a loop?)
                obj.fv(sub2ind(size(obj.fv), nodei, nodej)) = f;
            end
            
            % Check consistency
            if (any(any((obj.fv>0) & (obj.fv==obj.fv'))))
                fprintf('Inconsistent. At leat one non-zero term in fv is symmetric.\n');
            end
            if (~isequal(obj.fv>0, obj.fv'>0))
                fprintf('Inconsistent. fv is not topologically symmetric.\n');
            end
    
            % Build Face adjacency (same dance as above)
            obj.ff = zeros(obj.nf, size(obj.face_incid, 2));
            for f = 1:obj.nf
                nodei = obj.face_incid(f,obj.face_incid(f,:)>0);
                nodej = circshift(nodei, [0 -1]);
                obj.ff(f, nodei>0) = obj.fv(sub2ind(size(obj.fv), nodej, nodei));
            end
        end
    end
end