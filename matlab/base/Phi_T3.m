% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ Phi_T3 ] = Phi_T3( x )
% Shape functions for the 3 node triangle

Phi_T3 = zeros(size(x,2),3);
Phi_T3(:,1) = 1 - x(1,:) - x(2,:);
Phi_T3(:,2) = x(1,:);
Phi_T3(:,3) = x(2,:);
