% base = '../KT/data/Example2D%s.msh';
namein = sprintf(base, '');
nameout = sprintf(base, '-m');

m = che_gmsh();
m.load(namein);

% The vertices explicitly defined in the gmsh file
g_verts = find(m.types(m.ele_infos(:,2),1)==0)';
ng_verts = size(g_verts,2);

out = fopen(nameout, 'w');

for i=1:m.nc
    [~, verts] = m.get_ev_c(i);
    if (length(verts) ~= 3)
        fprintf ('Element %d (%d) has %d verts instead of 3\n', ...
            i, m.elems_faces(i), length(verts));
    end
end

fprintf(out, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n$Nodes\n%d\n', ...
    m.nv+m.nc);

for i=1:m.nv
    node = m.nodes_verts(i);
    fprintf(out, '%d %f %f %f\n', i , m.coords(i,:));
end

for i=1:m.nc
    [~, verts] = m.get_ev_c(i);
    nodes = m.nodes_verts(verts);
    pos = sum(m.coords(nodes,:))/3;
    fprintf(out, '%d %f %f %f\n', m.nv+i , pos);
end

fprintf(out, '$EndNodes\n$Elements\n%d\n', ...
    ng_verts+m.n_elem_by_dimension(2)+m.nc*3);

count = 0;
for i=1:m.n_elem
    if (m.ele_infos(i,2) == 1 || m.ele_infos(i,2) == 15 ) % 2 nodes line and nodes
        count = count + 1;
        fprintf(out, '%d %d %d', count, m.ele_infos(i,2), ...
            length(m.ele_tags{i}));
        fprintf(out, ' %d', m.ele_tags{i});
        fprintf(out, ' %d', m.ele_nodes{i});
        fprintf(out, '\n');
    end
end
    
nextIJK = [ 2 3 1 ];
prevIJK = [ 3 1 2 ];
for i=1:m.nc
    [~, verts] = m.get_ev_c(i);
    elem = m.elems_faces(i);
    for k = 1:3
        %         k1 = mod(k,3)+1;
        count = count + 1;
        fprintf(out, '%d 2 %d', count, length(m.ele_tags{elem}));
        fprintf(out, ' %d', m.ele_tags{elem});
        %         fprintf(out, ' %d %d %d\n', verts([k,k1]), m.nv+i);
        fprintf(out, ' %d %d %d\n', m.nv+i, verts(nextIJK(k)), verts(prevIJK(k)));
    end
end

fprintf(out, '$EndElements\n');

fclose(out);