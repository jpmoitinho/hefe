% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ DPhi_T3 ] = DPhi_T3(~)
% FFORMA Calcula as derivadas das funcoes de forma no elemento mestre
%  Linha 1 -> d Phi/d chi
%  Linha 2 -> d Phi/d eta

%  Coluna 1 -> Phi 1
%  Coluna 2 -> Phi 2
%  Coluna 3 -> Phi 3

DPhi_T3 = zeros(2,3);

DPhi_T3(1,1) = -1;
DPhi_T3(1,2) = 1;

DPhi_T3(2,1) = -1;
DPhi_T3(2,3) = 1;
