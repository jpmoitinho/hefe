function [ Rule3D, Rule2D ] = def_integration_3D( degreev, degreef )
%Define the numerical integration rules

max_integrand_3d = degreev; % Integrate sigma'*f*sigma
max_integrand_2d = degreef;

if (max_integrand_3d > 15)
    max_integrand_3d = 15;
    fprintf("Asked for degree %d, using 15, the highest available\n", degreev);
end

% RULES FOR TETS
n = tetrahedron_arbq_size ( max_integrand_3d );
  
% fprintf ('Using %d points for functions of degree %d in tets\n', ...
%     n, max_integrand_3d);
[xyz, Rule3D.GW] = tetrahedron_arbq ( max_integrand_3d, n );
Rule3D.xietazeta=[(3+4*sqrt(3)*(xyz(2,:))-sqrt(6)*(xyz(3,:)))/12;...
                  (3+6*(xyz(1,:))-2*sqrt(3)*(xyz(2,:))-sqrt(6)*(xyz(3,:)))/12;...
                  (1+sqrt(6)*(xyz(3,:)))/4];
Rule3D.npts = n;

% Adjust the weights for the tetrahedron
Rule3D.GW = Rule3D.GW /( 6*sum(Rule3D.GW));

% RULES FOR TRIS
aux = set_xiao_gimbutas_standard(max_integrand_2d);

Rule2D.xieta = aux(:,1:2)';
Rule2D.GW =  aux(:,3)';
Rule2D.npts = size(aux,1);

% the weights are adjusted for the triangle
% Rule2D.GW = Rule2D.GW / 2;


