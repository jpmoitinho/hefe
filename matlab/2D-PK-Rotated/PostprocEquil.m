% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = PostprocEquil( prob, type, sol, s0, rotate)
% Post process an equilibrium solution
%
% Create a text file with some results
namename = prob.meshname.name;
probname = prob.probname.name;

degree = prob.equil.degree_s;
nB = (degree+1)*(degree+2)/2;
nS = (degree+1)*(degree+6)/2;
nS0 = (degree)*(degree+1)/2;
nV = degree+1;

stressfuncs = eval(sprintf("@DefStressPK_d%d", degree));
stress0funcs = eval(sprintf("@DefStress0PK_d%d", degree));

DPhi = DPhi_T3();

if ((size(sol, 1) == prob.mesh.nc) && (size(sol, 2) == prob.equil.DS.nS))
    byelem = true;
else
    byelem = false;
end

outname = sprintf('Results/%s-%s-Stress-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/ISchemeEq_2D_%d.msh', prob.funcs, degree);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

weights = zeros(nB, 3, prob.mesh.nc);

incid_s = 1:nS;

for elem = 1:prob.mesh.nc
    [ ~, verts ] = prob.mesh.get_ev_c(elem);
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts),1:2);

    % Jacobian of the element
    Jac = (DPhi*elem_coords)';
    
    % Rotate (scaling by the length to obtain a "true" rotation matrix)
    Lxi = norm(Jac(:,1));
    R = [ Jac(1,1), Jac(2,1); -Jac(2,1), Jac(1,1) ]/Lxi;
    RJac = R*Jac;
    
    F11R = RJac(1,1); F12R = RJac(1,2); F21R = RJac(2,1); F22R = RJac(2,2);

    % Use the Jacobian for the local frame, then rotate the components of
    % the stress tensor
    TP0 = [F11R,0,F12R,0;0,F22R,0,F21R;F21R,0,F22R,0];
    c = R(1,1); s = R(2,1);
    TP = [ ...
        c^2, s^2, 2*c*s; ...
        s^2, c^2, -2*c*s; ...
        -c*s, c*s, c^2-s^2]*TP0;
    
    [ TS, TD ] = stressfuncs(F11R,F21R,F12R,F22R);
    [ ~, L0 ] = stress0funcs(F11R,F21R,F12R,F22R);

    TPTSfull = kron(TP*TS, speye(nB));
    dJac = det(Jac);
    aux = (TPTSfull*TD*sol(incid_s) + TPTSfull(:,L0)*prob.equil.s0(:,1,elem))/dJac;
    weights(:, :, elem) = reshape( aux, [ nB 3 ]);
    incid_s = incid_s + nS;
end

% variable rotate has the rotation angle of x' relative to x (in radians)
if (exist('rotate', 'var'))
    sa = sin(rotate);
    ca = cos(rotate);
    T = [ ca*ca sa*sa 2*ca*sa ; sa*sa ca*ca -2*ca*sa ; -ca*sa ca*sa (ca*ca-sa*sa) ]';
    for elem = 1:prob.mesh.nc
        weights(:, :, elem) = weights(:, :, elem)*T;
    end
end

names = {'Sxx', 'Syy', 'Sxy'};
for i = 1:3
    fprintf(output, '$ElementNodeData\n2\n');
    fprintf(output, '"%s (%s) Stresses - %s %s"\n"Basis2D %d"\n', names{i}, ...
        type, namename, probname, degree);
    fprintf(output, '1\n0.0\n3\n0\n1\n%d\n', prob.mesh.nc);
    for elem = 1:prob.mesh.nc
        fprintf(output, '%d %d', prob.mesh.elems_faces(elem), nB);
        fprintf(output, ' %e', weights(:, i, elem));
        fprintf(output,'\n');
    end
    fprintf(output, '$EndElementNodeData\n');
end

fclose(output);

outname = sprintf('Results/%s-%s-Disps-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');

aux = sprintf('%s/ISchemeEq_1D_%d.msh', prob.funcs, degree);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

fprintf(output, '$Nodes\n');
% We need to define all nodes and edges
% By using offsets we can merge these results with the other mesh, without
% messing up numbers
offset_verts = prob.mesh.nv;
offset_elems = prob.mesh.n_elem;

fprintf(output, '%d\n', prob.mesh.nv);
fprintf(output, '%d %e %e 0\n', ...
    [ offset_verts+(1:prob.mesh.nv); prob.mesh.coords(prob.mesh.nodes_verts(1:prob.mesh.nv), [1 2])']);
fprintf(output, '$EndNodes\n$Elements\n%d\n', prob.mesh.ne);
for i = 1:prob.mesh.ne
    [~, verts] = prob.mesh.get_cv_e(i);
    fprintf(output, '%d 1 0 %d %d\n', offset_elems+i, offset_verts+verts);
end
fprintf(output, '$EndElements\n');

if (~byelem)
    % The solution
    fprintf(output, '$ElementNodeData\n2\n');
    fprintf(output, '"%s Disps - %s %s"\n"Basis1D %d"\n', type, namename, probname, degree);
    fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.ne);
    
    incid_v = prob.mesh.nc*nS + (1:nV);
    for edge = 1:prob.mesh.ne
        fprintf(output, '%d %d', offset_elems+edge, nV);
        fprintf(output, ' %e', [ sol(incid_v), sol(incid_v + nV), zeros(nV,1)]');
        incid_v = incid_v + 2*nV;
        fprintf(output, '\n');
    end
    fprintf(output, '$EndElementNodeData\n');
    
%     if ( isfield( prob.equil, 'bdisp') )
%         % Without the skm's
%         fprintf(output, '$ElementNodeData\n2\n');
%         fprintf(output, '"%s Filtered Disps - %s %s"\n"Basis %d"\n', type, namename, probname, degree);
%         fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.ne);
%         
%         aux = (1:nV);
%         for edge = 1:prob.mesh.ne
%             fprintf(output, '%d %d', offset_elems+edge, nV);
%             fprintf(output, ' %e', [ prob.equil.bdisp(aux), prob.equil.bdisp(aux + nV), zeros(nV,1)]');
%             aux = aux + 2*nV;
%             fprintf(output, '\n');
%         end
%         fprintf(output, '$EndElementNodeData\n');
%     end

end

% The boundary tractions

fclose(output);
