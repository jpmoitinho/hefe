% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = EquilMats( prob )
%
% EquilMats - Compute the matrices relevant for equilibrium formulations
%   

% Define the integration rules for _this_ problem
% excess defines how much we add (should be zero?)
excess = 20;
[ NI2D, NI1D ] = def_integration_2D(prob.equil.degree_s*2+excess, ...
    prob.equil.degree_s+prob.equil.degree_v+excess);

% Stress approximation definitions
prob.equil.DS = DefStressKT(prob.equil.degree_s);
nS = prob.equil.DS.nS;
nS0 = prob.equil.DS.nS0;

% Boundary displacements approximation definitions
nV = prob.equil.degree_v + 1;
ViG = reshape(function_V1D(NI1D.xi, prob.equil.degree_v)', [1 nV NI1D.npts]);
VG = zeros(2, 2*nV, NI1D.npts);
VG(1, 1:nV, :) = ViG;
VG(2, nV+(1:nV), :) = ViG;

% The shape functions of the element (T3) at the 2D Gauss points
Phi = Phi_T3(NI2D.xieta);
DPhi = DPhi_T3();

% The shape functions of the linear side at the 1D Gauss points
Phi_side = [ (1-NI1D.xi)/2, (1+NI1D.xi)/2 ];

% The parameters of the particular solution
prob.equil.s0 = zeros(nS0, 1, prob.mesh.nc);
% The integrals of the parycular solutions with themselves
prob.equil.s0s0 = zeros(1, prob.mesh.nc);

% The elemental matrices
prob.equil.Flex = zeros(nS, nS, prob.mesh.nc);
prob.equil.e0 = zeros(nS, prob.mesh.nc);
prob.equil.Dside = zeros(2*nV, nS, 3, prob.mesh.nc);
prob.equil.Dvert = zeros(nS, 3, prob.mesh.nc);
prob.equil.tside = zeros(2*nV, 3, prob.mesh.nc);
prob.equil.tvert = zeros(3, prob.mesh.nc);

prob.geom.ledges = zeros(1, prob.mesh.ne);

GWSide = reshape(NI1D.GW, [ 1 1 NI1D.npts ]);

% The matrix that projects the boundary disp approximations 
% onto themselves (the same for all sides)
%
% The Jacobian (the length of the side) is inserted when post processing
aux = sum(mtimesx(mtimesx(ViG, 't', ViG), GWSide), 3);
prob.equil.VtV = aux;

for elem = 1:prob.mesh.nc
    [ cell_edges, cell_verts ] = prob.mesh.get_ev_c(elem);
    [ props, bload ] = GetProps(elem, prob.mesh.elems_faces, ...
        prob.mesh.ele_tags, prob.prob.Faces);
    Elast = props(1);
    nu = props(2);
    h = props(3);
    
    oh2 = 1/h^2;    
    flex = [ oh2 -nu*oh2 0; -nu*oh2 oh2 0; ...
        0 0 2*oh2*(1+nu)]*12/Elast/h; %

    % Get global coordinates of element. Use a frame at its center
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);

    % Transform the Gauss points into coordinates
    coordGP_g = Phi*elem_coords;

    % Properly centered and scaled
    coordGP = coordGP_g - ...
        ones(NI2D.npts,1)*prob.geom.center(:,elem)';
    elem_coords = elem_coords  - ...
        ones(3,1)*prob.geom.center(:,elem)';
    
    % Determinant of the Jacobian of the element
    dJac = det(DPhi*elem_coords);
    GWdJ = reshape(NI2D.GW*dJac, [ 1 1 NI2D.npts ]);
    

    if (strcmp(bload,'[zero]'))
        has_s0 = false;
    else
        % Find the weigths of the particular solution
        bfgp = reshape(atpoint2D( bload, coordGP_g)', [ 1 1 NI2D.npts]);
        [has_s0, prob.equil.s0(:, 1, elem)] = PartSol(bfgp, prob.equil.DS, coordGP, GWdJ);
    end
    
    % Equilibrated stresses at the Gauss points
    StressGP = CalcStressKT(coordGP, prob.equil.DS);
    % Equilibrated stresses at the vertices
    StressV = CalcStressKT(elem_coords, prob.equil.DS);

    ElemFlex = sum(mtimesx(GWdJ, mtimesx(StressGP, 't', ...
        mtimesx(-flex,StressGP))), 3);
    ElemFlex = (ElemFlex+ElemFlex')/2;
    % make the matrix "exactly" symmetric
    prob.equil.Flex(:,:,elem) = ElemFlex;

    % The equivalent strains due to the particular solution
    if (has_s0)
        Stress0GP = CalcStress0KT(coordGP, prob.equil.DS, ...
            prob.equil.s0(:,1,elem));
        prob.equil.e0(:,elem) = sum(mtimesx(GWdJ, mtimesx(StressGP, 't', ...
            mtimesx(flex,Stress0GP))), 3);
        prob.equil.s0s0(elem) = sum(mtimesx(GWdJ, mtimesx(Stress0GP, 't', ...
            mtimesx(flex,Stress0GP))), 3);       
        Stress0V = CalcStress0KT(elem_coords, prob.equil.DS, prob.equil.s0(:,1,elem));
    end
    
    % Loop on the edges of the element
    Dv = zeros(3, nS);
    bv = zeros(3, 1);
    for edge = 1:3
        [ ~, verts_edge ] = prob.mesh.get_cv_e(cell_edges(edge));
        % Check orientation of cell_edge relative to the edge entity
        % The orientation of the edge entity is the reference
        pm = prob.geom.sign(elem, edge);

        % GP's of the edge, in the frame of the element
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]) - ...
            ones(2,1)*prob.geom.center(:, elem)';

        coordGP = Phi_side*vert_coords;
        StressGP = CalcStressKT(coordGP, prob.equil.DS);
        [ DStressGPx, DStressGPy ] = CalcDStressKT(coordGP, prob.equil.DS);
        
        dxdy = pm*(vert_coords(2,:) - vert_coords(1,:));
        
                %         mn = mx * nx^2 + my * ny^2 + 2 * mxy * nx* ny;
        %         rn = dmxdx*(nx + nx*ny^2) - dmxdy*nx^2*ny - dmydx*nx*ny^2+...
        %             + dmydy*(ny + nx^2*ny) + dmxydx*(2*ny^3) + dmxydy*(2*nx^3);

        % This normal is not divided by the length
        lside = norm(dxdy);
        prob.geom.ledges(cell_edges(edge)) = lside;

        dxdy = dxdy/lside; % ????????????????????
        nx = dxdy(2); ny = -dxdy(1);
        Normal = pm*[ nx^2 ny^2 2*nx*ny ];
        NormalDx = [ (nx+nx*ny^2) -(nx*ny^2) (2*ny^3) ];
        NormalDy = [ -(nx^2*ny) (ny+nx^2*ny) (2*nx^3) ];
        
        % Here we need a unit normal (no integration to compensate)
        %         mnt = (M(:,:,2) - M(:,:,1))*nx*ny + M(:,:,3)*(nx^2-ny^2);
        NormalMxy = [ (-nx*ny) (nx*ny) (nx^2-ny^2) ];

                % The edge terms
        TSide = [ mtimesx(NormalDx, DStressGPx) + mtimesx(NormalDy, DStressGPy); ...
            mtimesx(Normal, StressGP) ];
        vtside = mtimesx(VG, 't', TSide);
        
        % Not multiplied by the length (compensates for the normal)
        GWdJ = lside*reshape(NI1D.GW, [ 1 1 NI1D.npts ]);
        prob.equil.Dside(:,:,edge,elem) = sum(mtimesx(vtside, GWdJ),3);

        % The vertex terms
        verts = [ edge (mod(edge,3)+1) ];
        mvs = mtimesx(NormalMxy, StressV(:,:,verts));
        % +mnt on one side, -mnt on the other
        Dv(verts,:) = Dv(verts,:) + [1 0; 0 -1]*reshape(mvs, [nS 2])';
        
        % The term from the particular solution
        if ( has_s0 )
            % Edges
            Stress0GP = CalcStress0KT(coordGP, prob.equil.DS, prob.equil.s0(:,1,elem));
            [ DStress0GPx, DStress0GPy ] = ...
                CalcDStress0KT(coordGP, prob.equil.DS, prob.equil.s0(:,1,elem));
            T0Side = ...
                [ mtimesx(NormalDx, DStress0GPx) + mtimesx(NormalDy, DStress0GPy); ...
                mtimesx(Normal, Stress0GP) ];
            vt0side = mtimesx(VG, 't', T0Side);
            prob.equil.tside(:,edge,elem) = sum(mtimesx(vt0side, GWdJ),3);
            % Verts
            mvs = mtimesx(NormalMxy, Stress0V(:,:,verts));
            bv(verts) = bv(verts) + [1 0; 0 -1]*reshape(mvs, [1 2])';
        end
    end
    prob.equil.Dvert(:,:,elem) = Dv';
    prob.equil.tvert(:,elem) = bv;
end

prob.equil.BoundaryConds = zeros(prob.prob.ng_edges, 2, nV, 2);
prob.equil.imposedDisps = nan(prob.mesh.nv,1);
for i = 1:prob.prob.ng_edges
    g_edge = prob.prob.g_edges(i);
    edge = full(prob.mesh.edges_elems(g_edge));
    tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
    which_tag = find(prob.prob.Edges.tags == tag);
    
    % If vals{end} is not zero we will need to impose
    % the corresponding BC. We integrate it now
    % This code is agnostic regarding the type of BC
    if (size(which_tag,1) && ~strcmp(prob.prob.Edges.vals{end}{which_tag}, '[zero,zero]'))
        sload = prob.prob.Edges.vals{end}{which_tag};
        [ ~, verts_edge ] = prob.mesh.get_cv_e(edge);
        % these coordinates are in the global frame (used for the bc's)
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]);
        coordGP = Phi_side*vert_coords;
        dxdy = (vert_coords(2,:) - vert_coords(1,:));
        lside = norm(dxdy);
        aux = mtimesx(reshape( atpoint2D(sload, coordGP)', [2 1 NI1D.npts]), ...
            reshape(Phi_side', [1 2 NI1D.npts]));
        vtside = mtimesx(VG, 't', aux);
        % Multiplied by the length (there is no normal!!!)
        aux = sum(mtimesx(vtside, GWSide),3)*lside;
        prob.equil.BoundaryConds(i, 1, :, :) = aux(1:nV, :);
        prob.equil.BoundaryConds(i, 2, :, :) = aux(nV+(1:nV), :);
        % Keep the values of the imposed displacements at the vertices
        % a map would be smaller....
        if (isfinite(prob.prob.incid_support(1,i)))
            vals = atpoint2D(sload, vert_coords)';
            for v = 1:2
                vert = verts_edge(v);
                if (isnan(prob.equil.imposedDisps(vert)))
                    prob.equil.imposedDisps(vert) = vals(1,v);
                else
                    if (prob.equil.imposedDisps(vert) ~= vals(1,v))
                        fprintf('Error: different disps on adjacent edges at vertex %d (%e and %e)\n', ...
                            vert, prob.equil.imposedDisps(vert), vals(1,v));
                    end
                end
            end
        end
    end
end

prob.equil.VertexConds = zeros(prob.prob.ng_verts,1);
if (isfield(prob.prob, 'Verts'))
    for i = 1:prob.prob.ng_verts
        g_vert = prob.prob.g_verts(i);
        vert = full(prob.mesh.verts_elems(g_vert));
        tag = prob.mesh.ele_tags{g_vert}(1); % Use the first tag
        which_tag = find(prob.prob.Verts.tags == tag);

        % If vals{end} is not zero we will need to impose
        % the corresponding BC. We integrate it now
        % This code is agnostic regarding the type of BC
        if (size(which_tag,1) && ~strcmp(prob.prob.Verts.vals{end}{which_tag}, '[0 0 0]'))
            sload = prob.prob.Verts.vals{end}{which_tag};
            % these coordinates are in the global frame (used for the bc's)
            vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(vert),[1 2]);
            % No PU here 
            % We drop the moments
            aux = atpoint2D(sload, vert_coords);
            if (norm(aux(2:3)) > 0)
                fprintf('Attention: NON-ZERO MOMENTS!!\n');
            end
            prob.equil.VertexConds(g_vert) = aux(1);

            if (isnan(prob.equil.imposedDisps(vert)))
                prob.equil.imposedDisps(vert) = aux(1);
            else
                if (prob.equil.imposedDisps(vert) ~= aux(1))
                    fprintf('Error: different disps on adjacent edges and explicit at vertex %d (%e and %e)\n', ...
                        vert, prob.equil.imposedDisps(vert), aux(1));
                end
            end
        end
    end
end

prob.equil.calc_mats = true;
