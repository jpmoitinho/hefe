% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
% You need to define 3 or 5 variables before calling this script
%  - meshname, probname and degree 
%  - meshname, probname, degree_s, degree_v and degree_u
%

addpath('../base')
if (exist('funcs','var'))
addpath(funcs);
else
funcs = '../funcs';
addpath(funcs);
end

prob = DefinePlaneProblem(meshname, probname, funcs, 2, 3);

if (exist('degree', 'var'))
    prob.equil.degree_s = degree;
    prob.equil.degree_v = degree;
    prob.compat.degree_u = degree+2;
else
    prob.equil.degree_s = degree_s;
    prob.equil.degree_v = degree_v;
    prob.compat.degree_u = degree_u;
end

% if (exist('dgamma', 'var'))
%     prob.compat.dgamma = dgamma;
% end

if (exist('thickness', 'var'))
    prob.prob.Faces.vals{3} = thickness;
end

fprintf ('%s %s %d %d\n', prob.meshname.name, prob.probname.name, ...
    prob.equil.degree_s, prob.compat.degree_u);
prob = EquilMats(prob);
prob = GlobalEquil(prob);
PostprocEquil(prob, 'EG', prob.equil.sol, prob.equil.s0);

prob = CompatMatsHCT(prob);
prob = GlobalCompatHCT(prob);
prob.compat.stress_params = CompatStressParamsHCT(prob,  prob.compat.sol);
PostprocCompatHCT(prob, 'CG', prob.compat.sol, prob.compat.stress_params);

% npts = 5;
% xx = linspace(0,2,npts);
% yy = linspace(0,1,npts);
% xy = [ xx; yy ]';
% 
% DumpEquilAtPoints(prob, prob.equil.sol, prob.equil.s0, xy, 'test-e');
% DumpCompatAtPoints(prob, prob.compat.stress_params, xy, 'test-c');

% prob = RecoveredComp(prob);
% prob.recovered_c.stress_paramsE = CompatStressParams(prob,  prob.recovered_c.dispE);
% PostprocCompat(prob, 'CE', prob.recovered_c.dispE, prob.recovered_c.stress_paramsE);

% prob.recovered_c.stress_params = CompatStressParams(prob,  prob.recovered_c.disp);
% PostprocCompat(prob, 'CR', prob.recovered_c.disp, prob.recovered_c.stress_params);
% 
% prob = RecoveredCompUU(prob);
% prob.recovered_c.stress_paramsEu = CompatStressParams(prob,  prob.recovered_c.dispE);
% PostprocCompat(prob, 'CEu', prob.recovered_c.dispE, prob.recovered_c.stress_paramsEu);
% prob.recovered_c.stress_paramsu = CompatStressParams(prob,  prob.recovered_c.disp);
% PostprocCompat(prob, 'CRu', prob.recovered_c.disp, prob.recovered_c.stress_paramsu);

% prob = DetectSKMsGlobal(prob);
% % prob = DetectSKMsOnStars(prob);
% prob = CleanSKMs(prob);
% 
% % PostprocRBM(prob, 'SKM');
% 
% prob = RecoveredCompUU(prob);
% prob.recovered_c.stress_paramsE = CompatStressParams(prob,  prob.recovered_c.dispE);
% PostprocCompat(prob, 'CEuf', prob.recovered_c.dispE, prob.recovered_c.stress_paramsE);
% prob.recovered_c.stress_params = CompatStressParams(prob,  prob.recovered_c.disp);
% PostprocCompat(prob, 'CRuf', prob.recovered_c.disp, prob.recovered_c.stress_params);
% 
% prob = RecoveredCompModes(prob);
% prob.recovered_c.stress_paramsE = CompatStressParams(prob,  prob.recovered_c.dispE);
% PostprocCompat(prob, 'CEf', prob.recovered_c.dispE, prob.recovered_c.stress_paramsE);
% prob.recovered_c.stress_params = CompatStressParams(prob,  prob.recovered_c.disp);
% PostprocCompat(prob, 'CRf', prob.recovered_c.disp, prob.recovered_c.stress_params);

% prob = RecoveredEquil(prob);
% PostprocEquil(prob, 'ER', prob.recovered_e.stresses, prob.recovered_e.s0);
