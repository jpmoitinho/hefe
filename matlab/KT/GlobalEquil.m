% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob, senerg, energ ] = GlobalEquil( prob )
% Solve global equilibrium model
%

% should check if prob.equil.calc_mats is true. Otherwise do it.

nS = prob.equil.DS.nS;
nV = prob.equil.degree_v + 1;

% The maximum number of non-zero terms in the governing system
maxnzA = prob.mesh.nc*nS^2 + (prob.mesh.nc*3*nS*(nV*2 + 1) + ...
    prob.prob.nR*nV^2)*2 + prob.prob.nRv + prob.mesh.ne;
Ai = zeros(maxnzA, 1); Aj = zeros(maxnzA, 1); Ap = zeros(maxnzA, 1); 
numnzA = 0;

% Right hand term
b = zeros(prob.mesh.nc*nS + prob.mesh.ne*2*nV + prob.mesh.nv + prob.prob.nR*nV + prob.prob.nRv, 1);

% The position of stress variables for each elem
incid_s = 1:nS;
offset_edges = prob.mesh.nc*nS;
offset_verts = offset_edges + prob.mesh.ne*nV*2;
for elem = 1:prob.mesh.nc
    [ cell_edges, cell_verts ] = prob.mesh.get_ev_c(elem);
    b(incid_s) = prob.equil.e0(:, elem);
    
    % Setup the flexibility matrix

    news = numnzA + (1:nS);
    for i1 = 1:nS
        Ai(news) = incid_s(i1);
        Aj(news) = incid_s;
        Ap(news) = prob.equil.Flex(:, i1, elem);
        news = news +nS;
    end
    numnzA = numnzA + nS*nS;
    
    for edge = 1:3
        incid_v = offset_edges + (cell_edges(edge)-1)*2*nV + (1:(2*nV));

        % The term from the particular solution
        b(incid_v) = b(incid_v) - prob.equil.tside(:, edge, elem);
        news = numnzA + (1:nS);
        for i1 = 1:(2*nV)
            Ai(news) = incid_v(i1);
            Aj(news) = incid_s;
            Ap(news) = prob.equil.Dside(i1, :, edge, elem);
            news = news + nS;
            Ai(news) = incid_s;
            Aj(news) = incid_v(i1);
            Ap(news) = prob.equil.Dside(i1, :, edge, elem);
            news = news + nS;
        end
        numnzA = numnzA + 2*nS*2*nV;
    end
    
    % Could do only one loop
    incid_v = offset_verts + cell_verts;
    for vert = 1:3
        Ai(news) = incid_v(vert);
        Aj(news) = incid_s; % In the firt pass we use news from the other loop
        Ap(news) = prob.equil.Dvert( :, vert, elem);
        news = news + nS;
        Ai(news) = incid_s;
        Aj(news) = incid_v(vert);
        Ap(news) = prob.equil.Dvert( :, vert, elem);
        news = news + nS;
    end
    b(incid_v) = b(incid_v) - prob.equil.tvert(:,elem);
    numnzA = numnzA + 2*nS*3;
    
    incid_s = incid_s + nS;
end

% For each edge, block the transverse displacement of degree d_v
incid_extra = offset_edges + nV + ((0:(prob.mesh.ne-1))*2*nV);
news = numnzA + (1:prob.mesh.ne);
Ai(news) = incid_extra;
Aj(news) = incid_extra;
Ap(news) = 1;
numnzA = numnzA + prob.mesh.ne;

% Add the matrices for the sides with fixed displacements

FixedVerts = [];
offset_R = offset_verts + prob.mesh.nv;
incid_R = offset_R + (1:nV);
if (isfield(prob.prob,'Edges'))
    for i = 1:prob.prob.ng_edges
        g_edge = prob.prob.g_edges(i);
        edge = full(prob.mesh.edges_elems(g_edge));
        lside = prob.geom.ledges(edge);
        for dir = 1:2
            if (isfinite(prob.prob.incid_support(dir,i)))
                incid_v = offset_edges + (edge-1)*2*nV + (dir-1)*nV + (1:nV);
                news = numnzA + (1:nV);
                for i1 = 1:nV
                    Ai(news) = incid_v(i1);
                    Aj(news) = incid_R;
                    Ap(news) = lside*prob.equil.VtV(i1, :);
                    news = news + nV;
                    Ai(news) = incid_R;
                    Aj(news) = incid_v(i1);
                    Ap(news) = lside*prob.equil.VtV(i1, :);
                    news = news + nV;
                end
                numnzA = numnzA + 2*nV*nV;
                incid_R = incid_R + nV;
                if (dir==1)
                    [ ~, edge_verts] = prob.mesh.get_cv_e(edge);
                    FixedVerts = unique([ FixedVerts, edge_verts ]);
                end
            end
        end
        
        if (max(max(max(abs(prob.equil.BoundaryConds(i,:,:,:))))) > 0) % Has something
            tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
            which_tag = find(prob.prob.Edges.tags == tag,1);
            
            for dir = 1:2
                aux = reshape(sum(prob.equil.BoundaryConds(i,dir,:,:),4), [ nV 1 ]);
                if (norm(aux) ~= 0)
                    if (prob.prob.Edges.vals{dir}(which_tag) == 0)
                        incid_r = offset_R + prob.prob.incid_support(dir,i)* nV + (1:nV);
                        b(incid_r) = b(incid_r) + aux;
                    else % Prob.Edges.vals{dir}(which_tag) has to be 1
                        incid_v = offset_edges + (edge-1)*2*nV + (dir-1)*nV + (1:nV);
                        b(incid_v) = b(incid_v) + aux;
                    end
                end
            end
        end
    end
end

incid_R = incid_R(1);
if (isfield(prob.prob,'Verts'))
    fprintf('\n\nAt Verts? THIS IS NOT FULLY CHECKED!!!!\n');
    for i = 1:prob.prob.ng_verts
        g_vert = prob.prob.g_verts(i);
        tag = prob.mesh.ele_tags{g_vert}(1);
        which_tag = find(prob.prob.Verts.tags == tag);
        vert = full(prob.mesh.verts_elems(g_vert));
        if (size(which_tag,1))
            if (prob.prob.Verts.vals{1}(which_tag) == 0)
                FixedVerts = unique([ FixedVerts, vert ]);
            else
                aux = prob.prob.Verts.vals{4}(which_tag);
                aux2 = eval(aux{1});
                b(offset_verts+vert) = aux2(1);
            end
            if (prob.prob.Verts.vals{2}(which_tag)*prob.prob.Verts.vals{3}(which_tag) == 0)
                fprintf ('Cannot fix rotations at nodes for the equilibrated model!\n');
            end            
        end
    end
end

for vert = FixedVerts
    %         tag = prob.mesh.ele_tags{g_vert}(1); % Use the first tag
    %         which_tag = find(prob.prob.Verts.tags == tag);
    incid_v = offset_verts + vert;
    news = numnzA + 1;
    Ai(news) = incid_v;
    Aj(news) = incid_R;
    Ap(news) = 1;
    news = news + 1;
    Ai(news) = incid_R;
    Aj(news) = incid_v;
    Ap(news) = 1;
    
    numnzA = numnzA + 2;
    
    if (isnan(prob.equil.imposedDisps(vert)))
        b(incid_R) = 0;
    else
        b(incid_R) = prob.equil.imposedDisps(vert);
    end
    
    incid_R = incid_R + 1;
    %     else
    %         b(incid_v) = b(incid_v) + prob.equil.VertexConds(g_vert);
    %     end
end


%  Check that the edge and vertex displacements match their constraints 

fprintf('Predicted %d non-zeros, got %d\n', maxnzA, numnzA);

A = sparse(Ai(1:numnzA), Aj(1:numnzA), Ap(1:numnzA));
clear('Ai'); clear('Aj'); clear('Ap');
    
Sd = sqrt(abs(diag(A)));
iszero = (Sd==0);
Sd(iszero) = 1;

S = diag(sparse(1./Sd));
B = S'*A*S;
bs = S'*b;

sol = B\bs;
%     sol = pinv(B)*bs;
%     [ sol, stats] = spqr_basic(B, bs);

% Solve
% tic; prob.equil.sol = mldivide(A,b); toc;
prob.equil.sol = S'*sol;

% control.thres = 0.5; control.order = 0; control.stpv = 1e-12;
% tic; [fA, i1, i2] = ma57_factor(A); [ prob.equil.sol, i3, i4] = ma57_solve(A, b, fA); toc;

% opts = spqr_rank_opts;
% % opts.tol = xxx;
% opts.get_details = 1;
% tic; [ prob.equil.sol, prob.equil.stats] = spqr_basic(A, b, opts); toc;

% prob.equil.A = A;
% prob.equil.b = b;

aux = norm(prob.equil.sol(incid_extra));
if (aux > 1e-10)
    fprintf('\nNon-zero (%e) blocked displacements?\n\n', aux);
end

nb = norm(b);
nA = nan;
nx = norm(prob.equil.sol);
nres = norm(b-A*prob.equil.sol);
cn = nan; % condest(A);
% fprintf (strcat('\nRHS, System and solution norms: %e %e %e\n', ...
%     'Residual norm: %e (relative is %e).\n(Estimated) condition number: %e, Dimension/rank: %d/%d (%d)\n\n'), ...
%     nb, nA, nx, nres, nres/(nb+nA*nx), cn, size(A,1), prob.equil.stats.rank, size(A,1)-prob.equil.stats.rank);
fprintf ('\nRHS, System and solution norms: %e %e %e\nResidual norm: %e (relative is %e). Dimension: %d (%d)\nEstimated condition number: %e\n\n', ...
    nb, nA, nx, nres, nres/(nb+nA*nx), size(prob.equil.sol,1), size(prob.equil.sol,1)-prob.mesh.nc*nS,cn);

prob.equil.nres = nres;
senerg = 0;
incid_s = 1:nS;
for elem = 1:prob.mesh.nc
    senerg = senerg + prob.equil.sol(incid_s)'* ...
        prob.equil.Flex(:, :, elem)*prob.equil.sol(incid_s) - ...
        2*prob.equil.e0(:, elem)'*prob.equil.sol(incid_s) - ...
        prob.equil.s0s0(elem);
    incid_s = incid_s + nS;
end
senerg = -0.5*senerg;

energ = 0;
incid_R = offset_R + (1:nV);
for i = 1:prob.prob.ng_edges
    for dir = 1:2
        if (isfinite(prob.prob.incid_support(dir,i)))
            energ = energ - prob.equil.sol(incid_R)'*b(incid_R);
            incid_R = incid_R + nV;
        end
    end
end

incid_R = incid_R(1);
for i = 1:prob.prob.ng_verts
    if (isfinite(prob.prob.incid_support_verts(i)))
        energ = energ - prob.equil.sol(incid_R)'*b(incid_R);
        incid_R = incid_R + 1;
    end
end

fprintf('Compl Str Energy %.14e\nCompl Pot Energy %.14e\nTotal Comp Energy %.14e\n', ...
    senerg, energ, senerg-energ);

prob.equil.global = true;
prob.equil.energ = energ;
prob.equil.senerg = senerg;

end
