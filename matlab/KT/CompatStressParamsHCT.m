% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ params ] = CompatStressParams( prob, sol)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% This procedure can be made more compact...

degree = prob.compat.degree_u;
% nB = prob.compat.nB;
nBB = prob.compat.nBB;

prob.compat.handleT = str2func(sprintf("@THCTr1d%d", degree));
MD = 0; eval(sprintf("DefMDHCT_d%d",degree));
TSub = 0; eval(sprintf("DefTSubHCT_d%d", degree));

% This is not used. Would be more efficient!
% % The second derivatives (implicitly the B matrix)
% MDD = zeros(nBB,nBB,3);
% % Index 3 is the component
% MDD(:,:,1) = -mtimesx(MD(:,:,1),MD(:,:,1)); 
% MDD(:,:,2) = -mtimesx(MD(:,:,2),MD(:,:,2));
% MDD(:,:,3) = -mtimesx(MD(:,:,1),MD(:,:,2)); % No factor of two here

DPhi = DPhi_T3();

params = zeros(5, nBB, 3, prob.mesh.nc);

for elem = 1:prob.mesh.nc
    [ props, ~ ] = GetProps(elem, prob.mesh.elems_faces, ...
        prob.mesh.ele_tags, prob.prob.Faces);
    Elast = props(1);
    nu = props(2);
    h = props(3);

    aux = 1/(1-nu^2);
    stiff= Elast * h^3/12 * [ ...
        aux nu*aux 0 ; ...
        nu*aux aux 0 ; ...
        0 0 1/(2*(1+nu))];

    [ ~, cell_verts ] = prob.mesh.get_ev_c(elem);
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);

    iJac = (DPhi*elem_coords)\eye(2);
    IJ2(:,:) = [ ...
        iJac(1,1)^2, iJac(2,1)^2, 2*iJac(1,1)*iJac(2,1); ...
        iJac(1,2)^2, iJac(2,2)^2, 2*iJac(1,2)*iJac(2,2); ...
        2*iJac(1,1)*iJac(1,2), 2*iJac(2,1)*iJac(2,2), 2*(iJac(1,1)*iJac(2,2)+iJac(1,2)*iJac(2,1)) ]';

    desls = prob.compat.signs(:,elem).*sol(prob.compat.incid(:,elem));

    % The parameters in each primitive
    % Leaving the change for the coordnates of the primitive to the end
    % avoids changing the Jacobian. Is it really simpler?
    for sub = 1:3
        deslsP = prob.compat.Transf(:,:,sub,elem)*desls;

        dwdxi = MD(:,:,1)*deslsP;
        dwdeta = MD(:,:,2)*deslsP;

        tetx = -(iJac(1,1)*dwdxi + iJac(1,2)*dwdeta);
        tety = -(iJac(2,1)*dwdxi + iJac(2,2)*dwdeta);

        dtxdxi = MD(:,:,1)*tetx;
        dtxdeta = MD(:,:,2)*tetx;

        dtydxi = MD(:,:,1)*tety;
        dtydeta = MD(:,:,2)*tety;

        DTxdX = iJac(1,1)*dtxdxi + iJac(1,2)*dtxdeta;
        DTxdY = iJac(2,1)*dtxdxi + iJac(2,2)*dtxdeta;
        DTydX = iJac(1,1)*dtydxi + iJac(1,2)*dtydeta;
        DTydY = iJac(2,1)*dtydxi + iJac(2,2)*dtydeta;

        mxx = stiff(1,1)*DTxdX + stiff(1,2)*DTydY;
        myy = stiff(2,1)*DTxdX + stiff(2,2)*DTydY;
        mxy = stiff(3,3)*( DTxdY + DTydX);

        dmxxdxi = MD(:,:,1)*mxx;
        dmxxdeta = MD(:,:,2)*mxx;
        dmxxdX = iJac(1,1)*dmxxdxi + iJac(1,2)*dmxxdeta;

        dmyydxi = MD(:,:,1)*myy;
        dmyydeta = MD(:,:,2)*myy;
        dmyydY = iJac(2,1)*dmyydxi + iJac(2,2)*dmyydeta;

        dmxydxi = MD(:,:,1)*mxy;
        dmxydeta = MD(:,:,2)*mxy;
        dmxydX = iJac(1,1)*dmxydxi + iJac(1,2)*dmxydeta;
        dmxydY = iJac(2,1)*dmxydxi + iJac(2,2)*dmxydeta;

        params(1, :, sub, elem) = TSub(:,:,sub)*mxx;
        params(2, :, sub, elem) = TSub(:,:,sub)*myy;
        params(3, :, sub, elem) = TSub(:,:,sub)*mxy;
        params(4, :, sub, elem) = TSub(:,:,sub)*(dmxxdX + dmxydY);
        params(5, :, sub, elem) = TSub(:,:,sub)*(dmxydX + dmyydY);
    end
end

% This is the return value
% prob.compat.stress_params = params;

end

