% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob, senerg, energ ] = GlobalCompatHCT( prob )
% Solve global compatible model
%

% should check if prob.compat.calc_mats is true. Otherwise do it.

prob.compat.global = true;

% nextIJK = [ 2 3 1 ];
prevIJK = [ 3 1 2 ];

degree = prob.compat.degree_u;
nU = prob.compat.nB;

% Number of variables for each vertex, function and normal derivatives
nVw = 1;
nVt = 2;
% Number of variables for each edge, function and normal derivatives
nEw = degree - 3;
nEt = degree - 2;

% The total number of variables per edge and vertex of the element
nVE = nVw + nVt + nEw + nEt;

nBubble = nU - 3*(nVE);

nW = prob.mesh.nv*(nVw+nVt) + prob.mesh.ne*(nEw+nEt) + prob.mesh.nc*nBubble;

% The kinematic bc's for disps and rots
nsupport_w = sum(isfinite(prob.prob.incid_support(1,:)));
nsupport_t = sum(isfinite(prob.prob.incid_support(2,:)));

dim = nW + nsupport_w*(degree-3) + nsupport_t*(degree-2);

maxnzA = round((prob.mesh.nc*(nU)^2 + (prob.prob.nR*(prob.compat.degree_u+1)*3)*2)*2);
Ai = zeros(maxnzA, 1); Aj = zeros(maxnzA, 1); Ap = zeros(maxnzA, 1); numnzA = 0;
Fg = zeros(dim,1);

prob.compat.incid = zeros(nU, prob.mesh.nc);
prob.compat.signs = ones(nU, prob.mesh.nc);

% The position of each dof in the global system
offsetVw = 0;
offsetVt = offsetVw + nVw*prob.mesh.nv;
offsetEw = offsetVt + nVt*prob.mesh.nv;
offsetEt = offsetEw + nEw*prob.mesh.ne;
offsetB = offsetEt + nEt*prob.mesh.ne;

%
ndofperside = 2*degree - 2; % or 3 + nEw + nEt

% The generic incidences at element level for the nodes on one side
incidEw = 1 + (1:nEw);
incidEt = 1 + nEw + 2 + (1:nEt);

% This is zero: offsetB + nBubble*prob.mesh.nc - nW

for elem = 1:prob.mesh.nc
    incid = zeros(nU,1);
    [ elem_edges, elem_verts ] = prob.mesh.get_ev_c(elem);
    % Adjust for the difference between che and "pure" triangle coords
    elem_edges = elem_edges(prevIJK);

    %     fprintf('Elem %d, verts %d %d %d, edges %d %d %d\n', elem, elem_verts, elem_edges);

    for side = 1:3
        % vertex disp
        incid((side-1)*ndofperside + 1) = offsetVw + elem_verts(side);
        % side disps
        incid((side-1)*ndofperside + incidEw) = offsetEw + (elem_edges(side)-1)*nEw + (1:nEw);
        % vertex rots
        incid((side-1)*ndofperside + 1 + nEw + (1:2)) = offsetVt + (elem_verts(side)-1)*2 + (1:2);
        % side rots
        incid((side-1)*ndofperside + incidEt) = offsetEt + (elem_edges(side)-1)*nEt + (1:nEt);

        % use the order in che
        if (prob.geom.sign(elem,prevIJK(side)) == -1)
            % the local incidence of the displacements on this side
            aux = (side-1)*ndofperside + incidEw;
            % Switch the numbering
            incid(aux) = flip(incid(aux));

            % the local incidence of the rotations on this side
            aux = (side-1)*ndofperside + incidEt;
            prob.compat.signs(aux, elem) = -1;
            % Switch the numbering
            incid(aux) = flip(incid(aux));
        end
    end
    % The bubble modes
    incid(3*ndofperside+(1:nBubble)) = offsetB + (elem-1)*nBubble + (1:nBubble);
    prob.compat.incid(:,elem) = incid;

    aux = diag(prob.compat.signs(:,elem));

    %     fprintf('Element %3d', elem);
    %     fprintf(' %4d', inc);
    %     fprintf('\n');

    Ke = aux*prob.compat.Ke(:,:,elem)*aux;
    Ke = (Ke+Ke')/2;
    news = numnzA + (1:nU);
    for i = 1:nU
        Ai(news) = incid(i);
        Aj(news) = incid;
        Ap(news) = Ke(:,i);
        news = news + nU;
    end
    numnzA = numnzA + nU*nU;

    Fg(incid) = Fg(incid) + aux*prob.compat.Fe(:,elem);
    %     disp(Fg(1:9)')
end

% Now the "real" number
fixed_verts_disp = prob.prob.g_verts(~isnan(prob.prob.incid_support_verts(1,:)));
imposed_verts_disp = zeros(2, size(fixed_verts_disp,2)); % The value and the incidence

fixed_verts_rot = unique( [ prob.prob.g_verts(~isnan(prob.prob.incid_support_verts(2,:))), ...
    prob.prob.g_verts(~isnan(prob.prob.incid_support_verts(3,:))) ]);
imposed_verts_rot = zeros(4, 2, size(fixed_verts_rot,2)); % The value, the incidence and the dir, for each rot, for each vert

incid_R = nW;

if (isfield(prob.prob, 'Verts'))
    for j = 1:prob.prob.ng_verts
        v = prob.prob.g_verts(j);
        g_vert = prob.mesh.elems_verts(v);
        tag = prob.mesh.ele_tags{g_vert}(1);
        where = prob.prob.Verts.tags == tag;
        if (any(where))
            % We could use atpoint2D here to have (for example) imposed
            % displacements that depend on the position
            vals = eval(prob.prob.Verts.vals{end}{where});
            % Translation
            if (~isnan(prob.prob.incid_support_verts(1,j)))
                incid_R = incid_R + 1;
                imposed_verts_disp(:, j==fixed_verts_disp) = [ vals(1), incid_R ];
            end
            k = 1;
            if (~isnan(prob.prob.incid_support_verts(2,j)))
                incid_R = incid_R + 1;
                % In the x direction [1 0]
                imposed_verts_rot(:, k, j==fixed_verts_rot) = [ vals(2), incid_R, 1 , 0];
                k = 2;
            end
            if (~isnan(prob.prob.incid_support_verts(3,j)))
                incid_R = incid_R + 1;
                % In the y direction [0 1]
                imposed_verts_rot(:, k, j==fixed_verts_rot) = [ vals(3), incid_R, 0, 1];
            end
        end
    end
end

small = 1/100;
for g_edge = 1:prob.prob.ng_edges
    edge = full(prob.mesh.edges_elems(prob.prob.g_edges(g_edge)));
    [ ~, aux ] = prob.mesh.get_cv_e(edge);
    dxdy = prob.mesh.coords(aux(2),1:2)-prob.mesh.coords(aux(1),1:2);
    LSide = norm(dxdy);
    tang = dxdy([2 1])/LSide; % Change terms to match the rotations
    normal = [-tang(2), tang(1)];
    if (isfinite(prob.prob.incid_support(1,g_edge)) || isfinite(prob.prob.incid_support(2,g_edge)))
        num_edge = prob.mesh.elems_edges(edge);
        tag = prob.mesh.ele_tags{num_edge}(1);
        imposed = prob.prob.Edges.vals{end}(prob.prob.Edges.tags == tag);
        for vert = 1:2
            AtV = atpoint2D(imposed{1}, prob.mesh.coords(aux(vert),1:2));
            AtVdx = atpoint2D(imposed{1}, prob.mesh.coords(aux(vert),1:2) + dxdy*small);

            % Fixed translation
            if (isfinite(prob.prob.incid_support(1,g_edge)))
                if (isempty(intersect(aux(vert), fixed_verts_disp)))
                    fixed_verts_disp(end+1) = aux(vert);
                    incid_R = incid_R + 1;
                    imposed_verts_disp(:,end+1) = [ AtV(1), incid_R ];
                end
                dwdt = (AtVdx-AtV)/(small*LSide);
                if (isempty(intersect(aux(vert), fixed_verts_rot)))
                    incid_R = incid_R + 1;
                    fixed_verts_rot(end+1) = aux(vert);
                    imposed_verts_rot(:, 1, end+1) = [ dwdt(1), incid_R, normal];
                else
                    where = aux(vert)==fixed_verts_rot;
                    if (imposed_verts_rot(2,2,where) == 0) % Not yet fixed
                        odir = imposed_verts_rot([3 4], 1, where);
                        if (abs(tang*odir) > 1e-2)
                            incid_R = incid_R + 1;
                            imposed_verts_rot(:, 2, where) = [ dwdt(1), incid_R, normal];
                        end
                    end
                end
            end

            % Fixed rotation
            if (isfinite(prob.prob.incid_support(2,g_edge)))
                if (isempty(intersect(aux(vert), fixed_verts_rot)))
                    incid_R = incid_R + 1;
                    fixed_verts_rot(end+1) = aux(vert);
                    imposed_verts_rot(:, 1, end+1) = [ AtV(2), incid_R, tang];
                else
                    where = aux(vert)==fixed_verts_rot;
                    if (imposed_verts_rot(2,2,where) == 0) % Not yet fixed
                        odir = imposed_verts_rot([3 4], 1, where);
                        if (abs(normal*odir) > 1e-2)
                            incid_R = incid_R + 1;
                            imposed_verts_rot(:, 2, where) = [ AtV(2), incid_R, tang];
                        end
                    end
                end
            end
        end
    end
end

% Increase RHS (the degree_u-1) discounts the vertex disps
dim2 = incid_R + nsupport_w*(nEw) + nsupport_t*(nEt);
Fg(dim2) = 0;

for i = 1:size(fixed_verts_disp,2)
    vert = fixed_verts_disp(i);
    dof = vert;
    incid = imposed_verts_disp(2, i);
    numnzA = numnzA + 1;
    Ai(numnzA) = incid;
    Aj(numnzA) = dof;
    Ap(numnzA) = 1;
    numnzA = numnzA + 1;
    Ai(numnzA) = dof;
    Aj(numnzA) = incid;
    Ap(numnzA) = 1;
    Fg(incid) = Fg(incid) + imposed_verts_disp(1, i);
end

for i = 1:size(fixed_verts_rot,2)
    vert = fixed_verts_rot(i);
    dofs = offsetVt + (vert-1)*2 + [1 2];
    for k = 1:2
        incid = imposed_verts_rot(2, k, i);
        if (incid > 0)
            dir = imposed_verts_rot([3 4], k, i);
            for j = 1:2
                numnzA = numnzA + 1;
                Ai(numnzA) = incid;
                Aj(numnzA) = dofs(j);
                Ap(numnzA) = dir(j);
                numnzA = numnzA + 1;
                Ai(numnzA) = dofs(j);
                Aj(numnzA) = incid;
                Ap(numnzA) = dir(j);
            end
            Fg(incid) = Fg(incid) + imposed_verts_rot(1, k, i);
        end
    end
end

for i_edge = 1:prob.prob.ng_edges % edges with properties
    g_edge = prob.prob.g_edges(i_edge);
    edge = full(prob.mesh.edges_elems(g_edge));
    [ edge_elems, ~ ] = prob.mesh.get_cv_e(edge);
    tag = prob.mesh.ele_tags{g_edge}(1);
    imposed = prob.prob.Edges.vals{end}(prob.prob.Edges.tags == tag);
    [ ~, aux ] = prob.mesh.get_cv_e(edge);
    dxdy = prob.mesh.coords(aux(2),1:2)-prob.mesh.coords(aux(1),1:2);

    dir = 1; % The translations in the edges
    if (isfinite(prob.prob.incid_support(dir,i_edge)))
        if (nEw > 0)

            % The verts have been fixed. Only worry about the "inside" of the edges
            incid_edge = offsetEw + (edge-1)*(nEw) + (1:nEw);
            incid_reaction = incid_R + (1:nEw);
            news = numnzA + (1:nEw);
            Ai(news) = incid_edge;
            Aj(news) = incid_reaction;
            Ap(news) = 1;
            news = news + nEw;
            Ai(news) = incid_reaction;
            Aj(news) = incid_edge;
            Ap(news) = 1;
            numnzA = numnzA + 2*nEw;

            % This is using the wrong positions!
            pts = repmat(prob.mesh.coords(aux(1),1:2)', [ 1,nEw ])  + dxdy'*(1:nEw)/(nEw+1);
            AtPts = atpoint2D(imposed{1}, pts');
            Fg(incid_reaction) = Fg(incid_reaction) + AtPts(:,1);

            incid_R = incid_R + nEw;
        end
    else
        % It is a force. We used the displacements of the first element adjcent to the side
        Fg(prob.compat.incid(:,edge_elems(1))) = Fg(prob.compat.incid(:,edge_elems(1))) + ...
            (prob.compat.signs(:,edge_elems(1))'.*prob.compat.BoundaryConds(i_edge, :, dir))';
    end

    dir = 2; % The normal rotation
    if (isfinite(prob.prob.incid_support(dir,i_edge)))
        if (nEt > 0 ) % It always is....
            incid_edge = offsetEt + (edge-1)*nEt + (1:nEt);
            incid_reaction = incid_R + (1:nEt);
            news = numnzA + (1:nEt);
            Ai(news) = incid_edge;
            Aj(news) = incid_reaction;
            Ap(news) = 1;
            news = news + nEt;
            Ai(news) = incid_reaction;
            Aj(news) = incid_edge;
            Ap(news) = 1;
            numnzA = numnzA + 2*nEt;

            % This is using the wrong positions!
            pts = repmat(prob.mesh.coords(aux(1),1:2)', [ 1,nEt ])  + dxdy'*(1:nEt)/(nEt+1);
            AtPts = atpoint2D(imposed{1}, pts');
            Fg(incid_reaction) = Fg(incid_reaction) + AtPts(:,2);

            incid_R = incid_R + nEt;
        end
    else
        % It is a force. We used the displacements of first element adjcent to the first side
        Fg(prob.compat.incid(:,edge_elems(1))) = Fg(prob.compat.incid(:,edge_elems(1))) + ...
            diag(prob.compat.signs(:,edge_elems(1)))*sum(prob.compat.BoundaryConds(i_edge, :, dir), 3)';
    end
end

% The vertices

if (isfield(prob.prob,'Verts'))
    fprintf('\n\nProperties of Verts? THIS NEEDS ATTENTION...\n');
    for i = 1:prob.prob.ng_verts
        g_vert = prob.prob.g_verts(i);
        tag = prob.mesh.ele_tags{g_vert}(1);
        which_tag = find(prob.prob.Verts.tags == tag);
        vert = full(prob.mesh.verts_elems(g_vert));
        aux = prob.prob.Verts.vals{4}(which_tag);
        aux2 = eval(aux{1});
        if (size(which_tag,1))
            for dir = 1:3
                val = aux2(dir);
                % Only for forces. Fixed displacements were considered before
                if (prob.prob.Verts.vals{dir}(which_tag) == 1)
                    if (dir==1)
                        incid = vert;
                    else
                        incid = prob.mesh.nc + (vert-1)*2 + dir;
                    end
                    Fg(incid) = val;
                end
            end
        end
    end
end

if (maxnzA < numnzA)
    fprintf ('Failed dimensions: %d %d\n', maxnzA, numnzA);
end

A = sparse(Ai(1:numnzA), Aj(1:numnzA), Ap(1:numnzA));
clear('Ai'); clear('Aj'); clear('Ap');

A = (A+A')/2;

Sd = sqrt(abs(diag(A)));
iszero = (Sd==0);
Sd(iszero) = 1;

S = diag(sparse(1./Sd));
B = S'*A*S;
bs = S'*Fg;

fprintf('\n\n');
tic;

control.thres = 0.25; control.order = 0; control.stpv = 0; 
[fB, i1, i2] = ma57_factor(B, control); [ sol, i3, i4] = ma57_solve(B, bs, fB, 100);
prob.compat.stats.rank = i1(25);
% sol = qmr(B, bs, 1e-14, 200, [], [], sol);
% sol = symmlq(B, bs, 1e-16, 200, [], [], sol);
% sol = minres(B, bs, 1e-18, 200, [], [], sol);

% sol = pinv(full(B))*bs;

% sol = lsqminnorm(B, bs, 0);

% sol = B\bs;

% Ctrl.ordering = 'best'; [sol, stats] = umfpack(B, '\', bs, Ctrl); % the same as matlab's \

% opts = spqr_rank_opts; [sol, stats] = spqr_basic(B, bs, opts);

toc;

prob.compat.sol = S'*sol;


prob.compat.b = Fg;

nb = norm(Fg);
nx = norm(prob.compat.sol);
nres = norm(Fg-A*prob.compat.sol)/norm(Fg);
fprintf (strcat('\nRHS and solution norms: %e %e\n', ...
    'Residual norm: %e Dimension: %d\n\n'), ...
    nb, nx, nres,  size(A,1));

senerg = 0;
energ = 0;
for elem = 1:prob.mesh.nc
    aux = prob.compat.signs(:,elem).*prob.compat.sol(prob.compat.incid(:,elem));
    senerg = senerg + aux'*prob.compat.Ke(:,:,elem)*aux;
    % For the potential energy we add all the work done by all the equivalent nodal forces
    energ = energ + prob.compat.Fe(:,elem)'*aux;
end
senerg = 0.5*senerg;

for i_edge = 1:prob.prob.ng_edges
    g_edge = prob.prob.g_edges(i_edge);
    edge = full(prob.mesh.edges_elems(g_edge));
    [ edge_elems, ~ ] = prob.mesh.get_cv_e(edge);
    elem = edge_elems(1);
    for dir = 1:2
        if (isnan(prob.prob.incid_support(dir,i_edge)))
            % NO SIGN?
            energ = energ + prob.compat.BoundaryConds(i_edge,:,dir)*prob.compat.sol(prob.compat.incid(:,elem));
        end
    end
end

fprintf('Str Energy   %.14e\nPot Energy   %.14e\nTotal Energy %.14e\n', ...
    senerg, energ, senerg-energ);

prob.compat.global = true;
prob.compat.energ = energ;
prob.compat.senerg = senerg;

if (nres > 1e-8)
    senerg = nan;
end
end

