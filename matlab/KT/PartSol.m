% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ has_s0, s0 ] = PartSol(bfgp, DS, coords, GWdJ)
% Find a particular solution

% bfgp is either [ 3 1 npts ] or [ 3 3 npts ] (the bf's at the gp's)

npts = size(coords,1);

DDBasis = DS.handleDDBasis(coords);
DBasis = DS.handleDBasis(coords);

BasisF = zeros(1, DS.nS0, npts);
% fz
BasisF(1,:,:) = (DDBasis(:,:,1)*DS.T0{1} + DDBasis(:,:,2)*DS.T0{2} + 2*DDBasis(:,:,3)*DS.T0{3})';

% The basis by itself
aux = sum(mtimesx( GWdJ, mtimesx(BasisF, 't', BasisF)), 3);

% Normalise diagonal
scale = diag(1./sqrt(abs(diag(aux))));
aux = scale*aux*scale;

% The basis by the defined loads
b = scale*sum(mtimesx( GWdJ, mtimesx(BasisF, 't', bfgp)), 3);

% Remove eventual singletons
b = squeeze(b);

s0 = -scale*(aux\b);

if (norm(s0)==0)
    has_s0 = false;
else
    has_s0 = true;
end