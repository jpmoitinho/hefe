% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = PostprocCompatHCT( prob, type, sol, params, rotate)
% Post process a compatible solution
%

namename = prob.meshname.name;
probname = prob.probname.name;

degree = prob.compat.degree_u;

% Write the results for file xxxx-m.msh. Here we assume that it exists and is consistent!

outname = sprintf('Results/%s-m-d%d-%s-Disps-%s.msh', namename, degree, probname, type);
output = fopen(outname,'w');

nBB = prob.compat.nBB;
TSub = 0; eval(sprintf("DefTSubHCT_d%d", degree));

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/ISchemeHCT_d%d.msh', prob.funcs, degree);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

fprintf(output, '$ElementNodeData\n2\n');
fprintf(output, '"%s Disps - %s %s"\n"BasisHCT d%d"\n', type, namename, probname, degree);
fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', 3*prob.mesh.nc);

zer = zeros(nBB,1);

offset_elems = prob.mesh.elems_faces(1)-1;

for elem = 1:prob.mesh.nc
    for sub = 1:3
        w = TSub(:,:,sub)*prob.compat.Transf(:,:,sub,elem)*(prob.compat.signs(:,elem).*sol(prob.compat.incid(:,elem)));
        fprintf(output, '%d %d', offset_elems + (elem-1)*3 + sub, nBB);
        fprintf(output, ' %e', [ zer, zer, w]');
        fprintf(output,'\n');
    end
end

fprintf(output, '$EndElementNodeData\n');

fclose(output);

if (exist('params', 'var'))
    outname = sprintf('Results/%s-m-d%d-%s-Stress-%s.msh', namename, degree, probname, type);
    output = fopen(outname,'w');

    fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
    aux = sprintf('%s/ISchemeHCT_d%d.msh', prob.funcs, degree);
    scheme = fileread(aux);
    fprintf(output, '%s', scheme);

    % variable rotate has the rotation angle of x' relative to x (in radians)
    if (exist('rotate', 'var'))
        sa = sin(rotate);
        ca = cos(rotate);
        T = [ ca*ca sa*sa 2*ca*sa ; sa*sa ca*ca -2*ca*sa ; -ca*sa ca*sa (ca*ca-sa*sa) ];
        R = [ ca, sa; -sa, ca];
        for elem = 1:prob.mesh.nc
            for sub = 1:3
                params(1:3, :, sub, elem) = T*params(1:3, :, sub, elem);
                params(4:5, :, sub, elem) = R*params(4:5, :, sub, elem);
            end
        end
    end


    names = {'mxx', 'myy', 'mxy', 'vx', 'vy'};
    for i = 1:5
        fprintf(output, '$ElementNodeData\n2\n');
        fprintf(output, '"%s (%s) Stresses - %s %s"\n"BasisHCT d%d"\n', ...
            names{i}, type, namename, probname, degree);
        fprintf(output, '1\n0.0\n3\n0\n1\n%d\n', 3*prob.mesh.nc);
        for elem = 1:prob.mesh.nc
            for sub = 1:3
                fprintf(output, '%d %d', offset_elems + (elem-1)*3 + sub, nBB);
                fprintf(output, ' %e', params(i,:,sub,elem));
                fprintf(output,'\n');
            end
        end
        fprintf(output, '$EndElementNodeData\n');
    end
    fclose(output);
end



