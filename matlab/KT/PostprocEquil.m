% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = PostprocEquil( prob, type, sol, s0, rotate)
% Post process an equilibrium solution
%
% Create a text file with some results
namename = prob.meshname.name;
probname = prob.probname.name;

degree = prob.equil.degree_s;
degree_v = prob.equil.degree_v;
nV = degree_v + 1;
nS = prob.equil.DS.nS;

outname = sprintf('Results/%s-d%d-%s-Stress-%s.msh', namename, degree, probname, type);
output = fopen(outname,'w');


fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/Basis2D-%d.msh', prob.funcs, prob.equil.degree_s);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

weights = zeros(prob.equil.DS.nB, 5, prob.mesh.nc);
incid = 1:nS;
for elem = 1:prob.mesh.nc
    [ ~, verts ] = prob.mesh.get_ev_c(elem);
    coords = prob.mesh.coords(prob.mesh.nodes_verts(verts),1:2) - ones(3,1)*prob.geom.center(:,elem)';
    T = prob.equil.DS.handleC(coords(:,1), coords(:,2))';
    welem = zeros(prob.equil.DS.nB,5);
    for k = 1:3
        welem(:, k) = ...
            prob.equil.DS.T{k}*sol(incid) + ...
            prob.equil.DS.T0{k}*sum(s0(:,:,elem),2);
    end
    incid = incid + nS;
    % The shear forces
    welem(:,4) = prob.equil.DS.DT{1}'*welem(:,1) + ...
        prob.equil.DS.DT{2}'*welem(:,3);
    welem(:,5) = prob.equil.DS.DT{1}'*welem(:,3) + ...
        prob.equil.DS.DT{2}'*welem(:,2);
    weights(:,:,elem) = T*welem;
end

% variable rotate has the rotation angle of x' relative to x (in radians)
if (exist('rotate', 'var'))
    sa = sin(rotate);
    ca = cos(rotate);
    T = [ ca*ca sa*sa 2*ca*sa ; sa*sa ca*ca -2*ca*sa ; -ca*sa ca*sa (ca*ca-sa*sa) ]';
    R = [ ca, -sa; sa, ca];
    for elem = 1:prob.mesh.nc
        weights(:, 1:3, elem) = weights(:, 1:3, elem)*T;
        weights(:, 4:5, elem) = weights(:, 4:5, elem)*R;
    end
end

names = {'mxx', 'myy', 'mxy', 'vx', 'vy'};
for i = 1:5
    fprintf(output, '$ElementNodeData\n2\n');
    fprintf(output, '"%s (%s) Stresses - %s %s"\n"Basis %d"\n', names{i}, ...
        type, namename, probname, prob.equil.degree_s);
    fprintf(output, '1\n0.0\n3\n0\n1\n%d\n', prob.mesh.nc);
    for elem = 1:prob.mesh.nc
        fprintf(output, '%d %d', prob.mesh.elems_faces(elem), prob.equil.DS.nB);
        fprintf(output, ' %e', weights(:, i, elem));
        fprintf(output,'\n');
    end
    fprintf(output, '$EndElementNodeData\n');
end

fprintf(output, '$NodeData\n1\n');
fprintf(output, '"Vertex displacements - %s %s"\n', namename, probname);
fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.nv);
aux = prob.mesh.nc*nS + prob.mesh.ne*2*nV + 1;
for vertex = 1:prob.mesh.nv
    fprintf(output, '%d', vertex);
    fprintf(output, ' 0 0 %e\n', sol(aux));
    aux = aux + 1;
end
fprintf(output, '$EndNodeData\n');

fclose(output);

node_offset = 0;
elem_offset = 0;

node_offset = node_offset + prob.mesh.nv;
elem_offset = elem_offset + prob.mesh.n_elem;
outname = sprintf('Results/%s-d%d-%s-Disps-%s.msh', namename, degree, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/Basis1D-%d.msh', prob.funcs, prob.equil.degree_v);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

fprintf(output, '$Nodes\n');
% We need to define all nodes and edges
fprintf(output, '%d\n', prob.mesh.nv);
fprintf(output, '%d %e %e 0\n', ...
    [ (1:prob.mesh.nv)+node_offset; prob.mesh.coords(prob.mesh.nodes_verts(1:prob.mesh.nv), [1 2])']);
fprintf(output, '$EndNodes\n$Elements\n%d\n', prob.mesh.ne);
for i = 1:prob.mesh.ne
    [~, verts] = prob.mesh.get_cv_e(i);
    fprintf(output, '%d 1 0 %d %d\n', i+elem_offset, verts+node_offset);
end
fprintf(output, '$EndElements\n');

fprintf(output, '$ElementNodeData\n2\n');
fprintf(output, '"Disps - %s %s"\n"Basis %d"\n', namename, probname, degree_v);
fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.ne);
for edge = 1:prob.mesh.ne
    fprintf(output, '%d %d', edge+elem_offset, nV);
    fprintf(output, ' %e', [ zeros(nV,1), zeros(nV,1), ...
        sol(prob.mesh.nc*nS+(edge-1)*2*nV + (1:nV)) ]');
    fprintf(output, '\n');
end
fprintf(output, '$EndElementNodeData\n');
fclose(output);

node_offset = node_offset + prob.mesh.nv;
elem_offset = elem_offset + prob.mesh.ne;

outname = sprintf('Results/%s-d%d-%s-Disps2-%s.msh', namename, degree, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/Basis1D-Lin-%d.msh', prob.funcs, prob.equil.degree_v);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

fprintf(output, '$Nodes\n');
% We need to define all nodes and quads for the edges
fprintf(output, '%d\n', 4*prob.mesh.ne);
% Define the height of each edge as the maximum height of the adjacent
% elements
h_plot = zeros(1, prob.mesh.ne);
for edge = 1:prob.mesh.ne
    [ elems, verts ] = prob.mesh.get_cv_e(edge);
    for elem = elems
        [ props, ~ ] = GetProps(elem, prob.mesh.elems_faces, ...
            prob.mesh.ele_tags, prob.prob.Faces);
        h_plot(edge) = max( h_plot(edge), props(4));
    end
    fprintf(output, '%d %e %e %e\n', ...
        [ edge+node_offset, edge+prob.mesh.ne+node_offset ; prob.mesh.coords(prob.mesh.nodes_verts(verts), [1 2])'; ...
        -h_plot(edge)/2, -h_plot(edge)/2 ]);
end

for edge = 1:prob.mesh.ne
    [ ~, verts ] = prob.mesh.get_cv_e(edge);
    fprintf(output, '%d %e %e %e\n', ...
        [ edge+2*prob.mesh.ne+node_offset, edge+3*prob.mesh.ne+node_offset ; ...
        prob.mesh.coords(prob.mesh.nodes_verts(verts), [1 2])'; ...
        h_plot(edge)/2, h_plot(edge)/2 ]);
end
fprintf(output, '$EndNodes\n$Elements\n%d\n', prob.mesh.ne);
aux = [ 0, 1, 3, 2 ]*prob.mesh.ne;
for i = 1:prob.mesh.ne
    fprintf(output, '%d 3 0 %d %d %d %d\n', i+elem_offset, i+aux+node_offset);
end
fprintf(output, '$EndElements\n');

fprintf(output, '$ElementNodeData\n2\n');
fprintf(output, '"Disps - %s %s"\n"BasisLin %d"\n', namename, probname, degree_v);
fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.ne);

% The derivatives of the Legendre polynomials
TL = zeros(nV, nV);
for i = 2:nV
    for j = (i-1):-2:1
        TL(i,j) = 2*(j-1)+1;
    end
end

incid = prob.mesh.nc*nS + (1:nV);
for edge = 1:prob.mesh.ne
    [ ~, verts_edge ] = prob.mesh.get_cv_e(edge);
    vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]);
    dxdy = vert_coords(2,:) - vert_coords(1,:);
    lside = norm(dxdy);
    dxdy = dxdy/lside;
    nx = dxdy(2); ny = -dxdy(1);

    fprintf(output, '%d %d', edge+elem_offset, 2*nV);
    w = sol(incid);
    incid = incid + nV;
    tn = sol(incid);
    incid = incid + nV;
    % -dw/dt
    tt = -TL'*w;
    % dw/dx
    tt = tt*(2/lside);

    tx = tn*nx - tt*ny;
    ty = tn*ny + tt*nx;

    fprintf(output, ' %e', [ ...
        zeros(nV,1), zeros(nV,1), ...
        w ; ...
        h_plot(edge)/2*tx, ...
        h_plot(edge)/2*ty, ...
        zeros(nV,1) ]');
    fprintf(output, '\n');
end
fprintf(output, '$EndElementNodeData\n');
fclose(output);

end