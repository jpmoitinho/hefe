% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = CompatMatsHCT( prob )
% CompatMats - Compute the matrices relevant for compatibility
%

degree = prob.compat.degree_u;

% The integration rules for _this_ problem
%

% HOW CAN WE GUESS APPROPRIATE INTEGRATION RULES, WITH PU's AND ALL?
excess = 20;

[ NI2D, NI1D ] = def_integration_2D((degree-2)*2+excess, degree*2+3*excess); 

% Build elementary stiffness matrices

prob.compat.handleBasis = str2func(sprintf('BasisHCT_d%d', degree));
prob.compat.handleT = str2func(sprintf("@THCTr1d%d", degree));
MD = 0; eval(sprintf("DefMDHCT_d%d",degree));
IOmegaPrims = 0; eval(sprintf("DefIOmegaPrimsHCT_d%d",degree));

% To obtain the dimensions
TBase = prob.compat.handleT([0 1 0],[0 0 1]);

nB = size(TBase,2);
nBB = size(TBase,1);
nU = nB;
prob.compat.nB = nB;
prob.compat.nBB = nBB;
% prob.compat.nE = 9 + 3*(degree-2) + 3*(degree-3);

% The second derivatives (implicitly the B matrix)
MDD = zeros(nBB,nBB,3);
% Index 3 is the component
MDD(:,:,1) = -mtimesx(MD(:,:,1),MD(:,:,1)); 
MDD(:,:,2) = -mtimesx(MD(:,:,2),MD(:,:,2));
MDD(:,:,3) = -mtimesx(MD(:,:,1),MD(:,:,2)); % No factor of two here

% For the geometry
DPhi = DPhi_T3();

% The (master) coordinates of the vertices of the primitive elements
split_coords = cat(3, ...
    [1/3 1/3 1/3; 0 1 0 ; 0 0 1], ...
    [1/3 1/3 1/3; 0 0 1 ; 1 0 0], ...
    [1/3 1/3 1/3; 1 0 0 ; 0 1 0]);
% Correct a lack of consistency in the numbering of the primitive elements
% This must be corrected somewhere else....
split_coords = split_coords(:,:,[3 1 2]);

% The area coordinates of the Gauss points for the master element
LsGPsMaster = [ 1-NI2D.xieta(1,:)'-NI2D.xieta(2,:)',NI2D.xieta(1,:)',NI2D.xieta(2,:)'];

% The area coordinates of the Gauss points for the primitive elements
% (in the coordinates of the master)
LsGPsPrims = mtimesx(LsGPsMaster, split_coords);

PhiC = zeros(NI2D.npts, nBB,3);
for sub = 1:3
    PhiC(:,:,sub) = prob.compat.handleBasis(LsGPsPrims(:,1,sub),LsGPsPrims(:,2,sub));
end


prob.compat.Ke = zeros(nU, nU, prob.mesh.nc);
prob.compat.Fe = zeros(nU, prob.mesh.nc);
% prob.compat.Qe = zeros(nBB, nB, 3, 3, prob.mesh.nc);
prob.compat.Transf = zeros(nBB, nB, 3, prob.mesh.nc);

Qe = zeros(nBB, nU, 3, 3);


for elem = 1:prob.mesh.nc
    [ ~, cell_verts ] = prob.mesh.get_ev_c(elem);
    [ props, bload ] = GetProps(elem, prob.mesh.elems_faces, ...
        prob.mesh.ele_tags, prob.prob.Faces);
    Elast = props(1);
    nu = props(2);
    h = props(3);
    
    aux = 1/(1-nu^2);
    stiff= Elast * h^3/12 * [ ...
        aux nu*aux 0 ; ...
        nu*aux aux 0 ; ...
        0 0 1/(2*(1+nu))];
    
    % Get global coordinates of element.
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);
    
    coordGP_g = mtimesx(LsGPsPrims, elem_coords);
    
    % Jacobian of the element and determinant
    % THIS IS FOR A LINEAR GEOMETRY (constant Jac)
    Jac = DPhi*elem_coords;
    dJac = det(Jac);
    %     fprintf('Elem %d, dJac=%g\n', elem, dJac);

    Transf = prob.compat.handleT( elem_coords(:,1), elem_coords(:,2));
    prob.compat.Transf(:,:,:,elem) = Transf;
    
    % Add a 3 from mapping the primitive?
    dJac = dJac/1;

    IJac = Jac\eye(2);
    IJ2(:,:) = [ ...
        IJac(1,1)^2, IJac(2,1)^2, 2*IJac(1,1)*IJac(2,1); ...
        IJac(1,2)^2, IJac(2,2)^2, 2*IJac(1,2)*IJac(2,2); ...
        2*IJac(1,1)*IJac(1,2), 2*IJac(2,1)*IJac(2,2), 2*(IJac(1,1)*IJac(2,2)+IJac(1,2)*IJac(2,1)) ]';
    % Transpose???????


    % The transformation to the second derivatives (in the global frame)
    for comp = 1:3
        aux = 0;
        for k = 1:3
            aux = aux + IJ2(comp, k)*MDD(:,:,k);
        end
        for sub = 1:3
            Qe(:,:,comp,sub) = aux*Transf(:,:,sub);
        end
    end
    %     prob.compat.Qe(:,:,:,:,elem) = Qe;

    % The stiffness matrix (Do not use the zeros on stiff
    aux = 0;
    
   for sub = 1:3
        aux = aux + stiff(3,3)*Qe(:,:,3,sub)'*IOmegaPrims(:,:,sub)*Qe(:,:,3,sub);
        for i = 1:2
            for j = 1:2
                aux = aux + stiff(i,j)*Qe(:,:,i,sub)'*IOmegaPrims(:,:,sub)*Qe(:,:,j,sub);
            end
        end
    end
    prob.compat.Ke(:,:,elem) = aux*(dJac);
    
    %     IJac = inv(Jac);
    %     DispGP([2 3], :, :) = -mtimesx(IJac, DPhiC);
    
    % The "real" body forces
    
    Fe = 0;
    for sub = 1:3
        %         Fe = Fe + mtimesx(PhiC(:,:,sub), 't', atpoint2D(bload, coordGP_g(:,:,sub)).*NI2D.GW');
        Fe = Fe + Transf(:,:,sub)'*PhiC(:,:,sub)'*(atpoint2D(bload, coordGP_g(:,:,sub)).*NI2D.GW'); 
    end
    prob.compat.Fe(:,elem) = Fe*dJac/3;
end

% Gauss points on a line, from 0 to 1
LsSide = [ (1-NI1D.xi)/2, (1+NI1D.xi)/2 ];
GPzeros = zeros(NI1D.npts,1);
% The basis on the external sides
PhiCSides = zeros(nBB, NI1D.npts, 3);
PhiCSides(:,:,1) = prob.compat.handleBasis(LsSide(:,1),LsSide(:,2))';
PhiCSides(:,:,2) = prob.compat.handleBasis(GPzeros,LsSide(:,1))';
PhiCSides(:,:,3) = prob.compat.handleBasis(LsSide(:,2),GPzeros)';

% PhiCSides(:,:,2) = prob.compat.handleBasis{2}((1-NI1D.xi)/2, (NI1D.xi+1)/2)';
% PhiCSides(:,:,3) = prob.compat.handleBasis{3}(zeros(size(NI1D.xi)), (1-NI1D.xi)/2)';



DispGP = zeros(nU, NI1D.npts, 2); % Translation and normal rotation
prob.compat.BoundaryConds = zeros(prob.prob.ng_edges, nU, 2);
nextIJK = [ 2 3 1 ];

for i = 1:prob.prob.ng_edges
    g_edge = prob.prob.g_edges(i);
    tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
    which_tag = find(prob.prob.Edges.tags == tag);
    if (~isempty(which_tag))
        edge = full(prob.mesh.edges_elems(g_edge));
        [ elems_edge, verts_edge ] = prob.mesh.get_cv_e(edge);
        % Project onto the first (with positive orientation) element
        elem = elems_edge(1);
        [ ~, cell_verts ] = prob.mesh.get_ev_c(elem);
        elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);
        Jac = DPhi*elem_coords;
        Transf = prob.compat.Transf(:,:,:,elem);
        
        sfunc = prob.prob.Edges.vals{end}{which_tag};
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]);
        coordGP = LsSide*vert_coords;
        dxdy = (vert_coords(2,:) - vert_coords(1,:));
        lside = norm(dxdy);

        % The element coordinates of the side GP's
        % Compensate the order
        % This assumes a positive circulation!!!!!!!!!!!!!!
        if (verts_edge(1) == cell_verts(2))
            side = 1;
        else
            if (verts_edge(1) == cell_verts(3))
                side = 2;
            else
                side = 3;
            end
        end
        % PhiCSides has nB rows
        DispGP(:, :, 1) = Transf(:,:,side)'*PhiCSides(:, :, side);
        
        dxdy = dxdy/lside; % Necessary ????????????????????
        nx = dxdy(2); ny = -dxdy(1);
        
        % The derivatives of the basis on the sides (local frame nB npts 2)
        DPhiCSide = permute( mtimesx(PhiCSides(:,:,side)', ...
            mtimesx(cat(3, MD(:,:,1), MD(:,:,2)), Transf(:,:,side))), [2 3 1] );
        
        rotn = permute(mtimesx(DPhiCSide, Jac\[nx;ny]), [ 1 3 2 ]);

        
        DispGP(:, :, 2) = rotn;
        % mtimesx(Transf',mtimesx(TBase(:,:,side)',mtimesx(DPhiCSides(:,:,:,side),Jac\[nx;ny])));
        %         DispGP(:, :, 2) = reshape(mtimesx(mtimesx(Transf,DPhiCSides(:,:,:,side)),Jac\[nx;ny]), [nB NI1D.npts]);
        DispGP = DispGP.*(NI1D.GW'*lside);
        
        funcGP = atpoint2D(sfunc, coordGP)';
        prob.compat.BoundaryConds(i,:,1) = DispGP(:,:,1)*funcGP(1,:)';
        % On the boundary the sign is always 1. Is this consistent?
        prob.compat.BoundaryConds(i,:,2) = prob.geom.sign(elem,nextIJK(side))*DispGP(:,:,2)*funcGP(2,:)';
    end
end




end
