function [ B ] = GetCompatBasis3D(Ls) %#ok<DEFNU>
L1 = Ls(:,1); L2 = Ls(:,2); L3 = Ls(:,3); L4 = Ls(:,4); 
B = [L1,L2,L3,L4];
end

