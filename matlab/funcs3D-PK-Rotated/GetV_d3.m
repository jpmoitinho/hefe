function [tmp] = GetV_d3(xieta)
xi = xieta(1,:)'; eta = xieta(2,:)';
o1=(-1).*eta;
o2=(-1).*xi;
o3=1+o1+o2;
o4=o3.^2;
o5=xi.^2;
o6=eta.^2;
tmp=[o3.^3,o4.*xi,eta.*o4,o3.*o5,eta.*o3.*xi,o3.*o6,xi.^3,eta.*o5,o6.*xi, ...
  eta.^3];
end

