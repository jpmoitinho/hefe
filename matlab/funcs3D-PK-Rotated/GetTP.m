function [tmp] = GetTP(F)
% Transforms PK stresses (in Voigt's notation) in Cauchy
o1=F(2,2);
o2=F(2,3);
o3=F(3,3);
tmp=[F(1,1),F(1,2),F(1,3),0,0,0,0,0,0;0,0,0,0,o1,o2,0,0,0;0,0,0,0,0,0,0,0, ...
  o3;0,0,0,0,0,o3,0,0,0;0,0,o3,0,0,0,0,0,0;0,o1,o2,0,0,0,0,0,0];
end

