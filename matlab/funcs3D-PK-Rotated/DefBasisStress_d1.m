function [f] = DefBasisStress_d1()
f = @BasisStress;
end

function [phi] = BasisStress(L2,L3,L4)
phi=[1+(-1).*L2+(-1).*L3+(-1).*L4,L2,L3,L4];
end

