function [tmp] = GetV_d2(xieta)
xi = xieta(1,:)'; eta = xieta(2,:)';
o1=(-1).*eta;
o2=(-1).*xi;
o3=1+o1+o2;
tmp=[o3.^2,o3.*xi,eta.*o3,xi.^2,eta.*xi,eta.^2];
end

