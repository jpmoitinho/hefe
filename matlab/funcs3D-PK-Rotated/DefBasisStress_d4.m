function [f] = DefBasisStress_d4()
f = @BasisStress;
end

function [phi] = BasisStress(L2,L3,L4)
o1=L2.^2;
o2=L2.^3;
o3=L2.^4;
o4=L3.^2;
o5=L3.^3;
o6=L3.^4;
o7=L4.^2;
o8=L4.^3;
o9=L4.^4;
o10=(-1).*o3;
o11=(-3).*L2.*L3;
o12=(-3).*o1.*o4;
o13=(-1).*L2.*o5;
o14=(-3).*L2.*L4;
o15=6.*L2.*L3.*L4;
o16=(-3).*L2.*L4.*o4;
o17=(-3).*o1.*o7;
o18=(-3).*L2.*L3.*o7;
o19=(-1).*L2.*o8;
o20=(-1).*L3.*o2;
o21=(-1).*o6;
o22=(-3).*L3.*L4;
o23=(-3).*L3.*L4.*o1;
o24=(-3).*o4.*o7;
o25=(-1).*L3.*o8;
o26=(-1).*L4.*o2;
o27=(-1).*L4.*o5;
o28=(-1).*o9;
o29=(-2).*L3.*o1;
o30=o1.*o4;
o31=(-2).*L4.*o1;
o32=2.*L3.*L4.*o1;
o33=o1.*o7;
o34=L3.*o2;
o35=(-2).*L2.*o4;
o36=L2.*o5;
o37=(-2).*L2.*L3.*L4;
o38=2.*L2.*L4.*o4;
o39=L2.*L3.*o7;
o40=L4.*o2;
o41=L2.*L4.*o4;
o42=(-2).*L2.*o7;
o43=2.*L2.*L3.*o7;
o44=L2.*o8;
o45=(-2).*L4.*o4;
o46=o4.*o7;
o47=L3.*L4.*o1;
o48=L4.*o5;
o49=(-2).*L3.*o7;
o50=L3.*o8;
o51=(-1).*o1.*o4;
o52=(-1).*L3.*L4.*o1;
o53=(-1).*o1.*o7;
o54=(-1).*L2.*L4.*o4;
o55=(-1).*L2.*L3.*o7;
o56=(-1).*o4.*o7;
phi=[1+(-4).*L2+(-4).*L3+12.*L2.*L3+(-4).*L4+12.*L2.*L4+12.*L3.*L4+(-24).*L2.*L3.*L4+6.*o1+(-12).*L3.*o1+(-12).*L4.*o1+12.*L3.*L4.*o1+(-4).*o2+4.*L3.*o2+4.*L4.*o2+o3+6.*o4+(-12).*L2.*o4+(-12).*L4.*o4+12.*L2.*L4.*o4+6.*o1.*o4+(-4).*o5+4.*L2.*o5+4.*L4.*o5+o6+6.*o7+(-12).*L2.*o7+(-12).*L3.*o7+12.*L2.*L3.*o7+6.*o1.*o7+6.*o4.*o7+(-4).*o8+4.*L2.*o8+4.*L3.*o8+o9,L2+(-3).*o1+6.*L3.*o1+6.*L4.*o1+(-6).*L3.*L4.*o1+o10+o11+o12+o13+o14+o15+o16+o17+o18+o19+3.*o2+(-3).*L3.*o2+(-3).*L4.*o2+3.*L2.*o4+3.*L2.*o7,L3+3.*L3.*o1+o11+o12+o15+o18+o20+o21+o22+o23+o24+o25+(-3).*o4+6.*L2.*o4+6.*L4.*o4+(-6).*L2.*L4.*o4+3.*o5+(-3).*L2.*o5+(-3).*L4.*o5+3.*L3.*o7,L4+3.*L4.*o1+o14+o15+o16+o17+o22+o23+o24+o26+o27+o28+3.*L4.*o4+(-3).*o7+6.*L2.*o7+6.*L3.*o7+(-6).*L2.*L3.*o7+3.*o8+(-3).*L2.*o8+(-3).*L3.*o8,o1+(-2).*o2+2.*L3.*o2+2.*L4.*o2+o29+o3+o30+o31+o32+o33,L2.*L3+o29+o32+o34+o35+o36+o37+o38+o39+2.*o1.*o4,L2.*L4+o31+o32+o37+o40+o41+o42+o43+o44+2.*o1.*o7,o30+o35+o38+o4+o45+o46+(-2).*o5+2.*L2.*o5+2.*L4.*o5+o6,L3.*L4+o37+o38+ ...
  o43+o45+o47+o48+o49+o50+2.*o4.*o7,o33+o42+o43+o46+o49+o7+(-2).*o8+2.*L2.*o8+2.*L3.*o8+o9,o10+o2+o20+o26,L3.*o1+o20+o51+o52,L4.*o1+o26+o52+o53,o13+L2.*o4+o51+o54,L2.*L3.*L4+o52+o54+o55,o19+o53+o55+L2.*o7,o13+o21+o27+o5,o27+L4.*o4+o54+o56,o25+o55+o56+L3.*o7,o19+o25+o28+o8,o3,o34,o40,o30,o47,o33,o36,o41,o39,o44,o6,o48,o46,o50,o9];
end

