function [tmp] = GetV_d1(xieta)
xi = xieta(1,:)'; eta = xieta(2,:)';
tmp=[1+(-1).*eta+(-1).*xi,xi,eta];
end

