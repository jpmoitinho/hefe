% The matrix that defines the components in the vector form of the strain components (6)
% as a function of the global directions (3), for each derivative in a global direction (3)
% The dimensions are  6 x 3 x 3 (number of Li's)
Bbar = cat(3, [1,0,0;0,0,0;0,0,0;0,0,0;0,0,1;0,1,0], [0,0,0;0,1,0;0,0,0;0,0,1;0,0,0;1,0,0], [0,0,0;0,0,0;0,0,1;0,1,0;1,0,0;0,0,0]);