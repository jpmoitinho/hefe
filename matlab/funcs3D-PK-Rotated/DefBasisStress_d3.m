function [f] = DefBasisStress_d3()
f = @BasisStress;
end

function [phi] = BasisStress(L2,L3,L4)
o1=L2.^2;
o2=L2.^3;
o3=(-1).*o2;
o4=L3.^2;
o5=L3.^3;
o6=(-1).*o5;
o7=L4.^2;
o8=L4.^3;
o9=(-1).*o8;
o10=(-2).*L2.*L3;
o11=L2.*o4;
o12=(-2).*L2.*L4;
o13=2.*L2.*L3.*L4;
o14=L2.*o7;
o15=L3.*o1;
o16=(-2).*L3.*L4;
o17=L3.*o7;
o18=L4.*o1;
o19=L4.*o4;
o20=(-1).*L3.*o1;
o21=(-1).*L4.*o1;
o22=(-1).*L2.*o4;
o23=(-1).*L2.*L3.*L4;
o24=(-1).*L2.*o7;
o25=(-1).*L4.*o4;
o26=(-1).*L3.*o7;
phi=[1+(-3).*L2+(-3).*L3+6.*L2.*L3+(-3).*L4+6.*L2.*L4+6.*L3.*L4+(-6).*L2.*L3.*L4+3.*o1+(-3).*L3.*o1+(-3).*L4.*o1+o3+3.*o4+(-3).*L2.*o4+(-3).*L4.*o4+o6+3.*o7+(-3).*L2.*o7+(-3).*L3.*o7+o9,L2+(-2).*o1+2.*L3.*o1+2.*L4.*o1+o10+o11+o12+o13+o14+o2,L3+o10+o13+o15+o16+o17+(-2).*o4+2.*L2.*o4+2.*L4.*o4+o5,L4+o12+o13+o16+o18+o19+(-2).*o7+2.*L2.*o7+2.*L3.*o7+o8,o1+o20+o21+o3,L2.*L3+o20+o22+o23,L2.*L4+o21+o23+o24,o22+o25+o4+o6,L3.*L4+o23+o25+o26,o24+o26+o7+o9,o2,o15,o18,o11,L2.*L3.*L4,o14,o5,o19,o17,o8];
end

