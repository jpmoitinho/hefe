% a unique number for each component of the basis (4 vars) on each face (3 vars)
% dimensions are  nB x 4 (number of faces) x 2  orientation (1 positive, 2 negative)
% x 3 (number of starting nodes) 
IncidStress = cat(4, cat(3, [2,3,4,1;3,1,1,3;4,4,2,2], [2,3,4,1;4,4,2,2;3,1,1,3]), cat(3, [3,1,1,3;4,4,2,2;2,3,4,1], [3,1,1,3;2,3,4,1;4,4,2,2]), cat(3, [4,4,2,2;2,3,4,1;3,1,1,3], [4,4,2,2;3,1,1,3;2,3,4,1]));