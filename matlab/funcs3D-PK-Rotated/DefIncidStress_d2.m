% a unique number for each component of the basis (4 vars) on each face (3 vars)
% dimensions are  nB x 4 (number of faces) x 2  orientation (1 positive, 2 negative)
% x 3 (number of starting nodes) 
IncidStress = cat(4, cat(3, [5,8,10,1;6,3,4,3;7,9,7,2;8,1,1,8;9,4,2,6;10,10,5,5], [5,8,10,1;7,9,7,2;6,3,4,3;10,10,5,5;9,4,2,6;8,1,1,8]), cat(3, [8,1,1,8;9,4,2,6;6,3,4,3;10,10,5,5;7,9,7,2;5,8,10,1], [8,1,1,8;6,3,4,3;9,4,2,6;5,8,10,1;7,9,7,2;10,10,5,5]), cat(3, [10,10,5,5;7,9,7,2;9,4,2,6;5,8,10,1;6,3,4,3;8,1,1,8], [10,10,5,5;9,4,2,6;7,9,7,2;8,1,1,8;6,3,4,3;5,8,10,1]));