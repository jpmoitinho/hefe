function [tmp] = GetDerivsLsXYZ(X,Y,Z)
% Get the derivatives of the L's with respect to the global coordinates,
% for the element with vertices at X,Y,Z, and the origin at vertex 1
% Do not forget: DETERMINANT NOT INCLUDED
%                USE TRANSLATED COORDINATES  coord = coord - coord(1,:) 
o1=Y(4);
o2=Z(2);
o3=(-1).*o2;
o4=Z(3);
o5=Y(3);
o6=Z(4);
o7=(-1).*o6;
o8=Y(2);
o9=(-1).*o4;
o10=X(4);
o11=X(2);
o12=X(3);
tmp=[o1.*(o3+o4)+o5.*(o2+o7)+o8.*(o6+o9),o12.*(o3+o6)+o11.*(o4+o7)+o10.*(o2+ ...
  o9),o11.*(o1+(-1).*o5)+o10.*(o5+(-1).*o8)+o12.*((-1).*o1+o8);(-1).*o1.* ...
  o4+o5.*o6,o10.*o4+(-1).*o12.*o6,o1.*o12+(-1).*o10.*o5;o1.*o2+(-1).*o6.* ...
  o8,(-1).*o10.*o2+o11.*o6,(-1).*o1.*o11+o10.*o8;(-1).*o2.*o5+o4.*o8,o12.* ...
  o2+(-1).*o11.*o4,o11.*o5+(-1).*o12.*o8];
end

