function [tmp] = GetV_d6(xieta)
xi = xieta(1,:)'; eta = xieta(2,:)';
o1=(-1).*eta;
o2=(-1).*xi;
o3=1+o1+o2;
o4=o3.^5;
o5=o3.^4;
o6=xi.^2;
o7=eta.^2;
o8=o3.^3;
o9=xi.^3;
o10=eta.^3;
o11=o3.^2;
o12=xi.^4;
o13=eta.^4;
o14=xi.^5;
o15=eta.^5;
tmp=[o3.^6,o4.*xi,eta.*o4,o5.*o6,eta.*o5.*xi,o5.*o7,o8.*o9,eta.*o6.*o8,o7.* ...
  o8.*xi,o10.*o8,o11.*o12,eta.*o11.*o9,o11.*o6.*o7,o10.*o11.*xi,o11.*o13, ...
  o14.*o3,eta.*o12.*o3,o3.*o7.*o9,o10.*o3.*o6,o13.*o3.*xi,o15.*o3,xi.^6, ...
  eta.*o14,o12.*o7,o10.*o9,o13.*o6,o15.*xi,eta.^6];
end

