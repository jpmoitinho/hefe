function [tmp] = GetV_d7(xieta)
xi = xieta(1,:)'; eta = xieta(2,:)';
o1=(-1).*eta;
o2=(-1).*xi;
o3=1+o1+o2;
o4=o3.^6;
o5=o3.^5;
o6=xi.^2;
o7=eta.^2;
o8=o3.^4;
o9=xi.^3;
o10=eta.^3;
o11=o3.^3;
o12=xi.^4;
o13=eta.^4;
o14=o3.^2;
o15=xi.^5;
o16=eta.^5;
o17=xi.^6;
o18=eta.^6;
tmp=[o3.^7,o4.*xi,eta.*o4,o5.*o6,eta.*o5.*xi,o5.*o7,o8.*o9,eta.*o6.*o8,o7.* ...
  o8.*xi,o10.*o8,o11.*o12,eta.*o11.*o9,o11.*o6.*o7,o10.*o11.*xi,o11.*o13, ...
  o14.*o15,eta.*o12.*o14,o14.*o7.*o9,o10.*o14.*o6,o13.*o14.*xi,o14.*o16, ...
  o17.*o3,eta.*o15.*o3,o12.*o3.*o7,o10.*o3.*o9,o13.*o3.*o6,o16.*o3.*xi, ...
  o18.*o3,xi.^7,eta.*o17,o15.*o7,o10.*o12,o13.*o9,o16.*o6,o18.*xi,eta.^7]; ...
  
end

