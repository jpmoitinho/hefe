function [f] = DefBasisStress_d2()
f = @BasisStress;
end

function [phi] = BasisStress(L2,L3,L4)
o1=L2.^2;
o2=L3.^2;
o3=L4.^2;
o4=(-1).*L2.*L3;
o5=(-1).*L2.*L4;
o6=(-1).*L3.*L4;
phi=[1+(-2).*L2+(-2).*L3+2.*L2.*L3+(-2).*L4+2.*L2.*L4+2.*L3.*L4+o1+o2+o3,L2+(-1).*o1+o4+o5,L3+(-1).*o2+o4+o6,L4+(-1).*o3+o5+o6,o1,L2.*L3,L2.*L4,o2,L3.*L4,o3];
end

