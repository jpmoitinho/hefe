% The matrix that defines the derivatives with respect to the Li's 
% in terms of the same basis
% The dimensions are  nB x nB x 4 (number of Li's)
Ms = cat(3, [1,0,0,0;1,0,0,0;1,0,0,0;1,0,0,0], [0,1,0,0;0,1,0,0;0,1,0,0;0,1,0,0], [0,0,1,0;0,0,1,0;0,0,1,0;0,0,1,0], [0,0,0,1;0,0,0,1;0,0,0,1;0,0,0,1]);