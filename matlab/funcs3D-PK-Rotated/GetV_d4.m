function [tmp] = GetV_d4(xieta)
xi = xieta(1,:)'; eta = xieta(2,:)';
o1=(-1).*eta;
o2=(-1).*xi;
o3=1+o1+o2;
o4=o3.^3;
o5=o3.^2;
o6=xi.^2;
o7=eta.^2;
o8=xi.^3;
o9=eta.^3;
tmp=[o3.^4,o4.*xi,eta.*o4,o5.*o6,eta.*o5.*xi,o5.*o7,o3.*o8,eta.*o3.*o6,o3.* ...
  o7.*xi,o3.*o9,xi.^4,eta.*o8,o6.*o7,o9.*xi,eta.^4];
end

