% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
% You need to define 3 or 5 variables before calling this script
%  - meshname, probname and degree 
%  - meshname, probname, degree_s, degree_v and degree_u
%

addpath('../base');
funcs = '../funcs';
addpath(funcs);

prob = DefinePlaneProblem(meshname, probname, funcs, 2);

if (exist('degree', 'var'))
    prob.equil.degree_s = degree;
    prob.equil.degree_v = degree;
    prob.compat.degree_u = degree;
else
    prob.equil.degree_s = degree_s;
    prob.equil.degree_v = degree_v;
    prob.compat.degree_u = degree_u;
end

fprintf ('%s %s %d %d\n', prob.meshname.name, prob.probname.name, ...
    prob.equil.degree_s, prob.compat.degree_u);

prob = EquilMats(prob);
prob = GlobalEquil(prob);
PostprocEquil(prob, 'EG', prob.equil.sol, prob.equil.s0);

prob = CompatMats(prob);
prob = GlobalCompat(prob);
prob.compat.stress_params = CompatStressParams(prob,  prob.compat.sol);
PostprocCompat(prob, 'CG', prob.compat.sol, prob.compat.stress_params);
