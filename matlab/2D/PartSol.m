function [ has_s0, s0 ] = PartSol(bfgp, DS, coords, GWdJ)
% Find a particular solution

% bfgp is either [ 2 1 npts ] or [ 2 3 npts ] (the bf's at the gp's)

npts = size(coords,1);

% Find the basis for D*S at the Gauss Points
DBasis = DS.handleDBasis(coords);

BasisF = zeros(2, DS.nS0, npts);
% fx
BasisF(1,:,:) = (DBasis(:,:,1)*DS.T0{1} + DBasis(:,:,2)*DS.T0{3})';
% fy
BasisF(2,:,:) = (DBasis(:,:,1)*DS.T0{3} + DBasis(:,:,2)*DS.T0{2})';

% The basis by itself
aux = sum(mtimesx( GWdJ, mtimesx(BasisF, 't', BasisF)), 3);

% Normalise diagonal
scale = diag(1./sqrt(abs(diag(aux))));
aux = scale*aux*scale;

% The basis by the defined loads
b = scale*sum(mtimesx( GWdJ, mtimesx(BasisF, 't', bfgp)), 3);

% Remove eventual singletons
b = squeeze(b);

s0 = -scale*(aux\b);

if (norm(s0)==0)
    has_s0 = false;
else
    has_s0 = true;
end
