% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = EquilMats( prob )
%
% EquilMats - Compute the matrices relevant for equilibrium formulations
%   

% Define the integration rules for _this_ problem
% excess defines how much we add (should be zero?)
excess = 1;
[ NI2D, NI1D ] = def_integration_2D(prob.equil.degree_s*2+excess, ...
    prob.equil.degree_s+prob.equil.degree_v+2*excess);

% Stress approximation definitions
prob.equil.DS = DefStress2D(prob.equil.degree_s);
nS = prob.equil.DS.nS;
nS0 = prob.equil.DS.nS0;

nrbm = 3;

% Boundary displacements approximation definitions
nV = prob.equil.degree_v + 1;
ViG = reshape(function_V1D(NI1D.xi, prob.equil.degree_v)', [1 nV NI1D.npts]);
VG = zeros(2, 2*nV, NI1D.npts);
VG(1, 1:nV, :) = ViG;
VG(2, nV+(1:nV), :) = ViG;

% The shape functions of the element (T3) at the 2D Gauss points
Phi = Phi_T3(NI2D.xieta);
DPhi = DPhi_T3();

% The shape functions of the linear side at the 1D Gauss points
Phi_side = [ (1-NI1D.xi)/2, (1+NI1D.xi)/2 ];

% The parameters of the particular solution
prob.equil.s0 = zeros(nS0, 1, prob.mesh.nc);
% The integrals of the particular solutions with themselves
prob.equil.s0s0 = zeros(1, prob.mesh.nc);

% The elemental matrices
prob.equil.Flex = zeros(nS, nS, prob.mesh.nc);
prob.equil.e0 = zeros(nS, prob.mesh.nc);
prob.equil.Dside = zeros(2*nV, nS, 3, prob.mesh.nc);
prob.equil.tside = zeros(2*nV, 3, prob.mesh.nc);

prob.geom.ledges = zeros(1, prob.mesh.ne);

GWSide = reshape(NI1D.GW, [ 1 1 NI1D.npts ]);

% The matrix that projects the boundary disp approximations 
% onto themselves (the same for all sides)
%
% The Jacobian (the length of the side) is inserted when post processing
aux = sum(mtimesx(mtimesx(ViG, 't', ViG), GWSide), 3);
prob.equil.VtV = aux;

VtV = [ aux, zeros(nV) ; zeros(nV), aux];

for elem = 1:prob.mesh.nc
    [ cell_edges, cell_verts ] = prob.mesh.get_ev_c(elem);
    [ props, bload ] = GetProps(elem, prob.mesh.elems_faces, ...
        prob.mesh.ele_tags, prob.prob.Faces);
    Elast = props(1);
    nu = props(2);
    
    flex = [ 1 -nu 0 ; -nu 1 0; 0 0 2*(1+nu)]/Elast; % Plane stress
    %     flex = [ (1-nu^2) -nu*(1+nu) 0 ; -nu*(1+nu) (1-nu^2) 0; 0 0 2*(1+nu)]/Elast; % Plane strain

    % Get global coordinates of element. Use a frame at its center
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);

    % Transform the Gauss points into coordinates
    coordGP_g = Phi*elem_coords;

    % Properly centered and scaled
    coordGP = coordGP_g - ...
        ones(NI2D.npts,1)*prob.geom.center(:,elem)';
    
    % Determinant of the Jacobian of the element
    dJac = det(DPhi*elem_coords);
    GWdJ = reshape(NI2D.GW*dJac, [ 1 1 NI2D.npts ]);
    

    if (strcmp(bload,'[zero,zero]'))
        has_s0 = false;
    else
        % Find the weigths of the particular solution
        bfgp = reshape(atpoint2D( bload, coordGP_g)', [ 2 1 NI2D.npts]);
        [has_s0, prob.equil.s0(:, 1, elem)] = PartSol(bfgp, prob.equil.DS, coordGP, GWdJ);
    end
    
    % Equilibrated stresses at the Gauss points
    StressGP = CalcStress2D(coordGP, prob.equil.DS);
    
    ElemFlex = sum(mtimesx(GWdJ, mtimesx(StressGP, 't', ...
        mtimesx(-flex,StressGP))), 3);
    ElemFlex = (ElemFlex+ElemFlex')/2;
    % make the matrix "exactly" symmetric
    prob.equil.Flex(:,:,elem) = ElemFlex;

    % The equivalent strains due to the particular solution
    if (has_s0)
        Stress0GP = CalcStress02D(coordGP, prob.equil.DS, ...
            prob.equil.s0(:,1,elem));
        prob.equil.e0(:,elem) = sum(mtimesx(GWdJ, mtimesx(StressGP, 't', ...
            mtimesx(flex,Stress0GP))), 3);
        prob.equil.s0s0(elem) = sum(mtimesx(GWdJ, mtimesx(Stress0GP, 't', ...
            mtimesx(flex,Stress0GP))), 3);
    end
    
    % Loop on the edges of the element
    for edge = 1:3
        [ ~, verts_edge ] = prob.mesh.get_cv_e(cell_edges(edge));
        % Check orientation of cell_edge relative to the edge entity
        % The orientation of the edge entity is the reference
        pm = prob.geom.sign(elem, edge);

        % GP's of the edge, in the frame of the element
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]) - ...
            ones(2,1)*prob.geom.center(:, elem)';

        coordGP = Phi_side*vert_coords;
        StressGP = CalcStress2D(coordGP, prob.equil.DS);
        dxdy = pm*(vert_coords(2,:) - vert_coords(1,:));
        lside = norm(dxdy);
        prob.geom.ledges(cell_edges(edge)) = lside;
        
        % The normal is not divided by the length
        Normal = [ dxdy(2) 0 -dxdy(1); 0 -dxdy(1) dxdy(2) ];
        TSide = mtimesx(Normal, StressGP);
        vtside = mtimesx(VG, 't', TSide);
        
        % Not multiplied by the length (compensates for the normal)
        aux = sum(mtimesx(vtside, GWSide),3);
        prob.equil.Dside(:, :, edge, elem)  = aux;
        
        % The term from the particular solution
        if (has_s0)
            Stress0GP = CalcStress02D(coordGP, prob.equil.DS, ...
                prob.equil.s0(:,1,elem));
            T0Side = mtimesx(Normal, Stress0GP);
            prob.equil.tside(:, edge, elem) = sum(mtimesx(mtimesx(VG, 't', ...
                T0Side), GWSide), 3);
        end
        
    end
    
end

prob.equil.BoundaryConds = zeros(prob.prob.ng_edges, 2, nV, 2);
for i = 1:prob.prob.ng_edges
    g_edge = prob.prob.g_edges(i);
    edge = full(prob.mesh.edges_elems(g_edge));
    tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
    which_tag = find(prob.prob.Edges.tags == tag);
    
    % If vals{end} is not zero we will need to impose
    % the corresponding BC. We integrate it now
    % This code is agnostic regarding the type of BC
    if (size(which_tag,1) && ~strcmp(prob.prob.Edges.vals{end}{which_tag}, '[zero,zero]'))
        sload = prob.prob.Edges.vals{end}{which_tag};
        [ ~, verts_edge ] = prob.mesh.get_cv_e(edge);
        % these coordinates are in the global frame (used for the bc's)
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]);
        coordGP = Phi_side*vert_coords;
        dxdy = (vert_coords(2,:) - vert_coords(1,:));
        lside = norm(dxdy);
        aux = mtimesx(reshape( atpoint2D(sload, coordGP)', [2 1 NI1D.npts]), ...
            reshape(Phi_side', [1 2 NI1D.npts]));
        vtside = mtimesx(VG, 't', aux);
        % Multiplied by the length (there is no normal!!!)
        aux = sum(mtimesx(vtside, GWSide),3)*lside;
        prob.equil.BoundaryConds(g_edge, 1, :, :) = aux(1:nV, :);
        prob.equil.BoundaryConds(g_edge, 2, :, :) = aux(nV+(1:nV), :);
    end
end

prob.equil.calc_mats = true;
