% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = CompatMats( prob )
% EquilMats - Compure the matrices relevant for comaptibility
%  


% The integration rules for _this_ problem
%

% HOW CAN WE GUESS APPROPRIATE INTEGRATION RULES, WITH PU's AND ALL?
excess = 6;

[ NI2D, NI1D ] = def_integration_2D((prob.compat.degree_u-1)*2+excess, ...
    prob.compat.degree_u*2+2*excess); 

% Build elementary stiffness matrices

prob.compat.DU = DefCompatBasis2D(prob.compat.degree_u);

PhiC = prob.compat.DU.handleBasis(NI2D.xieta');
DPhiC = prob.compat.DU.handleDBasis(NI2D.xieta');

nU = 2*prob.compat.DU.nB;
DispGP = zeros(2, nU, NI2D.npts);
DispGP(1, 1:prob.compat.DU.nB, :) = PhiC';
DispGP(2, prob.compat.DU.nB+(1:prob.compat.DU.nB), :) = PhiC';

Phi_side = [ (1-NI1D.xi)/2, (1+NI1D.xi)/2 ];

% For the geometry
Phi = Phi_T3(NI2D.xieta);
DPhi = DPhi_T3();

prob.compat.Ke = zeros(nU, nU, prob.mesh.nc);
prob.compat.Fe = zeros(nU, 3, prob.mesh.nc);

% The displacement approximations on the sides
prob.compat.BasisGP = zeros(prob.compat.DU.nB, NI1D.npts,3);
prob.compat.BasisGP(:,:,1) = prob.compat.DU.handleBasis([ (NI1D.xi+1)/2, zeros(size(NI1D.xi))])';
prob.compat.BasisGP(:,:,2) = prob.compat.DU.handleBasis([ (1-NI1D.xi)/2, (NI1D.xi+1)/2])';
prob.compat.BasisGP(:,:,3) = prob.compat.DU.handleBasis([ zeros(size(NI1D.xi)), (1-NI1D.xi)/2 ])';

% The matrix that projects the boundary displacements onto themselves
prob.compat.UUSide = zeros(prob.compat.DU.nB, prob.compat.DU.nB, 3);
for side = 1:3
    aux = reshape(prob.compat.BasisGP(:, :, side), [prob.compat.DU.nB 1 NI1D.npts ]);
    prob.compat.UUSide(:, :, side) = ...
        sum(mtimesx(reshape(NI1D.GW, [ 1 1 NI1D.npts ]), mtimesx(aux,aux,'t')), 3);
end

for elem = 1:prob.mesh.nc
    [ ~, cell_verts ] = prob.mesh.get_ev_c(elem);
    [ props, bload ] = GetProps(elem, prob.mesh.elems_faces, ...
        prob.mesh.ele_tags, prob.prob.Faces);
    Elast = props(1);
    nu = props(2);

    flex = [ 1 -nu 0 ; -nu 1 0; 0 0 2*(1+nu)]/Elast; % Plane stress
    %     flex = [ (1-nu^2) -nu*(1+nu) 0 ; -nu*(1+nu) (1-nu^2) 0; 0 0 2*(1+nu)]/Elast; % Plane strain

    %     sflex = sqrtm(flex);
    stiff = inv(flex);
    
    % Get global coordinates of element.
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);
    coordGP_g = Phi*elem_coords;

    % Jacobian of the element and determinant 
    % THIS IS FOR A LINEAR GEOMETRY (constant Jac)
    Jac = DPhi*elem_coords;
    dJac = det(Jac);
    GWdJ = reshape(NI2D.GW*dJac, [ 1 1 NI2D.npts ]);
    
    % The stiffness matrix
    Bi = B(DPhiC, Jac);
    prob.compat.Ke(:,:,elem) = sum(mtimesx(GWdJ, mtimesx(Bi, 't', mtimesx( stiff, Bi))), 3);
    
    % The "real" body forces
    force = inline(bload, 'x', 'y', 'zero', 'one');
    forceGP = reshape(force(coordGP_g(:,1), coordGP_g(:,2), ...
        zeros(size(coordGP_g(:,1))), ones(size(coordGP_g(:,1))))', [ 2, 1, NI2D.npts, 1]);
    forceGP_i = mtimesx(forceGP,reshape(Phi, [1 1 NI2D.npts 3]));

    EquivForceGP_i = mtimesx(DispGP, 't', forceGP_i);
   
    GWdJ = reshape(NI2D.GW*dJac, [ 1 1 NI2D.npts 1 ]);
    prob.compat.Fe(:,:,elem) = reshape(sum(mtimesx(GWdJ, EquivForceGP_i),3), [nU 3]);
    
end


DispGP = zeros(2, 2*prob.compat.DU.nB, NI1D.npts);
prob.compat.BoundaryConds = zeros(prob.prob.ng_edges, 2*prob.compat.DU.nB, 3);
for i = 1:prob.prob.ng_edges
    
    g_edge = prob.prob.g_edges(i);
    tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
    which_tag = find(prob.prob.Edges.tags == tag);
    if (~isempty(which_tag))
        edge = full(prob.mesh.edges_elems(g_edge));
        [ elems_edge, verts_edge ] = prob.mesh.get_cv_e(edge);
        % Project onto the first (with positive orientation) element
        elem = elems_edge(1);
        [ ~, verts_elem ] = prob.mesh.get_ev_c(elem);
        sfunc = prob.prob.Edges.vals{end}{which_tag};
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]);
        coordGP = Phi_side*vert_coords;
        dxdy = (vert_coords(2,:) - vert_coords(1,:));
        lside = norm(dxdy);
        funcGP = reshape( atpoint2D(sfunc, coordGP)', [2 1 NI1D.npts]);
        % The element coordinates of the side GP's
        if (verts_edge(1) == verts_elem(1))
            side = 1;
        else
            if (verts_edge(1) == verts_elem(2))
                side = 2;
            else
                side = 3;
            end
        end
        DispGP(1, 1:prob.compat.DU.nB, :) = prob.compat.BasisGP(:, :, side);
        DispGP(2, prob.compat.DU.nB+(1:prob.compat.DU.nB), :) = prob.compat.BasisGP(:, :, side);
        GWdJSide = reshape(NI1D.GW, [ 1 1 NI1D.npts ])*lside;
        % The first three member of the base are our PU
        funcGP_i = mtimesx(funcGP,reshape(squeeze(prob.compat.BasisGP(1:3, :, side))', [1 1 NI1D.npts 3])); % CLUMSY!
        
        prob.compat.BoundaryConds(g_edge,:,:) = sum(mtimesx(GWdJSide, mtimesx(DispGP, 't', funcGP_i)), 3);
    end
end



end