% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [B] = B(dphis, jac)

npts = size(dphis,1);
nfuncs = size(dphis,2);
ijac = inv(jac);

dphix = zeros(npts, nfuncs);
dphiy = zeros(npts, nfuncs);

for i=1:npts
    dphix(i,:) = ijac(1,:)*(reshape(dphis(i,:,:), nfuncs, 2)');
    dphiy(i,:) = ijac(2,:)*(reshape(dphis(i,:,:), nfuncs, 2)');
end

aux = zeros(npts, nfuncs);

B = zeros(3, 2*nfuncs, npts);
B(1,:,:) = [ dphix, aux]';
B(2,:,:) = [ aux, dphiy]';
B(3,:,:) = [ dphiy, dphix]';
