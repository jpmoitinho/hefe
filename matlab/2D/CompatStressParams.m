% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ params ] = CompatStressParams( prob, sol)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if (size(sol) == [ prob.mesh.nc prob.compat.DU.nB*2])
    byelem = true;
else
    byelem = false;
end

DPhi = DPhi_T3();
nB = prob.compat.DU.nB;

params = zeros(3, nB, prob.mesh.nc);
for elem = 1:prob.mesh.nc
    [ props, ~ ] = GetProps(elem, prob.mesh.elems_faces, ...
        prob.mesh.ele_tags, prob.prob.Faces);
    Elast = props(1);
    nu = props(2);
    
    flex = [ 1 -nu 0 ; -nu 1 0; 0 0 2*(1+nu)]/Elast; % Plane stress
    stiff = inv(flex);
    [ ~, cell_verts ] = prob.mesh.get_ev_c(elem);
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);
    iJac = inv(DPhi*elem_coords);
    if (byelem)
        desls = sol(elem,:)';        
    else
        desls = prob.compat.signs(elem,:)'.*sol(prob.compat.incid(elem,:));
    end
    
    deslx = desls(1:nB);
    desly = desls(nB+(1:nB));
    
    duxdxi = prob.compat.DU.DT{1}*deslx;
    duxdeta = prob.compat.DU.DT{2}*deslx;
    
    duydxi = prob.compat.DU.DT{1}*desly;
    duydeta = prob.compat.DU.DT{2}*desly;
    
    DUxdX = iJac(1,1)*duxdxi + iJac(1,2)*duxdeta;
    DUxdY = iJac(2,1)*duxdxi + iJac(2,2)*duxdeta;
    DUydX = iJac(1,1)*duydxi + iJac(1,2)*duydeta;
    DUydY = iJac(2,1)*duydxi + iJac(2,2)*duydeta;
    
    params(1, :, elem) = [ stiff(1,1)*DUxdX + stiff(1,2)*DUydY];
    params(2, :, elem) = [ stiff(2,1)*DUxdX + stiff(2,2)*DUydY];
    params(3, :, elem) = [ stiff(3,3)*( DUxdY + DUydX)];

end

prob.compat.stress_params = params;

end

