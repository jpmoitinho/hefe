% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [V] = function_V1D (t, n)
% 
% pts = size(t, 1);
% V = zeros(pts, n+1);
% V(:, 1) = 1.;
% for i = 1:n
%     V(:, i+1) =  V(:,i).*t;
% end
m = size(t,1);
V =  p_polynomial(m,n,t);

% Normalise the integral
% for n = 1:(n+1)
%     V(:,n) = V(:,n)*sqrt((2*n-1));
% end

return
