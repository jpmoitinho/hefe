% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = PostprocCompat( prob, type, sol, params, rotate)
% Post process a compatible solution
%
% Create a text file with some results
namename = prob.meshname.name;
probname = prob.probname.name;

outname = sprintf('Results/%s-%s-Disps-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/CompatBasis2D-%d.msh', prob.funcs, prob.compat.degree_u);
scheme = fileread(aux);
fprintf(output, '%s', scheme);


fprintf(output, '$ElementNodeData\n2\n');
fprintf(output, '"%s Disps - %s %s"\n"Basis %d"\n', type, namename, probname, prob.compat.degree_u);
fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.nc);

if (size(sol) == [ prob.mesh.nc prob.compat.DU.nB*2])
    byelem = true;
else
    byelem = false;
end

for elem = 1:prob.mesh.nc
    fprintf(output, '%d %d', prob.mesh.elems_faces(elem), prob.compat.DU.nB);
    if (byelem)
        fprintf(output, ' %e', [ reshape(sol(elem,:)', [prob.compat.DU.nB 2]), zeros(prob.compat.DU.nB,1)]');        
    else
        fprintf(output, ' %e', [ reshape(prob.compat.signs(elem,:)'.*sol(prob.compat.incid(elem,:)), [prob.compat.DU.nB 2]), zeros(prob.compat.DU.nB,1)]');
    end
    fprintf(output,'\n');
end

fprintf(output, '$EndElementNodeData\n');

fclose(output);

outname = sprintf('Results/%s-%s-Stress-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/CompatBasis2D-%d.msh', prob.funcs, prob.compat.degree_u);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

% variable rotate has the rotation angle of x' relative to x (in radians)
if (exist('rotate', 'var'))
    sa = sin(rotate);
    ca = cos(rotate);
    T = [ ca*ca sa*sa 2*ca*sa ; sa*sa ca*ca -2*ca*sa ; -ca*sa ca*sa (ca*ca-sa*sa) ];
    for elem = 1:prob.mesh.nc
        params(:,:,elem) = mtimesx ( T, reshape(params(:,:,elem), [3 prob.compat.DU.nB ]));
    end
end

names = {'Sxx', 'Syy', 'Sxy'};
for i = 1:3
    fprintf(output, '$ElementNodeData\n2\n');
    fprintf(output, '"%s (%s) Stresses - %s %s"\n"Basis %d"\n', ...
        names{i}, type, namename, probname, prob.compat.degree_u);
    fprintf(output, '1\n0.0\n3\n0\n1\n%d\n', prob.mesh.nc);
    for elem = 1:prob.mesh.nc
        fprintf(output, '%d %d', prob.mesh.elems_faces(elem), prob.compat.DU.nB);
        fprintf(output, ' %e', params(i,:,elem));
        fprintf(output,'\n');
    end
    fprintf(output, '$EndElementNodeData\n');
end



fclose(output);
