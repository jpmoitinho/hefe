% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob, senerg, energ ] = GlobalCompat( prob )
% Solve global compatible model
%

% should check if prob.compat.calc_mats is true. Otherwise do it.

prob.compat.global = true;

nB = prob.compat.DU.nB;
nD = nB*2;

halfdim = ( prob.mesh.nv + prob.mesh.ne*(prob.compat.degree_u-1) + prob.mesh.nc*prob.compat.DU.nBubble);
dim = 2*halfdim;
maxnzA = prob.mesh.nc*(nD)^2 + (prob.prob.nR*(prob.compat.degree_u+1)*2) *2;
Ai = zeros(maxnzA, 1); Aj = zeros(maxnzA, 1); Ap = zeros(maxnzA, 1); numnzA = 0;
Fg = zeros(dim,1);


prob.compat.incid = zeros(prob.mesh.nc, nD);
prob.compat.signs = ones(prob.mesh.nc, nD);
for elem = 1:prob.mesh.nc
    [ elem_edges, elem_verts ] = prob.mesh.get_ev_c(elem);
    
    % The 3 vertices
    prob.compat.incid(elem,1:3) = elem_verts;
    
    % Edges
    where = 3 + (1:3);
    for i = 1:(prob.compat.degree_u-1)
        prob.compat.incid(elem,where) = prob.mesh.nv + (elem_edges-1)*(prob.compat.degree_u-1) + i;
        if ( mod(i,2) == 0)
            prob.compat.signs(elem,where) = prob.geom.sign(elem,:);
            prob.compat.signs(elem,where + nB) = prob.geom.sign(elem,:);
        end
        where = where + 3;
    end
        
    % Elements
    if (prob.compat.degree_u > 2)
        where = (3*prob.compat.degree_u+1):nB;
        prob.compat.incid(elem,where) = prob.mesh.nv + prob.mesh.ne*(prob.compat.degree_u-1) + ...
            (elem-1)*prob.compat.DU.nBubble + (1:prob.compat.DU.nBubble);
    end
end
prob.compat.incid(:,nB+(1:nB)) = halfdim + prob.compat.incid(:,1:nB);

for elem = 1:prob.mesh.nc
    aux = diag(prob.compat.signs(elem,:));
    inc = prob.compat.incid(elem,:);
    Ke = aux*prob.compat.Ke(:,:,elem)*aux;
    for i = 1:(2*prob.compat.DU.nB)
        news = numnzA + (1:nD);
        Ai(news) = inc(i);
        Aj(news) = inc;
        Ap(news) = Ke(:,i);
        numnzA = numnzA + nD;
    end
    % Undo the PU
    Fg(inc) = Fg(inc) + sum(prob.compat.Fe(:,:,elem),2);
end

% The kinematic bc's
nsupport = sum(sum(isfinite(prob.prob.incid_support(:,1:prob.prob.ng_edges))));

aux = any(isfinite(prob.prob.incid_support(:,1:prob.prob.ng_edges)));
fixed_edges = prob.prob.g_edges(aux);
FixedVerts = [];

for g_edge = fixed_edges
    edge = full(prob.mesh.edges_elems(g_edge));
    [ ~, aux ] = prob.mesh.get_cv_e(edge);
    FixedVerts = unique([ FixedVerts, aux ]);
end

nFixedVerts = size(FixedVerts,2);
incFixedVerts = nan(2, nFixedVerts);
incid_R = dim;
for v = 1:nFixedVerts
    % The edges of the vertex
    [ ~, edges ] = prob.mesh.get_ce_v(FixedVerts(v));
    % Only those that have properties
    aux = prob.mesh.elems_edges(edges);
    edges = aux(aux>0);
    % Check when they are on the kinematic boundary
    aux = any(isfinite(prob.prob.incid_support(:,edges)),2);
    % For fixed displacements in the side coordinate this will have to
    % change
    for dir = 1:2
        if (aux(dir))
            incid_R = incid_R + 1;
            incFixedVerts(dir,v) = incid_R;
        end
    end
end

prob.compat.FixedVerts = FixedVerts;
prob.compat.incFixedVerts = incFixedVerts;

% Increase RHS
if (nsupport > 0)
    Fg(incid_R+nsupport*(prob.compat.degree_u-1)) = 0;
end

for g_edge = prob.prob.g_edges % fixed_edges
    edge = full(prob.mesh.edges_elems(g_edge));
    [ edge_elems, ~ ] = prob.mesh.get_cv_e(edge);
    [ elem_edges, ~ ] = prob.mesh.get_ev_c(edge_elems(1));
    which = find(elem_edges == edge);
    which1 = mod(which, 3) + 1;
    
    aux = [ which which1 ];
    [ ~, verts ] = prob.mesh.get_cv_e(edge);
    [ liverts, iverts ] = ismember(verts, FixedVerts, 'R2012a');

    % AGAIN!!!
    vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts),[1 2]);
    dxdy = (vert_coords(2,:) - vert_coords(1,:));
    lside = norm(dxdy);

    for dir = 1:2
        if (isfinite(prob.prob.incid_support(dir,g_edge)))
            offset = (dir-1)*halfdim;
            disps = incFixedVerts(dir, iverts);
            verts = verts(liverts);
            incid_edge = [ verts(isfinite(disps))+offset , prob.mesh.nv + (edge-1)*(prob.compat.degree_u-1) + offset + (1:(prob.compat.degree_u-1))];
            incid_basis = [ aux(isfinite(disps)), 3 + (which:3:(3*(prob.compat.degree_u-1))) ];
            incid_reaction = [ disps(isfinite(disps)), incid_R + (1:(prob.compat.degree_u-1)) ];
            incid_R = incid_R + prob.compat.degree_u - 1;
            num = size(incid_edge, 2);
            aux2 = diag(prob.compat.signs(edge_elems,incid_basis));
            aux1 = aux2*prob.compat.UUSide(incid_basis, incid_basis,which)*aux2*lside;
            for i1 = 1:num
                news = numnzA + (1:num);
                Ai(news) = incid_edge;
                Aj(news) = incid_reaction(i1);
                Ap(news) = aux1(:,i1);
                news = news + num;
                Ai(news) = incid_reaction(i1);
                Aj(news) = incid_edge;
                Ap(news) = aux1(:,i1);
                numnzA = numnzA + 2*num;
            end
            Fg(incid_reaction) = Fg(incid_reaction) + ...
                aux2*sum(prob.compat.BoundaryConds(g_edge, incid_basis+(dir-1)*prob.compat.DU.nB, :), 3)';
        else
            % It is a force. We used the displacements of first element adjcent to the first side
            aux1 = (dir-1)*prob.compat.DU.nB + (1:prob.compat.DU.nB);
            Fg(prob.compat.incid(edge_elems(1),aux1)) = Fg(prob.compat.incid(edge_elems(1),aux1)) + ...
                sum(prob.compat.BoundaryConds(g_edge, aux1, :), 3)';
        end
    end
end

A = sparse(Ai(1:numnzA), Aj(1:numnzA), Ap(1:numnzA));
clear('Ai'); clear('Aj'); clear('Ap');

% Block floating structure
if (nsupport == 0)
    fixs = zeros(3,1);
    
    % For the first node of first element fix all disps
    fixs(1) = prob.compat.incid(1,1);
    fixs(2) = prob.compat.incid(1,1+prob.compat.DU.nB);
    % For the second node check direction
    dx = prob.mesh.coords(prob.mesh.ele_infos(1,2),1:2) - prob.mesh.coords(prob.mesh.ele_infos(1,1),1:2);
    if (abs(dx(1)) > abs(dx(2)))
        fixs(3) = prob.compat.incid(2,1);
    else
        fixs(3) = prob.compat.incid(2,1+prob.compat.DU.nB);
    end
    
    for i=1:3 
        A(fixs(i),fixs(i)) = 1.001*A(fixs(i),fixs(i));
    end
end

prob.compat.sol = A\Fg;

if (nsupport == 0)
    if (norm(prob.compat.sol(fixs)) > 1e-10)
        fprintf('\nProblem with floating stucture (@%d %d %d): %e %e %e\n', ...
            fixs, prob.compat.sol(fixs));
    end
end

% prob.compat.A = A;
prob.compat.b = Fg;

nb = norm(Fg);
nA = nan; % normest(A);
nx = norm(prob.compat.sol);
nres = norm(Fg-A*prob.compat.sol);
cn = condest(A);
fprintf ('\nRHS, System and solution norms: %e %e %e\nResidual norm: %e (relative is %e).\nEstimated condition number: %e\n\n', ...
    nb, nA, nx, nres, nres/(nb+nA*nx), cn);

senerg = 0;
energ = 0;
for elem = 1:prob.mesh.nc
    aux = diag(prob.compat.signs(elem,:))*prob.compat.sol(prob.compat.incid(elem,:));
    senerg = senerg + aux'* ...
        reshape(prob.compat.Ke(:,:,elem), [nD nD])*aux;
    % For the potential energy we add all the work done by all the equivalent nodal forces
    energ = energ + sum(prob.compat.Fe(:,:,elem),2)'*aux;
end
senerg = 0.5*senerg;
 
for g_edge = prob.prob.g_edges
    edge = full(prob.mesh.edges_elems(g_edge));
    [ edge_elems, ~ ] = prob.mesh.get_cv_e(edge);
    elem = edge_elems(1);
    for dir = 1:2
        if (isnan(prob.prob.incid_support(dir,g_edge)))
            aux = (dir-1)*nB+(1:nB);
            energ = energ + sum(prob.compat.BoundaryConds(g_edge,aux,:),3)*prob.compat.sol(prob.compat.incid(elem,aux));
        end
    end
end

fprintf('Str Energy   %.14e\nPot Energy   %.14e\nTotal Energy %.14e\n', ...
    senerg, energ, senerg-energ);

prob.compat.global = true;
prob.compat.energ = energ;
prob.compat.senerg = senerg;

end

