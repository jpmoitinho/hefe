% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob, senerg, energ ] = GlobalEquil( prob )
% Solve global equilibrium model
%

% should check if prob.equil.calc_mats is true. Otherwise do it.

nS = prob.equil.DS.nS;
nV = prob.equil.degree_v + 1;

% The maximum number of non-zero terms in the governing system
maxnzA = prob.mesh.nc*nS^2 + (3*prob.mesh.nc*nS + prob.prob.nR*nV)*(nV*2)*2;
Ai = zeros(maxnzA, 1); Aj = zeros(maxnzA, 1); Ap = zeros(maxnzA, 1); 
numnzA = 0;

% Right hand term
b = zeros(prob.mesh.nc*nS + prob.mesh.ne*2*nV + prob.prob.nR*nV, 1);

% The position of stress variables for each elem
incid_s = 1:nS;
for elem = 1:prob.mesh.nc
    [ cell_edges, ~ ] = prob.mesh.get_ev_c(elem);
    b(incid_s) = prob.equil.e0(:, elem);
    
    % Setup the flexibility matrix

    news = numnzA + (1:nS);
    for i1 = 1:nS
        Ai(news) = incid_s(i1);
        Aj(news) = incid_s;
        Ap(news) = prob.equil.Flex(:, i1, elem);
        news = news +nS;
    end
    numnzA = numnzA + nS*nS;
    
    for edge = 1:3
        incid_v = prob.mesh.nc*nS + (cell_edges(edge)-1)*2*nV + (1:(2*nV));

        % The term from the particular solution
        b(incid_v) = b(incid_v) - prob.equil.tside(:, edge, elem);

        news = numnzA + (1:nS);
        for i1 = 1:(2*nV)
            Ai(news) = incid_v(i1);
            Aj(news) = incid_s;
            Ap(news) = prob.equil.Dside(i1, :, edge, elem);
            news = news + nS;
            Ai(news) = incid_s;
            Aj(news) = incid_v(i1);
            Ap(news) = prob.equil.Dside(i1, :, edge, elem);
            news = news + nS;
            numnzA = numnzA + 2*nS;
        end

    end
    
    incid_s = incid_s + nS;
end

% Add the matrices for the sides with fixed displacements
incid_R = prob.mesh.nc*nS + prob.mesh.ne*2*nV + (1:nV);
for i = 1:prob.prob.ng_edges
    g_edge = prob.prob.g_edges(i);
    edge = full(prob.mesh.edges_elems(g_edge));
    lside = prob.geom.ledges(edge);
    for dir = 1:2
        if (isfinite(prob.prob.incid_support(dir,i)))
            incid_v = prob.mesh.nc*nS + (edge-1)*2*nV + (dir-1)*nV + (1:nV);
            for i1 = 1:nV
                news = numnzA + (1:nV);
                Ai(news) = incid_v(i1);
                Aj(news) = incid_R;
                Ap(news) = lside*prob.equil.VtV(i1, :);
                news = news + nV;
                Ai(news) = incid_R;
                Aj(news) = incid_v(i1);
                Ap(news) = lside*prob.equil.VtV(i1, :);
                numnzA = numnzA + 2*nV;
            end
            incid_R = incid_R + nV;
        end
    end
    
    if (max(max(max(abs(prob.equil.BoundaryConds(i,:,:,:))))) > 0) % Has something
        tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
        which_tag = find(prob.prob.Edges.tags == tag,1);
        
        for dir = 1:2
            aux = reshape(sum(prob.equil.BoundaryConds(i,dir,:,:),4), [ nV 1 ]);
            if (norm(aux) ~= 0)
                if (prob.prob.Edges.vals{dir}(which_tag) == 0)
                    incid_r = prob.mesh.nc*nS + prob.mesh.ne*2*nV + ...
                        prob.prob.incid_support(dir,i)* nV + (1:nV);
                    b(incid_r) = b(incid_r) + aux;
                else % Prob.Edges.vals{dir}(which_tag) has to be 1
                    incid_v = prob.mesh.nc*nS + (edge-1)*2*nV + (dir-1)*nV + (1:nV);
                    b(incid_v) = b(incid_v) + aux;
                end
            end
        end
    end
end

fprintf('Predicted %d non-zeros, got %d\n', maxnzA, numnzA);

A = sparse(Ai(1:numnzA), Aj(1:numnzA), Ap(1:numnzA));
clear('Ai'); clear('Aj'); clear('Ap');

% Solve
% A = A + A';
prob.equil.sol = mldivide(A,b);
% prob.equil.sol = A\b;
% prob.equil.sol = pinv(full(A))*b;

% prob.equil.A = A;
% prob.equil.b = b;

nb = norm(b);
nA = nan;
nx = norm(prob.equil.sol);
nres = norm(b-A*prob.equil.sol);
cn = nan; % condest(A);
fprintf ('\nRHS, System and solution norms: %e %e %e\nResidual norm: %e (relative is %e).\nEstimated condition number: %e\n\n', ...
    nb, nA, nx, nres, nres/(nb+nA*nx), cn);

prob.equil.nres = nres;
senerg = 0;
incid_s = 1:nS;
for elem = 1:prob.mesh.nc
    senerg = senerg + prob.equil.sol(incid_s)'* ...
        prob.equil.Flex(:, :, elem)*prob.equil.sol(incid_s) - ...
        2*prob.equil.e0(:, elem)'*prob.equil.sol(incid_s) - ...
        prob.equil.s0s0(elem);
    incid_s = incid_s + nS;
end
senerg = -0.5*senerg;

energ = 0;
incid_R = prob.mesh.nc*nS + prob.mesh.ne*2*nV + (1:nV);
for i = 1:prob.prob.ng_edges
    for dir = 1:2
        if (isfinite(prob.prob.incid_support(dir,i)))
            energ = energ - prob.equil.sol(incid_R)'*b(incid_R);
            incid_R = incid_R + nV;
        end
    end
end

fprintf('Compl Str Energy %.14e\nCompl Pot Energy %.14e\nTotal Comp Energy %.14e\n', ...
    senerg, energ, senerg-energ);

prob.equil.global = true;
prob.equil.energ = energ;
prob.equil.senerg = senerg;

end
