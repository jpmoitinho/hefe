% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = PostprocEquil( prob, type, sol, s0, rotate)
% Post process an equilibrium solution
%
% Create a text file with some results
namename = prob.meshname.name;
probname = prob.probname.name;

nV = prob.equil.degree_v + 1;

outname = sprintf('Results/%s-%s-%s.txt', namename, probname, type);
output = fopen(outname,'w');

if ((size(sol, 1) == prob.mesh.nc) && (size(sol, 2) == prob.equil.DS.nS))
    byelem = true;
else
    byelem = false;
end

fprintf(output, 'Stresses\n');
% At the vertices of the elements
for elem = 1:prob.mesh.nc
    [ ~, verts ] = prob.mesh.get_ev_c(elem);
    coords = prob.mesh.coords(prob.mesh.nodes_verts(verts),1:2) - ones(3,1)*prob.geom.center(:,elem)';
    if (byelem)
        Stress = mtimesx(CalcStress2D(coords, prob.equil.DS), sol(elem,:)') + ...
            CalcStress02D(coords, prob.equil.DS, sum(s0(:,:,elem),2));
    else
        Stress = mtimesx(CalcStress2D(coords, prob.equil.DS), sol((elem-1)*prob.equil.DS.nS+(1:prob.equil.DS.nS))) + ...
            CalcStress02D(coords, prob.equil.DS, sum(s0(:,:,elem),2));
    end
    
    fprintf(output, 'Element %d\n', elem);
    for pt = 1:3
        fprintf(output, 'x=%13.6g y=%13.6g', ...
            prob.mesh.coords(prob.mesh.nodes_verts(verts(pt)),1:2));
        fprintf(output, ' %13.6g', Stress(:,1,pt));
        fprintf(output, '\n');
    end
end

% At equally spaced points
nsteps = 20;
xi = (0:nsteps)'/nsteps; onexi = 1 - xi;

for edge = 1:prob.mesh.ne
    [ elems, verts ] = prob.mesh.get_cv_e(edge);
    fprintf(output, 'Edge %d\n', edge);
    for elem = elems
        coords = onexi*prob.mesh.coords(verts(1), 1:2) + xi*prob.mesh.coords(verts(2), 1:2) ...
            - ones(nsteps+1,1)*prob.geom.center(:,elem)';
        if (byelem)
            Stress = mtimesx(CalcStress2D(coords, prob.equil.DS), sol(elem,:)') + ...
                CalcStress02D(coords, prob.equil.DS, sum(s0(:,:,elem),2));
        else
            Stress = mtimesx(CalcStress2D(coords, prob.equil.DS), sol((elem-1)*prob.equil.DS.nS+(1:prob.equil.DS.nS))) + ...
                CalcStress02D(coords, prob.equil.DS, sum(s0(:,:,elem),2));
        end
        dxdy = coords(2,:) - coords(1,:);
        dxdy = dxdy /norm(dxdy);
        Normal = [ dxdy(2) 0 -dxdy(1); 0 -dxdy(1) dxdy(2) ];
        Traction = mtimesx(Normal, Stress(:,1,:));

        fprintf(output, 'Elem= %d\n', elem);
        for pt = 1:(nsteps+1)
            fprintf(output, 'x=%13.6g y=%13.6g\n\t\t\t', coords(pt,:)+prob.geom.center(:,elem)');
            fprintf(output, ' %13.6g', Stress(:,1,pt));
            fprintf(output, '\n\t\t\t');
            fprintf(output, ' %13.6g', Traction(:,pt));
            fprintf(output, '\n');
            
            fprintf(output,'AAA %13.6g %13.6g %13.6g %13.6g\n', coords(pt,:)+prob.geom.center(:,elem)', Traction(:,pt));
        end
        fprintf(output,'BBBAAA\n');
    end
end

if (~byelem)
    npts = 3;
    side_coords = ((1:npts)'-(npts+1)/2)*2/(npts-1);
    Phi_side_plot = [ (1-side_coords)/2, (1+side_coords)/2 ];
    Vi = function_V1D(side_coords, prob.equil.degree_v);
    fprintf(output, '\n\nBoundary displacements\n');
    for edge = 1:prob.mesh.ne
        [ ~, verts ] = prob.mesh.get_cv_e(edge);
        coords = Phi_side_plot*prob.mesh.coords(prob.mesh.nodes_verts(verts),1:2);
        fprintf(output, 'Edge %d\n', edge);
        for pt = 1:npts
            fprintf(output, 'x=%13.6g y=%13.6g ux=%13.6g uy=%13.6g\n', ...
                coords(pt,:), ...
                Vi(pt,:)*sol(prob.mesh.nc*prob.equil.DS.nS+(edge-1)*2*nV + (1:nV)), ...
                Vi(pt,:)*sol(prob.mesh.nc*prob.equil.DS.nS+(edge-1)*2*nV + nV + (1:nV)));
        end
        fprintf(output, '\n');
    end
end

fclose(output);

outname = sprintf('Results/%s-%s-Stress-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/Basis2D-%d.msh', prob.funcs, prob.equil.degree_s);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

weights = zeros(prob.equil.DS.nB, 3, prob.mesh.nc);
for elem = 1:prob.mesh.nc
    [ ~, verts ] = prob.mesh.get_ev_c(elem);
    coords = prob.mesh.coords(prob.mesh.nodes_verts(verts),1:2) - ones(3,1)*prob.geom.center(:,elem)';
    T = prob.equil.DS.handleC(coords(:,1), coords(:,2))';
    for k = 1:3
        if (byelem)
            weights(:, k, elem) = ...
                T*(prob.equil.DS.T{k}*sol(elem,:)' + ...
                prob.equil.DS.T0{k}*sum(s0(:,:,elem),2));
        else
            weights(:, k, elem) = ...
                T*(prob.equil.DS.T{k}*sol((elem-1)*prob.equil.DS.nS+(1:prob.equil.DS.nS)) + ...
                prob.equil.DS.T0{k}*sum(s0(:,:,elem),2));
        end
    end
end

% variable rotate has the rotation angle of x' relative to x (in radians)
if (exist('rotate', 'var'))
    sa = sin(rotate);
    ca = cos(rotate);
    T = [ ca*ca sa*sa 2*ca*sa ; sa*sa ca*ca -2*ca*sa ; -ca*sa ca*sa (ca*ca-sa*sa) ]';
    for elem = 1:prob.mesh.nc
        weights(:, :, elem) = weights(:, :, elem)*T;
    end
end

names = {'Sxx', 'Syy', 'Sxy'};
for i = 1:3
    fprintf(output, '$ElementNodeData\n2\n');
    fprintf(output, '"%s (%s) Stresses - %s %s"\n"Basis %d"\n', names{i}, ...
        type, namename, probname, prob.equil.degree_s);
    fprintf(output, '1\n0.0\n3\n0\n1\n%d\n', prob.mesh.nc);
    for elem = 1:prob.mesh.nc
        fprintf(output, '%d %d', prob.mesh.elems_faces(elem), prob.equil.DS.nB);
        fprintf(output, ' %e', weights(:, i, elem));
        fprintf(output,'\n');
    end
    fprintf(output, '$EndElementNodeData\n');
end

fclose(output);

outname = sprintf('Results/%s-%s-Disps-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');

aux = sprintf('%s/Basis1D-%d.msh', prob.funcs, prob.equil.degree_v);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

fprintf(output, '$Nodes\n');
% We need to define all nodes and edges
% By using offsets we can merge these results with the other mesh, without
% messing up numbers
offset_verts = prob.mesh.nv;
offset_elems = prob.mesh.n_elem;

fprintf(output, '%d\n', prob.mesh.nv);
fprintf(output, '%d %e %e 0\n', ...
    [ offset_verts+(1:prob.mesh.nv); prob.mesh.coords(prob.mesh.nodes_verts(1:prob.mesh.nv), [1 2])']);
fprintf(output, '$EndNodes\n$Elements\n%d\n', prob.mesh.ne);
for i = 1:prob.mesh.ne
    [~, verts] = prob.mesh.get_cv_e(i);
    fprintf(output, '%d 1 0 %d %d\n', offset_elems+i, offset_verts+verts);
end
fprintf(output, '$EndElements\n');

if (~byelem)
    % The solution
    fprintf(output, '$ElementNodeData\n2\n');
    fprintf(output, '"%s Disps - %s %s"\n"Basis %d"\n', type, namename, probname, prob.equil.degree_v);
    fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.ne);
    
    aux = prob.mesh.nc*prob.equil.DS.nS + (1:nV);
    for edge = 1:prob.mesh.ne
        fprintf(output, '%d %d', offset_elems+edge, nV);
        fprintf(output, ' %e', [ sol(aux), sol(aux + nV), zeros(nV,1)]');
        aux = aux + 2*nV;
        fprintf(output, '\n');
    end
    fprintf(output, '$EndElementNodeData\n');
    
    if ( isfield( prob.equil, 'bdisp') )
        % Without the skm's
        fprintf(output, '$ElementNodeData\n2\n');
        fprintf(output, '"%s Filtered Disps - %s %s"\n"Basis %d"\n', type, namename, probname, prob.equil.degree_v);
        fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.ne);
        
        aux = (1:nV);
        for edge = 1:prob.mesh.ne
            fprintf(output, '%d %d', offset_elems+edge, nV);
            fprintf(output, ' %e', [ prob.equil.bdisp(aux), prob.equil.bdisp(aux + nV), zeros(nV,1)]');
            aux = aux + 2*nV;
            fprintf(output, '\n');
        end
        fprintf(output, '$EndElementNodeData\n');
    end

end

% The boundary tractions

fclose(output);
