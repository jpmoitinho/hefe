Point(1) = {0,0,0,1};
Point(2) = {1,0,0,1};
Point(3) = {1,1,0,1};
Point(4) = {0,1,0,1};
Point(5) = {0,0.5,0,1};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,1};
Line Loop(6) = {3,4,5,1,2};
Plane Surface(7) = {6};
Physical Line(21) = {1};
Physical Line(22) = {2};
Physical Line(23) = {3,4};
Physical Line(24) = {5};
Physical Surface(50) = {7};

Field[1] = Attractor;
Field[1].NodesList = {5};

Field[2] = MathEval;
Field[2].F = Sprintf("Min((%g*F1+%g),%g)", f,mins,maxs);

Background Field = 2;

