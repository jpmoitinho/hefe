function [ prob ] = CompatMats( prob )
% EquilMats - Compure the matrices relevant for comaptibility
%

degree_u = prob.compat.degree_u;

% The integration rules for _this_ problem
%
% Define the numerical integration rules
excess3 = 10;
excess2 = 10;
[ NI3D, NI2D ] = def_integration_3D(0*(degree_u*2 + excess3)+15, degree_u*2 + excess2);

L3gF = [ 1-sum(NI2D.xieta); NI2D.xieta ];
L4g  = [ 1-sum(NI3D.xietazeta); NI3D.xietazeta ];

% HOW CAN WE GUESS APPROPRIATE INTEGRATION RULES?

% Build elementary stiffness matrices

eval(sprintf('prob.compat.DU=DefCompatBasis3D_d%d(%d);', degree_u, degree_u));
Bbar = 0; DefBbar;
Ms = 0; eval(sprintf('DefMMatrices_d%d;', degree_u));
eval(sprintf('DefIOmegaCompat_d%d;', degree_u));
nB = prob.compat.DU.nB;
nU = 3*nB;

Phi_face = [ 1-NI2D.xieta(1,:)-NI2D.xieta(2,:); NI2D.xieta(1,:); NI2D.xieta(2,:)]'; % CHECK TRANSPOSE????

% For the geometry
Phi = Phi_T4(NI3D.xietazeta);
DPhi = DPhi_T4();

prob.compat.Ke = zeros(nU, nU, prob.mesh.nc);
prob.compat.Fe = zeros(nU, 4, prob.mesh.nc);
prob.geom.dJac = zeros(1, prob.mesh.nc);

% The displacement approximations at the GPs on the faces
GetCompatBasis3D = str2func(sprintf('GetCompatBasis3D_d%d', degree_u));
prob.compat.BasisGP = zeros(nB, NI2D.npts, 4);
for iface = 1:4
    elem_verts = prob.mesh.cells3(1).fv(iface,:);
    L4gF = zeros(4, NI2D.npts);
    L4gF(elem_verts,:) = L3gF;
    prob.compat.BasisGP(:, :, iface) = ...
        GetCompatBasis3D( L4gF')';
end

% The matrix that projects the boundary displacements onto themselves
prob.compat.UUSide = zeros(nB, nB, 4);
prob.compat.iUUSide = zeros(nB, nB, 4);

for iface = 1:4
    aux = reshape(prob.compat.BasisGP(:, :, iface), [nB 1 NI2D.npts ]);
    prob.compat.UUSide(:, :, iface) = ...
        sum(mtimesx(reshape(NI2D.GW, [ 1 1 NI2D.npts ]), mtimesx(aux,aux,'t')), 3);
    prob.compat.iUUSide(:, :, iface) = pinv(prob.compat.UUSide(:, :, iface));
end



BbDBb   = zeros(3,3,6);
Qs      = zeros(nB,nB,3);
NextI = [2 3 1];

PhiC = GetCompatBasis3D(L4g');
DispGP = zeros(3, nU, NI3D.npts);
DispGP(1, 1:nB, :) = PhiC';
DispGP(2, nB + (1:nB), :) = PhiC';
DispGP(3, 2*nB + (1:nB), :) = PhiC';

prob.compat.global = true;

nB = prob.compat.DU.nB;


for elem = 1:prob.mesh.nc
    [ ~, ~, cell_verts ] = prob.mesh.get_fev_c(elem);
    [ props, bload ] = GetProps(elem, prob.mesh.elems_cells, ...
        prob.mesh.ele_tags, prob.prob.Vols);
    Elast = props(1);
    nu = props(2);
    if (nu == 0.5)
        nu = 0.499;
    end

    aux = 2*(1+nu);
    flex = [ ...
        1 -nu -nu 0 0 0 ;...
        -nu 1 -nu 0 0 0 ;...
        -nu -nu 1 0 0 0 ;...
        0  0  0 aux 0 0 ; ...
        0  0  0 0 aux 0 ; ...
        0  0  0 0 0 aux]/Elast;

    %     sflex = sqrtm(flex);
    stiff = flex\eye(6);

    % Get global coordinates of element.
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), :);
    % Origin at node 1 of the element
    elem_coords0 = elem_coords-elem_coords(1,:);
    coordGP_g = Phi*elem_coords;

    % Jacobian of the element and determinant
    % THIS IS FOR A LINEAR GEOMETRY (constant Jac)
    Jac = DPhi*elem_coords;
    dJac = det(Jac);

    DerivsLs = GetDerivsLsXYZ(elem_coords0(:,1),elem_coords0(:,2),elem_coords0(:,3));

    for i = 1:3
        aux = 0;
        for j = 1:4
            aux = aux + Ms(:,:,j)*DerivsLs(j,i);
        end
        Qs(:,:,i) = aux;
    end

    prob.compat.Qs(:,:,:,elem) = Qs;
    prob.geom.dJac(elem) = dJac;

    % The stiffness matrix
    for i = 1:3
        % Diagonal terms
        BbDBb(:,:,i)     = Bbar(:,:,i)'*stiff*Bbar(:,:,i)/dJac;
        % Off-diagonal
        BbDBb(:,:,i+3)   = Bbar(:,:,i)'*stiff*Bbar(:,:,NextI(i))/dJac;
    end

    Ke = 0;
    for i = 1:3
        % i and j
        aux = kron(BbDBb(:,:,i+3),Qs(:,:,i)'*IOmega*Qs(:,:,NextI(i)));
        % sum i and j twice, i and i only once
        Ke  = Ke + aux + aux' + kron(BbDBb(:,:,i),Qs(:,:,i)'*IOmega*Qs(:,:,i));
    end

    prob.compat.Ke(:,:,elem) = Ke;


    % The "real" body forces (avoid eval, because there is user input)
    force = str2func(sprintf('@(x,y,z,zero,one)(%s)',bload));
    forceGP = reshape(force(coordGP_g(:,1), coordGP_g(:,2), coordGP_g(:,3), ...
        zeros(size(coordGP_g(:,1))), ones(size(coordGP_g(:,1))))', [ 3, 1, NI3D.npts, 1]);
    forceGP_i = mtimesx(forceGP,reshape(Phi, [1 1 NI3D.npts 4]));

    EquivForceGP_i = mtimesx(DispGP, 't', forceGP_i);

    GWdJ = reshape(NI3D.GW*dJac, [ 1 1 NI3D.npts 1 ]);
    prob.compat.Fe(:,:,elem) = reshape(sum(mtimesx(GWdJ, EquivForceGP_i),3), [nU 4]);
end

DispGP = zeros(3, 3*nB, NI2D.npts);
prob.compat.BoundaryConds = zeros(prob.prob.ng_faces, 3*nB, 4);


for i = 1:prob.prob.ng_faces
    g_face = prob.prob.g_faces(i);
    tag = prob.mesh.ele_tags{g_face}(1); % Use the first tag
    which_tag = find(prob.prob.Faces.tags == tag);
    if (~isempty(which_tag))
        face = full(prob.mesh.faces_elems(g_face));
        [ elems_face, ~, verts_face ] = prob.mesh.get_cev_f(face);
        % Project onto the first (with positive orientation) element
        elem = elems_face(1);
        %         [ ~, ~, cell_verts ] = prob.mesh.get_fev_c(elem);

        %        [ ~, ~, verts_elem ] = prob.mesh.get_fev_c(elem);
        sfunc = prob.prob.Faces.vals{end}{which_tag};

        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_face),:);
        coordGP = Phi_face*vert_coords;
        dxdy = cross( (vert_coords(2,:) - vert_coords(1,:)), ...
            (vert_coords(3,:) - vert_coords(1,:)));
        dJac = norm(dxdy);

        funcGP = reshape( atpoint3D(sfunc, coordGP)', [3 1 NI2D.npts]);

        % Which face of the element?
        [ faces_elem, ~, ~ ] = prob.mesh.get_fev_c(elem);
        which_face = find(faces_elem == face);

        DispGP(1, 1:nB, :) = prob.compat.BasisGP(:, :, which_face);
        DispGP(2, nB+(1:nB), :) = prob.compat.BasisGP(:, :, which_face);
        % AQUI!
        DispGP(3, 2*nB+(1:nB), :) = prob.compat.BasisGP(:, :, which_face);
        GWdJSide = reshape(NI2D.GW, [ 1 1 NI2D.npts ])*dJac;

        % The first four member of the base are our PU
        funcGP_i = mtimesx(funcGP, ...
            reshape(squeeze(prob.compat.BasisGP(1:4,:, which_face))', [1 1 NI2D.npts 4])); % CLUMSY!

        prob.compat.BoundaryConds(g_face,:,:) = ...
            sum(mtimesx(GWdJSide, mtimesx(DispGP, 't', funcGP_i)), 3);
    end
end

end
