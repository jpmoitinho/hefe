function [ prob, senerg, energ ] = GlobalEquil( prob )
% Solve global equilibrium model
%
degree = prob.equil.degree_s;

% should check if prob.equil.calc_mats is true. Otherwise do it.

nS = (degree+1)*(degree+2)*(degree+6)/2;
nV = (degree + 1)*(degree + 2)/2;

% The maximum number of non-zero terms in the governing system
maxnzA = prob.mesh.nc*nS^2 + 4*prob.mesh.nc*nS*nV*3*2 + prob.prob.nR*nV*nV*2;
Ai = zeros(maxnzA, 1); Aj = zeros(maxnzA, 1); Ap = zeros(maxnzA, 1); 
numnzA = 0;

% Right hand term
b = zeros(prob.mesh.nc*nS + prob.mesh.nf*3*nV + prob.prob.nR*nV, 1);

eval(sprintf('DefVtV_d%d', degree));

% The position of stress variables for each elem
incid_s = 1:nS;
for elem = 1:prob.mesh.nc
    [ cell_faces, ~, ~ ] = prob.mesh.get_fev_c(elem);
    b(incid_s) = prob.equil.e0(:,elem);
    
    % Setup the flexibility matrix

    news = numnzA + (1:nS);
    for i1 = 1:nS
        Ai(news) = incid_s(i1);
        Aj(news) = incid_s;
        Ap(news) = prob.equil.Flex(:,i1,elem);
        news = news +nS;
    end
    numnzA = numnzA + nS*nS;
    
    for face = 1:4
        incid_v = prob.mesh.nc*nS + (cell_faces(face)-1)*3*nV + (1:(3*nV));

        % The term from the particular solution
        b(incid_v) = b(incid_v) + prob.equil.tface(:, face, elem);

        news = numnzA + (1:nS);
        for i1 = 1:(3*nV)
            Ai(news) = incid_v(i1);
            Aj(news) = incid_s;
            Ap(news) = prob.equil.Dface(i1, :, face, elem);
            news = news + nS;
            Ai(news) = incid_s;
            Aj(news) = incid_v(i1);
            Ap(news) = prob.equil.Dface(i1, :, face, elem);
            news = news + nS;
            numnzA = numnzA + 2*nS;
        end
    end
    incid_s = incid_s + nS;
end

% Add the matrices for the sides with fixed displacements
incid_R = prob.mesh.nc*nS + prob.mesh.nf*3*nV + (1:nV);
for i = 1:prob.prob.ng_faces
    g_face = prob.prob.g_faces(i);
    face = full(prob.mesh.faces_elems(g_face));
    [ ~, ~, verts_face ] = prob.mesh.get_cev_f(face);
    vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_face),:);
    
    e1 = vert_coords(2,:) - vert_coords(1,:);
    e2 = vert_coords(3,:) - vert_coords(1,:);
    normal = cross (e1, e2);
    aface = norm(normal); % VtV already has the 1/2

    for dir = 1:3
        if (isfinite(prob.prob.incid_support(dir,i)))
            incid_v = prob.mesh.nc*nS + (face-1)*3*nV + (dir-1)*nV + (1:nV);
            for i1 = 1:nV
                news = numnzA + (1:nV);
                Ai(news) = incid_v(i1);
                Aj(news) = incid_R;
                Ap(news) = aface*VtV(i1, :);
                news = news + nV;
                Ai(news) = incid_R;
                Aj(news) = incid_v(i1);
                Ap(news) = aface*VtV(i1, :);
                numnzA = numnzA + 2*nV;
            end
            incid_R = incid_R + nV;
        end
    end
    
    if (max(max(max(abs(prob.equil.BoundaryConds(i,:,:,:))))) > 0) % Has something
        tag = prob.mesh.ele_tags{g_face}(1); % Use the first tag
        which_tag = find(prob.prob.Faces.tags == tag,1);
        
        for dir = 1:3
            aux = reshape(sum(prob.equil.BoundaryConds(i,dir,:,:),4), [ nV 1 ]);
            if (norm(aux) ~= 0)
                if (prob.prob.Faces.vals{dir}(which_tag) == 0)
                    incid_r = prob.mesh.nc*nS + prob.mesh.nf*3*nV + ...
                        prob.prob.incid_support(dir,i)* nV + (1:nV);
                    b(incid_r) = b(incid_r) + aux;
                else % Prob.Edges.vals{dir}(which_tag) has to be 1
                    incid_v = prob.mesh.nc*nS + (face-1)*3*nV + (dir-1)*nV + (1:nV);
                    b(incid_v) = b(incid_v) + aux;
                end
            end
        end
    end
end

A = sparse(Ai(1:numnzA), Aj(1:numnzA), Ap(1:numnzA));
clear('Ai'); clear('Aj'); clear('Ap');


% Solve
% tic; prob.equil.sol = mldivide(A,b); toc;
% prob.equil.sol = A\b;
% prob.equil.sol = pinv(full(A))*b;

% Scaling
% s = sparse(1./sqrt(max(abs(diag(A)), max(abs(A))')));
sd = sqrt(abs(diag(A)));
sm = sqrt(max(abs(A)))';
s = sparse(1./max(sd,sm));
% sz = sd==0;
% s = 1./sd;
% s(sz) = 1./sm(sz);
% s = 1./sqrt(dot(A,A));
As = diag(s)*A*diag(s);
bs = diag(s)*b;


tic;
% if (prob.equil.degree_s <= 0)
%     opts = spqr_rank_opts;
%     opts.tol = 1e-14;
%     opts.ordering = 'best'; % 'default' 'amd' 'colamd' 'metis' 'best' 'fixed' 'natural';
%     opts.get_details = 1;
%     opts.repeatable = 1;
%     opts.spumoni = 3;
%     opts.solution = 'min2norm';
%     % opts.ssi_nblock_increment = 1; % : 5 : block size inc. if convergence not met.
%     % opts.ssi_convergence_factor = 1e12; % : 0.1 : spqr_ssi termination criterion.
%     % opts.k = 4; %:   ssp_min_iters : 4 : min # of iterations before checking convergence
%     % opts.ssp_max_iters = 50; % : 10 : max # of ssp iterations before stopping iterations
%     % opts.ssp_convergence_factor = 1e12; % : 0.1 ssp terminates when relative error drops below this value.
%     [ ss, prob.equil.stats] = spqr_basic(As, bs, opts);
% else
id = 'MATLAB:rankDeficientMatrix';
warning('off',id);
% ss = lsqminnorm(As, bs);
[ ss, ~ ] = spqr_solve(As,bs); 
% ss = As\bs; 
% ss = pinv(full(As))*bs; 
prob.equil.stats.rank = nan;
warning('on',id);
% end
toc;
prob.equil.sol = diag(s)*ss;

nres = norm(b-A*prob.equil.sol);
fprintf ('\nResidual norm mldivide (%d): %e\n', prob.equil.stats.rank, nres);

% opts = spqr_rank_opts;
% opts.tol = 1e-14;
% % opts.tol = xxx;
% opts.get_details = 1;
% tic; [ sol, stats] = spqr_basic(A, b, opts); toc;
% nres = norm(b-A*sol);
% fprintf ('\nResidual norm spqr: %e (%d)\n\n\n', nres, size(A,1)-stats.rank);

% fprintf ('Difference: %e\n', norm(sol(1:prob.mesh.nc*nS)-prob.equil.sol(1:prob.mesh.nc*nS)));

% nb = norm(b);
% nA = normest(A);
% nx = norm(prob.equil.sol);
% nres = norm(b-A*prob.equil.sol);
% cn = condest(A);
% fprintf ('\nRHS, System and solution norms: %e %e %e\nResidual norm: %e (relative is %e).\nEstimated condition number: %e\n\n', ...
%     nb, nA, nx, nres, nres/(nb+nA*nx), cn);

prob.equil.nres = nres;
senerg = 0;
incid_s = 1:nS;
for elem = 1:prob.mesh.nc
    senerg = senerg + prob.equil.sol(incid_s)'* ...
        prob.equil.Flex(:,:,elem)*prob.equil.sol(incid_s) - ...
        2*prob.equil.e0(:,elem)'*prob.equil.sol(incid_s) - ...
        prob.equil.s0s0(elem);
    incid_s = incid_s + nS;
end
senerg = -0.5*senerg;

energ = 0;
incid_R = prob.mesh.nc*nS + prob.mesh.nf*3*nV + (1:nV);
for i = 1:prob.prob.ng_faces
    for dir = 1:3
        if (isfinite(prob.prob.incid_support(dir,i)))
            energ = energ - prob.equil.sol(incid_R)'*b(incid_R);
            incid_R = incid_R + nV;
        end
    end
end

fprintf('Compl Str Energy %.14e\nCompl Pot Energy %.14e\nTotal Comp Energy %.14e\n', ...
    senerg, energ, senerg-energ);

prob.equil.global = true;
prob.equil.energ = energ;
prob.equil.senerg = senerg;

end
