
funcs = '../funcs3D-PK-Rotated';
addpath('../base');
addpath(funcs);

prob = DefineSolidProblem(meshname, probname, funcs);

if (exist('degree', 'var'))
    prob.equil.degree_s = degree;
    prob.equil.degree_v = degree;
    prob.compat.degree_u = degree;
else
    prob.equil.degree_s = degree_s;
    prob.equil.degree_v = degree_v;
    prob.compat.degree_u = degree_u;
end

fprintf ('%s %s %d %d\n', prob.meshname.name, prob.probname.name, ...
    prob.equil.degree_s, prob.compat.degree_u);

prob = EquilMats(prob);
prob = GlobalEquil(prob);
PostprocEquil(prob, 'EG', prob.equil.sol, prob.equil.s0);

prob = CompatMats(prob);
prob = GlobalCompat(prob);
prob.compat.stress_params = CompatStressParams(prob,  prob.compat.sol);
PostprocCompat(prob, 'CG',prob.compat.sol);

EnError = GetError(prob, prob.equil.sol, prob.equil.s0, prob.compat.sol);

fprintf(['\n   Compl Str Energy = %.8f\n','         Str Energy = %.8f\n', ...
    'Energy of the Error = %.8f\n','            Balance = %.8f\n\n'], ...
    prob.compat.senerg,prob.equil.senerg,EnError,prob.compat.senerg-prob.equil.senerg+EnError);