function [ params ] = CompatStressParams( prob, sol)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% DPhi = DPhi_T4();
nB = prob.compat.DU.nB;

params = zeros(6, nB, prob.mesh.nc);

Bbar = 0; DefBbar;

for elem = 1:prob.mesh.nc
    %     [ ~, ~, cell_verts ] = prob.mesh.get_fev_c(elem);
    [ props, ~ ] = GetProps(elem, prob.mesh.elems_cells, ...
        prob.mesh.ele_tags, prob.prob.Vols);
    Elast = props(1);
    nu = props(2);
    if (nu == 0.5)
        nu = 0.499;
    end

    aux = 2*(1+nu);
    flex = [ ...
        1 -nu -nu 0 0 0 ;...
        -nu 1 -nu 0 0 0 ;...
        -nu -nu 1 0 0 0 ;...
        0  0  0 aux 0 0 ; ...
        0  0  0 0 aux 0 ; ...
        0  0  0 0 0 aux]/Elast;
    stiff = flex\eye(6);

    desls = sol(prob.compat.incid(:,elem));

    B = 0; 
    desls = desls/prob.geom.dJac(elem);
    
    for i = 1:3
        B = B + kron(Bbar(:,:,i),prob.compat.Qs(:,:,i,elem)); 
    end   
    
    strains = B*desls;
    strains = reshape(strains,[nB 6])';    
    
    params(:,:,elem) = stiff*strains;

end

prob.compat.stress_params = params;

end

