% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = EquilMats( prob )
%
% EquilMats - Compute the matrices relevant for equilibrium formulations
%   

% % The parameters of the particular solution
% prob.equil.s0 = zeros(nS0, 1, prob.mesh.nc);
% % The integrals of the parycular solutions with themselves
% prob.equil.s0s0 = zeros(1, prob.mesh.nc);

degree_s = prob.equil.degree_s;

nB = (degree_s+1)*(degree_s+2)*(degree_s+3)/6;
nS = (degree_s+1)*(degree_s+2)*(degree_s+6)/2;
nS0 = (degree_s)*(degree_s+1)*(degree_s+2)/2;
nV = (degree_s+1)*(degree_s+2)/2;

% The parameters of the particular solution
prob.equil.s0 = zeros(nS0, 1, prob.mesh.nc);
% The integrals of the particular solutions with themselves
prob.equil.s0s0 = zeros(1, prob.mesh.nc);

% The elemental matrices
prob.equil.Flex = zeros(nS, nS, prob.mesh.nc);
% prob.equil.e0 = zeros(nS, prob.mesh.nc);
prob.equil.Dface = zeros(3*nV, nS, 4, prob.mesh.nc);
prob.equil.tface = zeros(3*nV, 4, prob.mesh.nc);
prob.equil.e0 = zeros(nS, prob.mesh.nc);
prob.equil.R = zeros(3, 3, prob.mesh.nc);
prob.equil.RJac = zeros(3, 3, prob.mesh.nc);
prob.geom.dJac = zeros(1, prob.mesh.nc);
% prob.geom.ledges = zeros(1, prob.mesh.ne);

% Integration rules
excess = 1;
[ NI3D, NI2D ] = def_integration_3D(degree_s*2 + excess, degree_s*2 + excess);

% Boundary displacements approximation definitions
nV = (degree_s + 1)*(degree_s + 2)/2;
ViG = eval(sprintf("GetV_d%d(NI2D.xieta)'", degree_s));
VG = zeros(3, 3*nV, NI2D.npts);
VG(1, 1:nV, :) = ViG;
VG(2, nV+(1:nV), :) = ViG;
VG(3, 2*nV+(1:nV), :) = ViG;

% The shape functions of the element (T4) at the Gauss points
Phi = Phi_T4(NI3D.xietazeta);
DPhi = DPhi_T4();

% The shape functions of the linear face at the Gauss points
Phi_face = Phi_T3(NI2D.xieta);

GWFace = reshape(NI2D.GW, [ 1 1 NI2D.npts ]);

% Define the terms that are common 

stressfuncs = eval(sprintf('@DefStressPK_d%d', degree_s));
stress0funcs = eval(sprintf('@DefStress0PK_d%d', degree_s));

basisBf = eval(sprintf('DefBasisStress_d%d', degree_s-1));
phiBfGP = basisBf(NI3D.xietazeta(1,:)', NI3D.xietazeta(2,:)', NI3D.xietazeta(3,:)');

eval(sprintf('DefIncidStress_d%d', degree_s));
% eval(sprintf('DefIncidV_d%d', degree));

eval(sprintf('DefIOmega_d%d', degree_s));
eval(sprintf('DefInvIOmega_d%d', degree_s-1));

InvIOmegaBf_phiBfGP_GW = (InvIOmega*phiBfGP').*NI3D.GW; % Compute once

eval(sprintf('DefVtV_d%d', degree_s));

% eval(sprintf('ColsIGamma_d%d',degree));

% The projection of the (9) PK stresses on each face of the element
DefNormFaces;

for elem = 1:prob.mesh.nc
    [ cell_faces, ~, cell_verts ] = prob.mesh.get_fev_c(elem);
    [ props, bload ] = GetProps(elem, prob.mesh.elems_cells, ...
        prob.mesh.ele_tags, prob.prob.Vols);
    Elast = props(1);
    nu = props(2);
    
    aux = 2*(1+nu);
    flex = [ ...
        1 -nu -nu 0 0 0 ;...
        -nu 1 -nu 0 0 0 ;...
        -nu -nu 1 0 0 0 ;...
        0  0  0 aux 0 0 ; ...
        0  0  0 0 aux 0 ; ...
        0  0  0 0 0 aux]/Elast;

    % Get global coordinates of element. Use a frame at its center
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), :);

    % Determinant of the Jacobian of the element
    Jac = (DPhi*elem_coords)';
    dJac = det(Jac);
    prob.geom.dJac(elem) = dJac;
    %     GWdJ = reshape(NI3D.GW*dJac, [ 1 1 NI3D.npts ]);
    
    % Rotation matrix
    xp = Jac(:,1); xp = xp/norm(xp);
    zp = cross(Jac(:,1), Jac(:,2)); zp = zp/norm(zp);
    yp = cross(zp,xp);
    R = [ xp, yp, zp ]';
    
    RJac = R*Jac;
    prob.equil.R(:,:,elem) = R;
    prob.equil.RJac(:,:,elem) = RJac;

    % This transforms into the Cartesian stresses in the rotated frame
    % The "true" Jacobian should produce the same result
    TP = GetTP(RJac);
    
    [ TS, TD ] = stressfuncs(RJac);

    TPTS = TP*TS;

    PreFlex = kron(TPTS'*(flex/dJac)*TPTS,IOmega);
    ElemFlex = TD'*PreFlex*TD;

    prob.equil.Flex(:,:,elem) = -ElemFlex;

    if (strcmp(bload,'[zero,zero,zero]'))
        has_s0 = false;
    else
        [ T0, L0 ] = stress0funcs(RJac);
        % Transform the Gauss points into coordinates
        coordGP = Phi*elem_coords;
        % Find the weigths of the particular solution
        % Rotate the forces to x', y', z'
        bfgp = atpoint3D( bload, coordGP)*R';
        bfpars = InvIOmegaBf_phiBfGP_GW*bfgp; % No dJac here, affects both terms
        P0 = -dJac*(T0*bfpars(:));
        prob.equil.s0(:, 1, elem) = P0;

        % The equivalent strains due to the particular solution
        prob.equil.e0(:,elem) = TD'*PreFlex(:,L0)*P0;
        prob.equil.s0s0(elem) = P0'*PreFlex(L0,L0)*P0;
        has_s0 = true;
    end
    
    % Loop on the faces of the element
    for face = 1:4
        [ cells_face, ~, verts_face ] = prob.mesh.get_cev_f(cell_faces(face));
        if ( cells_face(1) == elem )
            pm = 1;
        else
            pm = 2;
        end
        
        % The number of the vertices of the face for the element
        verts_face_elem = cell_verts(prob.mesh.cells3(1).fv(face,:));
        
        %         verts_face_elem = circshift(verts_face_elem,1);

        IGamma = zeros(nV, nB);
        
        %         iii = find(verts_face(1) == verts_face_elem);
        iii = verts_face(1) == verts_face_elem;
      
        IGamma(:,IncidStress(:,face,pm,iii)) = VtV; %#ok<USENS> % c)

        % CHECK!!!!!!!!!!!
        DTilde = kron(R'*NormFaces(:,:,face)*TS,IGamma); %#ok<USENS>
        prob.equil.Dface(:, :, face, elem)  = DTilde*TD;
        % The term from the particular solution
        if (has_s0)
            prob.equil.tface(:, face, elem) = -DTilde(:,L0)*P0;
        end
    end
end

prob.equil.BoundaryConds = zeros(prob.prob.ng_faces, 3, nV, 3);

% Old
for i = 1:prob.prob.ng_faces
    g_face = prob.prob.g_faces(i);
    face = full(prob.mesh.faces_elems(g_face));
    tag = prob.mesh.ele_tags{g_face}(1); % Use the first tag
    which_tag = find(prob.prob.Faces.tags == tag);
    
    % If vals{end} is not zero we will need to impose
    % the corresponding BC. We integrate it now
    % This code is agnostic regarding the type of BC
    if (size(which_tag,1) && ~strcmp(prob.prob.Faces.vals{end}{which_tag}, '[zero,zero,zero]'))
        sload = prob.prob.Faces.vals{end}{which_tag};
        [ ~, ~, verts_face ] = prob.mesh.get_cev_f(face);
        % these coordinates are in the global frame (used for the bc's)
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_face),:);
        coordGP = Phi_face*vert_coords;
        e1 = vert_coords(2,:) - vert_coords(1,:);
        e2 = vert_coords(3,:) - vert_coords(1,:);
        normal = cross (e1, e2);
        dJFace = norm(normal);
        
        aux = mtimesx(reshape( atpoint3D(sload, coordGP)', [3 1 NI2D.npts]), ...
            reshape(Phi_face', [1 3 NI2D.npts]));
        vtface = mtimesx(VG, 't', aux);
        % Multiplied by the length (there is no normal!!!)
        aux = sum(mtimesx(vtface, GWFace),3)*dJFace;
        prob.equil.BoundaryConds(g_face, 1, :, :) = aux(1:nV, :);
        prob.equil.BoundaryConds(g_face, 2, :, :) = aux(nV+(1:nV), :);
        prob.equil.BoundaryConds(g_face, 3, :, :) = aux(2*nV+(1:nV), :);
    end
end

prob.equil.calc_mats = true;
