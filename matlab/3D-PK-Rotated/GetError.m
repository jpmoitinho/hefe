function [EnError] = GetError(prob, sol_s, sol_s0, sol_c)
% Compute the bound of the error
%   Detailed explanation goes here

degree_u = prob.compat.degree_u;
degree_s = prob.equil.degree_s;

nS = (degree_s+1)*(degree_s+2)*(degree_s+6)/2;

DPhi = DPhi_T4();

stressfuncs = eval(sprintf('@DefStressPK_d%d', degree_s));
stress0funcs = eval(sprintf("@DefStress0PK_d%d", degree_s));

Bbar = 0; DefBbar;
IOmegaS = 0; eval(sprintf('DefIOmegaS_d%d_d%d',degree_s,degree_u));

EnError = 0;
incid_s = 1:nS;
for elem = 1:prob.mesh.nc   
    pars_s = sol_s(incid_s);
    pars_s0 = sol_s0(:,1,elem);
    pars_c = sol_c(prob.compat.incid(:,elem));
    
    R = prob.equil.R(:,:,elem);
    RJac = prob.equil.RJac(:,:,elem);
    TP = GetTP(RJac);

    % These expressions are derived in RotateTensor.nb
    TR = [R(1,1).^2,R(2,1).^2,R(3,1).^2,2.*R(2,1).*R(3,1),2.*R(1,1).*R(3,1),2.*R(1,1).*R(2,1);R(1,2).^2,R(2,2).^2,R(3,2).^2,2.*R( ...
        2,2).*R(3,2),2.*R(1,2).*R(3,2),2.*R(1,2).*R(2,2);R(1,3).^2,R(2,3).^2,R(3,3).^2,2.*R(2,3).*R(3,3),2.*R(1,3).*R(3,3),2.*R( ...
        1,3).*R(2,3);R(1,2).*R(1,3),R(2,2).*R(2,3),R(3,2).*R(3,3),R(2,3).*R(3,2)+R(2,2).*R(3,3),R(1,3).*R(3,2)+R(1,2).*R(3,3),R( ...
        1,3).*R(2,2)+R(1,2).*R(2,3);R(1,1).*R(1,3),R(2,1).*R(2,3),R(3,1).*R(3,3),R(2,3).*R(3,1)+R(2,1).*R(3,3),R(1,3).*R(3,1)+R( ...
        1,1).*R(3,3),R(1,3).*R(2,1)+R(1,1).*R(2,3);R(1,1).*R(1,2),R(2,1).*R(2,2),R(3,1).*R(3,2),R(2,2).*R(3,1)+R(2,1).*R(3,2),R( ...
        1,2).*R(3,1)+R(1,1).*R(3,2),R(1,2).*R(2,1)+R(1,1).*R(2,2)];
    
    [ TS, TD ] = stressfuncs(RJac);
    [ ~, L0 ] = stress0funcs(RJac);
    TRTPTS = TR*TP*TS;

    aux = 0;
    for i = 1:3
        aux = aux + kron((TRTPTS'*Bbar(:,:,i)),(IOmegaS*prob.compat.Qs(:,:,i,elem)));
    end

    % Divide by the determinant because the Q's do not include it (see GetDerivsLsXYZ)
    % T0 is selecting the rows of aux, better do it by indexing
    EnError = EnError ...
        - 0.5*pars_s'*prob.equil.Flex(:,:,elem)*pars_s ...
        + pars_s'*prob.equil.e0(:,elem) ...
        + 0.5*prob.equil.s0s0(elem) ...
        + 0.5*pars_c'*prob.compat.Ke(:,:,elem)*pars_c ...
        - pars_s'*TD'*aux*pars_c/prob.geom.dJac(elem) ...
        - prob.equil.s0(:,:,elem)'*aux(L0,:)*pars_c/prob.geom.dJac(elem);
    incid_s = incid_s + nS;
end

if (norm(sol_s0(:)) ~= 0)
    fprintf('\nThe non-zero particular solution is not being considered....\n\n');
end


