function [ prob ] = PostprocEquil( prob, type, sol, s0) %, rotate)
% Post process an equilibrium solution
%
% Create a text file with some results
namename = prob.meshname.name;
probname = prob.probname.name;

degree_s = prob.equil.degree_s;
nB = (degree_s+1)*(degree_s+2)*(degree_s+3)/6;
nS = (degree_s+1)*(degree_s+2)*(degree_s+6)/2;
% nS0 = (degree)*(degree+1)*(degree+2)/2;

nV = (degree_s+1)*(degree_s+2)/2;

stressfuncs = eval(sprintf("@DefStressPK_d%d", degree_s));
stress0funcs = eval(sprintf("@DefStress0PK_d%d", degree_s));

DPhi = DPhi_T4();

outname = sprintf('Results/%s-%s-Stress-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');
aux = sprintf('%s/ISchemeEq_3D_d%d.msh', prob.funcs, prob.equil.degree_s);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

weights = zeros(nB, 6, prob.mesh.nc);

incid_s = 1:nS;

for elem = 1:prob.mesh.nc
    R = prob.equil.R(:,:,elem);
    RJac = prob.equil.RJac(:,:,elem);
    TP = GetTP(RJac);

    % These expressions are derived in RotateTensor.nb
    TR = [R(1,1).^2,R(2,1).^2,R(3,1).^2,2.*R(2,1).*R(3,1),2.*R(1,1).*R(3,1),2.*R(1,1).*R(2,1);R(1,2).^2,R(2,2).^2,R(3,2).^2,2.*R( ...
        2,2).*R(3,2),2.*R(1,2).*R(3,2),2.*R(1,2).*R(2,2);R(1,3).^2,R(2,3).^2,R(3,3).^2,2.*R(2,3).*R(3,3),2.*R(1,3).*R(3,3),2.*R( ...
        1,3).*R(2,3);R(1,2).*R(1,3),R(2,2).*R(2,3),R(3,2).*R(3,3),R(2,3).*R(3,2)+R(2,2).*R(3,3),R(1,3).*R(3,2)+R(1,2).*R(3,3),R( ...
        1,3).*R(2,2)+R(1,2).*R(2,3);R(1,1).*R(1,3),R(2,1).*R(2,3),R(3,1).*R(3,3),R(2,3).*R(3,1)+R(2,1).*R(3,3),R(1,3).*R(3,1)+R( ...
        1,1).*R(3,3),R(1,3).*R(2,1)+R(1,1).*R(2,3);R(1,1).*R(1,2),R(2,1).*R(2,2),R(3,1).*R(3,2),R(2,2).*R(3,1)+R(2,1).*R(3,2),R( ...
        1,2).*R(3,1)+R(1,1).*R(3,2),R(1,2).*R(2,1)+R(1,1).*R(2,2)];
  
    [ TS, TD ] = stressfuncs(RJac);
    [ ~, L0 ] = stress0funcs(RJac);
    
    TPTSfull = kron(TR*TP*TS, speye(nB));
    dJac = prob.geom.dJac(elem);
    aux = (TPTSfull*TD*sol(incid_s) + TPTSfull(:,L0)*s0(:,1,elem))/dJac;
    weights(:, :, elem) = reshape( aux, [ nB 6 ]);
    incid_s = incid_s + nS;
    
end

% % variable rotate has the rotation angle of x' relative to x (in radians)
% if (exist('rotate', 'var'))
%     sa = sin(rotate);
%     ca = cos(rotate);
%     T = [ ca*ca sa*sa 2*ca*sa ; sa*sa ca*ca -2*ca*sa ; -ca*sa ca*sa (ca*ca-sa*sa) ]';
%     for elem = 1:prob.mesh.nc
%         weights(elem,:,:) = mtimesx ( reshape(weights(elem,:,:), [prob.equil.DS.nB 3]), T);
%     end
% end

names = {'Sxx', 'Syy', 'Szz', 'Syz', 'Sxz', 'Sxy'};
for i = 1:6
    fprintf(output, '$ElementNodeData\n2\n');
    fprintf(output, '"%s (%s) Stresses - %s %s"\n"Basis3DE %d"\n', names{i}, ...
        type, namename, probname, prob.equil.degree_s);
    fprintf(output, '1\n0.0\n3\n0\n1\n%d\n', prob.mesh.nc);
    for elem = 1:prob.mesh.nc
        fprintf(output, '%d %d', full(prob.mesh.elems_cells(elem)), nB);
        fprintf(output, ' %e', weights(:,i,elem));
        fprintf(output,'\n');
    end
    fprintf(output, '$EndElementNodeData\n');
end

fclose(output);

outname = sprintf('Results/%s-%s-Disps-%s.msh', namename, probname, type);
output = fopen(outname,'w');

fprintf(output, '$MeshFormat\n2.2 0 8\n$EndMeshFormat\n');

aux = sprintf('%s/ISchemeEq_2D_d%d.msh', prob.funcs, prob.equil.degree_v);
scheme = fileread(aux);
fprintf(output, '%s', scheme);

fprintf(output, '$Nodes\n');
% We need to define all nodes and edges
% By using offsets we can merge these results with the other mesh, without
% messing up numbers
offset_verts = prob.mesh.nv;
offset_elems = prob.mesh.n_elem;

% We need to define all nodes and faces
fprintf(output, '%d\n', prob.mesh.nv);
fprintf(output, '%d %e %e %e\n', ...
    [ offset_verts+(1:prob.mesh.nv); prob.mesh.coords(prob.mesh.nodes_verts(1:prob.mesh.nv), :)']);
fprintf(output, '$EndNodes\n$Elements\n%d\n', prob.mesh.nf);
for i = 1:prob.mesh.nf
    [~, ~, verts] = prob.mesh.get_cev_f(i);
    fprintf(output, '%d 2 0 %d %d %d\n', offset_elems+i, offset_verts+verts);
end
fprintf(output, '$EndElements\n');

% The solution
fprintf(output, '$ElementNodeData\n2\n');
fprintf(output, '"%s Disps - %s %s"\n"Basis2DE %d"\n', type, namename, probname, prob.equil.degree_v);
fprintf(output, '1\n0.0\n3\n0\n3\n%d\n', prob.mesh.nf);

aux = prob.mesh.nc*nS + (1:nV);
for face = 1:prob.mesh.nf
    fprintf(output, '%d %d', offset_elems+face, nV);
    fprintf(output, ' %e', [ sol(aux), sol(aux + nV), sol(aux + 2*nV)]');
    aux = aux + 3*nV;
    fprintf(output, '\n');
end
fprintf(output, '$EndElementNodeData\n');

fclose(output);

