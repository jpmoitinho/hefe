function [ prob, senerg, energ ] = GlobalCompat( prob )
% Solve global compatible model
%

% should check if prob.compat.calc_mats is true. Otherwise do it.

prob.compat.global = true;

nB = prob.compat.DU.nB;
nEdge = prob.compat.DU.nEdge;
nFace = prob.compat.DU.nFace;
nVol = prob.compat.DU.nVol;

nU = nB*3;

DimOneDir = prob.mesh.nv + prob.mesh.ne*nEdge + ...
    prob.mesh.nf*nFace + prob.mesh.nc*nVol;
dim = 3*DimOneDir;

maxnzA = prob.mesh.nc*(nU)^2;
Ai = zeros(maxnzA, 1); Aj = zeros(maxnzA, 1); Ap = zeros(maxnzA, 1); numnzA = 0;
Fg = zeros(dim,1);

prob.compat.incid = zeros(nU, prob.mesh.nc);
% 6 signs for the edges, 4 for the faces
prob.compat.elem_signs = ones(10, prob.mesh.nc);

for elem = 1:prob.mesh.nc
    [ elem_faces, elem_edges, elem_verts ] = prob.mesh.get_fev_c(elem);
    
    % Vertices
    prob.compat.incid(1:4,elem) = elem_verts;
    
    % Edges
    if (prob.compat.degree_u > 1)
        offset = prob.mesh.nv;
        pos = 5;
        which = 1:nEdge;
        for iedge = 1:6
            verts = prob.compat.DU.EdgeVerts(iedge,:);
            [ ~, ~, GlobalEdgeVerts ] = prob.mesh.get_cfv_e(elem_edges(iedge));
            LocalEdgeVerts = elem_verts(verts);
            
            if (GlobalEdgeVerts(1) == LocalEdgeVerts(2))
                verts = circshift(verts, [0 1]);
                prob.compat.elem_signs(iedge, elem) = -1;
            end
            % Just check for some peace of mind
            if (~all(GlobalEdgeVerts==elem_verts(verts)))
                fprintf ('Oops\n');
            end
            
            ThisEdgeDegrees = zeros(4, nEdge);
            ThisEdgeDegrees(verts,:) = prob.compat.DU.OneEdgeDegrees;
            offset_e = offset + (elem_edges(iedge)-1)*nEdge;
            for var = 1:nEdge
                where = find(ismember(prob.compat.DU.EdgeDegrees(:,which)',ThisEdgeDegrees(:,var)','rows'));
                prob.compat.incid(pos, elem) = offset_e + where;
                pos = pos + 1;
            end
            which = which + nEdge;
        end
    end
    
    % Faces
    if (prob.compat.degree_u > 2)
        offset = offset + prob.mesh.ne*nEdge;
        pos = 4 + 6*nEdge + 1;
        which = 1:nFace;
        for iface = 1:4
            verts = prob.compat.DU.FaceVerts(iface,:);
            [ ~, ~, GlobalFaceVerts ] = prob.mesh.get_cev_f(elem_faces(iface));
            LocalFaceVerts = elem_verts(verts);
            
            % Adjust initial vertex
            shift = find(LocalFaceVerts == GlobalFaceVerts(1)) - 1;
            verts = circshift(verts, [0 -shift]);
            prob.compat.elem_signs(6+iface,elem) = shift;
            
            % Adjust orientation
            if (GlobalFaceVerts(2) ~= elem_verts(verts(2)))
                verts([2 3]) = verts([3 2]);
                prob.compat.elem_signs(6+iface, elem) = -shift;
            end
            
            % Just check for some peace of mind
            if (~all(GlobalFaceVerts==elem_verts(verts)))
                fprintf ('Oops\n');
            end
            
            ThisFaceDegrees = zeros(4, nFace);
            ThisFaceDegrees(verts,:) = prob.compat.DU.OneFaceDegrees;
            offset_f = offset + (elem_faces(iface)-1)*nFace;
            for var = 1:nFace
                where = find(ismember(prob.compat.DU.FaceDegrees(:,which)', ...
                    ThisFaceDegrees(:,var)','rows'));
                prob.compat.incid(pos, elem) = offset_f + where;
                pos = pos + 1;
            end
            which = which + nFace;
        end
    end
    
    % Elements
    if (prob.compat.degree_u > 3)
        offset = offset + prob.mesh.nf*nFace;
        idegs = 1:nVol;
        where = 4 + 6*nEdge + 4*nFace + idegs;
        prob.compat.incid(where, elem) = offset + ...
            (elem-1)*nVol + idegs;
    end
end

prob.compat.incid(nB+(1:nB),:) = DimOneDir + prob.compat.incid(1:nB,:);
prob.compat.incid(2*nB+(1:nB),:) = 2*DimOneDir + prob.compat.incid(1:nB,:);

for elem = 1:prob.mesh.nc
    inc = prob.compat.incid(:,elem);
    Ke = prob.compat.Ke(:,:,elem);
    for i = 1:nU
        news = numnzA + (1:nU);
        Ai(news) = inc(i);
        Aj(news) = inc;
        Ap(news) = Ke(:,i);
        numnzA = numnzA + nU;
    end
    % Undo the PU
    Fg(inc) = Fg(inc) + sum(prob.compat.Fe(:,:,elem),2);
end

% The kinematic bc's
nsupport = sum(sum(isfinite(prob.prob.incid_support(:,prob.prob.g_faces))));

aux = any(isfinite(prob.prob.incid_support(:,prob.prob.g_faces)));
FixedGFaces = prob.prob.g_faces(aux);
FixedFaces = prob.mesh.faces_elems(FixedGFaces);
FixedVerts = [];
FixedEdges = [];

for face = FixedFaces
    [ ~, aux1, aux2 ] = prob.mesh.get_cev_f(face);
    FixedEdges = unique([ FixedEdges, aux1 ]);
    FixedVerts = unique([ FixedVerts, aux2 ]);
end

nFixedFaces = size(FixedFaces,2);
nFixedEdges = size(FixedEdges,2);
nFixedVerts = size(FixedVerts,2);

incFixedVerts = nan(3, nFixedVerts);
incFixedEdges = nan(3, nFixedEdges);
incFixedFaces = nan(3, nFixedFaces);

for iface = 1:nFixedFaces
    gFace = FixedGFaces(iface);
    face = FixedFaces(iface);
    [ ~, face_edges, face_verts ] = prob.mesh.get_cev_f(face);
    [ ~, pos_verts ] = ismember(face_verts, FixedVerts);
    [ ~, pos_edges ] = ismember(face_edges, FixedEdges);
    for dir = 1:3
        if (isfinite(prob.prob.incid_support(dir,gFace)))
            incFixedVerts(dir, pos_verts) = 0;
            incFixedEdges(dir, pos_edges) = 0;
            incFixedFaces(dir, iface) = 0;
        end
    end
end

% Adjust if it is not diagonal,
% Could count the real number of fixed directions
maxnzA = numnzA + 3*(nFixedVerts + nFixedEdges*nEdge + nFixedFaces*nFace);
Ai(maxnzA) = 0;
Aj(maxnzA) = 0;
Ap(maxnzA) = 0;

% The incidences are offsets. They can be directly summed with (1:nEdge) or
% (1:nFace)

incid_R = dim;
for v = 1:nFixedVerts
    for dir = 1:3
        if (isfinite(incFixedVerts(dir,v)))
            incFixedVerts(dir,v) = incid_R;
            incid_R = incid_R + 1;
        end
    end
end

for e = 1:nFixedEdges
    for dir = 1:3
        if (isfinite(incFixedEdges(dir,e)))
            incFixedEdges(dir,e) = incid_R;
            incid_R = incid_R + nEdge;
        end
    end
end

for f = 1:nFixedFaces
    for dir = 1:3
        if (isfinite(incFixedFaces(dir,f)))
            incFixedFaces(dir,f) = incid_R;
            incid_R = incid_R + nFace;
        end
    end
end

prob.compat.FixedVerts = FixedVerts;
prob.compat.incFixedVerts = incFixedVerts;

prob.compat.FixedEdges = FixedEdges;
prob.compat.incFixedEdges = incFixedEdges;

prob.compat.FixedFaces = FixedFaces;
prob.compat.incFixedFaces = incFixedFaces;

% Increase RHS
if (nsupport > 0)
    Fg(incid_R) = 0;
end

for g_face = prob.prob.g_faces
    face = full(prob.mesh.faces_elems(g_face));
    pos_gface = prob.prob.g_faces == g_face;
    pos_fface = prob.compat.FixedFaces == face;
    [ face_cells, face_edges, face_verts ] = prob.mesh.get_cev_f(face);
    [ cell_faces, cell_edges, ~ ] = prob.mesh.get_fev_c(face_cells(1));
    which = find(cell_faces == face);
    
    % The displacements of face "which" in the numbering of the element
    d_verts = prob.mesh.cells3(1).fv(which,:);
    [ liedges, iedges ] = ismember(face_edges, cell_edges, 'R2012a');
    if (sum(liedges) ~= 3)
        fprintf('Edges of a face (%d) are not in the element?\n', face);
    end
    
    %     d_edges = zeros(1, 3*nEdge);
    %     where = 1:nEdge;
    %     for e = 1:3
    %         if (prob.compat.elem_signs(iedges(e), face_cells(1)) > 0)
    %             d_edges(1,where) = 4 + (iedges(e)-1)*nEdge + (1:nEdge);
    %         else
    %             d_edges(1,where) = 4 + (iedges(e)-1)*nEdge + (nEdge:-1:1);
    %         end
    %         where = where + nEdge;
    %     end
    d_edges = 4 + reshape(repmat((iedges-1)*nEdge,[nEdge 1]) + (1:nEdge)', [ 1 (3*nEdge)]);
    
    %     if (prob.compat.elem_signs(7, face_cells(1)) > 0)
    %         d_face = 4 + 6*nEdge + (which-1)*nFace + (1:nFace);
    %     else
    %         fprintf('Unexpected orientation of a supported face...\n');
    %         d_face = 4 + 6*nEdge + (which-1)*nFace + (nFace:-1:1);
    %     end
    d_face = 4 + 6*nEdge + (which-1)*nFace + (1:nFace);
    
    incid_basis = [ d_verts d_edges d_face ];
    
    [ liverts, iverts ] = ismember(face_verts, FixedVerts, 'R2012a');
    [ liedges, iedges_s ] = ismember(face_edges, FixedEdges, 'R2012a');
    % If the face is free liverts (and liedges) may be all false
    
    vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(face_verts),:);
    dxdy = cross( (vert_coords(2,:) - vert_coords(1,:)), ...
        (vert_coords(3,:) - vert_coords(1,:)));
    dJac = norm(dxdy);
    
    offset_g = 0; offset_l = 0;
    for dir = 1:3
        incid_disp = offset_g + prob.compat.incid(incid_basis, face_cells(1));
        
        if (isfinite(prob.prob.incid_support(dir, pos_gface)))
            % Displacement
            
            % Here the verts and edges have all to be fixed
            if (sum(liverts) ~= 3)
                fprintf('At least one vert (%d %d %d) of a fixed face that is not fixed?\n', face_verts);
            end
            if (sum(liedges) ~= 3)
                fprintf('At least one edge (%d %d %d) of a fixed face that is not fixed?\n', face_edges);
            end
            
            
            fix_v = incFixedVerts(dir, iverts) + 1;
            
            % The orientation of the edges matters
            fix_e = zeros(1, 3*nEdge);
            where = 1:nEdge;
            for e = 1:3
                if (prob.compat.elem_signs(iedges(e), face_cells(1)) > 0)
                    fix_e(1,where) = incFixedEdges(dir, iedges_s(e)) + (1:nEdge);
                else
                    fix_e(1,where) = incFixedEdges(dir, iedges_s(e)) + (nEdge:-1:1);
                end
                where = where + nEdge;
            end
            %             fix_e = reshape(repmat(incFixedEdges(dir, iedges),[nEdge 1]) + (1:nEdge)', [ 1 (3*nEdge)]);
            
            nums = prob.compat.incid((4+6*nEdge+(which-1)*nFace)+(1:nFace), face_cells(1));
            [ ~, ord] = sort(nums);
            fix_f = incFixedFaces(dir, pos_fface) + ord';
                
            incid_reaction = [ fix_v fix_e fix_f ];
            
            aux1 = prob.compat.UUSide(incid_basis, incid_basis, which)*dJac;
            
            num = size(incid_basis, 2);
            
            for i1 = 1:num
                news = numnzA + (1:num);
                Ai(news) = incid_disp;
                Aj(news) = incid_reaction(i1);
                Ap(news) = aux1(:,i1);
                news = news + num;
                Ai(news) = incid_reaction(i1);
                Aj(news) = incid_disp;
                Ap(news) = aux1(:,i1);
                numnzA = numnzA + 2*num;
            end
            Fg(incid_reaction) = Fg(incid_reaction) + ...
                sum(prob.compat.BoundaryConds(pos_gface, incid_basis + offset_l, :), 3)';
        else
            % A force
            Fg(incid_disp) = Fg(incid_disp) + ...
                sum(prob.compat.BoundaryConds(pos_gface, incid_basis + offset_l, :),3)';
        end
        offset_g = offset_g + DimOneDir;
        offset_l = offset_l + nB;
    end
end


A = sparse(Ai(1:numnzA), Aj(1:numnzA), Ap(1:numnzA));
clear('Ai'); clear('Aj'); clear('Ap');

% Block floating structure
% if (nsupport == 0)
%     fixs = zeros(3,1);
%
%     % For the first node of first element fix all disps
%     fixs(1) = prob.compat.incid(1,1);
%     fixs(2) = prob.compat.incid(1,1+prob.compat.DU.nB);
%     % For the second node check direction
%     dx = prob.mesh.coords(prob.mesh.ele_infos(1,2),1:2) - prob.mesh.coords(prob.mesh.ele_infos(1,1),1:2);
%     if (abs(dx(1)) > abs(dx(2)))
%         fixs(3) = prob.compat.incid(2,1);
%     else
%         fixs(3) = prob.compat.incid(2,1+prob.compat.DU.nB);
%     end
%
%     for i=1:3
%         A(fixs(i),fixs(i)) = 1.001*A(fixs(i),fixs(i));
%     end
% end

% prob.compat.sol = A\Fg;


% Scaling
% s = sparse(1./sqrt(max(abs(diag(A)), max(abs(A))')));
sd = sqrt(abs(diag(A)));
sm = sqrt(max(abs(A)))';
s = sparse(1./max(sd,sm));
% sz = sd==0;
% s = 1./sd;
% s(sz) = 1./sm(sz);
% s = 1./sqrt(dot(A,A));
As = diag(s)*A*diag(s);
bs = diag(s)*Fg;

% opts = spqr_rank_opts;
% opts.tol = 1e-14;
% opts.ordering = 'best'; % 'default' 'amd' 'colamd' 'metis' 'best' 'fixed' 'natural';
% opts.get_details = 1;
% opts.repeatable = 1;
% opts.spumoni = 3;
% opts.solution = 'min2norm';
% % opts.ssi_nblock_increment = 1; % : 5 : block size inc. if convergence not met.
% % opts.ssi_convergence_factor = 1e12; % : 0.1 : spqr_ssi termination criterion.
% % opts.k = 4; %:   ssp_min_iters : 4 : min # of iterations before checking convergence
% % opts.ssp_max_iters = 50; % : 10 : max # of ssp iterations before stopping iterations
% % opts.ssp_convergence_factor = 1e12; % : 0.1 ssp terminates when relative error drops below this value.
tic;
% [ ss, prob.compat.stats] = spqr_basic(As, bs, opts);
% ss = spqr_solve(As,bs); prob.compat.stats.rank=NaN;
ss = As\bs; prob.compat.stats.rank=NaN;
toc;
prob.compat.sol = diag(s)*ss;


if (nsupport == 0)
    if (norm(prob.compat.sol(fixs)) > 1e-10)
        fprintf('\nProblem with floating stucture (@%d %d %d): %e %e %e\n', ...
            fixs, prob.compat.sol(fixs));
    end
end

% prob.compat.A = A;
prob.compat.b = Fg;

% nb = norm(Fg);
% nA = normest(A);
% nx = norm(prob.compat.sol);
nres = norm(Fg-A*prob.compat.sol);
% cn = condest(A);
% fprintf ('\nRHS, System and solution norms: %e %e %e\nResidual norm: %e (relative is %e).\nEstimated condition number: %e\n\n', ...
%     nb, nA, nx, nres, nres/(nb+nA*nx), cn);
fprintf ('Residual norm: %e   (%d dependent)\n\n', nres, size(A,1)-prob.compat.stats.rank);

prob.compat.nres = nres;
senerg = 0;
energ = 0;
for elem = 1:prob.mesh.nc
    aux = prob.compat.sol(prob.compat.incid(:, elem));
    senerg = senerg + aux'*prob.compat.Ke(:,:,elem)*aux;
    % For the potential energy we add all the work done by all the
    % equivalent nodal forces (internal)
    energ = energ + sum(prob.compat.Fe(:,:,elem),2)'*aux;
end
senerg = 0.5*senerg;

for g_face = prob.prob.g_faces
    face = full(prob.mesh.faces_elems(g_face));
    pos_gface = prob.prob.g_faces == g_face;
    [ face_cells, ~, ~ ] = prob.mesh.get_cev_f(face);
    % Just use the incidence for the first cell
    incid = prob.compat.incid(:,face_cells(1));
    where = 1:nB;
    for dir = 1:3
        if (isnan(prob.prob.incid_support(dir,pos_gface)))
            energ = energ + ...
                sum(prob.compat.BoundaryConds(pos_gface, where, :),3) ...
                *prob.compat.sol(incid(where));
        end
        where = where + nB;
    end
end

fprintf('Str Energy   %.14e\nPot Energy   %.14e\nTotal Energy %.14e\n', ...
    senerg, energ, senerg-energ);

prob.compat.global = true;
prob.compat.energ = energ;
prob.compat.senerg = senerg;

end

