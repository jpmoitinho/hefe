% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
function [ prob ] = EquilMats( prob )
%
% EquilMats - Compute the matrices relevant for equilibrium formulations
%   

% % The parameters of the particular solution
% prob.equil.s0 = zeros(nS0, 1, prob.mesh.nc);
% % The integrals of the parycular solutions with themselves
% prob.equil.s0s0 = zeros(1, prob.mesh.nc);

degree = prob.equil.degree_s;
%  nB = (degree+1)*(degree+2)/2;
nS = (degree+1)*(degree+6)/2;
nS0 = (degree)*(degree+1);
nV = degree+1;

% The parameters of the particular solution
prob.equil.s0 = zeros(nS0, 1, prob.mesh.nc);
% The integrals of the particular solutions with themselves
prob.equil.s0s0 = zeros(1, prob.mesh.nc);

% The elemental matrices
prob.equil.Flex = zeros(nS, nS, prob.mesh.nc);
% prob.equil.e0 = zeros(nS, prob.mesh.nc);
prob.equil.Dside = zeros(2*nV, nS, 3, prob.mesh.nc);
prob.equil.tside = zeros(2*nV, 3, prob.mesh.nc);
prob.equil.e0 = zeros(nS, prob.mesh.nc);

prob.geom.ledges = zeros(1, prob.mesh.ne);

% Integration rules
excess = 1;
[ NI2D, NI1D ] = def_integration_2D(degree*2+excess, ...
    degree*2+excess);

Phi = Phi_T3(NI2D.xieta);
DPhi = DPhi_T3();

% reshape and normalise for the interval 0:1
GWSide = reshape(NI1D.GW, [ 1 1 NI1D.npts ]);

Phi_side = [ (1-NI1D.xi)/2, (1+NI1D.xi)/2 ];
ViG = zeros(NI1D.npts, degree+1);

for i=0:degree
    ViG(:,i+1) = Phi_side(:,1).^(degree-i).*Phi_side(:,2).^i;
end
VG = zeros(2, 2*nV, NI1D.npts);
VG(1, 1:nV, :) = ViG';
VG(2, nV+(1:nV), :) = ViG';


% Define the terms that are common 
stressfuncsh = str2func(sprintf('DefStressPK_d%d', degree));
stressfuncs = stressfuncsh();
stress0funcsh = str2func(sprintf('DefStress0PK_d%d', degree));
stress0funcs = stress0funcsh();

auxh=str2func(sprintf('InvIOmega_d%d', degree-1));
auxh();

basisBfh = str2func(sprintf('Basis_d%d', degree-1));
basisBf = basisBfh();
phiBfGP = basisBf(NI2D.xieta(1,:)', NI2D.xieta(2,:)');

InvIOmegaBf_phiBfGP_GW = (InvIOmega*phiBfGP').*NI2D.GW; % Compute once

auxh=str2func(sprintf('IOmega_d%d', degree));
auxh();
for side=1:3
    auxh=str2func(sprintf('IGammaPlus_d%d_s%d', degree, side));
    auxh();
    auxh=str2func(sprintf('IGammaMinus_d%d_s%d', degree, side));
    auxh();
end

% basisS = eval(sprintf('Basis_d%d', degree));
% phiSGP = basisS(Ni2D.xieta(1,:)', NI2D.xieta(2,:)+);


IGammaPlus = cat(3, IGammaPlusS1, IGammaPlusS2, IGammaPlusS3);
IGammaMinus = cat(3, IGammaMinusS1, IGammaMinusS2, IGammaMinusS3);

N0J = cat(3, [0,0,-1,0;0,-1,0,0], [1,0,1,0;0,1,0,1], [-1,0,0,0;0,0,0,-1]);

for edge = 1:prob.mesh.ne
    [ ~, edge_verts ] = prob.mesh.get_cv_e(edge);
    vert_coords = prob.mesh.coords(edge_verts, 1:2);
    prob.geom.ledges(edge) = norm(vert_coords(2,:)-vert_coords(1,:));
end

for elem = 1:prob.mesh.nc
    [ ~, cell_verts ] = prob.mesh.get_ev_c(elem);
    [ props, bload ] = GetProps(elem, prob.mesh.elems_faces, ...
        prob.mesh.ele_tags, prob.prob.Faces);
    Elast = props(1);
    nu = props(2);
    
    flex = [ 1 -nu 0 ; -nu 1 0; 0 0 2*(1+nu)]/Elast; % Plane stress
    %     flex = [ (1-nu^2) -nu*(1+nu) 0 ; -nu*(1+nu) (1-nu^2) 0; 0 0 2*(1+nu)]/Elast; % Plane strain

    % Get global coordinates of element. Use a frame at its center
    elem_coords = prob.mesh.coords(prob.mesh.nodes_verts(cell_verts), 1:2);

    % Jacobian of the element
    Jac = (DPhi*elem_coords)';
    F11 = Jac(1,1); F12=Jac(1,2); F21=Jac(2,1); F22=Jac(2,2);
    
    dJac = det(Jac);

    caseJ = find(abs(Jac(:))==max(abs(Jac(:))),1); % Beware, indices by columns, the code must be adjusted
    
    %     eval('SetTPfTP');
    %     eval(sprintf('TS_c%d', caseJ));
    %     eval(sprintf('TD_d%d_c%d', degree, caseJ));
    
    %     TSfull = kron(TS, eye(nB));
    
    TP = [F11,0,F12,0;0,F22,0,F21;F21,0,F22,0];
    
    [ TS, TD ] = stressfuncs{caseJ}(F11,F21,F12,F22);

    TPTS = TP*TS;

    PreFlex = kron(TPTS'*(flex/dJac)*TPTS,IOmega);
    ElemFlex = TD'*PreFlex*TD;

    prob.equil.Flex(:,:,elem) = -ElemFlex;

    if (strcmp(bload,'[zero,zero]'))
        has_s0 = false;
    else
        [ T0, L0 ] = stress0funcs{caseJ}(F11,F21,F12,F22);
        % Transform the Gauss points into coordinates
        coordGP = Phi*elem_coords;
        % Find the weigths of the particular solution
        bfgp = atpoint2D( bload, coordGP);
        bfpars = InvIOmegaBf_phiBfGP_GW*bfgp; % No dJac here, affects both terms
        P0 = -dJac*(T0*bfpars(:));
        prob.equil.s0(:, 1, elem) = P0;

        % The equivalent strains due to the particular solution
        prob.equil.e0(:,elem) = TD'*PreFlex(:,L0)*P0;
        prob.equil.s0s0(elem) = P0'*PreFlex(L0,L0)*P0;
        has_s0 = true;
    end
    
    % Loop on the edges of the element
    for edge = 1:3
        % Check orientation of cell_edge relative to the edge entity
        % The orientation of the edge entity is the reference
        pm = prob.geom.sign(elem, edge);
        % map_edge adjusts the different numberings....
        if (pm > 0)
            IGamma = sparse(IGammaPlus(:,:,edge));
        else
            IGamma = sparse(IGammaMinus(:,:,edge));
        end
        DTilde = kron(N0J(:,:,edge)*TS,IGamma);
        prob.equil.Dside(:, :, edge, elem)  = DTilde*TD;
        % The term from the particular solution
        if (has_s0)
            prob.equil.tside(:, edge, elem) = -DTilde(:,L0)*P0;
        end
    end
end

prob.equil.BoundaryConds = zeros(prob.prob.ng_edges, 2, nV, 2);
for i = 1:prob.prob.ng_edges
    g_edge = prob.prob.g_edges(i);
    edge = full(prob.mesh.edges_elems(g_edge));
    tag = prob.mesh.ele_tags{g_edge}(1); % Use the first tag
    which_tag = find(prob.prob.Edges.tags == tag);
    
    % If vals{end} is not zero we will need to impose
    % the corresponding BC. We integrate it now
    % This code is agnostic regarding the type of BC
    if (size(which_tag,1) && ~strcmp(prob.prob.Edges.vals{end}{which_tag}, '[zero,zero]'))
        sload = prob.prob.Edges.vals{end}{which_tag};
        [ ~, verts_edge ] = prob.mesh.get_cv_e(edge);
        % these coordinates are in the global frame (used for the bc's)
        vert_coords = prob.mesh.coords(prob.mesh.nodes_verts(verts_edge),[1 2]);
        coordGP = Phi_side*vert_coords;
        lside = prob.geom.ledges(edge);
        aux = lside*mtimesx(reshape( atpoint2D(sload, coordGP)', [2 1 NI1D.npts]), ...
            reshape(Phi_side', [1 2 NI1D.npts]));
        vtside = mtimesx(VG, 't', aux);
        % Multiplied by the length (there is no normal!!!)
        aux = sum(mtimesx(vtside, GWSide),3);
        prob.equil.BoundaryConds(g_edge, 1, :, :) = aux(1:nV, :);
        prob.equil.BoundaryConds(g_edge, 2, :, :) = aux(nV+(1:nV), :);
    end
end

prob.equil.calc_mats = true;
