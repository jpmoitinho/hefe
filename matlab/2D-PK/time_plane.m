% (C) Jose Paulo Moitinho de Almeida (moitinho@civil.ist.utl.pt)
%
% This file is distributed under the terms of the attached LICENSE.TXT file.
%
% You need to define 3 or 5 variables before calling this script
%  - meshname, probname and degree 
%  - meshname, probname, degree_s, degree_v and degree_u
%

ntests = 10;
degrees = 1:10;
% degrees = 1:2;
ndegrees = size(degrees,2);
meshes = 0:5;
% meshes = 0:2;
nmeshes = size(meshes,2);

run_times = zeros(nmeshes,ndegrees,ntests);

for id = 1:ndegrees
    degree = degrees(id);
    for im = 1:nmeshes
        mesh = meshes(im);
        fprintf('Degree %d mesh %d', degree, mesh);
        meshname = sprintf(meshname_b, mesh);
        
        prob = DefinePlaneProblem(meshname, probname, funcs, 2);
        prob.equil.degree_s = degree;
        prob.equil.degree_v = degree;
        prob.compat.degree_u = degree;
        for tests = 1:ntests
            %             f = @() EquilMats(prob);
            %             t3 = timeit(f);
            %             run_times(mesh+1,degree,tests) = t3;
            tic;
            EquilMats(prob);
            run_times(mesh+1,degree,tests) = toc;
	    fprintf('.');
        end
	fprintf('\n');
    end
end
