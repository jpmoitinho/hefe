nelems = (4.^(1:6))';
% nelems = (4.^(1:3))';

probnames = {'../2D/data/Smooth-f.prob','../2D/data/Smooth-b.prob'};
meshname_b = '../2D/data/Smooth-%d.msh';

nthreads = [1 12];

for pp = 1:2
    for tt = 1:2
        probname = probnames{pp};
        maxNumCompThreads(nthreads(tt));
        
        cd ../2D-PK
        addpath('../base');
        funcs = '../funcs-PK';
        addpath(funcs);
        time_plane
        timesPK = run_times;
        tPK = min(run_times,[],3)./nelems;
        
        rmpath(funcs)
        cd ../2D
        funcs = '../funcs';
        addpath(funcs);
        time_plane
        timesGQ = run_times;
        tGQ = min(run_times,[],3)./nelems;
        rmpath(funcs)
        
        cd ../2D-PK
        
        aa = tGQ./tPK;
        
        diary testing.log
        fprintf('\n\n------------------\n');
        fprintf('Problem %s, multithreading %d\n\n', probnames{pp}, maxNumCompThreads);
        fprintf('relative\n');
        for i=1:size(aa,1)
            fprintf('%d & (%d elements) ', i-1, nelems(i));
            fprintf('& %.2f ', aa(i,:));
            fprintf('\\\\\n');
        end
        
        fprintf('Per element PK\n');
        for i=1:size(aa,1)
            fprintf('%d & (%d elements) ', i-1, nelems(i));
            fprintf('& %.2f ', 1000*tPK(i,:));
            fprintf('\\\\\n');
        end
        
        fprintf('Per element GQ\n');
        for i=1:size(aa,1)
            fprintf('%d & (%d elements) ', i-1, nelems(i));
            fprintf('& %.2f ', 1000*tGQ(i,:));
            fprintf('\\\\\n');
        end
        diary off
    end
end
