﻿# Hybrid Equilibrium Finite Elements

* Matlab codes for Hybrid Equilibrium Finite Element Formulations, basically those that were used in the preparation of the book 
"Equilibrium Finite Element Formulations" by J.P. Moitinho de Almeida and E.A.W. Maunder (Print ISBN:9781118424155 Online ISBN:9781118925782 DOI:10.1002/9781118925782)

The following libraries are required:
 - mtimesx [(obtainable here)](https://www.mathworks.com/matlabcentral/fileexchange/25977-mtimesx-fast-matrix-multiply-with-multi-dimensional-support)
 - ibhm matlab classes (che for 2D meshes and chf for 3D meshes, both included in the funcs directory). Gitlab pages [here](https://gitlab.com/lfmartha/ibhm),
 

There is (there will be) one directory for each type of problem considered (2D for plane elasticity, 3D for solid elasticity, RM for Reissner Mindlin plates, KT for Kirchhoff plates, Pot2D and Pot3D for potential problems in 2D and 3D, respectivelly, etc.)

The funcs directory contains files used by different problems.

The mathematica directory contains the programs used to generate matlab code.

The 2D-PK directory contains the files working with the approximation of the Piola-Kirchhoff stresses (in the master element) with analytical expressions for the integrals, described in “An efficient methodology for stress-based finite element approximations in two-dimensional elasticity” - DOI: 10.1002/nme.6458.

The 2D-PK-Rotated and 3D-PK-Rotated directories extend this approach by considering a rotated frame that avoids the consideration of multiple cases.
